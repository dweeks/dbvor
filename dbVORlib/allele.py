#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import types
from   operator import itemgetter, concat

from util     import printnl, printn, log

class Allele(object):
    def __init__(self, db, runt=1):
        self._dbi_           = db
#       self._Ids            = None
#       self._members        = None
        self._hash           = None
        self._member_        = None
        self._marker_        = None
        self._proj2name      = None
        self._tech2name      = None
        self._expr2name      = None
        self._runt           = runt

    OTHER = {'1':'2', '2':'1', 'A':'G', 'G':'A', 'T':'C', 'C':'T'}
    def guess_allele(self, al, default='?'):
        return self.OTHER.get(al, default)

# multiple alleles from different experiments ... count each unique entry.
# (0, 0) goes to front
    def merge_alleles(self, cd, ab):
        if ab[0] == '0' and ab[1] == '0':               # zero at front
            if cd[0][1][0] == '0' and cd[0][1][1] == '0':     # old == 0 --> incr
                cd[0][0] += 1
            else:                                       # new == 0 --> prepend
                cd.insert(0, [1, ab])
        else:                                           #find it
            for e in cd:
                if e[1][0] == ab[0] and e[1][1] == ab[1]:    # equal
                    e[0] += 1
                    return
            cd.append([1, ab])                                 # new  --> append

    def merge_alleles2(self, cd, ab):
        if ab[0] == '0' and ab[1] == '0':               # zero at front
            if cd[0][1][0] == '0' and cd[0][1][1] == '0':     # old == 0 --> incr
### new
                cd[0].append(ab)
                cd[0][0] += 1
            else:                                       # new == 0 --> prepend
                cd.insert(0, [1, ab])
        else:                                           #find it
            for e in cd:
                if e[1][0] == ab[0] and e[1][1] == ab[1]:    # equal
### new
                    e.append(ab)
                    e[0] += 1
                    return
### 12 == 21
                if e[1][0] == ab[1] and e[1][1] == ab[0]:    # cross
### new
                    e.append(ab)
                    e[0] += 1
                    return
            cd.append([1, ab])                                 # new  --> append

    def merge_len(self, ab):
        return reduce(lambda x, y:x+y[0], ab, 0)

    def observed_len(self, ab):
        return reduce(lambda x, y:x+(y[0] if y[1][0]!='0' and y[1][1]!='0' else 0) , ab, 0)

    def load_map(self, dbi, maptype, options):
        cmds = "select marker, experiment, allelemap, maptype, active from allele_map where maptype = ? and active = ?"
        hashmap = {}
        for rec in dbi.select_all(cmds, maptype, options.active):
            marker, experiment, allelemap, recmaptype, recactive = rec
            if recmaptype != maptype: continue
            if recactive  != options.active: continue
            if experiment not in hashmap:
                hashmap[experiment] = {}
            if marker not in hashmap[experiment]:
                hashmap[experiment][marker] = {}
            for amap in allelemap.split():
                pair = amap.split(":")
                hashmap[experiment][marker][pair[0]] = pair[1]
        return hashmap or None

    def conflict_stats(self, options, Hash, member, marker, emap=None):
        self._member_   = member
        self._marker_   = marker
#       self._Ids       = member.sortedlist()
#       self._members   = marker.sortedlist()
        self._proj2name = dict(self._dbi_.select_all("select id, name from projects"))
        self._tech2name = dict(self._dbi_.select_all("select id, technology from technologies"))
        self._expr2name = dict(self._dbi_.select_all("select id, filename from experiments"))
        if getattr(options, 'technology', None):
            self._proj2name[0] = options.project
            self._tech2name[0] = options.technology
            self._expr2name[0] = options.experiment

        if options.verbose or options.debug:
            fmtl = self.fmtlen('member', 'pedigree', 'person', 'marker', 'snp name', 'genotypes')
            printnl("Conflicts by members: begin")
            if options.debug:
                hfs1 = "{0:{fld[0]}}  {1:{fld[1]}}  {2:{fld[2]}}  {3:{fld[3]}}  {4:{fld[4]}}  {5:{fld[5]}}"
                hdr = hfs1.format('member', 'pedigree', 'person', 'marker', 'snp name', 'genotypes', fld=fmtl)

            else:
                hfs1 = "{0:{fld[1]}}  {1:{fld[2]}}  {2:{fld[4]}}  {3:{fld[5]}}"
                hdr = hfs1.format('pedigree', 'person', 'snp name', 'genotypes', fld=fmtl)

            self.concordance(Hash, hdr, fmtl, options)
            self.concordance_missing(Hash, hdr, fmtl, options)
            self.discordance(Hash, hdr, fmtl, options)
##experimental
##      self.member_allcordance(Hash, options)
        self.marker_allcordance(Hash, options)

        if options.debug:
            printnl("Conflicts by members: end")

    def fmtlen(self, mem, ped, per, mar, snp, *ls):
        ans = []
        ans.append(max(len(mem), log.member_length))
        ans.append(max(len(ped), log.pedigree_name_length))
        ans.append(max(len(per), log.person_name_length))
        ans.append(max(len(mar), log.marker_length))
        ans.append(max(len(snp), log.snp_name_length))
        ans.extend(map(len, ls))
        return tuple(ans)

    def print_line(self, Id, marker_id, ab, fmtl, options):
        names = self._member_.rev(Id)
        snp   = self._marker_.rev(marker_id)
        if type(names) != types.ListType:
            names = [names]
        for ped, per in names:
            ab = self.prune_alleles(ab, options)
            if options.debug:
                hfs1 = "{0:{fld[0]}d}  {1:>{fld[1]}}  {2:>{fld[2]}}  {3:{fld[3]}d}  {4:{fld[4]}}  {5}"
                printnl(hfs1.format(Id, ped or '', per, marker_id, snp, ab, fld=fmtl))
            else:
                hfs1 = "{0:>{fld[1]}}  {1:>{fld[2]}}  {2:{fld[4]}}  {3}"
                printnl(hfs1.format(ped or '', per, snp, ab, fld=fmtl))

    def verbose_allele(self, allele, project):
        a, b = (allele[0], allele[1]) if allele[0] <= allele[1] else (allele[1], allele[0])
        return "{0}/{1} {2}/{3}/{4}@{5:d}".format(a, b, self._proj2name[project], self._tech2name[allele[2]], self._expr2name[allele[3]], allele[4])

    def prune_alleles(self, ab, options):
        if options.debug:
            return ab
        elif options.verbose:
#           return [x[0:1]+[y[0] + y[1] + self._tech2name[y[2]], self._expr2name[y[3]], active] for y in x[1:] for x in ab]
            return [x[0:1]+[self.verbose_allele(y, options.project_id) for y in x[1:]] for x in ab]
        else:
#           return [x[0:1]+ [y[0:2] for y in x[1:]] for x in ab]
            return [x[0:1]+ ([x[1][0:2]] if x[0] > 1 else [y[0:2] for y in x[1:]]) for x in ab]


    def concordance(self, Hash, hdr, fmtl, options):
        log.push('concordance')
        printnl("Concordance across all multiple observed genotypes:")
        printnl(hdr)
        log.endheader()
        cnt = 0
        for Id in self._member_.sortedlist():
            for marker_id in self._marker_.sortedlist():
                ab = Hash.get(Id, {}).get(marker_id, None)
                if ab and type(ab) == types.ListType:
                    if len(ab) == 1:
                        self.print_line(Id, marker_id, ab, fmtl, options)
                        cnt += 1
        log.beginfooter()
        printnl("Concordance count = {0:d}".format(cnt))
        log.pop('concordance')

    def concordance_missing(self, Hash, hdr, fmtl, options):
        log.push('concordmissing')
        printnl("Missing in some but concordance otherwise:")
        printnl(hdr)
        log.endheader()
        cnt = 0
        for Id in self._member_.sortedlist():
            for marker_id in self._marker_.sortedlist():
                ab = Hash.get(Id, {}).get(marker_id, None)
                if ab and type(ab) == types.ListType:
                    if len(ab) == 2 and ab[0][1][0] == '0' and ab[0][1][1] == '0':
                        self.print_line(Id, marker_id, ab, fmtl, options)
                        cnt += 1
        log.beginfooter()
        printnl("Missing in some but concordance otherwise count = {0:d}".format(cnt))
        log.pop('concordmissing')

    def discordance(self, Hash, hdr, fmtl, options):
        log.push('discordance')
        printnl("Discordant genotypes:")
        printnl(hdr)
        log.endheader()
        cnt = 0
        for Id in self._member_.sortedlist():
            for marker_id in self._marker_.sortedlist():
                ab = Hash.get(Id, {}).get(marker_id, None)
                if ab and type(ab) == types.ListType:
                    if len(ab) <= 1:
                        pass
                    elif len(ab) == 2 and (ab[0][1][0] == '0' and ab[0][1][1] == '0'):
                        pass
                    else:
### hack
                        def allelek(rec):
                            return rec[1][-2]
                        ab.sort(key=allelek)
                        self.print_line(Id, marker_id, ab, fmtl, options)
                        cnt += 1
        log.beginfooter()
        printnl("Discordant genotypes count = {0:d}".format(cnt))
        log.pop('discordance')

    def member_allcordance(self, Hash, options):
        fmtl = self._member_.fmtlen('member', 'pedigree', 'person', '  dup', 'missing', 'concordant', 'concordant', 'discordant')
        log.push('member_allconcordance', newrecord=True)
        printnl("Summary by member:")
        log.endheader()
        member_allc_sort = []
        for Id in self._member_.sortedlist():
            dup = concord = discord = missingconcord = missing = 0
#n2
            alleles = {}
            exper   = {}
            for marker_id in self._marker_.sortedlist():
                ab = Hash.get(Id, {}).get(marker_id, None)
                if ab:
                    if type(ab) == types.TupleType:
                        pass
                    elif len(ab) == 1:  # viz. [[2, ('B', 'B')]]
                        dup += 1
                        if ab[0][1][0] == '0' and ab[0][1][1] == '0':
                            missing += 1
                        else:
                            concord += 1
#n2+2
                        if ab[0][0] == 1:  # should have been left a TupleType ...
                            pass
                        else:
                            self.allele_pair(ab[0][1:], alleles, exper) #, marker_id, Id)
                    elif len(ab) == 2 and ab[0][1][0] == '0' and ab[0][1][1] == '0':
                        dup += 1
                        missingconcord += 1
#n2
                        lst = reduce(concat, (x[1:] for x in ab))
                        self.allele_pair(lst, alleles, exper) #, marker_id, Id)
                    else:
                        dup += 1
                        discord += 1
#n2
                        lst = reduce(concat, (x[1:] for x in ab))
                        self.allele_pair(lst, alleles, exper) #, marker_id, Id)
            if dup:
                log.newrecord(show=True)
                hfs1 = "{0:{fld[0]}} {1:{fld[1]}} {2:{fld[2]}} {3:{fld[3]}} {4:{fld[4]}} {5:{fld[5]}} {6:{fld[6]}}"
                printnl(hfs1.format('', '', '', '', '', '', 'missing or', fld=fmtl))

                hfs2 = "{0:{fld[0]}} {1:{fld[1]}} {2:>{fld[2]}} {3:{fld[3]}} {4:{fld[4]}} {5:{fld[5]}} {6:{fld[6]}} {7:{fld[7]}}"
                printnl(hfs2.format('member', 'pedigree', 'person', '  dup', 'missing', 'concordant', 'concordant', 'discordant', fld=fmtl))
                names = self._member_.rev(Id)
                if type(names) != types.ListType:
                    names = [names]
                hfs3 = "{0:{fld[0]}} {1:>{fld[1]}} {2:>{fld[2]}} {3:{fld[3]}} {4:{fld[4]}} {5:{fld[5]}} {6:{fld[6]}} {7:{fld[7]}}"
                for ped, per in names:
                    printnl(hfs3.format(Id, ped or '', per, dup, missing, concord, missingconcord, discord, fld=fmtl))
                    member_allc_sort.append((Id, ped or '', per, dup, missing, concord, missingconcord, discord))
#n1
                self.member_pair_loop(alleles, exper, Id, options)
        log.pop('member_allconcordance')

        log.push('member_allconcordance_sort')
        printnl("Summary by member -- sorted by total:")
        hfs1 = "{0:{fld[0]}} {1:{fld[1]}} {2:{fld[2]}} {3:{fld[3]}} {4:{fld[4]}} {5:{fld[5]}} {6:{fld[6]}}"
        printnl(hfs1.format('', '', '', '', '', '', 'missing or', fld=fmtl))

        hfs2 = "{0:{fld[0]}} {1:{fld[1]}} {2:{fld[2]}} {3:{fld[3]}} {4:{fld[4]}} {5:{fld[5]}} {6:{fld[6]}} {7:{fld[7]}}"
        printnl(hfs2.format('member', 'pedigree', 'person', '  dup', 'missing', 'concordant', 'concordant', 'discordant', fld=fmtl))
        log.endheader()
        member_allc_sort.sort(key=itemgetter(3, 1, 2))
        hfs3 = "{rec[0]:{fld[0]}d} {rec[1]:>{fld[1]}} {rec[2]:>{fld[2]}} {rec[3]:{fld[3]}d} {rec[4]:{fld[4]}d} {rec[5]:{fld[5]}d} {rec[6]:{fld[6]}d} {rec[7]:{fld[7]}d}}"
        for rec in member_allc_sort:
            printnl(hfs3.format(rec=rec, fld=fmtl))
        log.pop('member_allconcordance_sort')

    def marker_allcordance(self, Hash, options):
        fmtl = self._marker_.fmtlen('marker', 'snp name', '  dup', 'missing', 'concordant', 'concordant', 'discordant')
        log.push('marker_allconcordance', newrecord=True)
        printnl("Summary by marker:")
        log.endheader()
        for marker_id in self._marker_.sortedlist():
            dup = concord = discord = missingconcord = missing = 0
            alleles = {}
            exper   = {}
            for Id in self._member_.sortedlist():
                ab = Hash.get(Id, {}).get(marker_id, None)
                if ab is None:
                    continue
                if type(ab) == types.TupleType:
                    pass
                elif len(ab) <= 1:
                    dup += 1
                    if ab[0][1][0] == '0' and ab[0][1][1] == '0':
                        missing += 1
                    else:
                        concord += 1
                    if ab[0][0] == 1:  # should have been left a TupleType ...
                        pass
                    else:
                        self.allele_pair(ab[0][1:], alleles, exper) #, marker_id, Id)
                elif len(ab) == 2 and (ab[0][1][0] == '0' and ab[0][1][1] == '0'):
                    dup += 1
                    missingconcord += 1
#                   if ab[1][0] > 1:
#                       self.allele_pair(ab[1][1:], alleles, exper) #, marker_id, Id)
                    lst = reduce(concat, (x[1:] for x in ab))
                    self.allele_pair(lst, alleles, exper) #, marker_id, Id)
                else:
                    dup += 1
                    discord += 1  # what is correct
                    lst = reduce(concat, (x[1:] for x in ab))
                    self.allele_pair(lst, alleles, exper) #, marker_id, Id)

            if dup:
                log.newrecord(show=True)
                snp = self._marker_.rev(marker_id)
                hfs1 = "{0:{fld[0]}} {1:{fld[1]}} {2:{fld[2]}} {3:{fld[3]}} {4:{fld[4]}} {5:{fld[5]}}"
                printnl(hfs1.format('', '', '', '', '', 'missing or', fld=fmtl))

                hfs2 = "{0:{fld[0]}} {1:{fld[1]}} {2:{fld[2]}} {3:{fld[3]}} {4:{fld[4]}} {5:{fld[5]}} {6:{fld[6]}}"
                printnl(hfs2.format('marker', 'snp name', '  dup', 'missing', 'concordant', 'concordant', 'discordant', fld=fmtl))
                hfs3 = "{0:{fld[0]}d} {1:{fld[1]}} {2:{fld[2]}d} {3:{fld[3]}d} {4:{fld[4]}d} {5:{fld[5]}d} {6:{fld[6]}d}"
                printnl(hfs3.format(marker_id, snp, dup, missing, concord, missingconcord, discord, fld=fmtl))

                self.marker_pair_loop(alleles, exper, marker_id, snp, options)
        log.pop('marker_allconcordance')

    def allele_pair(self, allelelist, alleles, mexp): #, marker, Id):
        la = len(allelelist)
        for i in xrange(0, la):
            exp = allelelist[i][-2]
            mexp[exp] = mexp[exp] + 1 if exp in mexp else 1
            for j in xrange(i+1, la):
                ab = (allelelist[i], allelelist[j], 'NEW')
                k1, k2 = allelelist[i][-2], allelelist[j][-2]
                if k1 > k2:
                    k1, k2 = k2, k1
                    ab = (allelelist[j], allelelist[i], 'NEW')
                else:
                    ab = (allelelist[i], allelelist[j], 'NEW')

#               printnl("{0:d}/{1:d} ({2} {3}) @ {4:d}, {5:d}: {6}".format(i, j, marker, Id, k1, k2, ab))
                alleles.setdefault(k1, {}).setdefault(k2, []).append(ab)

#XXX
    def member_pair_loop(self, Hash, exper, member_id, options):
        pedper = self._member_.rev(member_id)
        fmtl = self._member_.fmtlen('member', 'pedigree', 'person')
        for expr1, rest in sorted(Hash.iteritems(), key=itemgetter(0)):
            for expr2, lst in sorted(rest.iteritems(), key=itemgetter(0)):
                l = len(lst)
                hfs1 = "{0:{fld[0]}d} {1:>{fld[1]}} {2:>{fld[2]}} VS: exp {3:d} (cnt {4:d}) & exp {5:d} (cnt {6:d}): below {7:d}; "
                printnl(hfs1.format(member_id, pedper[0], pedper[1], expr1, exper[expr1], expr2, exper[expr2], l, fld=fmtl))
                printn("    exp {0:d} == {1}, ".format(expr1, self._expr2name[expr1]))
                printnl("exp {0:d} == {1}".format(expr2, self._expr2name[expr2]))
                if l >= self._runt:
                    self.marker_pair(expr1, expr2, lst, None, None)
                else:
                    for i, x in enumerate(lst):
                        printnl("{0:3d}: {1}".format(i, x))
                printnl('')

    def marker_pair_loop(self, Hash, exper, marker_id, snp, options):
        fmtl = self._marker_.fmtlen('marker', 'snp name')
        for expr1, rest in sorted(Hash.iteritems(), key=itemgetter(0)):
            for expr2, lst in sorted(rest.iteritems(), key=itemgetter(0)):
                l = len(lst)
                hfs1 = "{0:{fld[0]}d} {1:{fld[1]}} VS: exp {2:d} (cnt {3:d}) & exp {4:d} (cnt {5:d}): below {6:d}; "
                printnl(hfs1.format(marker_id, snp, expr1, exper[expr1], expr2, exper[expr2], l, fld=fmtl))
                printn("    exp {0:d} == {1}, ".format(expr1, self._expr2name[expr1]))
                printnl("exp {0:d} == {1}".format(expr2, self._expr2name[expr2]))
                if l >= self._runt:
                    self.marker_pair(expr1, expr2, lst, snp, marker_id)
                else:
                    for i, x in enumerate(lst):
                        printnl("{0:3d}: {1}".format(i, x))
                printnl('')

    def marker_pair(self, expr1, expr2, lst, snp, marker_id):
        L0 = set()
        L1 = set()
        Mat = {}
        Sum = 0
        for ab in lst:
            if ab[-1] == 'NEW':
                t0, t1 = ab[0], ab[1]
            else:
                t0, t1 = ab[0][1], (ab[1][1] if len(ab) > 1 else ab[0][2])
            row = t0[0]+t0[1] if t0[0] <= t0[1] else t0[1]+t0[0]
            col = t1[0]+t1[1] if t1[0] <= t1[1] else t1[1]+t1[0]
            L0.add(t0[0]); L0.add(t0[1])
            L1.add(t1[0]); L1.add(t1[1])
            Mat.setdefault(row, {}).setdefault(col, 0)
            Mat[row][col] = Mat[row][col] + 1
            if row != '00' and col != '00':
                Sum += 1
        def lxpand(L):
            expand = True
            if '0' in L:
                L.remove('0')
            L = sorted(L)
            ln = len(L)
            if ln == 0:
                L.append('?')
                L.append('?')
                ln += 2
            elif ln == 1 and expand:
                L.append(self.guess_allele(L[0]))
                ln += 1
            L.sort()
# just to prove ...
#           L.append('x')
#           L.append('y')
#           ln += 2
            Ll = ['00']
            for i in xrange(ln):
                Ll.append(L[i]+L[i])
                for j in xrange(i+1, ln):
                    Ll.append(L[i]+L[j])
            return L, Ll
        L0a, L0 = lxpand(L0)
        L1a, L1 = lxpand(L1)

        printn("{1:{0}} {2:4}".format(71-5*len(L1), '', ''))
        for col in L1:
            printn("{0:>4}".format(col))
        printnl('')
        for row in L0:
            printn("{1:{0}} {2:>4}".format(71-5*len(L1), '', row))
            for col in L1:
                dat =  Mat.get(row, {}).get(col, 0)
                printn("{0:4}".format(dat))
            printnl('')

        diag = 0
        transposediag = 0
        L2 = L1[1:]
        L2.reverse()
        if len(L0) == len(L1):
            for row, col in zip(L0[1:], L1[1:]):
                diag +=  Mat.get(row, {}).get(col, 0)
            for row, col in zip(L0[1:], L2):
                transposediag +=  Mat.get(row, {}).get(col, 0)
#           printnl("diagonal {0:.4f}, transposediagonal {1:.4f}".format(diag/Sum if Sum else 0, transposediag/Sum if Sum else 0))
            if (L0a == L1a and diag >= transposediag) or snp is None:
#           if L0a == L1a  or snp is None:
                printnl("diagonal  {0:5d}, rest {1:5d}".format(diag, Sum - diag))

                printnl("transpose {0:5d}, rest {1:5d}".format(transposediag, Sum - transposediag))
            elif (L0a == L1a and diag < transposediag) or snp is None:
                ps = "diagonal  {0:5d}, flip {1:5d} map {2}:{3} {4}:{5} marker {6} exp {7}"
                printnl(ps.format(diag, transposediag,
                                  L0a[0],L1a[1], L0a[1],L1a[0],
                                  snp, self._expr2name[expr1] )
                       )
            elif snp is not None:
#               ps = "diagonal  {0:5d}, rest {1:5d} [{2}:{3}, {4}:{5}], marker {6} ({7}), exp {8:d}:{9:d} ({10}:{11})"
                ps = "diagonal  {0:5d}, rest {1:5d} map {2}:{3} {4}:{5} marker {6} exp {7} vs {8}"
                printnl(ps.format(diag, Sum - diag,
                                  L0a[0],L1a[0], L0a[1],L1a[1],
                                  snp, self._expr2name[expr1], self._expr2name[expr2] )
                       )
#               sp = "transpose {0:5d}, rest {1:5d} [{2}:{3}, {4}:{5}], marker {6} ({7}), exp {8:d}:{9:d} ({10}:{11})"
                sp = "transpose {0:5d}, rest {1:5d} map {2}:{3} {4}:{5} marker {6} exp {7} vs {8}"
                printnl(sp.format(transposediag, Sum - transposediag,
                                  L0a[0],L1a[1], L0a[1],L1a[0],
                                  snp, self._expr2name[expr1], self._expr2name[expr2] )
                       )

# for comparison
#       self.marker_pairOld(expr1, expr2, lst)

# general matrix for storing pairs BUT only 3x3 output ... calculates labels
    def marker_pair_old(self, expr1, expr2, lst):
        TH0 = {}
        TH1 = {}
        for ab in lst:
            if ab[-1] == 'NEW':  # ( (a11, a12, txa1), (a21, a22, txa2) )
                t0, t1 = ab[0], ab[1]
            else:                # ( [#, (a11, a12, txa1)], [#, (a21, a22, txa2)] ) ... raw allele_merge2 fmt
                t0, t1 = ab[0][1], (ab[1][1] if len(ab) > 1 else ab[0][2])
                                 # ( [#, (a11, a12, txa1), (a11, a12, txa2)] ) ... raw allele_merge2 fmt len(ab == 1)

            f = t0[0]
            if f not in TH0: TH0[f] = 0
            TH0[f] += 1
            f = t0[1]
            if f not in TH0: TH0[f] = 0
            TH0[f] += 1

            f = t1[0]
            if f not in TH1: TH1[f] = 0
            TH1[f] += 1
            f = t1[1]
            if f not in TH1: TH1[f] = 0
            TH1[f] += 1

        def skf(x):
            return x[1]
        zero = TH0.pop('0') if '0' in TH0 else 0
        S0 = sorted(TH0.iteritems(), key=skf, reverse=True)
        if len(S0) > 2:
            printnl("ROW [0]  {0}; ('0', {1})".format(S0, zero))
        T0 = [t2[0] for t2 in S0][0:2]
        T0.sort()
        if len(T0) == 0:
            T0.append('?')
            T0.append('?')
        elif len(T0) == 1:
            T0.append(self.guess_allele(T0[0]))
            T0.sort()

        zero = TH1.pop('0') if '0' in TH1 else 0
        S1 = sorted(TH1.iteritems(), key=skf, reverse=True)
        if len(S1) > 2:
            printnl("COL [1]  {0}; ('0', {1})".format(S1, zero))
        T1 = [t2[0] for t2 in S1][0:2]

        T1.sort()
        if len(T1) == 0:
            T1.append('?')
            T1.append('?')
        elif len(T1) == 1:
            T1.append(self.guess_allele(T1[0]))
            T1.sort()

# for testing
#       self.agreex2(lst, T0, T1)
        Mat = {}
        for ab in lst:
            if ab[-1] == 'NEW':
                t0, t1 = ab[0], ab[1]
            else:
                t0, t1 = ab[0][1], (ab[1][1] if len(ab) > 1 else ab[0][2])
#viz        T0, T1 = ('G', 'T'), ('1', '2')
            row = t0[0]+t0[1] if t0[0] <= t0[1] else t0[1]+t0[0]
            col = t1[0]+t1[1] if t1[0] <= t1[1] else t1[1]+t1[0]
            Mat.setdefault(row, {}).setdefault(col, 0)
            Mat[row][col] = Mat[row][col] + 1

        printn("{0:51} {1:4}".format('', ''))
        for col in ('00', T1[0]+T1[0], T1[0]+T1[1],  T1[1]+T1[1]):
            printn("{0:4}".format(col))
        printnl('')
        for row in ('00', T0[0]+T0[0], T0[0]+T0[1],  T0[1]+T0[1]):
            printn("{0:51} {1:4}".format('', row))
            for col in ('00', T1[0]+T1[0], T1[0]+T1[1],  T1[1]+T1[1]):
                dat =  Mat.get(row, {}).get(col, 0)
                printn("{0:4}".format(dat))
            printnl('')


# really efficient ... but only for 3x3 matrix and NO 00 info
# marker_pair_old calculates T0 T1
    def agreex2(self, lst, T0, T1):
        matrix = [0, 0, 0,    0, 0, 0,    0, 0, 0]
        for ab in lst:
            if ab[-1] == 'NEW':  # ( (a11, a12, txa1), (a21, a22, txa2) )
                t0, t1 = ab[0], ab[1]
            else:                # ( [#, (a11, a12, txa1)], [#, (a21, a22, txa2)] ) ... raw allele_merge2 fmt
                t0, t1 = ab[0][1], (ab[1][1] if len(ab) > 1 else ab[0][2])
                                 # ( [#, (a11, a12, txa1), (a11, a12, txa2)] ) ... raw allele_merge2 fmt len(ab == 1)

#viz        T0, T1 = ('G', 'T'), ('1', '2')
            if   t0[0] == T0[0] and t0[1] == T0[0]:
                if   t1[0] == T1[0] and t1[1] == T1[0]:    matrix[0] += 1
                elif t1[0] == T1[0] and t1[1] == T1[1]:    matrix[1] += 1
                elif t1[0] == T1[1] and t1[1] == T1[0]:    matrix[1] += 1 #flip
                elif t1[0] == T1[1] and t1[1] == T1[1]:    matrix[2] += 1
            elif t0[0] == T0[0] and t0[1] == T0[1]:
                if   t1[0] == T1[0] and t1[1] == T1[0]:    matrix[3] += 1
                elif t1[0] == T1[0] and t1[1] == T1[1]:    matrix[4] += 1
                elif t1[0] == T1[1] and t1[1] == T1[0]:    matrix[4] += 1 # flip
                elif t1[0] == T1[1] and t1[1] == T1[1]:    matrix[5] += 1
            elif t0[0] == T0[1] and t0[1] == T0[0]:                       # row flip
                if   t1[0] == T1[0] and t1[1] == T1[0]:    matrix[3] += 1
                elif t1[0] == T1[0] and t1[1] == T1[1]:    matrix[4] += 1
                elif t1[0] == T1[1] and t1[1] == T1[0]:    matrix[4] += 1 # flip
                elif t1[0] == T1[1] and t1[1] == T1[1]:    matrix[5] += 1
            elif t0[0] == T0[1] and t0[1] == T0[1]:
                if   t1[0] == T1[0] and t1[1] == T1[0]:    matrix[6] += 1
                elif t1[0] == T1[0] and t1[1] == T1[1]:    matrix[7] += 1
                elif t1[0] == T1[1] and t1[1] == T1[0]:    matrix[7] += 1 # flip
                elif t1[0] == T1[1] and t1[1] == T1[1]:    matrix[8] += 1

        printnl("{0:61} {1:4} {2:4} {3:4}".format('', T1[0]+T1[0], T1[0]+T1[1], T1[1]+T1[1]))
        printnl("{0:61} {1:4d} {2:4d} {3:4d}".format(T0[0]+T0[0], matrix[0], matrix[1], matrix[2]))
        printnl("{0:61} {1:4d} {2:4d} {3:4d}".format(T0[0]+T0[1], matrix[3], matrix[4], matrix[5]))
        printnl("{0:61} {1:4d} {2:4d} {3:4d}".format(T0[1]+T0[1], matrix[6], matrix[7], matrix[8]))
        printnl('')
