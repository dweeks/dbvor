#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import os, os.path
import struct
import time

from   gwas     import GWAS
from   snp      import Snp
from   util     import printnl, gzopen, zipopen, input_dir

class Plink(GWAS):

    def __init__(self, db):
        super(Plink, self).__init__(db)
        self._ordered_markers = []
        self._ordered_members = []
        self._ordered_samples = []

    def add_options(self, config):
        super(Plink, self).add_options(config)

        config.add_option("--plinkbim", '--bim', action="store", type="string",
                          help="file for PLINK bim file")
        config.add_option("--plinkfam", '--fam', action="store", type="string",
                          help="file for PLINK fam file")
        config.add_option("--plinkbed", '--bed', action="store", type="string",
                          help="file for PLINK bed file")
        config.add_option("--plinkbfile", '--bfile', action="store", type="string",
                          help="stem for PLINK files")


    def prolog(self, dbi, options):
        if options.input is None and options.plinkbed is not None:
            options.input = options.plinkbed
        return super(Plink, self).prolog(dbi, options)

    def make_snps(self, dbi, options):
        if options.plinkbim is None:
            raise Exception("No PLINK BIM Map specified.")
        FILE = input_dir(options, options.plinkbim)
        if not os.path.isfile(FILE):
            raise Exception("For \"{0}\", cannot find PLINK BIM Map:\n\"{1}\"".format(options.config, FILE))
        self.read_snps(dbi, FILE, options)

    def marker_file(self, FILE, dbi, snp_exclude, options):
        Otime = time.time()

        self._Uchr = self.chrm2int('U')

        printnl("PLINK bim file: {0}".format(options.plinkbim))
        File = zipopen(FILE) if FILE.endswith('zip') else gzopen(FILE)
        Line = 0
        for line in File:
            line = line.strip().split(options.sep)
            Line += 1
            if line[0].startswith('#'):   continue
            if line[0] == '':             continue

            if options.debug:
                printnl("Line {0}:{1}".format(Line, line))
            self._snp_count += 1

            chrm, snp, gm, bp, a, b = line

            rs = snp
            if snp in snp_exclude:
                continue

            rs = self.do_probeid_remap(snp, rs, options)
            rs = self.do_rs_remap(rs, options)

            gm = float(gm)

            self.define_snp(snp, rs, chrm, bp, gm, a+b, Line, options)
        File.close()

        self.sort(options)
#       log.pop("reading mapping")

    def member_marker(self, options):
        AuxMFILE = input_dir(options, options.plinkbim)
        MFile = gzopen(AuxMFILE)
        Line = 0
#r
        for line in MFile:
#           line = line.strip().split(options.sep)
            line = line.strip()
            Line += 1
            if line[0] == '':             continue
            if line[0].startswith('#'):   continue
#           (chrm, rs) = line[:2]
            (chrm, rs, rest) = line.split(None, 2)
#           self._chr_list
            if rs in self._snp_hash:
                r = self._snp_hash[rs]
            else:
                r = None
            self._ordered_markers.append(r)
        MFile.close()

        AuxPFILE = input_dir(options, options.plinkfam)
        PFile = gzopen(AuxPFILE)
        Line = 0
        for line in PFile:
            line = line.strip()
            Line += 1
            if line[0] == '':             continue
            if line[0].startswith('#'):   continue
            (ped, per, rest) = line.split(None, 2)
            self._ordered_samples.append(ped + '_' + per)
            self._ordered_members.append((ped, per))
        PFile.close()

    def sample_file(self, FILE, options):
        sampled = {}
        import io

        File = io.open(FILE, mode='rb', buffering=0)
        self.member_marker(options)
        Line = 0

        BED_HDR = "\x6c\x1b"
        hd = File.read(2)
        if hd != BED_HDR:
            printnl("{0} is not a bed file.  Header is {1}".format(FILE, hd))
        snp_major = File.read(1)
        snp_major = struct.unpack('b', snp_major)[0]
        if snp_major != '0' and snp_major != 1:
            printnl("{0} is not a bed file.  SNP_MAJOR is {1}".format(FILE, snp_major))

        memberl = len(self._ordered_members)
        memberL = (memberl + 3 ) & (~3)        # round up
        samplel = len(self._ordered_samples)
        markerl = len(self._ordered_markers)

        cnti = cnto = 0
        stime = time.time()                    # showtime

        tell = 3
        if snp_major: # original version
#           pdb.set_trace()
            strides = {}
            groupl = 48 # 32
            groupL = (groupl + 3 ) & (~3)
            bytesread = bytearray( (groupl + 3 ) >> 2)
            for stride in range(0, memberl, groupl): # 0 32 64 .. 3124
#               printnl("stride {0}".format(stride))
                samples = self._ordered_samples[stride:stride+groupl] # 0:32, 32:64, 64:96
                vals = [ [] for x in range(0, groupl) ] # 0:32, 0:32
                File.seek(tell + stride / 4, 0) # 0 8 16
                for r in self._ordered_markers: # 0, 1, 895103
##AAAA              rl = File.readinto(self._bytes) # persons 8
##AAAA              File.seek( (memberL-groupl) / 4, 1) # (3124-32)/4 781-8 == 773
                    snp = r[Snp.pid]
                    bytesread = bytearray( groupL >> 2 )
                    strides[snp] = bytesread
                    rl = File.readinto(bytesread) # persons 8
                    File.seek( (memberL-groupl) >> 2, 1) # (3124-32)/4 781-8 == 773
                if options.debug:
                    printnl("1 full block read {0:.3f} sec".format(time.time()-stime))

                for r in self._ordered_markers: # 0, 1, 895103
                    snp = r[Snp.pid]
                    bytesread = strides[snp]
                    alleles = r[Snp.ab]
                    fourpl  = (2 * alleles[0], '00', alleles, 2 * alleles[1])

                    for group in range(0, groupl, 4):             # 0, 4, 8, .. 32; 0   4
#                       printnl("group {0}".format(group))
                        by4 = bytesread[group>>2] # 0 1 2 8; 0 1
                        for four in range(group, group+4):
#                           printnl("four {0}".format(four))
                            if stride + four >= memberl:
                                break
                            by = by4 & 3
                            by4 >>= 2
                            vals[four].append(fourpl[by])
                            cnti += 1

                if options.debug:
                    printnl("1 full block parser {0:.3f} sec".format(time.time()-stime))
                for s, val in zip(samples, vals):
                    btime = time.time()
                    for i, r in enumerate(self._ordered_markers):
                        r[Snp.cll] = val[i] # 0. 2sec
                    if options.debug:
                        printnl("{1} Snp.cll {0:.3f} sec".format(time.time()-btime, s))
                    if not self.store_sample(s, options): # 2.5
                        cnti = 0
                    self._process_count += 1
                    if options.debug:
                        printnl("{1} store {0:.3f} sec".format(time.time()-btime, s))

                if options.verbose:
                    printnl("#{0:2d} Sample {1}: defined snps {2:d}; unknown snps {3:d}; {4:.3f} sec".format(self._process_count, samples, cnti, cnto, time.time()-stime))
                cnti = cnto = 0
                stime = time.time()
            return

        else: # individual major
            bytesread = bytearray( (markerl + 3 ) >> 2)
            i = 0
            for Sample in self._ordered_samples:
                rl = File.readinto(bytesread)
                for i4 in range(0, markerl, 4):
                    by4 = bytesread[i4/4]
                    printnl("{0} {1}(ex {2}) {3:x}".format(Sample, i4, rl, by4)) #r
                    for i in range(i4, i4+4):
                        if i >= markerl:
                            break
                        by = by4 & 3
                        by4 >>= 2
                        r = self._ordered_markers[i]
                        if by == 0: # minor
                            r[Snp.cll] = 2 * r[Snp.ab][0]
                        elif by == 1: # 0/0
                            r[Snp.cll] = '00'
                        elif by == 2: # x/y
                            r[Snp.cll] = r[Snp.ab]
                        else: # major
                            r[Snp.cll] = 2 * r[Snp.ab][1]
                        name    = r[Snp.rs]
                        r[Snp.qul] = 0.0
                        cnti += 1
                        if r[Snp.chr] == 24:
                            printnl("{0} {1} {2} {3} ".format(by, r[Snp.cll], name, r[Snp.ab])) #r
                    printnl("")  #r
                if not self.store_sample(Sample, options):
                    cnti = 0
                if options.verbose:
                    printnl("#{0:2d} Sample {1}: defined snps {2:d}; unknown snps {3:d}; {4:.3f} sec".format(self._process_count, Sample, cnti, cnto, time.time()-stime))
                self._process_count += 1
                cnti = cnto = 0
                stime = time.time()
            return



