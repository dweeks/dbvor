#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#
# Look up marker values
#
# External Class
#	 Marker
# External Methods
#	 markers	-- look up pairs of markers
#	 marker		-- look up a marker

#import dbi

import types

from chrm     import Chrm
from util     import printnl, printn, log

class Marker_Alias(object):
    def __init__(self, dbi):
        self._dbi_  = dbi
        self._mt    = None

# needs cache
    def lookup(self, name, options):
        if self._mt is False:
            return None
        elif self._mt is None:
            count = self._dbi_.select_one("select count(*) from marker_aliases")
            if count[0] > 0:
                self._mt = True
            else:
                self._mt = False
                return None

        Mid = self._dbi_.select_all("""select marker, id from marker_aliases
                                          where alias = ?""", name)
        if len(Mid) == 0:
            return None
        if len(Mid) == 1:
            return Mid[0][0]

        printnl("warning: multiple marker_aliases entries for alias {0}.  Error!".format(name))
        for e in Mid: printnl("\t\t{0}".format(e))
        options.error += 1

        return Mid[0][0]


class Marker(Chrm):
    def __init__(self, dbi):
        super(Marker, self).__init__(dbi)
#       self._dbi_    = dbi
        self._alias_  = Marker_Alias(dbi)
        self._M2id    = {}
        self._revr    = {}
        self._missing = []
        self._multi   = []

# needs cache
    def markers(self, snps, options):
        snp_ids = []
        Len = len(snps)
        if Len % 2 == 1:
            printnl("SNP pairs have ODD length: {0}.  Error!".format(snps))
            Len -= 1
            options.error += 1
        for a, b in [(snps[i], snps[i+1]) for i in range(0, Len, 2)]:
            snp_a, snp_b = a[:-2], b[:-2]
            if snp_a != snp_b:
                printnl("SNP pair has different SNPs: \"{0}\", \"{1}\".  Error!".format(a, b))
                options.error += 1
            if snp_a in self._M2id:
                printnl("SNP {0} appears in list more than once.  Error!".format(repr(snp_a)))
                options.error += 1
                Id = self._M2id[snp_a]
                snp_ids.extend([Id, Id])
                continue
            marker_id = self.marker(snp_a, options)
            snp_ids.extend([marker_id, marker_id])
        return snp_ids

    def kers(self, snps, options):
        snp_ids = []
        for snp in snps:
            if snp in self._M2id:
                printnl("SNP {0} appears in list more than once.  Error!".format(snp))
                options.error += 1
                Id = self._M2id[snp]
                snp_ids.extend([Id])
                continue
            marker_id = self.marker(snp, options)
            snp_ids.extend([marker_id])
        return snp_ids

    def marker(self, snp, options):
        if snp in self._M2id:
            if options.debug > 10: printnl("SNP {0} is a repeat".format(snp))
            Id = self._M2id[snp]
            return Id
        Id = self._alias_.lookup(snp, options)
        if Id:
            self.cache2list(snp, Id)
            return Id

        Ids = self._dbi_.select_all("select id from markers where marker = ?\n", snp)

        if len(Ids) == 0:
            self._missing.append(snp)
            if options.debug:
#               printnl("SNP ({0}) is unknown and not in the MARKERS table".format(snp))
                printnl("Unable to find marker {0} in the markers table of the database; you may need to add it.  Error!".format(snp))
            options.error += 1
            Id = None
        else:
            if len(Ids) > 1:
                self._multi.append((snp, Ids))
                printnl("SNP ({0}) appears multiple times in the MARKERS table.  Error!".format(snp))
                options.error += 1
            Id = Ids[0][0]
        self.cache2list(snp, Id)
        return Id

    def rev(self, Id):
        return self._revr.get(Id, None)

    def cacheload(self, snp, Id):
        if Id in self._revr:
            return
        self.cache2list(snp, Id)

    def cache2list(self, snp, Id):
        self._M2id[snp] = Id
        if Id is None: return
        if Id not in self._revr:
            self._revr[Id] = snp
        else:
            old = self._revr[Id]
            if type(old) != types.ListType:
                old = [old]
                self._revr[Id] = old
            printn("SNP COLLISION: maps to {0} as do(es) ".format(Id))
            for v in old:
                printn("snp {0}".format(v))
            printnl('')
            old.append(snp)

    def list(self):
        return sorted(self._revr.iterkeys())

    def sortedlist(self):
        return self.list().__iter__()

    def stat(self, snp_stat, options):
        def snp_key(ta, tb):
            return cmp(ta[0], tb[0])

        if self._missing:
            self._missing.sort(snp_key)
            self.pr_missing(options)

        if self._multi:
            self._multi.sort(snp_key)
            self.pr_multi(options)

        if snp_stat:
            snps = snp_stat.keys()
            snps.sort()
            log.push('marker_contents')
            printnl("\nCounts for each marker field")
            log.endheader()
            for snp in snps:
                self.pr_snp_stat(snp, snp_stat[snp], options)
            log.pop('marker_contents')
            printnl("")

    def pr_missing(self, options):
        FILE = options.dir + options.cfgroot + '_MissingMarkers.txt'
        File = open(FILE, 'w')

        log.push('marker_missing')
        printnl("\nThe following markers are not in the marker table or marker_alias table:")
        log.endheader()
        for snp in self._missing:
            printnl("{0}".format(repr(snp)))
            File.write("{0}\n".format(snp))
        log.pop('marker_missing')

        printnl("\nMissing markers are stored in {0}.\nTo accept them, type:".format(FILE))
        printnl("	dbvor newmarker --dir=. --project={0} --input={1} --db={2} --commit\n".format \
                (options.project, FILE, options.db))

        printnl("\nTo look up the marker positions, type:")
        printnl("	gmi.pl {0} --outfile {1}".format(FILE, options.dir + options.cfgroot + '_gmi_out'))
        printnl("\nTo put them in the database, type the TWO lines below (correct the arguments where necessary):")
        printnl("	dbvor gmimarkerALIAS --dir=. --project={0} --input={1} --db={2} --commit\n".format \
                (options.project, options.dir + options.cfgroot + '_gmi_out_log.txt', options.db))
        printnl("\n	dbvor gmimarker --dir=. --project={0} --input={1} --build=<marker_build_version> --db={2} --commit\n".format \
                (options.project, options.dir + options.cfgroot + '_gmi_out_annot.txt', options.db))
        File.close()

    def pr_multi(self, options):
        log.push('marker_error')
        printnl("\nThe following snp's were found multiple times in marker table:\nsnp\tvalue")
        log.endheader()
        for tupl in self._multi:
            for i in tupl[1]:
                printnl("{0}\t{1}".format(tupl[0], i))
        log.pop('marker_error')

    def pr_snp_stat(self, snp, hsh, options):
        from   operator import itemgetter

        korder = [x[1] for x in sorted(((len(k), k) for k in hsh.iterkeys()), key=itemgetter(0, 1))]
        printn("{0:18}".format(snp))
        for k in korder:
            printn("{0:>5}: {1:4d},".format(repr(k), hsh[k]))
        printnl("")

    def fmtlen(self, *ls, **ks):
        ans = map(len, ls)
        marker = ks.get('marker', 0)
        if marker is not None:
            ans[marker] = max(ans[marker], log.marker_length)
        snp = ks.get('snp', 1)
        if snp is not None:
            ans[snp] = max(ans[snp], log.snp_name_length)
        return tuple(ans)
