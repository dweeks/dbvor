#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import os, os.path
import csv, time

from   gwas     import GWAS
from   snp      import Snp
from   util     import printnl, printn, log, gzopen, zipopen

class Affy(GWAS):

    def __init__(self, db):
        super(Affy, self).__init__(db)
        self._header_hash               = {}
        self._snp_remap_b               = {}  # own class ??
        self._snp_remap_d               = {}  # own class ??

    def add_options(self, config):
        super(Affy, self).add_options(config)
        config.add_option("--nocachefile", action="store_true", default=False,
                          help="re-read file to process every column pair")

#4/1    config.add_option("--pedmap", action="store", type="string",
#4/1                      help="file for pedigree data")
        config.add_option("--samplemap", action="store", type="string",
                          help="file for sample to pedigree:person mapping")
        config.add_option("--make_samples", action="store_true", default=False,
                          help="start sample.map entries")
        config.add_option("--more_samples", action="store_true", default=False,
                          help="additional sample.map entries and input files")
        config.add_option("--old_snp_remap", action="store", type="string",
                          help="file for SNP_A and rs translations to rs")

        config.add_option("--affymap", action="store", type="string",
                          help="file for Affymetrix map of SNP_A to data")
        config.add_option("--affymapfmt", action="store", type="string",
                          help="file format for Affymetrix map of SNP_A to data")

        config.add_option("--genmap", action="store", type="string",
                          help="genetic map choice: '0', '1'..'n', 'gmi'")
        config.add_option("--genmapfield", action="store", type="int",
                          help="Affymetrix genetic map column")

        config.add_option("--call_sfx", action="append", type="string",
                          help="Affymetrix data: suffix for call column")
        config.add_option("--qual_sfx", action="append", type="string",
                          help="Affymetrix data: suffix for call quality column")
        config.add_option("--extra_lines", action="store", type="int", default=0,
                          help="Affymetrix data: skip extra_lines before header row")
        config.add_option("--extra_hdr", action="append", type="string", default=[],
                          help="Affymetrix data; extra \"unused\" columns")

        config.add_option("--subsample", action="append", type="string", default=[],
                          help="Affymetrix data: pull some samples")
        config.add_option("--subsampleall", action="store", type="string",
                          help="directory for all sample data")
        config.add_option("--subsamplecnt", action="store", type="int", default=5,
                          help="Affymetrix data: number of elements in a sample")

    def init(self, dbi, options):    # prolog and hence init are done once per glob

        if options.subsample or options.subsampleall: return True

        if options.more_samples or options.make_samples:
            self.make_samples(dbi, options)
            return False    # terminate

        return super(Affy, self).init(dbi, options)

    def make_samples(self, dbi, options):
        if options.more_samples:    # add to samples
            self.sample_select(dbi, options)

#       we probably do not want to use the subject field of member table with pedmap and try to
#       align the sample
#       self.subject_select(dbi, options)

        if options.samplemap:
            self.sample_read(dbi, options)
            self.sample_insert(dbi, options)

    def make_snps(self, dbi, options):
        if options.affymap is None:
            raise Exception("No Affymetrix Map specified.")
        FILE = options.dir+options.affymap
        if not os.path.isfile(FILE):
            raise Exception("For \"{0}\", cannot find Affymetrix Map:\n\"{1}\"".format(options.config, FILE))

        if options.old_snp_remap:
            self.old_snp_remap_read(dbi, options)

        self.read_snps(dbi, FILE, options)
        return False

    def map_header(self, line):
        k, v = line.rstrip()[2:].split('=')
        self._header_hash[k] = v

    def marker_file(self, FILE, dbi, snp_exclude, options):
        Otime = time.time()

        printnl("Affymetrix Genome File: {0}\n".format(options.affymap))
        if FILE.endswith('zip'):
            File = zipopen(FILE)
        else:
            File = gzopen(FILE)
        Line = 0
        for line in File:
            Line += 1
            sline = line.strip()
            if sline == '':             continue
            if sline.startswith('#%'):  # magic header
                self.map_header(line)
            if sline.startswith('#'):
                printnl(sline)
                continue
            if options.verbose:          # show column header
                printnl('')
                printnl("{0:>3}: {1}".format('Col', 'Contents'))
                for i, inf in enumerate(line.split(',')):
                    printnl("{0:3d}: {1}".format(i, inf))
            break

        if options.genmap in ('1', '2', '3'):
            gmi = int(options.genmap) - 1

        log.push("problem probe")
        if options.verbose or True:
            printnl("Problem entries with missing chromosome or position")
            printnl("{0:>6}  {1:>19} {2:>12} {3:>3} {4:>8} {5:>2}".format('Line', 'PROBE SET ID', 'SNP', 'chr', 'pos', 'alleles'))
            log.endheader()

        self._Uchr = self.chrm2int('U')
##just csv  340 sec
##all action394 sec
        for line in csv.reader(File):    # should have been using csv all along
            Line += 1
            self._snp_count += 1
            if options.affymapfmt == 'Xba131':
                (snp, rs, chrm, bp, a, b) = (line[0], line[2], line[3], line[6], line[11], line[12])
            else:  #Xba142
                ((snp, rs, chrm, bp), a, b) = (line[:4], line[8], line[9])
            if snp in snp_exclude:
                continue

# old is gone
#           rs = self.old_snp_remap(snp, rs, options)
            rs = self.do_probeid_remap(snp, rs, options)
            rs = self.do_rs_remap(rs, options)

            if rs == '---' or rs == '' or rs is None:
                rs = 'rs000' + str(Line)
                printnl("{0:6d}: {1:>19} {2:>12} {3:>3} {4:>8} {5}{6}" .format(Line, snp, rs, chrm, bp, a, b))
#               raise Exception("No snp name ... map it")
# 11 is genetic map; 5 is X autosomal region 1; 21 is X autosomal region 2
#           printnl("#{0:d}: {1} {2} {3} {4} {5}{6} ({7})".format(Line, snp, rs, chrm, bp, a, b, line[11]))
            if chrm == 'X':
                if line[5] == '1' or line[21] == '1':
                    chrm += 'Y'  # will break horribly if set and not 'X' ... intentional

            gm = 0.0
            if options.genmap is None or options.genmap == '0':
                pass
            elif options.genmap in ('1', '2', '3'):
                gmf = line[options.genmapfield]
                try:
                    if gmf != '---':
                        gml = gmf.split('///')
                        gm = round(float(gml[gmi].split(None, 1)[0]), 3)
#                       printnl(gm)
                except Exception, error:
                    printnl("Line {0:d}, bad genetic map: {1}".format(Line, gmf))

            self.define_snp(snp, rs, chrm, bp, gm, a+b, Line, options)
        File.close()

        self.sort(options)
        log.beginfooter()
        printnl("\nPARSE AFFY \"{0}\" {1:d} snps in {2:.3f} sec\n".format(FILE, self._snp_count, (time.time() - Otime)))

        FILED = options.dir + options.cfgroot + '.rsDup'
        FileD = gzopen(FILED, "w")
        cnt  = 0
        print >>FileD, "{0:>19} {1:>12} {2:>3} {3:>10} {4:>8} {5:>2}".format('Affy Probe Id', 'SNP Id', 'chr', 'Position', 'Morgan', 'allele')
        for rs, lst in self._rs_hash.iteritems():
            if len(lst) > 1:
                cnt += 1
                for l in lst:
                    print >>FileD, ("{t5[0]:>19} {t5[1]:>12} {t5[2]:>3} {t5[3]:10d} {t5[4]:8.3f} ".format(t5=l[2:7])),
                    print >>FileD, "{0}".format(l[9])
                print >>FileD, ''
        log.pop("problem probe")
        FileD.close()
        if cnt != 0:
            printnl("\nWrote DUP rs into ({0:d}) to \"{1}\"".format(cnt, FILED))


    def sample_file(self, FILE, options):
# 'ome'
#       extra_line = 1
#       Call = '_Call'
#       Conf = '_Call Zone'
#       ExtraHeaders = ('SNP ID', 'Chromosome', 'Physical Position', 'dbSNP RS ID', 'TSC ID')

# 'samoa'
#       extra_line = 0
#       Call = '.birdseed-v2.chp Call'
#       Conf = '.birdseed-v2.chp Confidence'
#       ExtraHeaders = ()

        Calls = options.call_sfx            # sort in place
        Calls.sort(key=len, reverse=True)
        Confs = options.qual_sfx
        Confs.sort(key=len, reverse=True)

        self.sample_headers(FILE, options)

# go thru col's & find next "#_Call", "#_Call Zone" pair
        Call = Conf = None
        j = self._sample_file_header_offset
        subsample = False
        for call in self._sample_file_header[self._sample_file_header_offset:]:
            for Call in options.call_sfx:
                if call.endswith(Call): break
            for Conf in options.qual_sfx:
                if call.endswith(Conf): break

            j += 1
            if call.endswith(Call):
                i = j-1
                sample = call[:-len(Call)]
                self._process_count += 1
                if (options.subsample and sample in options.subsample) or options.subsampleall:
                    if options.subsampleall:
                        if not os.path.isdir(options.dir + options.subsampleall):
                            os.makedirs(options.dir + options.subsampleall)
                        subsampleFILE = os.path.join(options.dir, options.subsampleall, sample + '.txt')
                    else:
                        subsampleFILE = options.dir + 'Samples/' + sample + '.txt'
                    subsample = True
                    subsampleFile = gzopen(subsampleFILE, "w")

                    subsampleFile.write("Probeset ID")
                    for subsamplei in range(i, i + options.subsamplecnt):
                        subsampleFile.write("\t{0}".format(self._sample_file_header[subsamplei]))
                    subsampleFile.write("\n")
                continue
            elif call.endswith(Conf):
                iz = j-1
            else:
                if options.debug:
                    printnl('skipping: offset {0:d} {1}'.format(j+self._sample_file_header_offset, call))
                continue

            stime = time.time()                    # showtime
            if options.verbose:
                printn('SAMPLE {0}'.format(sample))
            if options.subsample and not subsample:
                if options.verbose:    printnl('')
                continue
            Line = 1
            if options.nocachefile:
                File = gzopen(FILE)
                for i in range(0, options.extra_lines):
                    File.readline()
                    Line += 1
                File.readline()  # hdr
                Line += 1

            cnti = cnto = 0
            for line in File if options.nocachefile else self._sample_file_cache:
                data = self.get_line(line, Line, len(self._sample_file_header), options.sep) if options.nocachefile else line
                if options.extra_lines:
                    ID = data[0]
                    snp = data[1]
                else:
                    ID = Line
                    snp = data[0]
                if options.debug >= 3:
                    printnl("{0} {1} {2} {3}".format(ID, snp, sample, data[i:iz+1]))

                if subsample:
                    subsampleFile.write(snp)
                    for subsamplei in range(i, i + options.subsamplecnt):
                        subsampleFile.write("\t{0}".format(data[subsamplei]))
                    subsampleFile.write("\n")
                    continue
                if snp in self._snp_hash:
                    r = self._snp_hash[snp]
                    if data[i] == 'NoCall':
                        pass
                    elif len(data[i]) != 2:
                        printnl('tilt: line {0}, file {1:d} line {2}, data {3}'.format(Line, i, line, data))
                    r[Snp.cll] = data[i]
                    r[Snp.qul] = data[iz]
                    cnti += 1
                else:
                    cnto += 1
                    self.undefined_snp(snp, options)
                Line += 1
            if options.nocachefile: File.close()
            if subsample:
                subsampleFile.close()
                subsample = False
                printnl("")
                continue

            if not self.store_sample(sample, options):
                cnti = 0
            if options.verbose:
                printnl("Defined snps {0:d}; unknown snps {1:d}; {2:.3f} sec".format(cnti, cnto, time.time()-stime))

# return after one sample
#           return

    def sample_headers(self, FILE, options):
        File = gzopen(FILE)
        Line = 1
        for i in range(0, options.extra_lines):
            title = File.readline()    # corn flakes
            title = title.strip()
            Line += 1
        self._sample_file_header = File.readline().rstrip().split(options.sep)
        if not options.nocachefile:
            self._sample_file_cache = []
            for line in File:
                data = self.get_line(line, Line, len(self._sample_file_header), options.sep)
                if data is None: continue
                self._sample_file_cache.append(data)
        File.close()
        if options.verbose:
            printnl("Header {0}".format(self._sample_file_header[:7]))

        self._sample_file_header_offset = None                                      # pylint does not understand scope rules for loop variables
        for self._sample_file_header_offset in range(1, 7):
            if self._sample_file_header[self._sample_file_header_offset] not in options.extra_hdr:      # skip known Extra Header Data
                break

################################################################
# Class KeyTrans
    def old_snp_remap_read(self, dbi, options):
        if options.snp_remap is None:
            return
        FILE = options.dir + options.snp_remap
        File = gzopen(FILE)
        for line in File:
            line = line.strip()
            if line.startswith('b'):
                fields = line.split('"')
                self._snp_remap_b[fields[1]] = fields[3]
            elif line.startswith('d'):
                fields = line.split('"')
                self._snp_remap_d[fields[1]] = fields[3]

### Q 197, B 103, D 40
    def old_snp_remap(self, snp, rs, options):
        if options.old_snp_remap is None:
            return rs
        nrs = rs
        if snp in self._snp_remap_b and rs in self._snp_remap_d:
            if self._snp_remap_b[snp] == self._snp_remap_d[rs]:
                printnl('Q {0} -> {1} / {2} -> {3}'.format(snp, self._snp_remap_b[snp], rs, self._snp_remap_d[rs]))
            else:
                printnl('Z')
            nrs = rs
        elif snp in self._snp_remap_b:
            nrs = self._snp_remap_b[snp]
            if rs != nrs:
                printnl('B {0} {1} {2}'.format(snp, nrs, rs))
        elif rs in self._snp_remap_d:
            nrs = self._snp_remap_d[rs]
            if rs != nrs:
                printnl('R {0} {1} {2}'.format(rs, nrs, snp))
        if snp in self._snp_remap_d:
            nrs = self._snp_remap_d[snp]
            if rs != nrs:
                printnl('D {0} {1} {2}'.format(snp, nrs, rs))
        return nrs
