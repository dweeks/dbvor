#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import time

from   chrm     import Chrm
from   util     import NoTrace, printnl, printn, gzopen, read_fields

class Snp(Chrm):
    cll = 0
    qul = 1
    pid = 2
    rs  = 3
    chr = 4
    bp  = 5
    cm  = 6
    blk = 7
    off = 8
    ab  = 9
    cnt = 10


    def __init__(self, db):
        super(Snp, self).__init__(db)
#       self._dbi_           = db
        self._snp_hash       = {}
        self._rs_hash        = {}
        self._snp_count      = 0
#       self._chr_list       = [[], [], [], [], [], [], [], [], [], [],
#                               [], [], [], [], [], [], [], [], [], [],
# added 2                       20  21  22  23  24  25  26 27
# added 2                       20  21  22   X   Y  XY  MT  U
#                               [], [], [], [], [], [], []]
        self._chr_list       = self.chrm_mt()
#11/19  self._insert_size    = 100
        self._insert_size    = 200
        self._probeid_remap  = {}  # own class ??
        self._rs_remap       = {}  # own class ??
        self._reposition     = {}  # own class ??

        self._block_size     = 0   # used by derived class (BlockSnp)
#       self._block_size     = 1000 / 2   # used by derived class (BlockSnp)

        self._gmi_head       = ['Chromosome', 'Map.k.a', 'Name', 'Map.k.m', 'Map.k.f', 'Ensembl', 'X.Extrapolation']

    def add_options(self, config):
#       super(Snp, self).add_options(config)
        config.add_option("--make_snps", action="store_true", default=False,
                          help="recompute chromosome/position with gmi")
#4/1    config.add_option("--make_rs_file", action="store_true", default=False,
#4/1                      help="dump rs file and stop")
        config.add_option("--dedup", action="store_true", default=False,
                          help="make duplicate snps inactive")
        config.add_option("--probeid_remap", action="store", type="string",
                          help="new improved snp->rs")
        config.add_option("--rs_remap", action="store", type="string",
                          help="new improved rs->rs")
        config.add_option("--reposition", action="store", type="string",
                          help="new improved rs-> chr/bp")

#4/1    config.add_option("--rs_file_output", action="store", type="string",
#4/1                      help="write rs ids to file")
        config.add_option("--snp_gclean_file_output", action="store", type="string",
                          help="write out snp data to file in geneva clean format")

        config.add_option("--snp_file_output", action="store", type="string",
                          help="write out snp data to file")
        config.add_option("--snp_file_input", action="store", type="string",
                          help="read snp data from file")

        config.add_option("--snp_db_output", action="store", type="boolean", default=False,
                          help="insert snp data into database")
        config.add_option("--build", action="store", type="string",
                          help="build name for marker_info")
        config.add_option("--snp_db_input", action="store_true", default=False,
                          help="select snp data from database")

    def record(self, r, options):
        snp = r[Snp.pid]
        old = self._snp_hash.get(snp, None)
        if old:                          # flush old record from "old" chr list
            self._chr_list[old[Snp.chr]].remove(old) # only last counts
            if options.debug:
                printnl("dup probe id {0}".format(old))
        self._snp_hash[snp] = r            # record in hash by snp
        self._chr_list[r[Snp.chr]].append(r)     # record in list by chr

        rsx = r[Snp.rs]
#0211
        rs, sfx = rsx.split('_', 1) if (not rsx.startswith('chr')) and rsx.find('_') != -1 else (rsx, None)
        sib = self._rs_hash.setdefault(rs, [])
#        if rs not in self._rs_hash:
#            self._rs_hash[rs] = []
#        sib = self._rs_hash[rs]
        if old:
            sib.remove(old)
        sib.append(r)
        if len(sib) > 1 and options.debug:
            printnl("dup rsname {0}".format(sib))
        return sib

    def dedup(self, dbi, options):
        cmd = "update snpblocks set active = 0 where rs like '%.%'"
        dbi.execute(cmd)
        dbi.endbatch(options)

    def sort(self, options):
        def chr_bp_key(a, b):                     # sort on chr then bp
            return cmp(a[Snp.chr], b[Snp.chr]) or cmp(a[Snp.bp], b[Snp.bp])

        COL_SZ = "select character_maximum_length from information_schema.columns where table_name = 'genotypeblocks' and column_name = 'data' and table_schema = '{0}'"
        bs = self._dbi_.select_one(COL_SZ.format(options.db))
        if bs is None:
            raise NoTrace("length for {}.genotypeblocks.data could not be found\n".format(options.db))
        self._block_size = bs[0] / 2
        printnl("GWAS block size {0} alleles\n".format(bs[0]), tag=True)

        CNT = self.chrm_0()
        BLK = self.chrm_0()
        OFF = self.chrm_0()

        for lst in self._chr_list:
            lst.sort(chr_bp_key)           # sort

        for chrm, lst in enumerate(self._chr_list):
            for r in lst:               # break chr into blocks of self._block_size
                CNT[chrm] += 1

                OFF[chrm] += 1
                if OFF[chrm] > self._block_size:
                    BLK[chrm] += 1
                    OFF[chrm] = 1
                c = OFF[chrm]
                b = BLK[chrm]
                r[Snp.off] = c
                r[Snp.blk] = b

        if options.debug:                   # stats
            printnl("Chromosome coverage")
            printnl("{0:>3} {1:>6} {2}".format('chr', 'count', 'misc'))
            for i, (cnt, blk, off) in enumerate(zip(CNT, BLK, OFF)):
                printnl("{0:3} {1:6d} [{2:d}, {3:d}]".format(self.int2chrm(i), cnt, blk, off))

### examine sort
#       for lst in self._chr_list:
#           for r in lst:
#               printnl("{t4[0]:10} {t4[1]:10} {t4[2]:3d} {t4[3]:10d}".format(t4=r[2:6]))

    def snp_file_input(self, options):
        FILE = options.dir + options.snp_file_input
        File = gzopen(FILE)
        cnt  = 0
        for line in File:
            rec = line.split()
            rec.append(0)
            rec.insert(0, None)
            rec.insert(0, None)
            rec[Snp.chr] = int(rec[Snp.chr])
            rec[Snp.bp]  = int(rec[Snp.bp])
            rec[Snp.cm]  = float(rec[Snp.cm])
            self.record(rec, options)
            cnt += 1
        File.close()
        self.sort(options)
        printnl("Read SNP info ({0:d}) from \"{1}\"".format(cnt, FILE))
        return cnt

    def snp_db_input(self, options):
#       import pdb;pdb.set_trace()
        Time = time.time()
        cnt = 0
        cmdb = "select max(block) from snpblocks where technology = ? and chromosome = ? and active = ?"
        cmd = "select snp, rs, chromosome, bp, cm, block, offset, allele from snpblocks where technology = ? and chromosome = ? and block = ? and active = ? order by offset, time_stamp"
        for chrm, unused in enumerate(self._chr_list):
            blocks = self._dbi_.select_one(cmdb, options.tech_id, chrm, options.active)
            if blocks == None:
                continue
            else:
                blocks = blocks[0]
                if blocks == None: continue  # i.e. (None, )
            for b in range(0, blocks + 1):
                lst = self._dbi_.select_all(cmd, options.tech_id, chrm, b, options.active)
                for t in lst:
                    rec = list(t)
                    rec.append(0)
                    rec.insert(0, None)
                    rec.insert(0, None)
                    self.record(rec, options)
#                    rac = [None, None]
#                    rac.extend(t)
#                    rac.append(0)
#                    self.record(rac, options)
                    cnt += 1
        self.sort(options)
        printnl("Selected SNP info ({0:d} records) from \"{1}.snpblocks\" in {2:6.3f} sec\n".format(cnt, options.db, time.time()-Time))
        return cnt

    def rs_file_output(self, options):
        FILE = options.dir + options.rs_file_output
        File = gzopen(FILE, 'w')
        cnt  = 0
        for rs in self._rs_hash.iterkeys():
            if rs != '---' and rs.find('.') == -1:
                print >>File, rs
                cnt += 1
        File.close()
        printnl("Wrote RS names ({0:d}) to \"{1}\"".format(cnt, FILE))

    def snp_file_output(self, options):
        FILE = options.dir + options.snp_file_output
        File = gzopen(FILE, 'w')
        cnt  = 0
        for lst in self._chr_list:
            for r in lst:
                print >>File, r[Snp.pid], r[Snp.rs], r[Snp.chr], r[Snp.bp], r[Snp.cm], r[Snp.blk], r[Snp.off], r[Snp.ab]
                cnt += 1
        File.close()
        printnl("Wrote SNP info ({0:d}) to \"{1}\"".format(cnt, FILE))

    def snp_gclean_file_output(self, options):
        FILE = options.dir + options.snp_gclean_file_output
        File = gzopen(FILE, 'w')
        U    = self.chrm2int('U')
        cnt  = 1
        print >>File, "probe.id", "rs.id", "position", "chrom.std", "chrom.int", "int.id"
        for lst in self._chr_list:
            for r in lst:
#               snp2 <- snp[,c("probe.id", "rs.id", "position", "chrom.std", "chrom.int","int.id")]
                chrm = r[Snp.chr]
                chrmchar = self.int2chrm(chrm)
                if chrmchar == 'MT': chrmchar = 'M'
                bp = r[Snp.bp] if chrm != U else -1
                print >>File, r[Snp.pid], r[Snp.rs], bp, chrmchar, chrm, cnt
                cnt += 1
        File.close()
        printnl("Wrote Geneva Clean info ({0:d}) to \"{1}\"".format(cnt-1, FILE))

    def which_build(self, options):
        builds = dict(self._dbi_.select_all("select lower(build), id from builds"))
        if not options.build.lower() in builds:
            self._dbi_.insert("insert into builds (build) VALUES (?)", [(options.build,)])
            bid = self._dbi_.select_one("select id from builds where build = ?", options.build)
            if bid is None:
                raise Exception("oops: Build({0}) insert into database failed".format(options.build))
            bid = bid[0]
            if options.commit: printnl("Adding build name \"{0}\" (#{1}) to database".format(options.build, bid))
            mi = {}
        else:
            bid = builds[options.build.lower()]
##          mi = dict(self._dbi_.select_all("select lower(marker), id from marker_info where build = ?", bid))
            mi = dict(self._dbi_.select_all("select marker, id from marker_info where build = ?", bid))
        return bid, mi

    def snp_db_output(self, options):
# test add technology/experiment
        markers = dict(self._dbi_.select_all("select marker, id from markers"))
        total  = 0
        total2 = 0
        total3 = 0
        cmd = 'insert into snpblocks (projectid, technology, experiment, snp, rs, chromosome, bp, cm, block, offset, allele, active)'
        cmd2 = 'insert into markers (marker, active)'
        fmt  = "({0:d}, {1:d}, {2:d},".format(options.project_id, options.tech_id, options.experiment_id)
        fmt2 = "('{0}', " + "{0:d})".format(options.active)
        fmt += " '{0}', '{1}', {2:d}, {3:d}, '{4:f}', {5:d}, {6:d}, '{7}',"
        fmt += " {0:d})".format(options.active)
        im = self._dbi_.insert_many(cmd, fmt, self._insert_size)
        im2 = self._dbi_.insert_many(cmd2, fmt2, self._insert_size)
        if options.build:  # one more table ...
            bid, mi = self.which_build(options)
            cmd3 = 'insert into marker_info (marker, build, male, female, avg, pos, chromosome, active)'
            fmt3  = "('{0}',"
            fmt3 += " {0:d},".format(bid)
            fmt3 += " '{1:f}', {2:f}, {3:f}, '{4:d}', {5:d},"
            fmt3 += " {0:d})".format(options.active)
            im3 = self._dbi_.insert_many(cmd3, fmt3, self._insert_size)
        else:
            mi = {}
        for lst in self._chr_list:
            for r in lst:
                total += 1
                im((r[Snp.pid], r[Snp.rs], r[Snp.chr], r[Snp.bp], r[Snp.cm], r[Snp.blk], r[Snp.off], r[Snp.ab]))
                if markers.get(r[Snp.rs], None) is None:
                    total2 += 1
                    im2((r[Snp.rs],))
                if options.build and r[Snp.rs] not in mi:
                    total3 += 1
                    im3((r[Snp.rs], r[Snp.cm], r[Snp.cm], r[Snp.cm], r[Snp.bp], r[Snp.chr], options.active))

        im()
        im2()
        if options.build:  # one more table ...
            im3()

        printnl("Inserted SNP info ({0:d}) into \"{1}.snpblocks\"".format(total, options.db))
        printnl("Inserted SNP info ({0:d}) into \"{1}.markers\"".format(total2, options.db))
        printnl("Inserted SNP info ({0:d}) into \"{1}.marker_info\"".format(total3, options.db))

#       self._dbi_.endbatch(options, commit=True)
        self._dbi_.endbatch(options)


    def clean(self):
        cll = Snp.cll
        qul = Snp.qul
        for lst in self._chr_list:
            for r in lst:
##              r[Snp.cll], r[Snp.qul] = (None, None)
                r[cll] = None
                r[qul] = None

    def pr_all(self):
        for lst in self._chr_list:
            for r in lst:
                printnl(r)

    def pr_chr(self, chrm):
        for r in self._chr_list[chrm]:
            printnl(r)

################################################################
# Remap SNP/RS
    def probeid_remap_read(self, options):
        if options.probeid_remap is None:
            return
        FILE = options.dir + options.probeid_remap
        cnt = 0
        for cnt, line, fields in read_fields(FILE):
            self._probeid_remap[fields[0]] = fields[1]
        printnl("SNP REMAP \"{0}\" {1:6d} lines".format(FILE, cnt))

    def rs_remap_read(self, options):
        if options.rs_remap is None:
            return
        FILE = options.dir + options.rs_remap
        cnt = 0
        for cnt, line, fields in read_fields(FILE):
            self._rs_remap[fields[0]] = fields[1]
        printnl("RS  REMAP \"{0}\" {1:6d} lines".format(FILE, cnt))

    _SNP_match = _SNP_change = 0
    def do_probeid_remap(self, snp, rs, options):
        if options.probeid_remap is None:
            return rs
        nrs = self._probeid_remap.get(snp)
        if nrs != None:
            self._SNP_match += 1
            if nrs != rs:
                self._SNP_change += 1
                if options.verbose:
                    printnl("SNP REMAP {0} to {1} (was {2})".format(snp, nrs, rs))
                rs = nrs
        return rs

    _RS_match = _RS_change = 0
    def do_rs_remap(self, rsx, options):
        if options.rs_remap is None:
            return rsx
        rs, sfx = rsx.split('.', 1) if rsx.find('.') != -1 else (rsx, None)
        nrs = self._rs_remap.get(rs)
        if nrs != None:
            self._RS_match += 1
            if nrs != rs:
                self._RS_change += 1
                if options.verbose:
                    printnl("RS REMAP {0} to {1}".format(nrs, rsx))
                if sfx is not None:
                    nrs += '.' + sfx
                rs = nrs
        return rs

    def do_stat_remap(self):
        printnl("SNP REMAP: {0:6d} matched {1:6d} changed".format(self._SNP_match, self._SNP_change))
        printnl("RS  REMAP: {0:6d} matched {1:6d} changed".format(self._RS_match, self._RS_change))
        printnl('')

    def do_remap(self, options):
        if options.probeid_remap is None and options.rs_remap is None:
            return
        SNP_match = SNP_change = RS_match = RS_change = 0
        for lst in self._chr_list:
            for r in lst:
                snp, rs = r[Snp.pid], r[Snp.rs]
                nrs = self._probeid_remap.get(r[Snp.pid])
                if nrs != None:
                    SNP_match += 1
                    if nrs != rs:
                        SNP_change += 1
                        if options.verbose:
                            printnl("SNP REMAP {0} to {1} (was {2})".format(snp, nrs, rs))
                        rs = nrs
### check with dan
### probeid_remap changes rs so that rs_remap does not have to ???
                nrs = self._rs_remap.get(rs)
                if nrs != None:
                    RS_match += 1
                    if nrs != rs:
                        RS_change += 1
                        if options.verbose:
                            printnl("RS REMAP {0} to {1}".format(nrs, rs))
                        rs = nrs
                r[Snp.rs] = rs
        printnl("SNP REMAP {0:6d} matched {1:6d} changed".format(SNP_match, SNP_change))
        printnl("RS  REMAP {0:6d} matched {1:6d} changed".format(RS_match, RS_change))
        printnl('')

# This code is superceded by the marker_info structure.  i.e. gmi data is in the db
    def reposition_read(self, options):
        if options.reposition is None:
            return
        FILE = options.dir + options.reposition
        File = gzopen(FILE)
        cnt = 0
        header = File.readline().split()
        if header[0:5] != self._gmi_head[0:5] or header[6] != self._gmi_head[6] or not header[5].startswith(self._gmi_head[5]):
            raise Exception("File \"{0}\": bad header/format\nread: {1}\nneed: {2}\n".format(FILE, header, self._gmi_head))

        for cnt, line, fields in read_fields(FILE, File=File, sep="\t"):
            chrm = self.chrm2int(fields[0])
            self._reposition[fields[2]] = (chrm, fields[5], fields[1])
        printnl("Read CHR/POS MAP \"{0}\" {1:6d} lines".format(FILE, cnt))

    def do_reposition(self, options):
#was see header of Snp class
#           0        1    2   3        4        5   6  7    8    9   10
#   r = [CALL, quality, snp, rs, int(chrm), int(bp), a, b, blk, off, cnt
        if options.reposition is None:
            return
        RS_match = RS_miss = RS_nors = RS_NA = RS_chr = RS_chr0 = 0
        MT = (None, None, 0.0)
        for lst in self._chr_list:
            for r in lst[:]:           # lst copy is VERY important ... we remove from it ...
                snp, rs = r[Snp.pid], r[Snp.rs]
                (chrm, pos, cm) = self._reposition.get(rs, MT)
                if options.debug > 3:
                    printn("SNP {0}, RS {1}, chr/pos {2:d}/{3:d}, nchr/npos {4}/{5}".format(snp, rs, r[Snp.chr], r[Snp.bp], chrm, pos))
# 'MT'
                if chrm == 'MT': chrm = 24
# 'Y'
                if chrm == 'Y': chrm = 24
                if chrm != None and pos != None:
                    RS_match += 1
                else:
                    RS_miss  += 1

                if rs == '---':
                    RS_nors  += 1
                    if options.debug > 3: printnl("")
                    if options.debug: printnl("affy: snp {0}, rs {1}".format(snp, rs))
                    continue
                if pos == 'NA':
                    RS_NA    += 1
                    if options.debug > 3: printnl("")
                    if options.debug: printnl("gmi:  snp {0}, rs {1} NA".format(snp, rs))
                    continue

                chrm = int(chrm)
                pos = int(pos)
                if options.debug > 3:
                    printnl("delta {0:d}".format(pos - r[Snp.bp]))

                if chrm == r[Snp.chr]:
                    r[Snp.chr], r[Snp.bp] = chrm, pos
                else:
                    if r[Snp.chr] == 0:
                        RS_chr0  += 1
                    else:
                        RS_chr   += 1
                    self._chr_list[r[Snp.chr]].remove(r)    # from chr
                    if options.debug > 3:
                        printn("{0}/{1} from {2:d}, ".format(r[Snp.pid], r[Snp.rs], r[Snp.chr]))
                    r[Snp.chr], r[Snp.bp] = chrm, pos
                    self._chr_list[r[Snp.chr]].append(r)    # to   chr
                    if options.debug > 3:
                        printnl("to {0:d}".format(r[Snp.chr]))
                r[Snp.cm] = float(cm) if cm != 'NA' else 0.0
        printnl("Reposition: {0:6d} matched, {1:6d} missed, {2:6d} ---, {3:6d} NA, {4:6d} chr move, {5:6d} chr id'ed".format(RS_match, RS_miss, RS_nors, RS_NA, RS_chr, RS_chr0))
        printnl('')

        self.sort(options)

