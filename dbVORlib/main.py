#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import sys, os, os.path, time

from config   import Config
from dbi      import DBI
from util     import NoTrace, printnl, printn, printalways, log, logs_set
from notes    import NoteMain

DBVOR_VERSION = "1.12"

class Version(object):
    NO_DIR = True
    NO_LOG = True
    NO_DB  = True
    activities = ('run',)
    def __init__(self):
        pass
    def prnt(self):
        print "dbVor version: {0}".format(DBVOR_VERSION)
    def get(self):
        return DBVOR_VERSION

class Main(object):
    TimeFmt = "%m/%d/%Y %H:%M:%S"
    def __init__(self, Task, action=None, cmd=None, sub=None):
        self.check_version()

        if action:
            config = Config(rc='.dbvorrc', debug=False,
                            usage="%prog {0} [options] ConfigFile".format(action))
        else:
            config = Config(rc='.dbvorrc', debug=False, usage="%prog [options] ConfigFile")
        dbi    = DBI(strict=True)
        self._config_  = config
        self._dbi_     = dbi

        self._task_    = Task(dbi)

        self._Time     = None      # pylint forward reference
        self._Log      = None      # pylint forward reference
        self._LogName  = None      # pylint forward reference
        self._cmd      = cmd       # pylint forward reference
        self._sub      = sub

        config.add_option("--config", action="store", type="string",
                          help="configuration file; default: None")
        config.add_option("--source", action="append", type="string", default=[],
                          help="source additional configuration file; default: None")
        config.add_option("-l", "--log", type="string", default='', help="log file")
        config.add_option("--genote", action="store_true", default=False, help="put Log into notes table")
        config.add_option("--screen_always", action="append", type="string", default=[],
                          help="always print related log info to stderror")
        dbi.add_options(config)
        fn = getattr(self._task_, 'add_options', None)
        if fn is not None:
            fn(config)

        config.add_option("--genote_date", type="string", help="genote date; used iff --genote")
        config.add_option("--genote_note", type="string", help="genote note; used iff --genote")
        config.add_option("--genote_text", action="append", type="string",
                          help="genote text; used iff --genote")
        config.add_option("--genote_summary", action="append", type="string",
                          help="genote summary (added to comment); used iff --genote")
        config.add_option("-d", "--debug", action="count", default=0, help="Debug Flag")
        config.add_option("-v", "--verbose", action="count", default=0, help="Verbose Flag")
        config.add_option("-z", "--pdb", action="store_true", default=False, help="pdb Flag")
        config.add_option("-V", "--version", action="store_true", default=False, help="Show version number")


    def hint(self):
        fn = getattr(self._task_, 'hint', None)
        if fn is None:
            print "No information available; try adding the --help flag"
        else:
            fn()

    def print_version(self):
        Version().prnt()


    def __call__(self):
        dbi     = self._dbi_

        if self._cmd is None:
            cmd = " ".join(sys.argv)
            self._cmd = cmd

        try:
            (options, args) = self._config_.parse_args()
        except Exception, error:
            print error
            return

        if options.version:
            self.print_version()
            return

        if not getattr(self._task_, 'NO_DIR', False):
            self.check_dir(options, self._config_)
        if not getattr(self._task_, 'NO_LOG', False):
            self.start_log(options, self._cmd)
            logging = True
        else:
            self.start_log_stderr(options)
            logging = False

        if options.screen_always:
            printalways(options.screen_always)

        error = options.error = 0

        if not getattr(self._task_, 'NO_DB', False):
            dbi.check_and_connect(options)

        if args:
            raise NoTrace("Illegal arguments: {0}".format(args))
        elif options.pdb:
            self.__run(dbi, options)
        else:
            try:
                self.__run(dbi, options)
            except Exception, error:
                typ, error = sys.exc_info()[:2]
                if not getattr(typ, 'NoTrace', False):
#                   printnl("{0}: {1}".format(typ, error), tag=True)
                    import traceback
                    report = traceback.format_exception(*sys.exc_info())
                    printnl('', tag=True)
                    printn(report[-2], tag=True)
                    printnl(report[-1], tag=True)
                else:
                    printnl("{0}".format(error), tag=True)
                options.error += 1
## new 3/15/16
                printnl("Aborting Database Operations.", tag=True)
                printnl("The Database will be restored to its state before the program was run.", tag=True)
                dbi.rollback()

        if logging:
            self.epi_log(error, options)

        if logging and options.genote:
            note = NoteMain(dbi)
            options.comment   = self._cmd
            options.summary   = options.genote_summary
            options.file      = self._LogName
            options.storefile = True
            options.keywords  = []
            if options.config is not None:
                options.keywords  = ['log', options.config]
            options.date      = options.genote_date
            if options.genote_note is None and options.genote_text is None:
                options.note      = options.config
                options.text      = None
                note.run(dbi, options)
            else:
                options.text      = options.genote_text    # text is prefered if both
                options.note      = options.genote_note
                note.run(dbi, options)

                options.storefile = False
                if options.config is not None:
                    options.keywords  = ['cfg', options.config]
                options.note      = options.config
                options.text      = None
                note.run(dbi, options)

    def __run(self, dbi, options):

        if not getattr(self._task_, 'NO_DB', False):
#           will most probably be recalculated later after selection/pruning.
            kId, snplen = dbi.select_one("select max(length(id)), max(length(marker)) from markers")
            mId, pedlen, perlen = dbi.select_one("select max(length(id)), max(length(pedigree)), max(length(person)) from members")
            log.sizes(marker_length=kId, snp_name_length=snplen)
            log.sizes(member_length=mId, pedigree_name_length=pedlen, person_name_length=perlen)

        activities = getattr(self._task_, 'activities', ('prolog', 'run', 'epilog'))
        for action in activities:
            fn = getattr(self._task_, action, None)
            if fn is not None:
                ret = fn(dbi, options)
                if options.debug and options.error > 0:
                    printnl("{0}: CUMULATIVE ERROR COUNT {1:d}".format(action, options.error), tag=True)
                if ret is False:
                    return

# iff there is a commit option ... do it
        if getattr(options, 'commit', None):
            if getattr(options, 'error', None) or getattr(options, 'rollback', None):
                printnl("Aborting Database Operations.", tag=True)
                printnl("The Database will be restored to its state before the program was run.", tag=True)
                dbi.rollback()
            else:
                printnl("Committing Changes to the Database.", tag=True)
                dbi.commit()
        elif dbi.db_expect_commit():
            printnl("No Database Commit Requested.", tag=True)
        dbi.close()

    def check_version(self):
        ver = sys.version_info
        if ver[0] <= 2 and ver[0] == 2 and ver[1] <= 5:
            print "ERROR: Python version must be at least 2.6.  Current version is:"
            print sys.version
            sys.exit(1)
        if ver[0] >= 3:
            print "ERROR: Python version 3.x is not supported yet."
            sys.exit(1)

    def check_dir(self, options, parser):
        if options.dir is None:
            print
            print "ERROR: No data directory specified (value {0})".format(options.dir)
            print
            parser.help()
            sys.exit(1)
        if not options.dir.endswith("/"): options.dir += "/"
        options.dir = os.path.expanduser(options.dir)
        if not os.path.isdir(options.dir):
            print
            print "ERROR: The data repository \"{0}\" is not a directory".format(options.dir)
            print
            parser.help()
            sys.exit(1)

    def log_open(self, options):
        directory, logname, expr = options.dir, options.log, getattr(options, 'cfgroot', None)
        if logname != '':
            FILE = directory + logname.replace('%', self._sub if self._sub else '')
            options.logroot = FILE
            self._LogName = FILE
            return file(FILE, "w")
        prefix = expr if expr is not None else 'Log'
#       z = time.strftime("_%Y%m%d_%H:%M.log.txt")    # sigh ...
        z = time.strftime("_%Y%m%d_%H_%M_%S")
        FILE = directory + prefix + z
        if self._sub:
            FILE += self._sub
        options.logroot = FILE
        FILE += ".log.txt"
        self._LogName = FILE
        return file(FILE, "w")

    def start_log(self, options, cmd):
#       self._Log = self.log_open(options.dir, options.log, getattr(options, 'experiment', None))
        self._Log = self.log_open(options)
        logs_set(None, self._Log)

        self._Time = time.time()
        printnl("START {0}, dbVOR version {1}\n".format(time.strftime(self.TimeFmt), DBVOR_VERSION))
        printnl("cmd: {0}".format(cmd))
        printnl("cwd: {0}\n".format(os.getcwd()))
        self._config_.log_options(options)

    def start_log_stderr(self, options):
        logs_set(None)

    def epi_log(self, error, options):
        if error == options.error:
            printnl("\nSUCCESS: dbVOR version {0}".format(DBVOR_VERSION), tag=True)
        else:
            printnl("\nFAILURE: {0:d} Errors, dbVOR version {1}".format(options.error, DBVOR_VERSION), tag=True)
        printnl("\nEND {0}, DURATION {1:.3f} sec".format(time.strftime(self.TimeFmt), time.time()-self._Time), tag=True)
        self._Log.close()
        logs_set()
