#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from   operator import itemgetter

from   dbVORlib.marker   import Marker
from   dbVORlib.util     import printnl, printn, log

class MarkerSelect(Marker):
    def __init__(self, db, snp, selected_members):
        super(MarkerSelect, self).__init__(db)
        self._snp_               = snp
        self._tech_chr_list      = {}
        self._selected_members_  = selected_members

        self._marker_map         = {}
        self._marker2info        = {}
        self._marker_ids         = []
        self._marker_constraint  = {}
        self._marker_stat        = {}
        self._marker_cnt         = {}

        self._technology_map     = {}
        self._technology_name2id = {}
        self._technology_ids     = []

        self._experiment_map     = {}
        self._experiment_name2id = {}
        self._experiment_exclusion = {}
        self._experiment_ids     = []

        self._TechIn             = {}
        self._TechOut            = {}
        self._ExprIn             = {}
        self._ExprOut            = {}

    def add_options(self, config):
        config.add_option("--marker", action="append", default=[],
                          help="markers to process")
        config.add_option("--chromosome", action="append", default=[],
                          help="chromosome to process")
        config.add_option("--chrrange", action="append", default=[],
                          help="chromosome bp range to process")
        config.add_option("--chrCMrange", action="append", default=[],
                          help="chromosome cM range to process")

        config.add_option("--technologies", action="append", default= [],
                          help="technologies to process")
        config.add_option("--experiments", action="append", default= [],
                          help="experiments to process")

    def __iter__(self):
        return self._marker_ids.__iter__()

    def sortedlist(self):
        return self.__iter__()

    def len(self):
        return len(self._marker_map)

    def name(self, mId=None, rec=None):
        return self._snp_.Ops().name(rec if rec else self._marker_map[mId])

    def chrm(self, mId=None, rec=None):
        return self._snp_.Ops().chrm(rec if rec else self._marker_map[mId])

    def alleles(self, mId=None, rec=None):
        return self._snp_.Ops().alleles(rec if rec else self._marker_map[mId])

    def snp2id(self, rec=None, mId=None):
        return self._snp_.Ops().snp2id(rec if rec else self._marker_map[mId])

    def technology_name(self, tech):
        return self._technology_map[tech]

    def tech_ids(self):
        return self._technology_ids

    def experiment_name(self, expr):
        return self._experiment_map[expr][0]

    def expr_ids(self):
        return self._experiment_ids

    def chrm_list(self, tech='fin'):
        return self._tech_chr_list.get(tech, [])

    def chrm_keys(self):
        return self._tech_chr_list.keys()

    def exclusion(self):
        return self._experiment_exclusion

    _snp_sort_byname = False
    def snp_sort(self, sId):
        return float(self.name(sId)[2:])       # i.e. sort by snp name, numerically

    def prune(self, dbi, options):
        MT  =        [0, 0, 0, 0, 0, False]
        chr_list   = self.chrm_mt()
        unmap = []
        log.push("marker_prune")

        for marker_id, snp in self._marker_map.iteritems():
            if marker_id not in self._marker_cnt:
                printnl("pruning markers: dropping {0} ({1:d}) -- not sampled".format(self.name(rec=snp), self.snp2id(rec=snp)) )
                unmap.append(marker_id)
            elif self._marker_cnt[marker_id] > 0:
                chr_list[snp[0]].append(snp)
            else:
                if options.marker or options.chromosome or options.chrrange or options.chrCMrange:
                    printnl("pruning markers: dropping {0} ({1:d}) -- not used".format(self.name(rec=snp), self.snp2id(rec=snp)) )
                unmap.append(marker_id)

        for marker_id in unmap:
            del self._marker_map[marker_id]

        log.beginfooter()
        if len(self._marker_ids) == len(self._marker_map):
            printnl("pruning markers: none pruned - {0:d} entries available".format(len(self._marker_map)) )
        else:
            printnl("pruning markers: had {0:d} entries pruned to {1:d} entries".format(len(self._marker_ids), len(self._marker_map)) )
        log.pop("marker_prune")

        self.Chr_list_sort(chr_list, options)
        self._tech_chr_list['fin'] = chr_list

        if self._snp_sort_byname:
            self._marker_ids = self._marker_map.keys()
            self._marker_ids.sort(key=self.snp_sort)
        else:
            self._marker_ids = [self.snp2id(snp) for snp in sorted(self._marker_map.itervalues(), key=itemgetter(0, 1))]
        self.snp_name_length()

    def snp_name_length(self):
        snp_name_length = reduce(max,
                                 (len(self.name(rec=x)) for x in self._marker_map.itervalues() ),
                                 log.snp_name_length)
        log.sizes(snp_name_length=snp_name_length)

    def filter(self, memrec, mkrrec, tech, expr, active, options):
# 227 sec (Hapmap)
#       0: in db; 1: active==0; 2: unused; 3: constraint; 4: ~tech/expr in; 5: unique; 6: allele count;
        marker_id = mkrrec[-2]
        if marker_id not in self._marker_cnt:
            self._marker_cnt[marker_id] = 0
        if options.stat:
            marker_stat = self._marker_stat.setdefault(marker_id, [0, 0, 0, 0, 0])
            marker_stat[0] += 1

        if options.technologies:
            if tech not in self._technology_ids:
                if options.verbose and self._TechOut.get(tech, 0) == 0:
                    printnl("skipping technology {0} ({1})".format(self._technology_map[tech], tech), tag='TECHFILTER')
                self._TechOut[tech] = self._TechOut.get(tech, 0) + 1
                member_id = memrec[0]
                if options.debug:
                    printnl("skipping technology {0} ({1}) member {2}:{3} ({4:d}) marker {5} ({6:d})".format(self._technology_map[tech], tech, memrec[1], memrec[2], member_id, self.name(rec=mkrrec), marker_id), tag='TECHFILTER')
                if options.stat:    marker_stat[4] += 1
                return True
            else:
                self._TechIn[tech] = self._TechIn.get(tech, 0) + 1

        if options.experiments:
            if expr not in self._experiment_ids:
                if options.verbose and self._ExprOut.get(expr, 0) == 0:
                    printnl("skipping experiment {0} ({1})".format(self._experiment_map[expr][0], expr), tag='EXPRFILTER')
                self._ExprOut[expr] = self._ExprOut.get(expr, 0) + 1
                member_id = memrec[0]
                if options.debug:
                    printnl("skipping experiment {0} ({1}) member {2}:{3} ({4:d}) marker {5} ({6:d})".format(self._experiment_map[expr][0], expr, memrec[1], memrec[2], member_id, self.name(rec=mkrrec), marker_id), tag='EXPRFILTER')
                if options.stat:    marker_stat[4] += 1
                return True
            else:
                self._ExprIn[expr] = self._ExprIn.get(expr, 0) + 1

        if active == 0 and not options.use_inactive:
            member_id = memrec[0]
            printnl("skipping inactive allele: tech {0} ({1:d}), expr {2} ({3:d}), marker {4} member {5}:{6} ({7:d})".format(self._technology_map[tech], tech, self._experiment_map[expr], expr, marker_id, memrec[1], memrec[2], member_id), tag="inactive")
            if options.stat:    marker_stat[1] += 1
            return True

        if marker_id not in self._marker_map:
            member_id =  memrec[0]
            printnl("skipping: marker {0} member {1}:{2} ({3:d})".format(marker_id, memrec[1], memrec[2], member_id), tag='notrequested')
            if options.stat:    marker_stat[2] += 1
            return True

        if len(self._marker_constraint) > 0:
            mall  = self._marker_constraint.get(marker_id, {})
            if len(mall) > 0:
                mtech = mall.get('technology', None)
                if mtech:
                    if tech not in mtech:
                        member_id = memrec[0]
                        if options.verbose:
                            printnl("skipping: marker {0}.technology.{1} for member {2}:{3}".format(self.name(rec=mkrrec), self._technology_map[tech], memrec[1], memrec[2]), tag='techfilter')
                        elif options.debug:
                            printnl("skipping: marker {0}({1}) technology {2}({3}) member {4}:{5} ({6:d})".format(self.name(rec=mkrrec), marker_id, self._technology_map[tech], tech, memrec[1], memrec[2], member_id), tag='techfilter')
                        if options.stat:    marker_stat[3] += 1
                        return True
                mexpr = mall.get('experiment', None)
                if mexpr:
                    if expr not in mexpr:
                        member_id = memrec[0]
                        if options.verbose:
                            printnl("skipping: marker {0}.experiment.{1} for member {2}:{3}".format(self.name(rec=mkrrec), self._experiment_map[expr][0], memrec[1], memrec[2]), tag='exprfilter')
                        elif options.debug:
                            printnl("skipping: marker {0}({1}) experiment {2}({3}) member {4}:{5} ({6:d})".format(self.name(rec=mkrrec), marker_id, self._experiment_map[expr][0], expr, memrec[1], memrec[2], member_id), tag='exprfilter')
                        if options.stat:    marker_stat[3] += 1
                        return True

        exclusions = self._experiment_exclusion.get(expr, None)
        if exclusions:
            if self.Check_exclusion(memrec, mkrrec, expr, exclusions, options):
                return True

        self._marker_cnt[marker_id] = self._marker_cnt[marker_id] + 1
        return False

    def exclusion_stat(self, options):
# make this a table
        MT = []
        if options.experiments:
            printnl("Experiment exclusion statistics")
            printnl("type\tcount\tmember\tchromo\tmarker\texperiment\ttag")

            for expr, exclusions in self._experiment_exclusion.iteritems():
                experiment = self.experiment_name(expr)

                choice = exclusions.get('exclude_marker', {})
                for marker_id, stuff in choice.iteritems():
                    if stuff[0] > 0 or options.debug:
                        name = self.name(marker_id) if self._marker_map.get(marker_id) else 'unknown'
                        printnl("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}".format('marker', stuff[0], 'ALL', 'ALL', name, experiment, stuff[1]))

                if 'exclude_marker' in exclusions:
                    exclusions.pop('exclude_marker')

                for member_id, rec in exclusions.iteritems():
                    pp = ':'.join(self._selected_members_.name(member_id))
                    if 'person' in rec:
                        stuff = rec['person']
                        if stuff[0] > 0 or options.debug:
                            printnl("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}".format('person', stuff[0], pp, 'ALL', 'ALL', experiment, stuff[1]))

                    if 'chromosome' in rec:
                        for chrm, stuff in rec['chromosome'].iteritems():
                            if stuff[0] > 0 or options.debug:
                                printnl("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}".format('chromo', stuff[0], pp, chrm, 'ALL', experiment, stuff[1]))

                    if 'chrrange' in rec:
                        for chrm, stuff in rec['chrrange'].iteritems():
                            if stuff[0] > 0 or options.debug:
                                printnl("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}".format('chromo', stuff[0], pp, chrm, 'RANGE', experiment, stuff[1]))

                    if 'genotype' in rec:
                        for marker_id, stuff in rec['genotype'].iteritems():
                            if stuff[0] > 0 or options.debug:
                                printnl("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}".format('genotype', stuff[0], pp, 'ALL', marker_id, experiment, stuff[1]))

    def filter_stat(self, options):
        if options.technologies:
            printnl("Technology statistics")
            for k in sorted(self._TechIn.iterkeys()):
                printnl("included technology {0} (#{1:d}) for {2:d} samples".format(self._technology_map[k], k, self._TechIn[k]))
            for k in sorted(self._TechOut.iterkeys()):
                printnl("excluded technology {0} (#{1:d}) for {2:d} samples".format(self._technology_map[k], k, self._TechOut[k]))

        if options.experiments:
            printnl("Experiment statistics")
            for k in sorted(self._ExprIn.iterkeys()):
                printnl("included experiment {0} (#{1:d}) for {2:d} samples".format(self._experiment_map[k], k, self._ExprIn[k]))
            for k in sorted(self._ExprOut.iterkeys()):
                printnl("excluded experiment {0} (#{1:d}) for {2:d} samples".format(self._experiment_map[k], k, self._ExprOut[k]))


    def marker_stat(self, allele_stat, options):
        if not options.stat:
            return
        if not options.debug:
            return
        cnt = 0
        fmtl = self.fmtlen('marker', 'snp name')
        log.push("markerstat", newrecord=True)
        printnl("MARKER statistics")
# 0: in db; 1: active==0; 2: unused; 3: constraint; 4: ~tech/expr; 5: unique
        hfs1 = "{0:{fld[0]}} {1:{fld[1]}} {2:4} {3:8} {4:6} {5:10} {6:10} {7:8} {8}"
        hfs2 = "{0:{fld[0]}} {1:{fld[1]}} {2:4} {3:8} {4:6} {5:10} {6:10} {7:8}"
        printnl(hfs1.format('marker', 'snp name', 'db', 'inactive', 'unused', 'constraint', '~tech/expr', 'observed', 'allele count', fld=fmtl))
        log.endheader()
        for mkr in self:
            tpl = self._marker_stat.get(mkr, None)
            apl = allele_stat.get(mkr, None)
            cnt += 1
            if tpl and apl:
                if apl[1] > 0 or options.debug:
                    log.newrecord()
                    printn(hfs2.format(mkr, self.name(mkr), tpl[0], tpl[1], tpl[2], tpl[3], tpl[4], apl[1]-apl[4].get('00', 0), fld=fmtl) )
                    for k, v in sorted(apl[4].iteritems(), key=itemgetter(0)):
                        printn("{0}: {1:4d} ".format(k, v))
                    printnl('')
            else:
                if options.debug:
                    log.newrecord()
                    printn("{0:{fld[0]}} {1:{fld[1]}}".format(mkr, self.name(mkr), fld=fmtl))
        log.beginfooter(tag="markerstat")
        printnl("{0:d} snps listed above".format(cnt), tag="always")
        log.pop("markerstat")

    def run(self, dbi, options):
        self.Technology(dbi, options)
        self.Experiment(dbi, options)
        self.Lookup_markers(dbi, options)
        chrms = self.Chrrange2chrms(options)

#       import time
#       Time = time.time()

#       force all markers to the most current name version
        options.markers = self._snp_.Marker_dealias(options.markers, options)

        chr_list, named_marker = self._snp_.Marker_info(self._technology_ids, self._experiment_ids,
                                                        options.markers, chrms, options)
        self.Chr_list_sort(chr_list, options)

        self.Filter_markers(dbi, chr_list, named_marker, options)

#       We have the set of markers (and details) self._marker2info that are requested.
#       But where are they: genotypes or gwas and for gwas what tech and do we even want them.
        self._snp_.Marker_classify(self._tech_chr_list,
                                   self._technology_ids, self._experiment_ids,
                                   self._marker2info, options)

#       Sort chr list on each tech
        for key, chr_list in self._tech_chr_list.iteritems():
            self.Chr_list_sort(chr_list, options)

        self.Set_marker_ids(dbi, options)

        self._snp_.free()
        self._marker2info.clear()

    def run_exclusion(self, dbi, options):
        self.Experiment_exclusion(dbi, options)

    def Technology(self, dbi, options):
        self._technology_map     = dict(dbi.select_all("select id, technology from technologies"))
        self._technology_name2id = dict((v, k) for (k, v) in self._technology_map.iteritems())

        log.push("technology_lookup")
        self._technology_ids = []
        for name in options.technologies:
            constraint = name.split('|')
            tech = self._technology_name2id.get(constraint[0], None)
            if tech is None:
                raise Exception("invalid technology name \"{0}\"; choose from {1}".format(name, sorted(self._technology_name2id.iterkeys() )))

            if tech not in self._technology_ids:
                self._technology_ids.append(tech)

        log.pop('technology_lookup')

    def Experiment(self, dbi, options):
        self._experiment_map     = dict((i[0], i[1:]) for i in dbi.select_all("select id, filename, projectid, techid from experiments"))
        self._experiment_name2id = dict((v[0], k) for (k, v) in self._experiment_map.iteritems())

        log.push("experiment_lookup")
        self._experiment_ids = []
        for name in options.experiments:
            constraint = name.split('|')
            expr = self._experiment_name2id.get(constraint[0], None)
            if expr is None:
                raise Exception("invalid experiment name \"{0}\"; choose from {1}".format(name, sorted(self._experiment_name2id.iterkeys() )))

            if expr not in self._experiment_ids:
                self._experiment_ids.append(expr)
        log.pop('experiment_lookup')

    def Lookup_markers(self, dbi, options):
        options.markers = []
        self._marker_ids = []
        if not options.marker or options.no_markers:
            return

        log.push('marker_lookup')
        for marker in options.marker:
            constraint = marker.split('|')
            error = options.error
            marker_id = self.marker(constraint[0], options)
            if error != options.error:
                printnl("error converting marker {0} to marker id".format(constraint[0]))
                continue
            if marker_id not in self._marker_ids:
                self._marker_ids.append(marker_id)
                options.markers.append(constraint[0])

            if len(constraint) > 2:
                if constraint[1] == 'experiment':
                    for name in constraint[2:]:
                        expr = self._experiment_name2id.get(name, None)
                        if expr is None:
                            printnl("unknown experiment name \"{0}\" choose from {1}".format(name, [k for k in self._experiment_name2id.iterkeys()]))
                            continue
                        self._marker_constraint.setdefault(marker_id, {}).setdefault('experiment', []).append(expr)
                elif constraint[1] == 'technology':
                    for name in constraint[2:]:
                        tech = self._technology_name2id.get(name, None)
                        if tech is None:
                            printnl("unknown technology name \"{0}\" choose from {1}".format(name, [k for k in self._technology_name2id.iterkeys()]))
                            continue
                        self._marker_constraint.setdefault(marker_id, {}).setdefault('technology', []).append(tech)
                else:
                    printnl("bad marker constraint \"{0}\" should be \"experiment\" or \"technology\"".format(constraint[1]))
                    continue
        log.pop('marker_lookup')

    def Check_exclusion(self, memrec, mkrrec, techxpr, exclusions, options):
        marker_id = mkrrec[-2]
        choice = exclusions.get('exclude_marker', None)
        if choice:
            if marker_id in choice:
                if options.verbose and choice[marker_id][0] == 0:
                    printnl("exclude_marker: member {2[1]}:{2[2]} (#{2[0]}), [marker {0} (#{1})]".format(self.name(marker_id), marker_id, memrec), tag="exclude_marker")
                choice[marker_id][0] += 1
                return True

        if memrec[0] in exclusions:
            rec = exclusions[memrec[0]]
            if 'person' in rec:
                if options.verbose and rec['person'][0] == 0:
                    printnl("exclude_person: member {2[1]}:{2[2]} (#{2[0]}), [marker {0} (#{1})]".format(self.name(marker_id), marker_id, memrec), tag="exclude_person")
                rec['person'][0] += 1
                return True

            if 'chromosome' in rec and rec['chromosome'].get(mkrrec[0], None):
#                pdb.set_trace()
                if options.verbose and rec['chromosome'][mkrrec[0]][0] == 0:

                    printnl("exclude_chromsome: member {2[1]}:{2[2]} (#{2[0]}), [marker {0} [chr {3[0]}] (#{1})]".format(self.name(marker_id), marker_id, memrec, mkrrec), tag="exclude_person")
                rec['chromosome'][mkrrec[0]][0] += 1
                return True

            if 'chrrange' in rec and rec['chrrange'].get(mkrrec[0], None):
                f = rec['chrrange'][mkrrec[0]]
                if f[2] <= mkrrec[1] <= f[3]:
                    if options.verbose and rec['chrrange'][mkrrec[0]][0] == 0:
                        printnl("exclude_chrrange: member {2[1]}:{2[2]} (#{2[0]}), [marker {0}@{3} [{4}]]".format(self.name(marker_id), marker_id, memrec, mkrrec[1], f[1]), tag="exclude_person")
                    rec['chrrange'][mkrrec[0]][0] += 1
                    return True

            if 'genotype' in rec and rec['genotype'].get(marker_id, None):
#                pdb.set_trace()
                if options.verbose and rec['genotype'][marker_id][0] == 0:
                    printnl("exclude_genotype: member {2[1]}:{2[2]} (#{2[0]}), [marker {0} (#{1})]".format(self.name(marker_id), marker_id, memrec), tag="exclude_person")
                rec['genotype'][marker_id][0] += 1
                return True

        return False

    def Experiment_exclusion(self, dbi, options):
        log.push("exclusion")
        for name in options.experiments:
            constraint = name.split('|')
            if len(constraint) > 1:
                expr = self._experiment_name2id[constraint[0]]
                self.Exclusion(expr, constraint, self._experiment_exclusion, options)
        log.pop('exclusion')

    def pr_context(self, string):
        printnl(string)

    def Exclusion(self, k_id, constraint, cache, options):
        if len(constraint) > 2:
            if constraint[1] == 'exclude_marker':
                for name in constraint[2:]:
                    mkr_chr = name.split(':')
                    mid = self.marker(mkr_chr[0], options)
                    if mid is not None:
                        cache.setdefault(k_id, {}).setdefault('exclude_marker', {})[mid] = [0, name]
                    else:
                        printnl("ERROR: exclude_marker: marker not found {0}".format(name))
            elif constraint[1] == 'exclude_person':
                for name in constraint[2:]:
                    pp = name.split(':')
                    if len(pp) == 1:
                        pedigree, person = 'UNUSED', pp[0]
                    else:
                        pedigree, person = pp
                    mid = self._selected_members_.member(pedigree, person, options, self.pr_context)
                    if mid is not None:
                        cache.setdefault(k_id, {}).setdefault(mid, {}).setdefault('person', [0, name])
                    else:
                        printnl("exclude_person: person not found {0}".format(name))
            elif constraint[1] == 'exclude_genotype':
                for name in constraint[2:]:
                    pp = name.split(':')
                    if len(pp) == 2:
                        pedigree, person = 'UNUSED', pp[0]
                    else:
                        pedigree, person = pp[0:2]
                    mid = self._selected_members_.member(pedigree, person, options, self.pr_context)
                    mkid = self.marker(pp[2], options)
                    if mid is not None:
                        if mkid is not None:
                            cache.setdefault(k_id, {}).setdefault(mid, {}).setdefault('genotype', {})[mkid] = [0, name]
                        else:
                            printnl("exclude_genotype: marker not found {0}".format(pp[2]))
                    else:
                        printnl("exclude_genotype: person not found {0}".format(name))
            elif constraint[1] == 'exclude_chromosome':
                for name in constraint[2:]:
                    pp = name.split(':')
                    if len(pp) == 2:
                        pedigree, person = 'UNUSED', pp[0]
                    else:
                        pedigree, person = pp[0:2]
                    mid = self._selected_members_.member(pedigree, person, options, self.pr_context)
                    chrm = self.chrm2int(pp[2])
                    if mid is not None:
                        if chrm is not None:
                            cache.setdefault(k_id, {}).setdefault(mid, {}).setdefault('chromosome', {})[chrm] = [0, name]
                        else:
                            printnl("exclude_chromosome: marker not found {0}".format(pp[2]))
                    else:
                        printnl("exclude_chromosome: person not found {0}".format(name))
            elif constraint[1] == 'exclude_chrrange':
                for name in constraint[2:]:
                    pp = name.split(':')
                    if len(pp) < 3:
                        printnl("exclude_chrrange: bad format {0} should be \"pedigree:person:chromosome:start..end\"".format(name))
                        return
                    if len(pp) == 3:
                        pedigree, person = 'UNUSED', pp[0]
                    else:
                        pedigree, person = pp[0:2]
                    mid = self._selected_members_.member(pedigree, person, options, self.pr_context)
                    f = pp[3].split('..')
                    if len(f) != 2:
                        printnl("exclude_chrrange: bad range {0}".format(f))
                        return
                    chrm = self.chrm2int(pp[2])
                    if mid is not None:
                        if chrm is not None:
                            cache.setdefault(k_id, {}).setdefault(mid, {}).setdefault('chrrange', {})[chrm] = [0, name, (int(f[0]) if f[0] else 0), (int(f[1]) if f[1] else 1e12)]
                        else:
                            printnl("exclude_chrrange: marker not found {0}".format(pp[2]))
                    else:
                        printnl("exclude_chrrange: person not found {0}".format(name))
            else:
                printnl("bad constraint \"{0}\" should be \"exclude_person\", \"exclude_chromosome\", \"exclude_marker\", or \"exclude_genotype\"".format(constraint[1]))


    def Chr_list_sort(self, chr_list, options):
        def chr_bp_key(a, b):                     # sort on bp
            return cmp(a[1], b[1])

        if options.debug >= 2:
            printnl("CHROMOSOMES")
            printnl("{0:>3} {1:>6}".format('chr', 'snps'))
        for chrm, lst in enumerate(chr_list):
            lst.sort(chr_bp_key)           # sort
            if options.debug >= 2:
                printnl("{0:3d} {1:6d}".format(chrm, len(lst)))

    def Filter_markers(self, dbi, chr_list_in, named_marker, options):
        chr_list   = self.chrm_mt()

        if options.marker or options.chromosome or options.chrrange or options.chrCMrange or options.no_markers:
            if options.chromosome:
                for chrm in options.chromosome:
                    chrm = self.chrm2int(chrm)
                    chr_list[chrm] = chr_list_in[chrm]

            if options.chrrange:
                for chrrange in options.chrrange:
                    self.Chrrange(chrrange, chr_list, chr_list_in, options)

            if options.chrCMrange:
                for chrCMrange in options.chrCMrange:
                    self.ChrCMrange(chrCMrange, chr_list, chr_list_in, options)

            if options.marker or options.no_markers:
                log.push('markermerge')
                for marker in options.markers:
                    if marker in named_marker:
                        rec = named_marker[marker]
                    else:
                        rec = (0, 0, 0, 0, 0, -1, marker) # chr/pos/avb/male/female/id/marker
                    chr_list[rec[0]].append(rec)
                log.pop('markermerge')

            self.Chr_list_sort(chr_list, options)
        else:  # take everything
            chr_list = chr_list_in

        self._marker_map = {}
        for chrm, snps in enumerate(chr_list):
            for snp in snps:
                self._marker2info[snp[-1]] = snp

    def Set_marker_ids(self, dbi, options):
        chr_list = self.chrm_mt()
        self._marker_map = {}
        for key, body in self._tech_chr_list.iteritems():
            for chrm, snps in enumerate(body):
                for snp in snps:
#                   Two issues: 1. in how many tech's is this particular id
                    if snp[-2] in self._marker_map:    # dup entry
                        printnl("Common marker {0} ({1})".format(snp[-1], snp[-2]), tag=True)
                    else:
#                   Two issues: 2. was marker moved to a different chromosome on a different build
#                     snp[0] (chr) comes from snpblocks ... old home
#                     chrm         comes from markerinfo ... new home
                        if snp[0] != chrm:
                            if options.verbose or True:
                                printnl("{0} belongs on chr {1}".format(snp, chrm))
                            snp = list(snp)
                            snp[0] = chrm
                        chr_list[chrm].append(snp)

                        self._marker_map[snp[-2]] = snp

        self.Chr_list_sort(chr_list, options)
        self._tech_chr_list['fin'] = chr_list
        for mkr, snp in self._marker_map.iteritems():
            self.cacheload(self.name(rec=snp), mkr)

        if self._snp_sort_byname:
            self._marker_ids = self._marker_map.keys()
            self._marker_ids.sort(key=self.snp_sort)
        else:
            self._marker_ids = [self.snp2id(snp) for snp in sorted(self._marker_map.itervalues(), key=itemgetter(0, 1))]

        self.snp_name_length()

        if options.debug >= 2:
            log.push("snp_list")
            printnl("SNPS")
            printnl("{0:>6} {1}".format('id', 'snp'))
            log.endheader()
            for t in self:
                printnl("{0:6d} {1}".format(t, self.name(t)))
            log.pop("snp_list")

    def Chrrange2chrms(self, options):
        chrms = {}

        for chrm in options.chromosome:
            chrm = self.chrm2int(chrm)
            if chrm not in chrms:
                chrms[chrm] = True

        for chrrange in options.chrrange:
            f = chrrange.split(':')
            if len(f) != 2:
                printnl("chrrange \"{0}\": bad chromosome syntax".format(chrrange))
                continue
            chrm = self.chrm2int(f[0])
            if chrm not in chrms:
                chrms[chrm] = True

        for chrCMrange in options.chrCMrange:
            f = chrCMrange.split(':')
            if len(f) != 2:
                printnl("chrCMrange \"{0}\": bad chromosome syntax".format(chrCMrange))
                continue
            chrm = self.chrm2int(f[0])
            if chrm not in chrms:
                chrms[chrm] = True
        return chrms.keys()

    def Chrrange(self, chrrange, chr_list, chr_lookup_list, options):
        bp = 1
        f = chrrange.split(':')
        if len(f) != 2:
            printnl("chrrange \"{0}\": bad chromosome syntax".format(chrrange))
            return
        chrm, rest = f
        f = rest.split('..')
        if len(f) != 2:
            printnl("chrrange \"{0}\": bad range syntax".format(rest))
            return
        start = int(f[0]) if f[0] else 0
        end   = int(f[1]) if f[1] else 1000000000000
        chrm = self.chrm2int(chrm)
        lst = chr_lookup_list[chrm]

        if lst and (lst[-1][bp] >= start):
            i = None                                 # pylint does not understand scope rules for loop variables
            for i in range(0, len(lst)):
                if lst[i][bp] >= start: break
            for i in range(i, len(lst)):
                rec = lst[i]
                if rec[bp] is None: continue
                if rec[bp] > end: break
                if rec in chr_list[chrm]:
                    printnl("chrrange: duplicate request for {0} {1}".format(self.name(rec=rec), rec))
                    continue
                if options.verbose:
                    printnl("chr {0:d} range {1} selecting {2} {3}".format(chrm, rest, self.name(rec=rec), rec))
                chr_list[chrm].append(rec)

    def ChrCMrange(self, chrCMrange, chr_list, chr_lookup_list, options):
        cm = 1
        f = chrCMrange.split(':')
        if len(f) != 2:
            printnl("chrCMrange \"{0}\": bad chromosome syntax".format(chrCMrange))
            return
        chrm, rest = f
        f = rest.split('..')
        if len(f) != 2:
            printnl("chrCMrange \"{0}\": bad range syntax".format(rest))
            return
        start = float(f[0]) if f[0] else 0.0
        end   = float(f[1]) if f[1] else 1000000000000.0
        chrm = self.chrm2int(chrm)
        lst = chr_lookup_list[chrm]

        if lst and (lst[-1][cm] >= start):
            i = None                                 # pylint does not understand scope rules for loop variables
            for i in range(0, len(lst)):
                if lst[i][cm] >= start: break
            for i in range(i, len(lst)):
                rec = lst[i]
                if rec[cm] is None: continue
                if rec[cm] > end: break
                if rec in chr_list[chrm]:
                    printnl("chrCMrange: duplicate request for {0} {1}".format(self.name(rec=rec), rec))
                    continue
                if options.verbose:
                    printnl("chr {0:d} range {1} selecting {2} {3}".format(chrm, rest, self.name(rec=rec), rec))
                chr_list[chrm].append(rec)

