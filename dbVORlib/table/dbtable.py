#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import sys, os.path

from dbVORlib.validate import Validate
from dbVORlib.util     import NoTrace, printnl, printn, log, gzopen, read_fields


#class DBTable(ValidateProject):    #    ????
class DBTable(Validate):
    ORIGIN = 1

    def __init__(self, db):
        super(DBTable, self).__init__(db)
        self._im          = None
        self._data        = []
        self._old_line_no = 0
        self._line_no     = 0
        self._this_line   = 0
        self._line        = None
        self._options     = None

        self._fields   = None
        self._name     = None

    def add_options(self, config):
        super(DBTable, self).add_options(config)
        config.change_default("config", None)    # can operate w/o config file

        config.add_option("--sep", "--file_separator", type="string",
                          help="prepDB experiment file internal separator; default white space")
        config.add_option("--input", action="store", type="string",
                          help="input file needed for xlong fmt; default: None")

        config.add_option("--commit", action="store_true", default=False,
                          help="commit changes to database")
        config.add_option("--update", action="store_true", default=False,
                          help="update op vs delete/insert")
        config.add_option("--rollback", action="store_true", default=False,
                          help="force all database operations to be aborted")
        config.add_option("--replace", action="store_true", default=False,
                          help="replace existing entries")
        config.add_option("--delete", action="store_true", default=False,
                          help="delete existing entries")
        config.add_option("--compare", action="store_true", default=False,
                          help="compare existing entries")

    def prolog(self, dbi, options):
        if not getattr(self, 'NO_INPUT', False):
            if options.input is None:
                hlp = getattr(self, "help", None)
                if hlp:
                    hlp()
                else:
                    raise NoTrace("\nERROR: An input file must be specified prefaced with --input=")
        if options.commit and options.compare:
            raise NoTrace("\nYou can not specify both --compare and --commit.")
        self.validate_project(options, create=True)
        self._options = options
        if options.error:
            return False
        else:
            return True

    def insert_many(self, cmd, fmt, options, cnt=100):
        if options.error == 0:
            if options.commit:
                self._im = self._dbi_.insert_many(cmd, fmt, cnt)
            return True
        else:
            return False

#   why not
    def epilog(self, dbi, options):
        return True

    tags = ('verbose', 'update', 'compare', 'misc')
    def run(self, dbi, options):
        FILE = options.input
        if FILE == '-':
            File = sys.stdin
        else:
            if not os.path.isfile(FILE):
                FILE = options.dir + options.input
                if not os.path.isfile(FILE):
                    raise NoTrace("ERROR: Can not find input file \"{0}\"".format(FILE))
            File = gzopen(FILE)

        log.push(self.tags)
        self._line_no = 0
        doHeader = getattr(self, 'header', None)
        if doHeader:
            doHeader(File, dbi, options)
            log.endheader()

        for self._line_no, self._line, fields in read_fields(FILE, File=File, cnt=self._line_no, sep=options.sep, field_cnt=self._fields):

            log.begingroup()
            if (options.verbose or options.debug) and not (options.compare or options.commit):
                log.newrecord()
                printnl("{0:3d}: {1}".format(self._line_no, fields))
            self.do_row(fields, dbi, options)
            log.endgroup()
        log.pop(self.tags)

        File.close()

        return True

    def do_row(self, fields, dbi, options):    # pylint
        raise Exception("do_row: Don't call me")
    def select(self, fields, options):         # pylint
        raise Exception("select: Don't call me")
    def delete(self, rec, options):            # pylint
        raise Exception("delete: Don't call me")
    def insert(self, fields, options):         # pylint
        raise Exception("insert: Don't call me")
    def update(self, fields, db, options):     # pylint
        raise Exception("update: Don't call me")
    def cmp(self, rec, fields, options):       # pylint
        raise Exception("cmp: Don't call me")
    def validate(self, fields, name, options): # pylint
        raise Exception("Don't call me")

    def pr_context(self, *err, **kw):
        tagv = kw.get('tag', None)
        if tagv:    log.begingroup(tag=tagv)
        if self._old_line_no != self._line_no:
            if tagv:    log.newrecord(tag=tagv)
            else:       log.newrecord()
            if self._options.debug:
                printn('Line {0:8d}|'.format(self._line_no), tag=tagv)
                printnl(self._line, tag=tagv)
            else:
                printnl('Line {0:8d}|'.format(self._line_no), tag=tagv)

            self._old_line_no = self._line_no
            self._this_line   = 1
        for e in err: printnl(e, tag=tagv, line=self._this_line)
        self._this_line += 1
        if tagv:    log.endgroup(tag=tagv)

    def row_util(self, fields, name, dbi, options):

        check = self.select(fields, options)

        if not self.validate(fields, name, options):
            return

        if options.compare:
            self.compare(check, fields, name, options)
            return

        if options.commit:
            if len(check) > 0:
                if self.already_there(check, fields, name, options):
                    return

            if options.delete:
                return

            op = "updating" if (options.update and check) else "inserting"
            if options.debug:
                self.pr_context("For \"{0}\", {1} {2}".format(name, op, self.pr_rec(fields, options)), tag='debug')
            elif options.verbose:
                self.pr_context("For \"{0}\", {1} {2}".format(name, op, self.pr_rec(fields, options)))

            if options.update and check:
                self.update(fields, check, options)
            else:
                self.insert(fields, options)

    def pr_rec(self, x, options, db=False):
        return x

    def compare(self, check, fields, name, options):
        xfields = self.pr_rec(fields, options)
        if len(check) == 0:
            self.pr_context("\"{0}\" not found in database. new record: {1}".format(name, xfields), tag='compare')
        for rec in check:

            if self.cmp(rec, fields, options):

                msg = "\"{0}\" has a different value than database:\n  new\t{1} vs \n   db\t{2}".format(name, xfields, self.pr_rec(rec, options, db=True))
                self.pr_context(msg, tag='compare')

            else:
                if options.debug:
                    msg = "\"{0}\" has the same value as database:\n  new\t{1} vs\n   db\t{2}".format(name, xfields, self.pr_rec(rec, options, db=True))
                    self.pr_context(msg, tag='compare')
                elif options.verbose:
                    msg = "\"{0}\" has the same value as database:\n     {1}".format(name, self.pr_rec(fields, options))
                    self.pr_context(msg, tag='compare')

    def already_there(self, check, fields, name, options):
        ret = True
        if options.update:
            lc = len(check)
            if lc != 1:
                printnl("Can not update; too many values in the database for : {0}".format(fields), tag='update')
                for rec in check:
                    printnl("  new value = {0}; database value = {1}\n".format(fields, rec), tag='update')
                return True
            else:
                return not self.cmp(check[0], fields, options)

        for rec in check:
            if self.cmp(rec, fields, options):
                if not options.delete:
                    self.pr_context('IGNORING NEW VALUE' if not options.replace else 'REPLACING EXISTING VALUE',
                                    " new value = {0}\ndatabase value = {1}\n".format(self.pr_rec(fields, options), self.pr_rec(rec, options, db=True)), tag='update')
                if options.replace or options.delete:
                    self.delete(rec, options)
                    ret = ret and False
                else:
                    ret = ret and True
            else:
                if options.replace or options.delete:
                    self.delete(rec, options)
                    ret = ret and False
        return ret

    def update_util(self, fields, db, cols, table, key, options):
        columns = []
        values  = []
        for (col, fldi, dbi) in cols:
            if fldi == -1:
                if db[0][dbi] != getattr(options, col):
                    columns.append("{0} = ?".format(col))
                    values.append(getattr(options, col))
            elif fields[fldi] is not None and fields[fldi] != db[0][dbi]:
                columns.append("{0} = ?".format(col))
                values.append(fields[fldi])
        if columns:
            cmdu = 'update ' + table + ' set '
            cmd = cmdu + ", ".join(columns) + " where id = ?"
            values.append(key)
            self._dbi_.execute_tuple(cmd, values)
