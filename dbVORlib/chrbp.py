#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import time

from   chrm     import Chrm
from   util     import NoTrace, printn, printnl, log, gzopen, read_fields

class ChrBp(Chrm):
    def __init__(self, db):
        super(ChrBp, self).__init__(db)
        self._dbi_          = db
        self._gmi_head      = ['Chromosome', 'Map.k.a', 'Name', 'Map.k.m', 'Map.k.f', 'Ensembl', 'X.Extrapolation']
        self._gmi_headX     = ['Chromosome', 'Map.k.a', 'Name', 'Map.k.m', 'Map.k.f', 'User.p', 'X.Extrapolation', 'X.Chromosome.e', 'X.Map.k.a.e', 'X.Map.k.m.e', 'X.Map.k.f.e', 'X.Ensembl', 'X.Extrapolation.e']
##NO    self._gmi_headY     = ['Chromosome', 'Map.k.a', 'Name', 'Map.k.m', 'Map.k.f', 'EnsemblUser.p', 'X.Extrapolation', 'X.Chromosome.e', 'X.Map.k.a.e', 'X.Map.k.m.e', 'X.Map.k.f.e', 'X.Ensembl60.p', 'X.Extrapolation.e']
        self._ops_          = None
        self._marker_pos    = {}
        self._marker_pos_rev = {}
        self._genotypes     = {}
        self._alias         = {}
        self._saila         = {}
        self._SNPbp         = 1
        self._SNPcm         = 2
        self._BP            = 0
        self._gwas_cnt      = 0
        self._snp_cnt       = 0

    def add_options(self, config):
#4/1    config.add_option("--gmi", action="store_true", default=False,
#4/1                      help="recompute chromosome/position with gmi")
#4/1    config.add_option("--gmi_file_output", action="store", type="string",
#4/1                      help="write rs ids to file")
#4/1    config.add_option("--snp_position", action="store", type="string",
#4/1                      help="use snp block output to gen chr position info")
        config.add_option("--gmi_position", action="store", type="string",
                          help="use gmi output to gen chr position info")
        config.add_option("--build", action="store", type="string",
                          help="build name for marker_info")

#4/1def gmi_file_output(self, options):
#4/1    if options.gmi_file_output is None:
#4/1        raise Exception("The \"gmi_file_output\" parameter must be specified.")
#4/1    FILE = options.dir + options.gmi_file_output
#4/1    File = gzopen(FILE, 'w')
#4/1    cnt  = 0
#4/1    for lst in self._dbi_.select_all("select marker from markers"):
#4/1        print >>File, lst[0].lower()
#4/1        cnt += 1
#4/1    File.close()
#4/1    printnl("Wrote RS info ({0:d}) to \"{1}\" for position data".format(cnt, FILE))

    def Ops(self):
        return self._ops_

    class GmiOps(object):
        def __init__(self):
            pass
        def name(self, rec):
            return rec[-1]
        def chrm(self, rec):
            return rec[0]
        def alleles(self, rec):
            return []
        def snp2id(self, rec):
            return rec[-2]

# chr bp cm  marker_id, marker

    def gmi_position(self, markers, chrms, options):
        if options.gmi_position is None:
            raise Exception("The \"gmi_position\" parameter must be specified.")
        FILE = options.dir + options.gmi_position
        File = gzopen(FILE)
        BP = cnt = 0
        header = File.readline().split()
        if len(header) == 7:
            if header[0:5] != self._gmi_head[0:5] or header[6] != self._gmi_head[6] or not header[5].startswith(self._gmi_head[5]):
                raise Exception("File \"{0}\": bad header/format\nread: {1}\nneed: {2}\n".format(FILE, header, self._gmi_head))
        elif len(header) == 14-1:    # name is not repeated
            if header[0:5] != self._gmi_headX[0:5] or header[6:11] != self._gmi_headX[6:11] or header[12] != self._gmi_headX[12]\
               or not (header[5].startswith(self._gmi_headX[5])) \
               or not header[11].startswith(self._gmi_headX[11]):
                raise Exception("File \"{0}\": bad header/format\nread: {1}\nneed: {2}\n".format(FILE, header, self._gmi_headX))
        else:
            raise Exception("File \"{0}\": bad header/format\nread: {1}\nneed: {2}\nor need: {3}\n".format(FILE, header, self._gmi_head, self._gmi_head))

        marker_id = 0
        chr_list = self.chrm_mt()
        named_marker  = {}
        markerhash = {}
        for marker in markers:
            markerhash[marker]=True

        log.push('gmi_position')
        for cnt, line, fields in read_fields(FILE, File=File):
            chrm = self.chrm2int(fields[0])
            avg, marker, male, female, pos = fields[1:6]
            marker_id += 1
            if chrm == 0 or fields[5] == 'NA':
                printnl("marker {0:{4:}} has missing gmi chr/bp == {2:}/{3:}".format(marker, -1, chrm, fields[5],
                                                                                     log.snp_name_length, log.marker_length))
                BP += 1
                rec = (0, BP, 0.0, 0.0, 0.0, marker_id, marker)
            else:
                rec = (chrm,
                       int(pos),
                       (float(avg) if avg != 'NA' else 0.0),
                       (float(male) if male != 'NA' else 0.0),
                       (float(female) if female != 'NA' else 0.0),
                       marker_id, marker)

            if   not markers and not chrms:
                Gchrm = rec[0]
                chr_list[Gchrm].append(rec)

            elif not markers and     chrms:
                Gchrm = rec[0]
                if Gchrm in chrms:
                    chr_list[Gchrm].append(rec)

            elif     markers and not chrms:
                if rec[-1] in markers:
                    named_marker[rec[-1]] = rec

            elif     markers and     chrms:
                if rec[0] in chrms:
                    if rec[-1] not in markerhash:
                        Gchrm = rec[0]
                        chr_list[Gchrm].append(rec)
                    else:
                        named_marker[rec[-1]] = rec
                if rec[-1] in markers:
                    named_marker[rec[-1]] = rec

#            cnt += 1
        log.beginfooter()
        printnl("Read CHR/POS MAP \"{0}\" {1:6d} lines".format(FILE, cnt))
        log.pop('gmi_position')
        return chr_list, named_marker

##        if dbmarkers != 0 and dbmarkers != markers:
##            raise NoTrace("\nThere are a different number of markers in the database ({0:d}) than in\n\"{1}\" file ({2:d})".format(dbmarkers, FILE, markers))

    def get_build(self, options):
        builds = dict(self._dbi_.select_all("select lower(build), id from builds"))
        if len(builds) == 0:
#           options.build = None
            options.build, options.build_id = "", 0
        elif len(builds) == 1:
            options.build, options.build_id = builds.popitem()
        elif options.build is None:
            printn("\nExisting Build names:", tag=True)
            for name in sorted(builds.iterkeys()):
                printn("{0},".format(name), tag=True)
            printnl("", tag=True)
            raise NoTrace("ERROR: With more than one build name present, a build must be specified!")
        elif options.build.lower() not in builds:
            printn("\nExisting Build names:", tag=True)
            for name in sorted(builds.iterkeys()):
                printn("{0},".format(name), tag=True)
            printnl("", tag=True)
            raise NoTrace("ERROR: The specified build \"{0}\" is not in the list of builds!".format(options.build))
        else:
            options.build_id = builds[options.build.lower()]

    class SnpOps(object):
        def __init__(self):
            pass
        def name(self, rec):
            return rec[-1]
        def chrm(self, rec):
            return rec[0]
        def alleles(self, rec):
            return list(rec[7])
        def snp2id(self, rec):
            return rec[-2]

# chr bp cm blk off {ab}  marker_id, marker
    def Marker_dealias(self, markers, options):
        cmdm = "select m.id, m.marker from markers m where m.alias is NULL and m.active = ?"
        self._marker_pos = {}
        for mid, marker in self._dbi_.select_all(cmdm, options.active):
            self._marker_pos[marker]  = mid
            self._marker_pos_rev[mid] = marker

        cmda = "select alias, marker from marker_aliases"
        tples = self._dbi_.select_all(cmda)
        self._alias = dict(tples)
        for mid in tples:
            self._saila[mid[1]] = self._marker_pos_rev[mid[1]]

        return [self._saila.get(self._alias.get(marker, 0), marker) for marker in markers]

    def Marker_info(self, tlist, elist, markers, chrms, options):
        self._ops_ = self.SnpOps()
        log.push('db_position')
##      Time = time.time()

        chr_list = self.chrm_mt()
        named_marker  = {}

        if options.gmi_position:
            return self.gmi_position(markers, chrms, options)
        else:
            self.get_build(options)
            if options.build is None:
                raise Exception("You must specify a build number for markers")
#           Time = time.time()
            cmdi = "select chromosome, pos, avg, male, female, id, marker from marker_info"
            if options.build_id != 0:
                cmdi += " where build = ?"
            else:
                cmdi += " where 0 = ?"
            if   not markers and not chrms:
                marker_info = self._dbi_.select_all(cmdi, options.build_id)
                for rec in marker_info:
                    Gchrm = rec[0]
                    chr_list[Gchrm].append(rec)

            elif not markers and     chrms:
                cmdi += ' and chromosome in '
                cmdi += '(' + ", ".join((str(x) for x in chrms)) + ')'
                marker_info = self._dbi_.select_all(cmdi, options.build_id)
                for rec in marker_info:
                    Gchrm = rec[0]
                    chr_list[Gchrm].append(rec)

            elif     markers and not chrms:
                cmdi += ' and marker in '
                cmdi += "('" + "', '".join(x for x in markers) + "')"
                marker_info = self._dbi_.select_all(cmdi, options.build_id)
                for rec in marker_info:
                    named_marker[rec[-1]] = rec

            elif     markers and     chrms:
                cmdm = cmdi
                markerhash = {}
                for marker in markers:
                    markerhash[marker]=True
                cmdi += ' and chromosome in '
                cmdi += '(' + ", ".join((str(x) for x in chrms)) + ')'
                marker_info = self._dbi_.select_all(cmdi, options.build_id)
                for rec in marker_info:
                    if rec[-1] not in markerhash:
                        Gchrm = rec[0]
                        chr_list[Gchrm].append(rec)
#                   else:
#                       named_marker[rec[-1]] = rec

                cmdm += ' and marker in '
                cmdm += "('" + "', '".join((x for x in markers)) + "')"
                marker_info = self._dbi_.select_all(cmdm, options.build_id)
                for rec in marker_info:
                    named_marker[rec[-1]] = rec

#           printnl("minf {0:6.3f} sec".format(time.time()-Time), tag=True)
            return chr_list, named_marker

    def Marker_classify(self, tech_chr_list, tlist, elist, marker2info, options):
        self._SNPbp = 1
        self._SNPcm = 2
        self._BP    = 0
        self._gwas_cnt = 0
        self._snp_cnt  = 0

        printn("collecting distinct markers from genotypes", tag=True)
        Time = time.time()
        cmdg = "select distinct(marker) from genotypes where active = ?"
        if tlist:
            cmdg += ' and technology in '
            cmdg += '(' + ", ".join((str(x) for x in tlist)) + ')'
        if elist:
            cmdg += ' and experiment in '
            cmdg += '(' + ", ".join((str(x) for x in elist)) + ')'

#       Marker (and marker_info) table has latest marker name coupled with marker_id, thus
#        1: genotypes adds the latest marker name to the genotype record (on marker_id)
        for mid in self._dbi_.select_all(cmdg, options.active):
            self._genotypes[mid[0]] = self._marker_pos_rev[mid[0]]

        printnl(" {0:6.3f} sec".format(time.time()-Time), tag=True)

        printn("collecting distinct markers from gwas", tag=True)
        Time = time.time()
        if self._dbi_.dbtable('snpblocks', options.db):
            cmdw = "select chromosome, bp, cm, 0, 0, block, offset, allele, technology, experiment, id, rs from snpblocks where active = ?"
            if tlist:
                cmdw += ' and technology in '
                cmdw += '(' + ", ".join((str(x) for x in tlist)) + ')'
            if elist and False:
                cmdw += ' and experiment in '
                cmdw += '(' + ", ".join((str(x) for x in elist)) + ')'
            for l in self._dbi_.select_all(cmdw, options.active):
                key = l[-1]
##              if key in ('rs2303873', 'rs4418557', 'rs1952426', 'rs460837'):   # alias
##                   import pdb;pdb.set_trace()
##               if key in ('rs2228604', 'rs2877815', 'rs1681576', 'rs1123500'): # merged markers
##                   import pdb;pdb.set_trace()
                if key not in marker2info:
#                   snpblocks are NEVER changed so alias[marker] gives latest marker_id and
#                    then saila[marker_id] -> latest marker name.
                    mid = self._alias.get(key, None)
                    if mid is None:    continue
                    key = self._saila.get(mid, None)
                    if key is None:
                        printnl("snpblocks: marker {0} alias error id {1}".format(l[-1], mid))
                        continue
                    else:
                        if options.verbose:
                            printnl("snpblocks: marker {0} aliased to {1} (via {2})".format(l[-1], key, mid))

#               store key (marker) parameters from marker2info into
#               tech_chr_list under given technology and chr
#               also key (marker) must be in selected markers (marker2info)
                self.place_gwas(tech_chr_list, key, l, marker2info)

        printnl(" {0:6.3f} sec".format(time.time()-Time), tag=True)

#       Time = time.time()
        for midm in self._genotypes.iteritems():
            self.place_genotypes(tech_chr_list, midm, marker2info)
#       printnl("geno {0:6.3f} sec".format(time.time()-Time), tag=True)

        log.beginfooter()
        printnl("Total Markers: Genotypes {0:6d}, GWAS {1:6d}".format(self._snp_cnt, self._gwas_cnt))
        log.pop('db_position')

    MT_LIST = [None, None, None, None, None]
    def place_genotypes(self, tech_chr_list, midm, marker2info):
        marker_id, marker = midm

        if marker not in marker2info: return
        gmi = marker2info[marker]

        Gchrm, Gpos, Gavg, Gmale, Gfemale = gmi[0:5]
        if Gchrm is None or (Gavg is None and Gpos is None):
            printnl("marker {0:<{5}} (id {1:{6}d}) has missing marker_info chr/bp/cM == {2}/{3}/{4}".format( \
                    marker, marker_id, Gchrm, Gpos, Gavg, \
                    log.snp_name_length, log.marker_length))
            self._BP += 1
            Gchrm = 0
            rec = ((Gchrm or 0, Gpos or self._BP, Gavg or 0.0, Gavg or 0.0, Gavg or 0.0,
                    0, 0, '', 0, 0, marker_id, marker))
        else:
            rec = ((Gchrm or 0, Gpos or 0, Gavg or 0.0, Gmale or 0.0, Gfemale or 0.0,
                    0, 0, '', 0, 0, marker_id, marker))

        tech = 'genotypes'
        if tech not in tech_chr_list:
            tech_chr_list[tech] = self.chrm_mt()
        chr_list = tech_chr_list[tech]
        chr_list[Gchrm].append(rec)
        self._snp_cnt += 1
        return

    def place_gwas(self, tech_chr_list, marker, gwas, marker2info):
#        if marker not in self._named_marker: return
        if marker not in marker2info: return
        gmi = marker2info[marker]
        marker_id = self._marker_pos.get(marker, None)
        if marker_id is None:
##xx??
            import pdb;pdb.set_trace()

        Gchrm, Gpos, Gavg, Gmale, Gfemale = gmi[0:5]
        gwas          = list(gwas)
        gwas[self._SNPbp]   = Gpos
        gwas[self._SNPcm]   = Gavg
        gwas[self._SNPcm+1] = Gmale
        gwas[self._SNPcm+2] = Gfemale
        gwas[-2]      = marker_id
        gwas[-1]      = marker
        gwas          = tuple(gwas)

        tech = gwas[-4]  # -4 is technology
        if tech not in tech_chr_list:
            tech_chr_list[tech] = self.chrm_mt()
        chr_list = tech_chr_list[tech]
        chr_list[Gchrm].append(gwas)
        self._gwas_cnt += 1

    def free(self):
        self._marker_pos = {}
        self._genotypes  = {}
