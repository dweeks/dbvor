#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from dbVORlib.validate import Validate
from dbVORlib.util     import NoTrace, printnl


class ExperimentCommon(Validate):
    NO_DIR = True
    NO_LOG = True

    def __init__(self, db):
        super(ExperimentCommon, self).__init__(db)

    def add_options(self, config):
        super(ExperimentCommon, self).add_options(config)
        config.change_default("config", None)    # can operate w/o config file
        config.add_option("--commit", action="store_true", default=False,
                          help="commit changes to database")
        config.add_option("--rollback", action="store_true", default=False,
                          help="force all database operations to be aborted")

    def prolog(self, dbi, options):
        dbi.db_might_commit()
        self.validate_required(options, experiment=True)
        return False if options.error else True

class CreateExperiment(ExperimentCommon):

    def prolog(self, dbi, options):
        self.validate_project(options, create=True)
        if options.error:    raise NoTrace("Try again!")

        self.validate_technology(options, create=True)
        if options.error:    raise NoTrace("Try again!")

        self.validate_experiment(options, create=True)
        if options.error:    raise NoTrace("Try again!")

        printnl('')
        printnl("project \"{0}\", id {1:d}"   .format(options.project, options.project_id))
        printnl("technology \"{0}\", id {1:d}".format(options.technology, options.tech_id))
        printnl("experiment \"{0}\", id {1:d}".format(options.experiment, options.experiment_id))
        printnl('')
        return True

class DeleteExperiment(ExperimentCommon):

    def run(self, dbi, options):

        dbi.execute("""delete from genotypes where
                                   projectid = ? and experiment = ? and technology = ?""",
                    options.project_id, options.experiment_id, options.tech_id)

        dbi.execute("""delete from experiments where id = ?""", options.experiment_id)

        return True

class SetExperimentDate(ExperimentCommon):

    def run(self, dbi, options):
        edate = options.experiment_date
        if edate is None or edate == '':
            raise NoTrace("No experiment_date specified")
        date = edate.strip()
        if date == '':
            raise NoTrace("Empty experiment_date specified")

        date = dbi.dbdate(date)
        if date == "1000-01-01":
            raise NoTrace("Bad date format specified for experiment_date (\"{0}\"); expecting MM/DD/YYYY or YYYY-MM-DD".format(edate.strip()))

        old = dbi.select_one("""select generated from experiments where
                                       projectid = ? and id = ? and techid = ?""",
                             options.project_id, options.experiment_id, options.tech_id)
        if old is not None and old[0] is not None:
            printnl("old date {0}/{1}/{2}".format(old[0].month, old[0].day, old[0].year ))

        printnl("new date {0}".format(edate))
        dbi.execute("""update experiments set generated = ? where
                               projectid = ? and id = ? and techid = ?""",
                    date, options.project_id, options.experiment_id, options.tech_id)
        return True
