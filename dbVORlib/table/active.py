
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from dbVORlib.marker           import Marker
from dbVORlib.util             import printnl

from dbVORlib.table.memtable   import MemTable


class Active(MemTable):

    def __init__(self, db):
        super(Active, self).__init__(db)
        self._all_entries = False
        self._marker_     = Marker(db)
        self._marker      = None


    def add_options(self, config):
        super(Active, self).add_options(config)

        config.add_option("--marker", action="store", type="int", default=3,
                          help="choose columns to get marker string from input")
        config.add_option("--active", action="store", type="int", default=4,
                          help="active value for member; members with 0 are ignored")

    def prolog(self, dbi, options):
        self.validate_all(options)
        if options.error:
            return False
        else:
            self._options = options
            self.show_projtechexp(options)
            return True

#   hide memtable method
    def header(self, File, dbi, options):
        self._line_no = 0

#   hide memtable method
    def epilog(self, dbi, options):
        pass

    def run(self, dbi, options):
        if options.input:
            return super(Active, self).run(dbi, options)
        else:
            self._all_entries = True
            new = []
            self._name = 'Active'
            new.append(options.active)
            self._line_no = 1
            self._line = "genodb active --active={0} ... --project={1} --technology={2} --experiment={3} all genotypes".format(options.active, options.project, options.technology, options.experiment)
            self.row_util(new, self._name, dbi, options)
            return True

    def xform_verbose(self, x, db=False):
        return x[-1:]

    def xform_full(self, x, db=False):
        if db:
            return x[-7:]
        else:
            return x[-6:]

    def do_row(self, fields, dbi, options):
        super(Active, self).do_row(fields, dbi, options)

        self._member = self._member_.member(self._pedigree, self._person, options, self.pr_context)
        self._marker = fields[options.marker   - self.ORIGIN]
        mid    = self._marker_.marker(self._marker, options)
        active = int(fields[options.active   - self.ORIGIN])
        if self._member is None or mid is None:
            options.error += 1
            return

        name = "{0}:{1}:{2} {3}/{4}/{5}".format(self._pedigree, self._person, self._marker, options.project, options.technology, options.experiment)
        self._name = name
        self.row_util([self._pedigree, self._person, self._marker, self._member, mid, options.project_id, options.tech_id, options.experiment_id, active], name, dbi, options)

    def pr_rec(self, fields, options, db=False):
        if len(fields) == 1:
            return "all active={0[0]}".format(fields)
        if options.verbose:
            cmd = "pedigree:{0[0]} person:{0[1]} marker:{0[2]} project:{0[5]} tech_id:{0[6]} experiment_id:{0[7]} active={0[8]}"
        else:
            cmd = "pedigree:{0[0]} person:{0[1]} marker:{0[2]} member:{0[3]} mid:{0[4]} project:{0[5]} tech_id:{0[6]} experiment_id:{0[7]} active={0[8]}"
        return cmd.format(fields)

    cmds = "select active, id from genotypes where member = ? and marker = ? and projectid = ? and technology = ? and experiment = ?"
    def select(self, fields, options):
        if self._all_entries:
            return tuple()
        ans = []
        for rec in self._dbi_.select_all(self.cmds, fields[3], fields[4], fields[5], fields[6], fields[7]):
            const = fields[:-1]  # skip new active value
            const.extend(rec)
            ans.append(const)
        return ans

    cmdd = "delete from members where pedigree = ? and person = ?"
    def delete(self, fields, options):
        printnl("\n\nSORRY! Delete is not possible: use --update")
        options.error += 1

    def insert(self, fields, options):
        printnl("\n\nSORRY! commit/insert is not possible: use --update")
        options.error += 1

    cmdU = "update genotypes set active = ? where projectid = ? and technology = ? and experiment = ?"
    cmdu = "update genotypes set active = ? where id = ?"
    def update(self, fields, check, options):
        if self._all_entries:
            self._dbi_.execute(self.cmdU, fields[0], options.project_id, options.tech_id, options.experiment_id)
            return
        self._dbi_.execute(self.cmdu, fields[-1], check[0][-1])

    def cmp(self, rec, fields, options):
        return cmp(rec[0], fields[0]) or cmp(rec[1], fields[1]) or \
            cmp(rec[2], fields[2]) or cmp(rec[8], fields[8])

    def validate(self, fields, name, options):
        return True
