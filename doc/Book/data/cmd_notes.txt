This file is not intended to be run as an executable.  The contents are provided to
allow easy cut and paste to run commands from the tutorial.

use this line to remove the example database and user to start over

dbvor createdb --revoke --user=example --db=exampledb --root=root

dbvor create --db=exampledb --host=localhost --user example --root=root --sqlpath=~/dbVOR/sql

dbvor member example.ds1.ped.cfg --commit -v -d
dbvor trait example.ds1.trait.cfg --commit -v -d

geno example.ds1.snp.cfg
dbvor newmarker --dir=. --project=project --input=./example.ds1.snp_MissingMarkers.txt --db=exampledb --commit
geno example.ds1.snp.cfg --commit -v
genout example.ds1.out.cfg

	gmi.pl ./example.ds1.snp_MissingMarkers.txt --outfile ./example.ds1.snp_gmi_out
	dbvor gmimarkerALIAS --dir=. --project=project --input=./example.ds1.snp_gmi_out_log.txt --db=exampledb --commit
	dbvor gmimarker --dir=. --project=project --input=./example.ds1.snp_gmi_out_annot.txt --build=<marker_build_version> --db=exampledb --commit

dbvor member example.ds2.ped.cfg --commit 

geno example.ds2.snp.cfg

     	dbvor newmarker --dir=. --project=project --input=./example.ds2.snp_MissingMarkers.txt --db=exampledb --commit
	gmi.pl ./example.ds2.snp_MissingMarkers.txt --outfile ./example.ds2.snp_gmi_out

To put them in the database, add database info to the TWO lines below:
	dbvor gmimarkerALIAS --dir=. --project=project --input=./example.ds2.snp_gmi_out_log.txt --db=exampledb --commit

	dbvor gmimarker --dir=. --project=project --input=./example.ds2.snp_gmi_out_annot.txt --build=<marker_build_version> --db=exampledb --commit

dbvor memberalias dataset2.alias.cfg --commit
dbvor trait trait2.cfg --commit

geno example.ds2.snp.cfg
geno example.ds2.snp.cfg --verify (-v or -d)
geno example.ds2.snp.cfg --commit

geno example.ds2.snp.cfg --commit --replace
dbvor DeleteExperiment --project=project --technology=exampletech2 --experiment=dataset2.txt --commit


geno example.ds3.snp.cfg --verify (-v or -d) with 
dbvor allelemap allelemap.ds3.cfg

geno example.ds3p.snp.cfg --commit

genout out.cfg

dbvor active --experiment dataset2.txt --technology=exampletech2 --project=project --commit --update --active=2
dbvor active --experiment dataset2.txt --technology=exampletech2 active.cfg --commit --update

genout out.cfg --include 2: --exclude 101 --exclude 201 --marker rs1073516 --trait "trait1|value|2" --trait trait2 --trait "gender|1" -v
