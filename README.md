## dbVOR Bitbucket repository ##

The latest development snapshot of the dbVOR code can
be obtained from this Bitbucket repository.  Please note
that this development snapshot is not as thoroughly tested
as our stable release version, but does contain the newest
features and changes.

To obtain the stable release version (including pre-compiled binaries), please go to
<http://watson.hgen.pitt.edu/register>.

## Documentation ##

Please see the dbVOR tutorial/manual, which is available in for the stable release
distribution as a PDF here:

<http://watson.hgen.pitt.edu/register/docs/dbVortutorial.pdf>

It is also available online as <http://watson.hgen.pitt.edu/register/docs/dbVORtutorial.html>.

The development version of the tutorial/manual is available here in the doc/Book directory of the source tree.