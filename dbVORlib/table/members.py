
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from dbVORlib.util             import printnl

from dbVORlib.table.memtable   import MemTable


class FAMParse(MemTable):

    def __init__(self, db):
        super(FAMParse, self).__init__(db)
        self._father = None
        self._mother = None
        self._gender = None
        self._new    = []

    def add_options(self, config):
        super(FAMParse, self).add_options(config)

        config.add_option("--father", action="store", type="int", default=None,
                          help="choose columns to get father string from input")
        config.add_option("--mother", action="store", type="int", default=None,
                          help="choose columns to get mother string from input")
        config.add_option("--gender", action="store", type="int", default=None,
                          help="choose columns to get gender value from input")
        config.add_option("--trait", action="store", type="int", default=None,
                          help="choose columns to get trait value from input; named PLINKtrait")
        config.add_option("--active", action="store", type="int", default=1,
                          help="active value for member; members with 0 are ignored")
        config.add_option("--activefwd", action="store", type="int", default=1,
                          help="active value for member of sample")
        config.add_option("--noheader", action="store", type="boolean", default=0,
                          help="ped file has no header")

    def hint(self):
        print "\nINPUT cfg FORMAT: --pedigree=1 --person=2 --father=3 --mother=4 --gender=5 --trait=6 --input=..."


class MMember(FAMParse):

    def prolog(self, dbi, options):
        super(MMember, self).prolog(dbi, options)

        columns = dbi.dbcolumns("members", options.db, columns=("father", "mother", "gender"))
        def value(col):
            info = columns[col]
            d, t = (info["column_default"], info["data_type"])
            d = d if t in ('varchar') else int(d)
            return d

        if options.father is None:
            f = value("father")
            printnl("father default value is \"{0}\"".format(f))
            self._father = f
        if options.mother is None:
            m = value("mother")
            printnl("mother default value is \"{0}\"".format(m))
            self._mother = m
        if options.gender is None:
            g = value("gender")
            printnl("gender default value is {0:d}".format(g))
            self._gender = g

        return self.insert_many(self.cmdi, self.fmti, options)

    def header(self, File, dbi, options):
        if not options.noheader:
            header = super(MMember, self).header(File, dbi, options)

    genders = {'0':0, '1':1, '2':2, 'f':2, 'F':2, 'm':1, 'M':1, None:None}
    def do_row(self, fields, dbi, options):
        super(MMember, self).do_row(fields, dbi, options)

        name = self._pedigree + ':' + self._person

        tmp = options.father
        father = fields[tmp - self.ORIGIN] if tmp is not None else self._father
        tmp = options.mother
        mother = fields[tmp - self.ORIGIN] if tmp is not None else self._mother

        tmp = options.gender
        if tmp is not None:
            gender = fields[tmp - self.ORIGIN]
            if gender is not None and gender not in self.genders:
                printnl("bad gender value for pedigree:person {0}:{1}: {2}".format(self._pedigree, self._person, gender))
                return
            gval = self.genders[gender]
        else:
            gval = self._gender

#sigh ... samoa
#       if gval is not None:    gval += 1
#sigh ... samoa

        self.row_util([self._project_id, father, mother, gval, options.active, self._pedigree, self._person], name, dbi, options)


    def pr_rec(self, fields, options, db=False):
        if options.verbose:
            return "father:{0[1]!r}, mother:{0[2]!r}, gender:{0[3]}".format(fields)
        else:
            return "project id:{0[0]}, father:{0[1]!r}, mother:{0[2]!r}, gender:{0[3]}, active:{0[4]}, pedigree:{0[5]!r}, person:{0[6]!r}".format(fields)

    cmds = "select projectid, father, mother, gender, active, pedigree, person from members where pedigree = ? and person = ? and projectid = ?"
    def select(self, fields, options):
        return self._dbi_.select_all(self.cmds, self._pedigree, self._person, self._project_id)

    cmdd = "delete from members where pedigree = ? and person = ?"
    def delete(self, fields, options):
        printnl("\n\nSORRY! We may not be able to delete the entry below from members. This")
        printnl("will be true if other database tables refer to this member: use --update")
        self._dbi_.execute(self.cmdd, fields[-2], fields[-1])

    cmdi = "insert into members (projectid, father, mother, gender, active, pedigree, person)"
    fmti = '({0:d}, "{1}", "{2}", {3:d}, {4:d}, "{5}", "{6}")'
    def insert(self, fields, options):
        new = tuple(fields[-2:])
        if new in self._new:
            self.pr_context("DUP {0} is already scheduled to be inserted".format(new))
            options.error += 1
        self._new.append(new)
        self._im(fields)

    cols = ("person", "father", "mother", "gender", "active")
    def update(self, fields, db, options):
        columns = []
        values  = []
        for i, col in enumerate(self.cols):
            if fields[i] is not None and fields[i] != db[0][i]:
                columns.append("{0} = ?".format(col))
                values.append(fields[i])
        if columns:
            cmd = "update members set " + ", ".join(columns) + (" where pedigree = ? and person = ?")
            values.append(fields[-2])    # pedigree
            values.append(fields[-1])    # person
            self._dbi_.execute_tuple(cmd, values)

    def cmp(self, rec, fields, options):
        return cmp(rec[0], fields[0]) or \
            (cmp(rec[1], fields[1]) if fields[1] is not None else 0) or \
            (cmp(rec[2], fields[2]) if fields[2] is not None else 0) or \
            (cmp(rec[3], fields[3]) if fields[3] is not None else 0) or \
            (cmp(rec[4], fields[4]) if fields[4] is not None else 0) or \
            (cmp(rec[5], fields[5]) if fields[5] is not None else 0) or \
            (cmp(rec[6], fields[6]) if fields[6] is not None else 0)


    def validate(self, fields, name, options):
        return True
