ceupheno.csv chbjptpheno.csv and yripheno.csv are reproduced with the permission of their
author Faheem Mitha.

SNPpy is described in:

Mitha F, Herodotou H, Borisov N, Jiang C, Yoder J, et al. (2011) 
SNPpy - Database Management for SNP Data from Genome Wide Association Studies.
PLoS ONE 6(10): e24982. doi:10.1371/journal.pone.0024982 
