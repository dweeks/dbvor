#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import os, os.path
import time
#import csv

from   gwas     import GWAS
from   snp      import Snp
from   util     import printnl, log, gzopen, zipopen

class Ilmn(GWAS):
    ORIGIN = 1

    def __init__(self, db):
        super(Ilmn, self).__init__(db)
        self._header_list               = []

    def add_options(self, config):
        super(Ilmn, self).add_options(config)

        config.add_option("--ilmnmap", action="store", type="string",
                          help="file for Illumina map of SNP_A to data")

#4/1    config.add_option("--genmap", action="store", type="string",
#4/1                      help="genetic map choice: '0', '1'..'n', 'gmi'")
#4/1    config.add_option("--genmapfield", action="store", type="int",
#4/1                      help="Illumina genetic map column")

        config.add_option("--name", action="store", type="int", default=0,
                          help="snp name column")
        config.add_option("--snp", action="store", type="int", default=0,
                          help="snp name column")
        config.add_option("--chr", action="store", type="int", default=0,
                          help="chromosome number column")
        config.add_option("--position", action="store", type="int", default=0,
                          help="snp position column")
        config.add_option("--alleles", action="store", type="int", default=0,
                          help="allele values column")

        config.add_option("--multi", action="store_true", default=False,
                          help="input file has multiple samples")
        config.add_option("--sample", action="store", type="int", default=0,
                          help="sample name column")
        config.add_option("--call", action="store", type="int", default=0,
                          help="call name column")
        config.add_option("--allele1", action="store", type="int", default=0,
                          help="allele1 name column")
        config.add_option("--allele2", action="store", type="int", default=0,
                          help="allele2 name column")

    def make_snps(self, dbi, options):
        if options.ilmnmap is None:
            raise Exception("No Illumina Map specified.")
        FILE = options.dir+options.ilmnmap
        if not os.path.isfile(FILE):
            raise Exception("For \"{0}\", cannot find Illumina Map:\n\"{1}\"".format(options.config, FILE))
        self.read_snps(dbi, FILE, options)

    def map_header(self, line):
        for k in line:
            self._header_list.append(k)

    def marker_file(self, FILE, dbi, snp_exclude, options):
        Otime = time.time()

        self._Uchr = self.chrm2int('U')
#4/1    if options.genmap in ('1', '2', '3'):
#4/1        gmi = int(options.genmap) - 1
        printnl("Illumina Genome File: {0}".format(options.ilmnmap))
        if FILE.endswith('zip'):
            File = zipopen(FILE)
        else:
            File = gzopen(FILE)

        Line = 0
#       csv.register_dialect('o_sep', delimiter=options.sep, quoting=csv.QUOTE_NONE)
#       for line in csv.reader(File, 'o_sep'):
        for line in File:
            line = line.strip().split(options.sep)
            Line += 1
            if line[0] == '':             continue
            if line[0].startswith('#'):   continue

            self.map_header(line)

            if options.verbose:          # show column header
                printnl('')
                printnl("{0:>8}: {1}".format('Column#',  'Contents'))
                for i, inf in enumerate(self._header_list):
                    printnl("{0:8d}:  {1}".format(i, inf))
            break

        log.push("plate mapping")
        if options.verbose:
            printnl("Problem entries with missing chromosome or position")
            printnl("{0:>6} ".format('Line'))
            log.endheader()

        snp_offset = (options.snp if options.snp != 0 else options.name)-self.ORIGIN
#       for line in csv.reader(File, 'o_sep'):
        for line in File:
            line = line.strip().split(options.sep)
            Line += 1
            if line[0] == '':             continue
            if line[0].startswith('#'):   continue

            if options.debug:
                printnl("Line {0}:{1}".format(Line, line))
            self._snp_count += 1

            if options.alleles == 0:
                a = line[options.allele1-self.ORIGIN]
                b = line[options.allele2-self.ORIGIN]
            else:
                ab = line[options.alleles-self.ORIGIN]
                if len(ab) != 5:
                    printnl("Line {0}: odd allele format {1}".format(Line, ab))
                    a = '0'
                    b = '0'
                else:
                    a = ab[1]
                    b = ab[3]

            (snp, chrm, bp) = (line[options.name-self.ORIGIN],
                               line[options.chr-self.ORIGIN],
                               line[options.position-self.ORIGIN])
            if snp in snp_exclude:
                continue
            if snp_offset <= len(line) - 1:
                rs = line[snp_offset]
            else:
                rs = snp
                printnl("Line {0}: missing snp {1}".format(Line, line))

# consortia exome mapping
            if rs.startswith("exm-rs"):
                rs = rs[4:]
            if chrm.startswith("chr"):
                chrm = chrm[3:]

            rs = self.do_probeid_remap(snp, rs, options)
            rs = self.do_rs_remap(rs, options)

            gm = 0.0

            self.define_snp(snp, rs, chrm, bp, gm, a+b, Line, options)
        File.close()

        self.sort(options)
        log.pop("plate mapping")

    def sample_hdr_file(self, File, options):
        Line = self.sample_headers(File, options)
#       for line in csv.reader(File):
        for line in File:
            line = line.strip().split(options.sep)
            Line += 1
            if line[0] == '':             continue
            if line[0].startswith('#'):   continue
            self._sample_file_header = line
            if options.debug:          # show column header
                printnl('')
                printnl("{0:>8}:  {1}".format('Column#', 'Contents'))
                for i, inf in enumerate(self._sample_file_header):
                    printnl("{0:8d}:  {1}".format(i, inf))
            break
        return Line

    def sample_file(self, FILE, options):
        sampled = {}

        File = gzopen(FILE)
        Line = self.sample_hdr_file(File, options)

        if options.multi:
            Sample = None
        else:
            Sample = FILE.rsplit('.', 1)[0]

        self._process_count += 1
        cnti = cnto = 0
        stime = time.time()                    # showtime

#       for line in csv.reader(File):
        for line in File:
            line = line.strip().split(options.sep)
            Line += 1
            if line[0] == '':             continue
            if line[0].startswith('#'):   continue

            name    = line[options.name    - self.ORIGIN]
            sample  = line[options.sample  - self.ORIGIN]
            call    = line[options.call    - self.ORIGIN]
            allele1 = line[options.allele1 - self.ORIGIN]
            allele2 = line[options.allele2 - self.ORIGIN]

            if options.multi:
                samplel = sample
                if Sample != samplel:
                    if Sample is not None:
                        if not self.store_sample(Sample, options):
                            cnti = 0
                        if options.verbose:
                            printnl("#{0:2d} Sample {1}: defined snps {2:d}; unknown snps {3:d}; {4:.3f} sec".format(self._process_count, Sample, cnti, cnto, time.time()-stime))
                        if samplel in sampled:
                            err = "ALL MARKERS for a sample must be contiguous.  First marker for {0} seen on line {1} and now on {2}.".format(Sample, sampled[samplel], Line)
                            raise Exception(err)
                        else:
                            sampled[samplel] = Line
                        self._process_count += 1
                        cnti = cnto = 0
                        stime = time.time()                    # showtime
                    Sample = samplel
            else:
                if not Sample.endswith(sample):
                    printnl("Line {0} sample {1} does not belong in file {2}".format(Line, sample, options.input))
                    printnl("{0} {1} {2} {3} {4}".format(name, sample, call, allele1, allele2))
                    continue

            if options.debug >= 3:
                printnl("{0} {1} {2} {3} {4}".format(name, sample, call, allele1, allele2))

            if name in self._snp_hash:
                r = self._snp_hash[name]
##?? & bad call value
                if allele1 == '-':
                    pass
                elif len(allele1)+len(allele2) != 2:
                    printnl('tilt: line {0}, file {1:} line {2}, alllele1 {3} allele2 {4}'.format(Line, FILE, line, allele1, allele2))
##??
                r[Snp.cll] = allele1+allele2
##??
##    this does not necessarily match ab at the end of the "r" line.
                r[Snp.qul] = call
                cnti += 1
            else:
                self.undefined_snp(name, options)
                cnto += 1
            Line += 1
        File.close()

        if options.multi:
            self.store_sample(Sample, options)
            if options.verbose:
                printnl("#{0:2d} Sample {1}: defined snps {2:d}; unknown snps {3:d}; {4:.3f} sec".format(self._process_count, Sample, cnti, cnto, time.time()-stime))
        else:
            self.store_sample(options.input, options)
            if options.verbose:
                printnl("Defined snps {0:d}; unknown snps {1:d}; {2:.3f} sec".format(cnti, cnto, time.time()-stime))

    def sample_headers(self, File, options):
        Line = 0
        for line in File:
            Line += 1
            sline = line.rstrip()
            if sline == '' or sline.startswith('#'):
                continue

            if options.debug:
                printnl("Header {0:2}:   {1}".format(Line, sline))
            if sline == '[Data]':
                break
        return Line
