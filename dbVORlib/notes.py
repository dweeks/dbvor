#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import os.path
import hashlib
import bz2

from validate   import ValidateProject
from util       import NoTrace, printnl, printn

class Note(ValidateProject):
    def __init__(self, db):
        super(Note, self).__init__(db)

    def run(self, dbi, options):
        user = options.user or os.getenv('USER', '???')
        COLS   = ['user']
        VALUES = [user]
        if options.project:
            COLS.append('project')
            VALUES.append(options.project)

        if options.keywords:
            keywords = []
            for kw in options.keywords:
                keywords.extend(kw.split())
            COLS.append('keywords')
            keywords.sort()
            VALUES.append(' '.join(keywords))

        if options.comment or options.summary:
            COLS.append('comment')
            comment = []
            if options.comment: comment.append(options.comment)
            if options.summary: comment += options.summary
            VALUES.append("\n".join(comment))

        if options.file:
            if os.path.isfile(options.file):
                lines = []
                m = hashlib.md5()
                if options.storefile: z = bz2.BZ2Compressor()
                for line in file(options.file):
                    if options.storefile: lines.append(z.compress(line))
                    m.update(line)
                md5 = m.hexdigest()
                if options.storefile: lines.append(z.flush())
            else:
                raise NoTrace("File \"{0}\" can not be found".format(options.file))
            COLS.append('file')
            VALUES.append(os.path.basename(options.file))
            if options.storefile:
                COLS.append('filedata')
                VALUES.append(''.join(lines))
            COLS.append('filemd5')
            VALUES.append(md5)

        if options.date:
            date = self.dbdate(dbi, options)
            COLS.append('filedate')
            VALUES.append(date)

        if options.text or options.note:
            lines = []
            m = hashlib.md5()
            if options.text:
                for line in options.text:
                    lines.append(line)
                    m.update(line)
                    line = "\n"
                    lines.append(line)
                    m.update(line)
            if options.note:
                if os.path.isfile(options.note):
                    for line in file(options.note):
                        lines.append(line)
                        m.update(line)
                else:
                    raise NoTrace("Note file \"{0}\" can not be found".format(options.note))
                if not options.text:
                    COLS.append('notefile')
                    VALUES.append(os.path.basename(options.note))
            md5 = m.hexdigest()
            COLS.append('notedata')
            VALUES.append(''.join(lines))
            COLS.append('notemd5')
            VALUES.append(md5)

        BINDS = ', '.join(len(COLS) * ('?',) )
        COLS  = ', '.join(COLS)

        s = "insert into notes ({0}) VALUES ({1})".format(COLS, BINDS)
        dbi.insert(s, [VALUES])
        dbi.commit()

    def dbdate(self, dbi, date):
        date = date.strip()
        if date == '':    raise NoTrace("Empty date specified")
        date = date.split(None, 1)
        mysql_date = dbi.dbdate(date[0])
        if mysql_date == '0000-00-00':
            raise NoTrace("Bad date format specified for file date; expecting MM/DD/YYYY or YYYY-MM-DD")
        if len(date) > 1:
            return mysql_date + ' ' + date[1]
        else:
            return mysql_date

class NotePgm(Note):
    NO_LOG = True
    NO_DIR = True

    def hint(self):
        print "dump database: --list, --view, or --fetch"
        print "\t\twith selector: --keyword, --comment, --file, --date{le,ge}, or --time{le,ge}"
        print "store metadata: --keywords(k), --comment(c), --file(f), --storefile(s), --note, --text, --date"
        print "administrative commands: --delete, --drop, or --create"

    def add_options(self, config):
        super(NotePgm, self).add_options(config)
        config.check_home()
        config.change_default('config', '.genoterc')

# insertion keywords
        config.add_option("-s", "--storefile", action="store_true", default=False, help="store file being annotated")
        config.add_option("-n", "-a", "--note", "--application",
                          action="store", type="string", help="annotation file")
        config.add_option("--text", action="append", type="string", help="annotation text")

# common keywords (both insertion and fetch)
        config.add_option("-k", "--keywords", action="append", type="string", help="keywords")
        config.add_option("-c", "--comment", action="store", type="string", help="comments")
        config.add_option("--summary", action="append", type="string", default=[], help="comments")
        config.add_option("-f", "--file", action="store", type="string", help="file being annotated")

# fetch keywords
        config.add_option("--list", action="store_true", default=False, help="list all notes")
        config.add_option("--fetch", action="store_true", default=False, help="fetch mode: comment, keyword, file")
        config.add_option("--view", action="store_true", default=False, help="view mode: comment, keyword, file")
        config.add_option("--all", action="store_true", default=False, help="get all notes")
        config.add_option("--id", action="append", type="string", help="get notes by ids")
        config.add_option("--timele", action="store", type="string", help="select db date for less than or equal")
        config.add_option("--timege", action="store", type="string", help="select db date for greater than or equal")
        config.add_option("--datele", action="store", type="string", help="select file date for less than or equal")
        config.add_option("--datege", action="store", type="string", help="select file date for greater than or equal")
        config.add_option("--date", action="store", type="string", help="date for file being annotated")
        config.add_option("--head", action="store_true", default=False, help="show \"head\" of notes")

# admin commands
        config.add_option("--delete", action="store", type="int", help="remove entry ... be careful")
        config.add_option("--create", action="store_true", default=False, help="add LabNotebook table")
        config.add_option("--drop", action="store_true", default=False, help="drop LabNotebook table")

    def run(self, dbi, options):
        if options.delete:
            dbi.execute("delete from notes where id = ?", options.delete)
            dbi.commit()
        elif options.view or options.fetch or options.list or options.head:
            self.getfile(dbi, options)
            return
        else:
            super(NotePgm, self).run(dbi, options)

    def prolog(self, dbi, options):
        if options.drop or options.create:
            self.table(dbi, options)

        return True

    def getfile(self, dbi, options):
        cmd  = "select id, comment, keywords, file, filemd5, notefile, notemd5, filedate, time_stamp from notes"
        cmdf = "select filedata from notes where id = ?"
        cmdn = "select notedata from notes where id = ?"
        if options.comment:
            ans = dbi.select_all(cmd + " where comment like ?", '%'+options.comment+'%')
        elif options.keywords:
            ans = dbi.select_all(cmd + " where keywords like ?", '%'+options.keywords[0]+'%')
        elif options.file:
            ans = dbi.select_all(cmd + " where file like ?", '%'+options.file+'%')
        elif options.all:
            ans = dbi.select_all(cmd)
        elif options.id:
            ans = dbi.select_all(cmd + " where id in (" + ', '.join(options.id) +')')
        elif options.timege:
            date = self.dbdate(dbi, options.timege)
            ans = dbi.select_all(cmd + " where time_stamp >= ?", date)
        elif options.timele:
            date = self.dbdate(dbi, options.timele)
            ans = dbi.select_all(cmd + " where time_stamp <= ?", date)
        elif options.datege:
            date = self.dbdate(dbi, options.datege)
            ans = dbi.select_all(cmd + " where filedate >= ?", date)
        elif options.datele:
            date = self.dbdate(dbi, options.datele)
            ans = dbi.select_all(cmd + " where filedate <= ?", date)
        else:
            raise NoTrace("bad fetch mode: not comment, keyword, file or date.\n")
        for dbid, comment, keywords, filefile, md5, notefile, notemd5, filedate, stamp in ans:
            printnl("genote id = {0}, time stamp = {1}".format(dbid, stamp))
            if comment: printnl("comment = {0}".format(comment))
            if keywords: printnl("file date = {0}, keywords = {1}".format(filedate, keywords))
            bz2file = dbi.select_one(cmdf, dbid)
            if bz2file is not None: bz2file = bz2file[0]
            if bz2file is not None:
                if filefile == None: filefile = "file{0:04d}.bz2".format(dbid)
                if filefile: printnl("file = {0}, md5 = {1}".format(filefile, md5))
                if options.view:
                    printn(bz2.decompress(bz2file))
                elif options.fetch:
                    dataf = open("genote." + filefile + '.bz2', 'w')
                    dataf.write(bz2file)
                    dataf.close()
            note = dbi.select_one(cmdn, dbid)
            if note is not None: note = note[0]
            if note is not None:
                if notefile == None: notefile = "note{0:04d}.txt".format(dbid)
                printnl("notefile = {0}, md5 = {1}".format(notefile, notemd5))
                if options.head:
                    lines = 1
                    while True:
                        line, note = note.split("\n", 1)
                        printnl(line)
                        lines += 1
                        if lines > 5: break
                elif options.view:
                    printn(note)
                elif options.fetch:
                    dataf = open("genote." + notefile, 'w')
                    dataf.write(note)
                    dataf.close()
            printnl("")



    def table(self, dbi, options):
        if options.drop:
            drop_table =\
    """drop table NOTES;  """
            dbi.execute(drop_table)
            dbi.commit()

        if options.create:
            create_table =\
    """create table NOTES
    (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(id),
    time_stamp TIMESTAMP,

    user       VARCHAR(30)  NOT NULL,
    project    VARCHAR(30),
    keywords   VARCHAR(256),
    comment    VARCHAR(1024),

    filedate   DATE         NOT NULL DEFAULT '0000-00-00',
    filemd5    CHAR(32),
    filedata   MEDIUMBLOB,
    file       VARCHAR(256),

    notemd5    CHAR(32),
    notedata   MEDIUMTEXT,
    notefile   VARCHAR(256)

) ENGINE = INNODB;
"""
            dbi.execute(create_table)
            dbi.commit()

class NoteMain(Note):
#   Nothing will be "automatically" called here, since it is a Class directly used by class Main.
#   __init__(dbi) is called as expected
#   run(dbi, options) is directly called by Main()
    pass
