#!/usr/bin/python
#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import sys

from dbVORlib.main                import Main, Version
#from dbVORlib.table              import Trait
from dbVORlib.table.allelemap     import AlleleMap
from dbVORlib.table.createdb      import CreateDB
from dbVORlib.table.traits        import TTrait, FAMTrait
from dbVORlib.table.newmarkers    import NewMMarker, MissingMarker
from dbVORlib.table.markers       import MMarker, GMIMMarker
from dbVORlib.table.markeraliases import MarkerAlias, GMIMarkerAlias
from dbVORlib.table.samples       import SSample, FAMSample
from dbVORlib.table.members       import MMember
from dbVORlib.table.memberaliases import MemberAlias
from dbVORlib.table.experiments   import DeleteExperiment, CreateExperiment, SetExperimentDate
from dbVORlib.table.active        import Active
from dbVORlib.table.block         import DeleteSnpBlockMap, DeleteBlockAllSamples, DeleteBlockMember, DeleteBlockSample, ShowSamples

def main():
    if len(sys.argv) == 1 or (len(sys.argv) == 2 and sys.argv[1].lower() in ('help', '--help', '-h')):
        print "subcommands:"
        print "\tnewmarker,", "missingmarkerinfo,", "allelemap,"
        print "\tmarkerinfo,", "markeralias,", "gmimarker,", "gmimarkeralias,", "sample,"
        print "\ttrait,", "member,", "plink,", "plinksample,", "memberalias,"
        print "\tcreate_experiment(CreateExperiment),", "set_experiment_date(SetExperimentDate),"
        print "\tdelete_experiment(DeleteExperiment),"
        print "\tdeletesnpblockmap,", "deleteblockallsamples,", "deleteblockmember,", "deleteblocksample,", "showsamples,"
        print "\tactive(set_active),", "createdb,", "version"
        print "\ntype \"dbvor <subcommand> -h\" for more information about each subcommand."
        sys.exit(0)

    cmd = " ".join(sys.argv)
    sysargv = sys.argv[:]    # copy; pop is destructive
    try:
        action = sys.argv.pop(1).lower() if sys.argv else None
        command = None
        mt     = len(sys.argv)
        if action == 'trait':
            command = Main(TTrait, action=action, cmd=cmd)
        elif action == 'newmarker':
            command = Main(NewMMarker, action=action, cmd=cmd)
        elif action == 'missingmarkerinfo':
            command = Main(MissingMarker, action=action, cmd=cmd)
        elif action == 'markerinfo':
            command = Main(MMarker, action=action, cmd=cmd)
        elif action == 'gmimarker':
            command = Main(GMIMMarker, action=action, cmd=cmd)
        elif action == 'markeralias':
            command = Main(MarkerAlias, action=action, cmd=cmd)
        elif action == 'gmimarkeralias':
            command = Main(GMIMarkerAlias, action=action, cmd=cmd)
        elif action == 'allelemap':
            command = Main(AlleleMap, action=action, cmd=cmd)
        elif action == 'sample':
            command = Main(SSample, action=action, cmd=cmd)
        elif action == 'plinksample':
            command = Main(FAMSample, action=action, cmd=cmd)
        elif action == 'plink':
            action = 'member'
            command1 = Main(MMember, action=action, cmd=cmd.replace('plink', action), sub=".M")
            command1()
            action = 'plinksample'
            command2 = Main(FAMSample, action=action, cmd=cmd.replace('plink', action), sub=".S")
            command2()
            action = 'plinktrait'
            command3 = Main(FAMTrait, action=action, cmd=cmd.replace('plink', action), sub=".T")
            command3()
        elif action == 'member':
            command = Main(MMember, action=action, cmd=cmd)
        elif action == 'memberalias':
            command = Main(MemberAlias, action=action, cmd=cmd)
        elif action in ('delete_experiment', 'deleteexperiment'):
            command = Main(DeleteExperiment, action=action, cmd=cmd)
        elif action in ('create_experiment', 'createexperiment'):
            command = Main(CreateExperiment, action=action, cmd=cmd)
        elif action in ('set_experiment_date', 'setexperimentdate'):
            command = Main(SetExperimentDate, action=action, cmd=cmd)
        elif action in ('set_active', 'setactive', 'active'):
            command = Main(Active, action=action, cmd=cmd)
        elif action in ('deletesnpblockmap', ):
            command = Main(DeleteSnpBlockMap, action=action, cmd=cmd)
        elif action in ('deleteblockallsamples', ):
            command = Main(DeleteBlockAllSamples, action=action, cmd=cmd)
        elif action in ('deleteblockmember', ):
            command = Main(DeleteBlockMember, action=action, cmd=cmd)
        elif action in ('deleteblocksample', ):
            command = Main(DeleteBlockSample, action=action, cmd=cmd)
        elif action in ('showsamples', 'showblocksamples'):
            command = Main(ShowSamples, action=action, cmd=cmd)
        elif action in ('createdb',):
            command = Main(CreateDB, action=action, cmd=cmd)
#        elif action in ('exec',):
#            cls = sys.argv.pop(1) if sys.argv else None
#            mod = sys.argv.pop(1) if sys.argv else 'dbVORlib.table.block'
#            act = getattr(sys.modules[mod], cls)
#            command = Main(act, action=action, cmd=cmd)
        elif action in ('version', '--version'):
            command = Version()
            command.prnt()
            command = None
        else:
            raise Exception("dbVor: Unknown subcommand requested (\"{0}\").  Aborting!!".format(action))

        if command is not None:
            if mt == 1:
                command.hint()
            else:
                command()

    except Exception, error:
        sys.argv = sysargv    # for debuggung restart
#       raise
        print error

#import cProfile
#cProfile.run('main()')

main()
