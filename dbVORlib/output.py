# spill list
#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import types

from allele   import Allele
from util     import printnl


class Output(object):
    def __init__(self, db):
        self._allele_ = Allele(db)
        self._Hash    = {}

    def process_one(self, options, Id, marker_id, ab, pr_pps):
        if Id in self._Hash:
            if marker_id in self._Hash[Id]:
                cd = self._Hash[Id][marker_id]
                if type(cd) != types.ListType:
                    cd = [[1, cd]]
                    self._Hash[Id][marker_id] = cd
                self._allele_.merge_alleles2(cd, ab)
                if options.verbose > 2:
                    if len(cd) == 1:
                        printnl("member/marker {0}/{1} identical alleles {2}".format(Id, marker_id, cd))
                    else:
                        printnl("member/marker {0}/{1} allele conflict {2}".format(Id, marker_id, cd))
            else:
                self._Hash[Id][marker_id] = ab
        else:
            self._Hash.setdefault(Id, {})[marker_id] = ab

    def process_last(self, options, member, marker):
        Ids     = member.list()
        markers = marker.list()

        if options.stat or not options.nostat:
            self.create_xlate_stat(options, member, marker)

        if options.output:
            out  = file(options.dir+options.output, "w")
            spll = file(options.dir+options.output+'.spill', "w")
        else:
            return
    #       out = None

        for Id in Ids:
            for marker_id in markers:
                ab = self._Hash[Id].get(marker_id, None)
                if ab:
                    if type(ab) != types.TupleType:
                        for cnt, abc in ab:
                            for unused in range(0, cnt):
                                print >>spll, Id, marker_id, abc[0], abc[1], 0

                    if type(ab) == types.TupleType:
                        print >>out, Id, marker_id, ab[0], ab[1]
                    elif len(ab) == 1:  # viz. [[2, ('B', 'B')]]
                        print >>out,  Id, marker_id, ab[0][1][0], ab[0][1][1]
                        print >>spll, Id, marker_id, ab[0][1][0], ab[0][1][1], 1
                    elif len(ab) == 2 and ab[0][1][0] == '0' and ab[0][1][1] == '0':
    #                                                  [[1, ('0', '0')], [1, ('A', 'A')]]
                        print >>out,  Id, marker_id, ab[1][1][0], ab[1][1][1]
                        print >>spll, Id, marker_id, ab[1][1][0], ab[1][1][1], 1
                    else:
                        print >>out,  Id, marker_id, 0, 0
                        print >>spll, Id, marker_id, 0, 0, 1

        if out:   out.close()
        if spll: spll.close()

    def create_xlate_stat(self, options, member, marker):
        Ids     = member.list()
        markers = marker.list()
        printnl("Statistics by members")
        printnl("{0:>6} {1:>7} {2:>6} {3:>7}".format('member', 'markers', "(0,0)", 'missing'))
        for Id in Ids:
            hit = zero = mt = 0
            for marker_id in markers:
                ab = self._Hash[Id].get(marker_id, None)
                if ab is None:
                    mt += 1
                elif type(ab[0]) == types.StringType and ab[0] == '0' and ab[1] == '0':
                    zero += 1
                else:
                    hit += 1
            printnl("{0:6d} {1:7d} {2:6d} {3:7d}".format(Id, hit, zero, mt))

        self._allele_.conflict_stats(options, self._Hash, member, marker)


    def stat(self, options, cmd, member, marker):
        pass

    def conflict_stats(self, options, member, marker):
        pass
