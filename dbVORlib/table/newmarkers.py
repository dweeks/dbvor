#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from dbVORlib.util             import printnl, log

from dbVORlib.table.dbtable    import DBTable


class NewMMarker(DBTable):

    def __init__(self, db):
        super(NewMMarker, self).__init__(db)
#       self._marker_    = Marker(db)
        self._name       = None

    def add_options(self, config):
        super(NewMMarker, self).add_options(config)
        config.add_option("--marker", action="store", type="int", default=1,
                          help="choose columns to get marker string from input")

        config.add_option("--active", action="store", type="int", default=1,
                          help="active value")

    def do_row(self, fields, dbi, options):
        new = []
        new.append(fields[options.marker - self.ORIGIN])
        self._name = new[0]
        new.append(options.active)
        self.row_util(new, self._name, dbi, options)

    def pr_rec(self, fields, options, db=False):
        if options.verbose:
            return "marker:{0[0]!r}".format(fields)
        else:
            return "marker:{0[0]!r} active:{0[1]}".format(fields)

    cmds = "select marker, active, id from markers where marker = ?"
    def select(self, fields, options):
        return self._dbi_.select_all(self.cmds, fields[0])

    cmdd = "delete from markers where id = ?"
    def delete(self, rec, options):
        self._dbi_.execute(self.cmdd, rec[-1])

    cmdi = "insert into markers (marker, active) VALUES (?, ?)"
    def insert(self, fields, options):
        self._dbi_.insert(self.cmdi, [fields])

    def cmp(self, rec, fields, options):
        return cmp(rec[0], fields[0]) or cmp(rec[1], fields[1])

    def validate(self, fields, name, options):
        ret = True
        return ret

class MissingMarker(DBTable):
    NO_INPUT = True

    def __init__(self, db):
        super(MissingMarker, self).__init__(db)
#       self._marker_    = Marker(db)
        self._builds     = {}
        self._name       = None

    def add_options(self, config):
        super(MissingMarker, self).add_options(config)
        config.add_option("--build", action="store", type="string",
                          help="build name")

    def prolog(self, dbi, options):
        super(MissingMarker, self).prolog(dbi, options)
        self._builds = dict(self._dbi_.select_all("select lower(build), id from builds"))
        return True

    def run(self, dbi, options):
        call = "select distinct marker from markers"
        cbld = "select distinct marker from markers where marker not in (select marker_info.marker from marker_info where marker_info.build= ?)"

        stem = 'MissingMarkerInfo'
        if options.build is None:
            markers = dbi.select_all(call)
        else:
            stem += '_' + options.build
            build = self._builds.get(options.build.lower(), None)
            if build is None:
                markers = dbi.select_all(call)
            else:
                markers = dbi.select_all(cbld, build)

        if len(markers) == 0:
            printnl("\nAll markers for build {0} have marker info.".format(options.build if options.build else ''))
            return True

        FILE = options.dir + stem + '.txt'
        out = open(FILE, "w")

        log.push('marker_info_missing')
        printnl("\nThe following markers have no marker info {0}:".format(('for build ' + options.build) if options.build else ''))
        log.endheader()
        for marker in markers:
            printnl("{0[0]!r}".format(marker))
            out.write("{0[0]}\n".format(marker))
        log.pop('marker_info_missing')
        out.close()

        printnl("\nTo look up the marker positions, type:")
        printnl("	gmi.pl {0} --outfile {1}".format(FILE, options.dir + stem + '_gmi_out'))

        printnl("\nTo put them in the database, type the TWO lines below (correct the arguments where necessary):")
        printnl("	dbvor gmimarkerALIAS --dir=. --project={0} --input={1} --db={2} --commit\n".format \
                (options.project, options.dir + stem + '_gmi_out_log.txt', options.db))

        printnl("\n	dbvor gmimarker --dir=. --project={0} --input={1} --build={2} --db={3} --commit\n".format \
                (options.project, options.dir + stem + '_gmi_out_annot.txt',
                 (options.build if options.build else '<marker_build_version>'), options.db))

        return True
