#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#
# This class is a shim around optparse, so that parameters can come from the
# command line AND/OR a configuration file.  The config file format is:
#    parameter : value
# white space is discarded.  Empty lines and lines beginning with'#' are ignored.
#
# The command line parameters take precedence.
# The latter stipulation implies that an optparse default parameter value
# cannot be applied if there is a matching configuration file value.
# Also, the file parameters must be converted as would the command line value.
# Note: optparse lets you define multiple aliases (syn) for a parameter.
# ... Hence, the complexity here.
#
# External Methods:
#	 add_options	-- mimicks optparse.add_options
#	 parse_args	-- mimicks optparse.parse_args and merges config file
#	 log_options	-- prints out option settings to Log file
#        check_home     -- searches homedir for config file before current dir
#                           if config file is a relative path
# NOTE:
#        If the config file is not explicitly specified, the first argument will be
#         used as the config file when check_home_dir is NOT set.
# NOTE:
#        Generally command line arguments take precedence over config file arguments,
#         except "append" style fields are appended from both sources.
# NOTE:
#        If the value includes embeded white space, it would be safest to surround it
#         in quotes.  (either matching ' or ")
# NOTE:
#        If you set debug=True in the constructor, the operations of the module will be
#	  printed out as they happen.

# HISTORY
#  05/14/2010
#	Stable!
#
#  05/19/2010
#	Small cleanups.
#	Convert config args during parse so that append will work correctly.


from optparse import OptionParser, Option, OptionValueError
import sys, os.path, csv

from util     import printnl, NoTrace

class Config(object):
    def __init__(self, rc=None, debug=False, usage=None):
        self._check_home_dir = False
        self._debug   = debug
        self._rc      = rc
        self._conv    = {}
        self._syn     = {}
        self._options = []
        self._hash    = {}
        self._default = {}
        self._append  = {}
        self._replay  = {}
#1127
        self._show    = []
        if usage:
            self._parser_  = OptionParser(usage = usage, option_class = Boolean)
        else:
            self._parser_  = OptionParser(option_class = Boolean)  # allow type boolean

    def parse_args(self, *l, **k):
        (options, args) = self._parser_.parse_args(*l, **k)

        if self._rc:
            FILE = self._rc
            DIR = os.path.expanduser('~')
            if self.read(os.path.join(DIR, FILE)) is None:
                print >>sys.stderr, "Config: parsed ~/{0} for config options".format(self._rc)

        FILE = options.config
        if FILE is None:
            if len(args) >= 1 and not self._check_home_dir:
                FILE = args.pop(0)
            else:
                FILE = self._default.get('config', 'No.cfg')
            options.config = FILE

        if FILE is not None:
#1127
            self._show.append('config')
            off = FILE.rfind('.')
            options.cfgroot = os.path.basename(FILE[:off] if off >= 0 else FILE)

            if self._check_home_dir and not os.path.dirname(FILE):  # file not absolute
                DIR = os.path.expanduser('~')
                self.read(os.path.join(DIR, FILE))

            oops = self.read(FILE)
#6/1        if oops and not self._check_home_dir:    raise Exception, oops
            if oops and not options.version and not self._check_home_dir:
                self.help()
                raise NoTrace(oops if FILE != 'No.cfg' else '')

        self.source_files(options)
        self.merge_options(options)
        return (options, args)

    def add_option(self, *l, **k):
        dest = k.get('dest', None)
        if dest is not None:                 # explicit
            if self._debug: print "add_options: explicit {0}, ".format(dest)
            self._options.append(dest)
            for flag in l:
                if flag.startswith("--"):
                    syn = flag[2:]
                    self._options.append(syn)
                    self._syn[syn] = dest
                    if self._debug: print "syn {0}, ".format(syn),
        else:
            for flag in l:                   # implicit ... find dest
                if flag.startswith("--"):
                    syn = flag[2:]
                    self._options.append(syn)
                    if dest is None:
                        dest = syn
                        if self._debug: print "add_options: implicit {0}, ".format(dest),
                    else:
                        self._syn[syn] = dest
                        if self._debug: print "syn {0}, ".format(syn),
            if dest is None:
                dest = l[0]
                if dest.startswith("-"):
                    dest = dest[1:]
                    if self._debug: print "oops dest {0}, ".format(dest),
                    self._options.append(dest)

        default = k.pop('default', None)       # we must save and later apply default
        if default is not None:
            self._default[dest] = default
            if self._debug: print "default {0}, ".format(default),

        conv = k.get('type', None) or "string" # we must convert string value
        self._conv[dest]    = conv
        if self._debug: print "conversion {0}, ".format(conv),

        action = k.get('action', None)
        if self._debug: print "action {0}, ".format(action),
        if action is None:
            pass
        elif action in ('append', ):        # viz. include & exclude lists
            self._append[dest] = action
        elif action == 'store_true':
            self._conv[dest]    = 'true'
            if self._debug: print "conversion True",
        elif action == 'store_false':
            self._conv[dest]    = 'false'
            if self._debug: print "conversion False",
        elif action == 'count':
            self._conv[dest]    = 'int'
            if self._debug: print "conversion int",
        elif action in ('replay', ):        # only for file read ...
            self._append[dest] = action
            if self._debug: print
            return

        self._parser_.add_option(*l, **k)
        if self._debug: print

    def change_default(self, key, default):
        key = self.parse_key(key)
        self._default[key] = default
        if self._debug: print "change_default _default[{0}] = {1}".format(key, default)

    def check_home(self):
        self._check_home_dir = True

    def read(self, FILE):
        if FILE is None: return None
        try:
#6/1        File = file(FILE, 'Ur')
            File = file(FILE, 'rU')
        except Exception, error:
            return "{0}\nunable to open file '{1}'".format(error, FILE)

        key = 'eh?'
        appnd = False
        while True:
            line = File.readline()
            if line == '': break
            line = line.strip()
            if line == '' or line.startswith('#'):
                continue
            kv = line.split(':', 1)
            if len(kv) == 2:
                key, value = kv
                key = key.strip()
                if key == 'zource':
#                   FILE1 = os.path.join(DIR, value)
                    FILE1 = value.strip()
                    error = self.read(FILE1)
                    if error is None:
                        print >>sys.stderr, "Config: sourced {0} for more options".format(FILE1)
                    else:
#6/1                    raise Exception, error
                        raise Exception(error)
                    continue
                elif key.lower() == 'end':
                    appnd = False
                    continue
                key = self.parse_key(key)
                if key not in self._append:
                    self.parse(key, value.strip())
                    continue
                elif key.endswith('text'):
                    self.parse(key, self.gettext(File))
                    continue
                elif self._append[key] == 'replay':
                    k = self._default[key]
                    i = 0
                    csvrdr = csv.reader(kv[1:], dialect='excel', skipinitialspace=True)
                    for value in csvrdr.next():
                        value = value.strip()
                        self.parse(k[i], value)
                        i += 1
                    continue
                else:
                    appnd = True
                    kv = kv[1:]
            if appnd:
                csvrdr = csv.reader(kv, dialect='excel', skipinitialspace=True)
                for value in csvrdr.next():
                    value = value.strip()
                    if value != '': self.parse(key, value)
            else:
#6/1            raise Exception, "Orphan value \"{0}\" is only allowed for an append parameter in the config file.".format(kv[0])
                raise Exception("Orphan value \"{0}\" is only allowed for an append parameter in the config file.".format(kv[0]) )
        File.close()
        return None

    def parse_key(self, key):
        if key not in self._options:
#6/1            raise Exception, "Parameter \"{0}\" is not an allowed key in the config file.".format(key)
            raise Exception("Parameter \"{0}\" is not an allowed key in the config file.".format(key) )

#1127
        if key not in self._show:    self._show.append(key)
        key = self._syn.get(key, key)
        return key

    def gettext(self, File):
        text = ''
        while True:
            line = File.readline()
            if line == '': break
            sline = line.strip()
            if sline.lower().startswith('end:'): break
            text += line
        return text

    def parse(self, key, value):
        if value != '' and \
           ((value[0] == '"' and value[-1]  == '"') or \
            (value[0] == "'" and value[-1] == "'")):
            value = value[1:-1]
        value = self.convert(key, self._conv[key], value)
        if key in self._append:
            self._hash.setdefault(key, []).append(value)
        else:
            self._hash[key] = value
        if self._debug: print "read_options: key/value {0}/{1} ".format(key, self._hash[key])

    def convert(self, key, conv, value):
        if conv == 'int':
            if value.lower() in ('true', ''):
                value = 1
            elif value.lower() in ('false', ):
                value = 0
            else:
                try:
                    value = int(value)
                except Exception, error:
                    print "Conversion of {0} to an integer failed: {1}".format(value, error)
                    raise
        elif conv == 'boolean':
            if value.lower() in  ('true',  '1'):
                value = True
            elif value.lower() in  ('false', '0'):
                value = False
            else:
                raise Exception("Conversion of {0} to a bool failed".format(value))
        elif conv == 'float':
            try:
                value = float(value)
            except Exception, error:
                print "Conversion of {0} to a float failed: {1}".format(value, error)
                raise
        elif conv == 'true':
            if value != '': raise Exception("ERROR Config: An explicit value for \"{0}\" should not be given.  (Got {1})".format(key, value))
            value = True
        elif conv == 'false':
            if value != '': raise Exception("ERROR Config: An explicit value for \"{0}\" should not be given.  (Got {1})".format(key, value))
            value = False
        return value

    def source_files(self, options):
        key = 'source'
        cmd = getattr(options, key, []) or []
        fil = self._hash.get(key, [])

        for value in fil + cmd:
            FILE = value
            error = self.read(FILE)
            if error is None:
                print >>sys.stderr, "Config: sourced {0} for more options".format(FILE)
            else:
#6/1            raise Exception, error
                raise Exception(error)

    def merge_options(self, options):
        keys = sorted(self._conv.keys())
        for key in keys:
            if self._debug: print "merge_options: ", key,
            if self._append.get(key, None) == 'replay': continue
            if getattr(options, key, None) is None: #not in cmd line options
                value = self._hash.get(key, None)
                if value is None:                   #not in config file options either
                    setattr(options, key, self._default.get(key, None))
                    if self._debug: print "default value {0} ".format(getattr(options, key))
                else:                               # use config but convert type
                    setattr(options, key, value)
                    if self._debug: print "config_file value {0} ".format(getattr(options, key))
            elif key in self._append:             # merge both lists
                configs = self._hash.get(key, None)
                if configs is not None:
                    cmds = getattr(options, key)
                    configs.extend(cmds)
                    setattr(options, key, configs)
                    if self._debug: print "config_file+cmd_line value {0} ".format(getattr(options, key))
                else:
                    if self._debug: print "cmd_line value {0} ".format(getattr(options, key))
#1127
                    if key not in self._show: self._show.append(key)
            else:
                if self._debug: print "cmd_line value {0} ".format(getattr(options, key))
#1127
                if key not in self._show: self._show.append(key)

    def help(self):
        self._parser_.print_help()

    def log_options(self, options):
#1127        keys = self._options      # in order add_options are executed
        keys = self._show[:]
        if 'password' in keys:
            keys.remove('password')
        for key in keys:
#1/23       if key in self._syn: continue
            key = self._syn.get(key, key)
            if self._append.get(key, None) == 'replay': continue
#1127            if key == 'password': continue
            if key == 'experiments': continue

            value = getattr(options, key, None)
            conv = self._conv[key]
            if key == 'log': value = options.logroot + (".log.txt" if options.log == '' else '')
#1127            elif value == self._default.get(key, None): continue

            if conv == 'string':
                value = repr(value)
            elif value is None:
                value = 'None'
            elif conv == 'true' and value is True:
                value = 'Set'
            elif conv == 'false' and value is False:
                value = 'Set'
            elif value == '':
                value = '""'
            printnl("{0:>20}: {1}".format(key, value))


################################################################
##                         Boolean type                       ##
################################################################

from copy import copy

def check_bool(option, opt, value):
    if value.lower() in  ('true',  '1'):
        return True
    if value.lower() in  ('false', '0'):
        return False
    raise OptionValueError("option {0}: invalid True/False value: {1}".format(opt, value))

class Boolean(Option):
    TYPES = Option.TYPES + ("boolean",)
    TYPE_CHECKER = copy(Option.TYPE_CHECKER)
    TYPE_CHECKER["boolean"] = check_bool
