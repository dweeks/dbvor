#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from marker   import Marker
from member   import Member
from util     import NoTrace, printnl, printn, log, gzopen

ORIGIN  = 1

class Input(object):
    def __init__(self, dbi):
        self._marker_     =  Marker(dbi)
        self._member_     =  Member(dbi)
        self._snp_stat    =  {}
        self._options     =  None

        self._snp         =  None
        self._marker_id   =  None
        self._col         =  None
        self._id          =  None
        self._pedigree    =  None
        self._person      =  None

        self._line_no     =  0
        self._line        =  None

        self._old_snp     = -1
        self._old_line_no =  0
        self._this_line   =  0
        self._log_push    = None

    def pr_widesh(self, *err, **kw):
        tagv = kw.get('tag', self._log_push)
        log.newrecord(tag=tagv)
        if self._old_line_no != self._line_no:
            if self._options.debug:
                printn('\nLine {0:8d}|'.format(self._line_no), tag=tagv)
                printn(self._line, tag=tagv)
            self._this_line = 0
            self._old_snp  = -1
        self._old_line_no = self._line_no
        if self._col >= 0 and self._old_snp != self._col:
            if self._options.debug:
                s = "pedigree[@{0}]:person[@{1}] ({2}:{3}) -> {4}; snp@{5:d}[{6}]->{7}".format(self._options.pedigree, self._options.person, self._pedigree, self._person, self._id, self._col+self._options.marker, self._snp, self._marker_id)
            else:
                s = "\nLine {0:8d}: pedigree:person {1}:{2} [snp {3}]".format(self._line_no, self._pedigree, self._person, self._snp)
            self._old_snp = self._col
            printnl(s, tag=tagv)
        elif self._old_snp != self._id:
            if self._options.debug:
                s = "pedigree[@{0}]:person[@{1}] ({2}:{3}) -> {4}".format(self._options.pedigree, self._options.person, self._pedigree, self._person, self._id)
            else:
                s = "\nLine {0:8d}: pedigree:person {1}:{2}".format(self._line_no, self._pedigree, self._person)
            self._old_snp = self._id
            printnl(s, tag=tagv)
        self._this_line += 1
        for e in err: printnl(e, line=self._this_line, tag=tagv)

    def common(self, dbi, options):
        self._options     = options

    def process_half(self, FILE, dbi, options, cmd):
        self.common(dbi, options)
        File = gzopen(FILE)

        self._line_no = 1
        header = File.readline().rstrip().split(options.sep)
#6/1    snps = map(lambda(x):x.strip(), header[(options.marker-ORIGIN):])
        snps = [x.strip() for x in header[(options.marker-ORIGIN):] ]
        self._snp_stat = dict((snp, {}) for snp in snps)
        error = options.error
        marker_ids = self._marker_.kers(snps, options)
        field_cnt = (options.marker-ORIGIN)+len(marker_ids)
        if options.debug > 0:
            printnl('')
            printnl("SNP names: {0}".format(snps))
            printnl('')
            printnl("SNP ids: {0}".format(marker_ids))
            printnl('')
        if error != options.error:
            if options.debug:
                printnl("error converting markers to marker ids; aborting")
            self._marker_.stat({}, options)
#6/1        raise NoTrace, "aborting!!"
            raise NoTrace("aborting!!")

        if options.commit: log.push('insert', clear='genotypehalf')
        log.push('genotypehalf', newrecord=True)
#6/1    cmd._log_push = 'genotypehalf'
        self._log_push = 'genotypehalf'
        for self._line in File:
            data = self.get_line(self._line, field_cnt, self.pr_widesh, options)
            if data is None: continue

            self.get_person(data, options)
            (self._id, self._col)  = ('...', -1)
            self._id               = self._member_.member(self._pedigree, self._person, options, self.pr_widesh)
            if self._id is None:
                continue
#            if options.verbose == 1 and options.debug == 0:
#                self.pr_widesh()

            self._col = 0
            for ele in data[(options.marker-ORIGIN):]:
                self._snp       = snps[self._col]
                self._marker_id = marker_ids[self._col]
                self._snp_stat[self._snp][ele] = self._snp_stat[self._snp].get(ele, 0) + 1
                ele = ele.strip()
                Len = len(ele)
                if Len == 0 or ele in ('0', ):
                    allele = ('0', '0')
                elif Len == 1:
                    allele = (ele, ele)
                elif Len == 2:
                    allele = tuple(ele)
                else:
                    ele2 = ele.split()
                    if len(ele2) == 2:
                        allele = tuple(ele2)
                    else:
                        ele2 = ele.split('/')
                        if len(ele2) == 2:
                            allele = tuple(ele2)
                        else:
                            options.error += 1
                            self.pr_widesh("can not decode {0}".format(ele))
                            continue
                    printnl("map {0} to {1}".format(ele, allele))
                cmd.process_one(options, self._id, self._marker_id, allele, self.pr_widesh)
                self._col += 1
        File.close()
        cmd.process_last(options, self._member_, self._marker_)
        log.pop('genotypehalf')
        if options.commit: log.pop('insert')
        cmd.stat(options, cmd, self._member_, self._marker_, self._snp_stat)

    def process_wide(self, FILE, dbi, options, cmd):
        self.common(dbi, options)
        File = gzopen(FILE)
        self._line_no = 1
        header = File.readline().rstrip().split(options.sep)
#6/1    snps  = map(lambda(x):x.strip(), header[(options.marker-ORIGIN):])
        snps  = [x.strip() for x in header[(options.marker-ORIGIN):] ]
        usnps = set(snp[:-2] for snp in snps)
        self._snp_stat = dict((snp, {}) for snp in usnps)
        error = options.error
        marker_ids = self._marker_.markers(snps, options)
        field_cnt = (options.marker-ORIGIN)+len(marker_ids)
        if options.debug > 0:
            printnl('')
            printnl("SNP names: {0}".format(snps))
            printnl("SNP ids: {0}".format(marker_ids))
            printnl('')
        if error != options.error:
            if options.debug:
                printnl("Error converting markers to marker ids; aborting")
            self._marker_.stat({}, options)
#6/1        raise NoTrace, "aborting!!"
            raise NoTrace("aborting!!")

        if options.commit: log.push('insert', clear='genotypewide')
        log.push('genotypewide', newrecord=True)
#6/1    cmd._log_push = 'genotypewide'
        self._log_push = 'genotypewide'
        for self._line in File:
            data = self.get_line(self._line, field_cnt, self.pr_widesh, options)
            if data is None: continue

            self.get_person(data, options)
            (self._id, self._col)    = ('...', -1)
            self._id                 = self._member_.member(self._pedigree, self._person, options, self.pr_widesh)
            if self._id is None:
                continue
#            if options.verbose == 1:
#                self.pr_widesh()

            self._col = 0
            for ele in [(data[j].strip(), data[j+1].strip()) for j in range((options.marker-ORIGIN), len(data), 2)]:
                self._snp       = snps[self._col][:-2]
                self._marker_id = marker_ids[self._col]
                self._snp_stat[self._snp][ele[0]] = self._snp_stat[self._snp].get(ele[0], 0) + 1
                self._snp_stat[self._snp][ele[1]] = self._snp_stat[self._snp].get(ele[1], 0) + 1
                cmd.process_one(options, self._id, self._marker_id, ele, self.pr_widesh)
                self._col += 2
        File.close()
        cmd.process_last(options, self._member_, self._marker_)
        log.pop('genotypewide')
        if options.commit: log.pop('insert')
        cmd.stat(options, cmd, self._member_, self._marker_, self._snp_stat)

    def pr_long(self, *err, **kw):
        tagv = kw.get('tag', self._log_push)
        log.newrecord(tag=tagv)
        if self._old_line_no != self._line_no:
            if self._options.debug:
                printn("\nline {0:8d}|".format(self._line_no), tag=tagv)
                printn(self._line, tag=tagv)
            if self._marker_id:
                if self._options.debug:
                    s = "pedigree[@{0}]:person[@{1}] ({2}:{3}) -> {4}; snp[{5}]->{6}".format(self._options.pedigree, self._options.person, self._pedigree, self._person, self._id, self._snp, self._marker_id)
                else:
                    s = "\nline {0:8d}: pedigree:person {1}:{2} [snp {3}]".format(self._line_no, self._pedigree, self._person, self._snp)
            else:
                if self._options.debug:
                    s = "pedigree[@{0}]:person[@{1}] ({2}:{3}) -> {4}".format(self._options.pedigree, self._options.person, self._pedigree, self._person, self._id)
                else:
                    s = "\nline {0:8d}: pedigree:person {1}:{2}".format(self._line_no, self._pedigree, self._person)
            printn(s, tag=tagv)
            self._old_line_no = self._line_no
            self._this_line = 0
        self._this_line += 1
        for e in err: printnl(e, line=self._this_line, tag=tagv)

    def process_long(self, FILE, dbi, options, cmd):
        self.common(dbi, options)
        File = gzopen(FILE)
        field_cnt = (options.marker-ORIGIN)+1+2

        if options.commit: log.push('insert', clear='genotypelong')
        log.push('genotypelong', newrecord=True)
#6/1    cmd._log_push = 'genotypelong'
        self._log_push = 'genotypelong'
        for self._line in File:
            data = self.get_line(self._line, field_cnt, self.pr_long, options)
            if data is None: continue

            self.get_person(data, options)
            (self._id, self._marker_id)  = ('...', None)
            self._id                     = self._member_.member(self._pedigree, self._person, options, self.pr_long)
            if self._id is None:
                continue

            snp = data[(options.marker-ORIGIN)]
            self._snp = snp
            error = options.error
            self._marker_id = self._marker_.marker(self._snp, options)
            if error != options.error:
                self.pr_long("Error converting markers to marker ids; skipping marker")
                continue
            elif self._marker_id is None:
                continue

            ele = data[(options.marker-ORIGIN)+1].strip(), data[(options.marker-ORIGIN)+2].strip()
            if snp not in self._snp_stat:
                self._snp_stat[snp] = {}
            self._snp_stat[snp][ele[0]] = self._snp_stat[snp].get(ele[0], 0) + 1
            self._snp_stat[snp][ele[1]] = self._snp_stat[snp].get(ele[1], 0) + 1
            cmd.process_one(options, self._id, self._marker_id, ele, self.pr_long)
        File.close()
        cmd.process_last(options, self._member_, self._marker_)
        log.pop('genotypelong')
        if options.commit: log.pop('insert')
        cmd.stat(options, cmd, self._member_, self._marker_, self._snp_stat)

    def pr_xlong(self, *err, **kw):
        tagv = kw.get('tag', self._log_push)
        log.newrecord(tag=tagv)
        if self._options.debug:
            if self._old_line_no != self._line_no:
                printnl("\nLine {0:8d}|".format(self._line_no), tag=tagv)
                printn(self._line, tag=tagv)
            self._old_line_no = self._line_no
            self._this_line = 0
        else:
            printn("\nLine {0:8d}:".format(self._line_no), tag=tagv)
        self._this_line += 1
        for e in err: printnl(e, line=self._this_line, tag=tagv)

    def process_xlong(self, FILE, dbi, options, cmd):
        self.common(dbi, options)
        Id2Person     = {}
        File = gzopen(FILE)

        if options.commit: log.push('insert', clear='genotypexlong')
        log.push('genotypexlong', newrecord=True)
        self._log_push = 'genotypexlong'
        for self._line in File:
    #hmm    data = line.rstrip().split(options.sep)
            options.sep = None
            data = self.get_line(self._line, 4, self.pr_xlong, options)
            if data is None:
                continue

            self._id = data[0]
            if self._id in Id2Person:
                self._person = Id2Person[self._id]
            else:
                self._person = dbi.select_one("select person from members where id = ?", self._id)
                if self._person is not None:     self._person = self._person[0]
                Id2Person[self._id] = self._person

#           self._id        = long(data[0])
            self._marker_id = long(data[1])
            ab              = data[2:4]
            cmd.process_one(options, self._id, self._marker_id, ab, self.pr_xlong)
        File.close()
        log.pop('genotypexlong')
        if options.commit: log.pop('insert')
        cmd.process_last(options, self._member_, self._marker_)

    def get_line(self, line, field_cnt, pr_context, options):
        self._line_no += 1
        line = line.rstrip()
        if line == '' or line.startswith('#'):
            return None
        data = line.split(options.sep)
        if len(data) != field_cnt:
            printn("\nERROR: Line {0:8d}|{1}".format(self._line_no, line), tag=True)
            printnl("Incorrect number of fields; needed {0:d} fields got {1:d}:\n{2}".format(field_cnt, len(data), data), line=1, tag=True)
            options.error += 1
            return None
        return data

    def get_person(self, data, options):
        self._person = data[(options.person-ORIGIN)]
        if options.pedigree:
            self._pedigree = data[(options.pedigree-ORIGIN)]
        else:
            self._pedigree = ''
