#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

#import re

from dbVORlib.util             import printnl

from dbVORlib.table.dbtable    import DBTable
from dbVORlib.table.members    import FAMParse


class PreSample(DBTable):
    def __init__(self, db):
        super(PreSample, self).__init__(db)
        self._fields = None
        self._name   = None
        self._im     = None

    def prolog(self, dbi, options):
        super(PreSample, self).prolog(dbi, options)
        self.validate_technology(options, create=options.commit, required=True)
        self.validate_experiment(options, create=options.commit, required=True)
        if options.error == 0:
            if options.commit:
                cmd  = 'insert into samples (projectid, techid, experiment, active, subject, activefwd, pedigree, person, plate, well, misc, sample)'
                fmt  = "({0:d}, {1:d}, {2:d}, {3:d}, ".format(options.project_id, options.tech_id, options.experiment_id, options.active)
                fmt += "'{0}', {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')"
                self._im = self._dbi_.insert_many(cmd, fmt, 100)
            return True
        return False

    def epilog(self, dbi_pylint, options):
        if options.commit:
            self._im()

    def header(self, File, dbi, options):
        if File is None:
            self._fields = 0
            return
        header = File.readline().strip().split(options.sep)
        self._fields = len(header)
        if options.verbose:
            printnl(header)

    def do_row(self, fields, dbi, options):
        fields = fields[:8]  # ... truncate
        fields[1] = int(fields[1])
        self._name = fields[-1]
        self.row_util(fields, fields[-1], dbi, options)

    cmds = "select projectid, techid, experiment, active, subject, activefwd, pedigree, person, plate, well, misc, sample, id from samples where sample = ? and experiment = ?"
    def select(self, fields, options):
#       import pdb;pdb.set_trace()
        return self._dbi_.select_all(self.cmds, fields[-1], options.experiment_id)

    cmdd = "delete from samples where id = ?"
    def delete(self, fields_pylint, options):
        self._dbi_.execute(self.cmdd, fields_pylint[-1])

    cols = (("subject",0,4), ("activefwd",1,5), ("pedigree",2,6), ("person",3,7),
            ("plate",4,8),   ("well",5,9),      ("misc",6,10),    ("sample",7,11),
            ("active",-1,3))
    def update(self, fields, db, options):
        self.update_util(fields, db, self.cols, "samples", db[0][12], options)

    def insert(self, fields_pylint, options):
        self._im(fields_pylint)

    def cmp(self, rec, fields, options):
        return cmp(rec[4], fields[0])  or cmp(rec[5], int(fields[1])) or cmp(rec[6], fields[2]) \
            or cmp(rec[7], fields[3])  or cmp(rec[8], fields[4])  or cmp(rec[9], fields[5]) \
            or cmp(rec[10], fields[6]) or cmp(rec[11], fields[7])

#   NUMBER = re.compile(r"^-?((\d+)|(\d+.\d+)|(.\d+))$")
    def validate(self, fields, name, options):
        return True

class SSample(PreSample):

    def add_options(self, config):
        super(SSample, self).add_options(config)
        config.add_option("--active", type="int", default=1, help="value for active flag")

    def hint(self):
        print "\nINPUT FILE FORMAT: subject_id active_value pedigree person plate well misc sample_id"


class FAMSample(FAMParse, PreSample):

    def header(self, File, dbi, options):
        if not options.noheader:
            super(FAMSample, self).header(None, dbi, options)

    def do_row(self, fields, dbi, options):
        super(FAMSample, self).do_row(fields, dbi, options)
        self._name = self._pedigree + ':' + self._person
        sample = self._pedigree + '_' + self._person
        new = sample, options.activefwd, self._pedigree, self._person, '', '', '', sample
        self.row_util(new, self._name, dbi, options)

    def pr_rec(self, fields, options, db=False):
        if db:
            if options.verbose:
                return "sample: {0[11]!r}, experiment: {1}, pedigree: {0[6]}, person: {0[7]}".format(fields, options.experiment_id)
            return "debug sample: {0[10]!r} {0}".format(fields)
        else:
            if options.verbose:
                return "sample: {0[7]!r}, experiment: {1}, pedigree: {0[2]}, person: {0[3]}".format(fields, options.experiment_id)
            return "debug sample: {0[7]!r} {0}".format(fields)

    def cmp(self, rec, fields, options):
        return cmp(rec[4], fields[0]) or cmp(rec[5], int(fields[1])) or cmp(rec[6], fields[2]) \
            or cmp(rec[7], fields[3]) or cmp(rec[-2], fields[-1]) \
            or rec[2] != options.experiment_id
