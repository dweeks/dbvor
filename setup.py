#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================


# need root to install to site-package and bin
#    scripts install to /usr/local/bin

# recipes
# wget https://bootstrap.pypa.io/get-pip.py 
# python get-pip.py

# pip install setuptools
# pip install wheel

# python setup.py bdist_egg
# python setup.py bdist_wheel

# pip install dbVOR
# pip install --no-index -f "dist" dbVOR
# pip install -v -v -v  'git+https://bitbucket.org/dweeks/dbvor.git#egg=master'
# pip install -v -v -v  'git+https://bitbucket.org/dweeks/dbvor.git#egg=stable'
# pip uninstall dbVOR

# pip wheel <path>; pip install --no-index -f wheelhouse dbVOR

# ls -l /Library/Python/2.7/site-packages

from setuptools import setup
#from setuptools.dist import Distribution
import sys

if sys.version_info < (3,):
    cvt = {}
else:
    cvt = {
#   add these someday
#   'test_suite':        [],

#   'use_2to3'               : True,
#   'convert_2to3_doctests'  : [],
#   'use 2to3_fixers'        : [],
#   'use 2to3_exclude_fixers': [],
}

setup(
    name             = 'dbvor',
    version          = '1.12',
    description      = 'dbVOR',
    long_description = """dbVOR: A database system for importing pedigree, phenotype and genotype data and exporting selected subsets.""",

    author           = 'rvb5',
    author_email     = 'rvb5@pitt.edu',
#   maintainer       = 'weeks',
#   maintainer_email = 'weeks@pitt.edu',

    url              = 'http://watson.hgen.pitt.edu/register/docs/dbvor.html',

#   requires         = ['MySQL-python'],  # syntax currently does not allow -

    package_dir      = {'' : '.'},
    packages         = ['dbVORlib', 'dbVORlib.select', 'dbVORlib.table'],

    scripts          = ['bin/dbvor', 'bin/geno',   'bin/genout', 'bin/genoi',
                        'bin/genoa', 'bin/genote', 'bin/genox', 'bin/genok'],

    include_package_data = True,
    package_data         = {'' : ['doc/*/*.{pdf,jpg,png}', 'sql/*']},
#   package_data        = {'dbVORlib' : ['*.BAK']}, # this atleast works; does what it says
#   data_files           = [('', ['README.md', 'LicenseDbVOR.txt', 'LicenseGNUGplv3.txt'])],

    license          = 'GPLv3+',

    classifiers      = [
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2 :: Only',
        'Topic :: Database :: Front-Ends',
        'Topic :: Scientific/Engineering :: Bio-Informatics'
        ],

    keywords         = ['database', 'biology', 'genotypes', 'GWAS'],

    **cvt
)
