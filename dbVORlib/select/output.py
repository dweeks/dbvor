#### bed performance
#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import sys
import struct

from   dbVORlib.util     import NoTrace, printnl, gzopen, read_fields

class Output(object):
    def __init__(self, db, select, markers, traits, members, mXm):
        self._dbi_              = db
        self._select_           = select
        self._selected_markers_ = markers
        self._selected_traits_  = traits
        self._selected_members_ = members
        self._member_X_marker   = mXm
        self._bytes             = None
        self._cache             = {}
        self._GARM_hack         = {}

    def add_options(self, config):
        config.add_option("--output", type="string",
                          help="output file name (or prefix)")
        config.add_option("--type", "--file_type", type="string",
                          help="output file type: long, wide, half, ped, tped, lgen, bed or mega2")
        config.add_option("--short", action="store", type="boolean", default=False,
                          help="for long type omit empty samples")
        config.add_option("--separator", "--OFS", type="string", default="\t",
                          help="output file field separator")

        config.add_option("--onetwo", action="store", type="boolean", default=False,
                          help="use 1/2 output for easier comparison to old data")
        config.add_option("--funny", action="store", type="boolean", default=False,
                          help="use F (conflict), Q (no trait), Z (no sample) alleles")

    def files(self, options):
        if options.type in ('long', 'wide', 'half'):
            return [options.output + '.txt']
        if options.type == 'mega2':
            return ['pedin.'+ options.output, 'map.'+ options.output, 'names.'+ options.output]

        ans = []
        if options.type == 'bed':
            mapfx = '.bim'
        elif options.type == 'tped':
            mapfx = '.tped'
        else:
            mapfx = '.map'
        ans.append(options.output + mapfx)

        if options.type in ('lgen', 'bed'):
            ped = '.fam'
        elif options.type == 'tped':
            ped = '.tfam'
        else:
            ped = '.ped'
        ans.append(options.output + ped)

        if options.type == 'lgen':
            ans.append(options.output + '.lgen')
        if options.type == 'bed':
            ans.append(options.output + '.bed')
        return ans

    def output_local(self, options):
        if options.output is None:
            File = sys.stdout
        else:
            FILE = options.dir + options.output + '.txt'
            File = gzopen(FILE, 'w')

        if options.type == 'long':
            self.output_long(File, options)
        elif options.type == 'wide' or options.type == 'half':
            self.output_wide(File, options)

        if options.output:
            File.close()

    def output_wide(self, File, options):
        self.output_wide_header(File, options)

        for memrec in self._selected_members_:
            self.output_wide_member(File, memrec[0], options)

    def output_wide_header(self, File, options):
        ofs = options.separator
        if True: # options.linkage:
            File.write(ofs.join(("#PEDIGR", "PERSON", "FATHER", "MOTHER", "SEX")) + ofs)
            File.write(ofs.join(self._selected_traits_.names()) + ofs)
        else:
            File.write(ofs.join(("#PEDIGR", "PERSON")) + ofs)

        if options.type == 'half':
            File.write(ofs.join(self._selected_markers_.name(marker_id) for marker_id in self._selected_markers_))
        else:
            sep = "{0}_1" + ofs + "{1}_2"
            File.write(ofs.join((sep.format(self._selected_markers_.name(marker_id), self._selected_markers_.name(marker_id)) for marker_id in self._selected_markers_)))
#       if options.linkage:
#           File.write(ofs + "Type\n")
#       else:
        File.write("\n")

    def output_wide_member(self, File, member_id, options):
        ofs = options.separator
        sep = "{0[0]}{0[1]}" if options.type == 'half' else "{0[0]}" + ofs + "{0[1]}"
        MT = ('Z', 'Z') if options.funny else ( '0',  '0')

        if True: # options.linkage:
            File.write(ofs.join(str(x) for x in self._selected_members_.linkage(member_id)) + ofs)
            File.write(ofs.join(self._selected_traits_.values(member_id, options)) + ofs)
        else:
            File.write(ofs.join(self._selected_members_.name(member_id)) + ofs)

        File.write(ofs.join( sep.format(self._member_X_marker.get(member_id, {}).get(marker_id, MT)[0:2]) for marker_id in self._selected_markers_))
#       if options.linkage:
#           File.write(ofs + "{}\n".format(self._selected_members_.type(member_id)))
#       else:
        File.write("\n")

    def output_long(self, File, options):
        self.output_long_header(File, options)
        self.output_long_data(File, options)

    def output_long_data(self, File, options):
        for memrec in self._selected_members_:
            member_id = memrec[0]
            if member_id not in self._member_X_marker: continue

            self.output_long_member(File, member_id, options)

    def output_long_header(self, File, options):
        ofs = options.separator
        File.write(ofs.join(("#PEDIGR", "PERSON", "SNP", "ALLELE1", "ALLELE2")) + "\n")

    def output_long_member(self, File, member_id, options):
        ofs = options.separator
        pedigree, person = self._selected_members_.name(member_id)
        if options.short:
# marker_map[marker_id] === [22, 14886561, 0, 1, 14131L, 'rs2226519']
            for marker_id in self._selected_markers_:
                allele = self._member_X_marker[member_id].get(marker_id, None)
                if allele is None:
                    continue
                if options.onetwo:
                    if allele[0] == 'W' and allele[1] == 'W': continue
                File.write(ofs.join((pedigree, person, self._selected_markers_.name(marker_id))) + ofs)

                File.write(ofs.join(allele[0:2]))
                File.write("\n")
        else:
            MT = ('Z', 'Z') if options.funny else ( '0',  '0')
# marker_ids === (14131L, 'rs2226519')
            for marker_id in self._selected_markers_:
                marker_name = self._selected_markers_.name(marker_id)
                File.write(ofs.join((pedigree, person, marker_name)) + ofs)
                File.write(ofs.join(self._member_X_marker[member_id].get(marker_id, MT)[0:2]))
                File.write("\n")

    def output_mega2(self, options):
        self.output_mega2_pedin(options)
        self.output_mega2_map(options)
        self.output_mega2_names(options)

    def output_mega2_pedin(self, options):
        FILE = options.dir + "pedin." + options.output
        File = gzopen(FILE, 'w')
        self.output_mega2_pedin_header(File, options)

        for memrec in self._selected_members_:
            self.output_mega2_pedin_member(File, memrec[0], options)

        File.close()

    def output_mega2_pedin_header(self, File, options):
#       code below was once in output_mega2_pedin ... BUT output_person_order_member needs the initialization also
        if options.project == 'GARM':
            typ = self._dbi_.select_one("select id from traitmeta where name = 'type' and projectid = ?", options.project_id)
            if typ is None:
                raise NoTrace("GARM needs a type trait for mega2!!!")
            tmp = self._dbi_.select_all("select member, value from traits where trait = ?", typ[0])
            self._GARM_hack = dict(tmp)

        ofs = options.separator
        sep = "{0}.{1}.1" + ofs + "{2}.{3}.2"

        File.write(ofs.join(("Pedigree", "ID", "Father", "Mother", "Sex")) )

        for tname, ttype in (self._selected_traits_.traittype(trait_id) for trait_id in self._selected_traits_):
            File.write(ofs + tname.replace('.', '') + '.' + ttype)
        File.write(ofs )

        names = []
        for marker_id in self._selected_markers_:
            name = self._selected_markers_.name(marker_id)
            chrm = self._selected_markers_.chrm(marker_id)
            sfx = self._selected_markers_.int2Mega2MapNames(chrm)
            names.append(sep.format(name, sfx, name, sfx))
        File.write(ofs.join(names) + ofs)
        File.write(ofs.join(("PedID", "PerID")) + "\n")


    def output_mega2_pedin_member(self, File, member_id, options):
        ofs = options.separator
        sep = "{0[0]}" + ofs + "{0[1]}"
        MT = ('NA', 'NA')

        linkage = [str(x) for x in self._selected_members_.linkage(member_id)]
        pp      = linkage[:2]
        for i in range(1, 4):
            # merge pedigree into id
            if linkage[i] != '0':
                linkage[i] = linkage[0] + '_' + linkage[i]
        if options.project == 'GARM':
            if self._GARM_hack.get(member_id, None) == '2':
                linkage[0] = linkage[1] = linkage[1]
        File.write(ofs.join(linkage) )

#       File.write(ofs.join(self._selected_traits_.mega2_values(member_id, options)) )
#       File.write(ofs)
        for val in self._selected_traits_.mega2_values(member_id, options):
            File.write(ofs + val)
        File.write(ofs)

        if ('0', '0') not in self._cache:
            self._cache[('0', '0')] = sep.format(MT)
        alleles = []
        for allele in (self._member_X_marker.get(member_id, {}).get(marker_id, MT)[0:2] for marker_id in self._selected_markers_):
            if allele in self._cache:
                alleles.append(self._cache[allele])
            else:
                ans = sep.format(allele)
                self._cache[allele] = ans
                alleles.append(ans)
        File.write(ofs.join(alleles) + ofs)
        File.write(ofs.join(pp) + "\n")

    def output_mega2_map(self, options):
        FILEO = options.dir + "map." + options.output
        FileO = gzopen(FILEO, 'w')
        if options.gmi_position:
            printnl("Using provided gmi positions \"{0}\" for mega2 map".format(options.gmi_position))
            rs2id = dict(((self._selected_markers_.name(marker_id), marker_id) for marker_id in self._selected_markers_))
            line_no = 0
            line = ""
            fields = []
            FILEI = options.dir + options.gmi_position
            FileI = gzopen(FILEI)
            header = FileI.readline()
            FileO.write(header)
            for line_no, line, fields in read_fields(FILEI, File=FileI, cnt=line_no):
                if rs2id.get(fields[2], False):
                    FileO.write(line + "\n")
            FileI.close()
        else:
            self.output_mega2_map_header(FileO, options)

            self.output_mega2_map_member(FileO, options)
        FileO.close()

    def output_mega2_map_header(self, File, options):
        ofs = options.separator
        File.write(ofs.join(("Chromosome", "Map.k.a", "Name", "Map.k.m", "Map.k.f", "Pos.p")) + "\n")

#GMI  chr bp cm marker_id, marker
#SNP  chr bp cm blk off {ab}  marker_id, marker
    def output_mega2_map_member(self, File, options):
        ofs = options.separator
        pos = 1
        cm  = 2
#       fmt = "{0} {1:.6f} {5} {2:.6f} {3:.6f} {4:d}\n".replace(" ", ofs)
        fmt = "{0} {1} {5} {2} {3} {4}\n".replace(" ", ofs)
        for snps in self._selected_markers_.chrm_list():
            for snp in snps:
                ch = self._selected_markers_.int2Mega2Map(snp[0])
                ga = snp[cm]
                gm = snp[cm+1]
                gf = snp[cm+2]
                ps = snp[pos]
                nm = self._selected_markers_.name(rec=snp)
                ch =  "U" if ch is None else "{0}".format(ch)
                ga = "NA" if ga is None else "{0:.6f}".format(ga)
                gm = "NA" if gm is None else "{0:.6f}".format(gm)
                gf = "NA" if gf is None else "{0:.6f}".format(gf)
                ps = "NA" if ps is None else "{0:d}".format(ps)
                File.write(fmt.format(ch, ga, gm, gf, ps, nm))

    def output_mega2_names(self, options):
        FILE = options.dir + "names." + options.output
        File = gzopen(FILE, 'w')
        self.output_mega2_names_header(File, options)

        self.output_mega2_names_member(File, options)
        File.close()

    def output_mega2_names_header(self, File, options):
        ofs = options.separator
        File.write(ofs.join(("Type", "Name")) + "\n")

    def output_mega2_names_member(self, File, options):
        ofs = options.separator
        for trait_id in self._selected_traits_:
            tname, ttype = self._selected_traits_.traittype(trait_id)
            tname = tname.replace('.', '')
            File.write(ofs.join((ttype, tname)) + "\n")

        # different from the definition above.  don't propagate 'U' here
        for marker_id in self._selected_markers_:
            name = self._selected_markers_.name(marker_id)
            chrm = self._selected_markers_.chrm(marker_id)
            sfx = self._selected_markers_.int2Mega2MapNames(chrm)
            File.write(sfx + ofs + name + "\n")

class OutputPlink(Output):
    def __init__(self, db, select, markers, traits, members, mXm):
        super(OutputPlink, self).__init__(db, select, markers, traits, members, mXm)

    def add_options(self, config):
        super(OutputPlink, self).add_options(config)

    def output_plink(self, options):
        if options.type == 'bed':
            self.output_bed(options)
            self.must_be_biallelic(options)
        self.output_map(options)
        self.output_phe(options)
        self.output_ped(options)
        if options.type == 'lgen':
            self.output_lgen(options)

#GMI  chr bp cm marker_id, marker
#SNP  chr bp cm blk off {ab}  marker_id, marker
    def output_map(self, options):
        ofs = options.separator
        sep = "{0[0]}" + ofs + "{0[1]}"
        fmt = "{0} {1} {2:.3f} {3:d}".replace(" ", ofs)
        MT = ('Z', 'Z') if options.funny else ( '0',  '0')
        bp = 1
        cm = 2

        FILE = options.dir + options.output + ('.bim' if options.type == 'bed' else ('.tped' if options.type == 'tped' else '.map'))
        File = gzopen(FILE, 'w')

        for snps in self._selected_markers_.chrm_list():
            for snp in snps:
                chrm = self._selected_markers_.int2chrm(snp[0]) if options.type != 'bed' else snp[0]
                bpos   = -9 if snp[1] is None else snp[1]
                genpos = -9 if snp[2] is None else snp[2]
                File.write(fmt.format(chrm, self._selected_markers_.name(rec=snp), genpos, bpos))
                marker_id = self._selected_markers_.snp2id(snp)
                if options.type == 'tped':
                    File.write( ofs )
                    File.write(ofs.join( (sep.format(self._member_X_marker.get(memrec[0], {}).get(marker_id, MT)[0:2]) for memrec in self._selected_members_)))
                elif options.type == 'bed':
                    File.write( ofs )
                    order = self._select_.alleleorder(marker_id)
                    File.write(order[0] + ofs + order[1])
                File.write("\n")

        File.close()

        if options.type not in ('ped', 'tped', 'lgen', 'bed'):
            return

        FILE = options.dir + options.output + '.excl'
        File = gzopen(FILE, 'w')
        FILX = options.dir + options.output + '.xsnp'
        Filx = gzopen(FILX, 'w')
        for snps in self._selected_markers_.chrm_list():
            for snp in snps:
                marker_id = self._selected_markers_.snp2id(snp)
                order = self._select_.alleleorder(marker_id)
                if order[0] == '?':
                    File.write("{0}".format(self._selected_markers_.name(rec=snp)))
                    File.write(ofs + order[0] + ofs + order[1])
                    File.write("\n")
                if len(order) > 2:
                    Filx.write("{0}".format(self._selected_markers_.name(rec=snp)))
                    Filx.write(ofs + ofs.join(order))
                    Filx.write("\n")
        File.close()
        Filx.close()

    def output_phe(self, options):
        FILE = options.dir + options.output + '.phe'
        File = gzopen(FILE, 'w')

        self.output_phe_header(File, options)
        for memrec in self._selected_members_:
            self.output_phe_member(File, memrec[0], options)

        File.close()

    def output_phe_header(self, File, options):
        ofs = options.separator
        File.write(ofs.join(("FID", "IID")) + ofs)
        File.write(ofs.join(self._selected_traits_.names()))
        File.write("\n")

    def output_phe_member(self, File, member_id, options):
        ofs = options.separator

        File.write(ofs.join(self._selected_members_.name(member_id)) + ofs)
        File.write(ofs.join(self._selected_traits_.values(member_id, options)))
        File.write("\n")

    def output_ped(self, options):
        FILE = options.dir + options.output + ('.fam' if options.type in ('lgen', 'bed') else ('.tfam' if options.type == 'tped' else '.ped'))
        File = gzopen(FILE, 'w')

##      self.output_ped_header(File, options)  # tends to confuse plink: tped in general, and lgen for .bed
        for memrec in self._selected_members_:
            self.output_ped_member(File, memrec[0], options)

        File.close()

    def output_ped_header(self, File, options):
        ofs = options.separator
        sep = "{0}_1" + ofs + "{1}_2"
        File.write(ofs.join(("#PEDIGR", "PERSON", "FATHER", "MOTHER", "SEX")) + ofs)
        File.write(self._selected_traits_.first() )
        if options.type == 'ped':
            File.write( ofs )
            File.write(ofs.join((sep.format(self._selected_markers_.name(marker_id), self._selected_markers_.name(marker_id)) for marker_id in self._selected_markers_)))
        File.write("\n")

    def output_ped_member(self, File, member_id, options):
# 128 sec
        ofs = options.separator
        sep = "{0[0]}" + ofs + "{0[1]}"
        MT = ('Z', 'Z') if options.funny else ( '0',  '0')

        File.write(ofs.join(str(x) for x in self._selected_members_.linkage(member_id)) + ofs)
        vals = self._selected_traits_.values(member_id, options)
        if vals:
            File.write( vals[0] )
        else:  # plink bed format wants 6 columns!!
            File.write( '0' )

        if options.type == 'ped':
            File.write( ofs )
#           File.write(ofs.join( (sep.format(self._member_X_marker.get(member_id, {}).get(marker_id, MT)[0:2]) for marker_id in self._selected_markers_)))
            markers = self._member_X_marker.get(member_id, {})
            File.write(ofs.join( (sep.format(markers.get(marker_id, MT)) for marker_id in self._selected_markers_)))
        File.write("\n")

    def output_lgen(self, options):
        FILE = options.dir + options.output + '.lgen'
        File = gzopen(FILE, 'w')
#       self.output_long(File, options) # .lgen header does not seem to cause problems.
        self.output_long_data(File, options)
        File.close()

    BED_HDR = "\x6c\x1b" # 01101100 00011011
    def output_bed(self, options, snp_major=True):
        FILE = options.dir + options.output + '.bed'
        File = gzopen(FILE, 'wb')
        File.write(self.BED_HDR)
        if snp_major == True:
            if self._bytes is None:
                self._bytes = bytearray( (self._selected_members_.len() + 3 ) >> 2)
            File.write("\x01")
            self.output_bed_snp(File, options)
        else:
            if self._bytes is None:
                self._bytes = bytearray( (self._selected_markers_.len() + 3 ) >> 2)
            File.write("\x00")
            self.output_bed_pp(File, options)
        File.close()

    def output_bed_snp(self, File, options):
        MT = ('0',  '0')
        MH = {}
        for snps in self._selected_markers_.chrm_list():
            for snp in snps:
                marker_id = self._selected_markers_.snp2id(snp)
                minor = self._select_.alleleorder(marker_id)[0]
                byte = None
                i    = 0
                bi   = 0
                for memrec in self._selected_members_:
                    gt =  self._member_X_marker.get(memrec[0], MH).get(marker_id, MT)
                    if i == 0:
                        if byte is not None:
#                           File.write(struct.pack("B", byte))
                            self._bytes[bi] = byte
                            bi += 1
                        byte = 0
                    if gt[0] != gt[1]:
                        bits = 2
                    elif gt[0] == '0':
                        bits = 1
                    elif gt[0] == minor:
                        bits = 0
                    else:
                        bits = 3
                    byte |= bits << i
                    i = (i + 2 if i < 6 else 0)
#               File.write(struct.pack("B", byte))
                self._bytes[bi] = byte
                File.write(self._bytes)

    def output_bed_pp(self, File, options):
        a1a2minor = {}
        for memrec in self._selected_members_:
            member_id = memrec[0]
            self.output_bed_member_pp(member_id, File, a1a2minor, options)

    def output_bed_member_pp(self, member_id, File, a1a2minor, options):
        MT = ('0',  '0')
        byte = None
        i    = 0
        bi   = 0
        Xm = self._member_X_marker.get(member_id, {})
        if Xm:
            for snps in self._selected_markers_.chrm_list():
                for snp in snps:
##                  marker_id = self._selected_markers_.snp2id(snp)
                    marker_id = snp[-2]
                    minor = self._select_.alleleorder(marker_id)[0]
                    gt = Xm.get(marker_id, MT)
                    if i == 0:
                        if byte is not None:
#                           File.write(struct.pack("B", byte))
                            self._bytes[bi] = byte
                            bi += 1
                        byte = 0
                    if gt[0] != gt[1]:
                        bits = 2
                    elif gt[0] == '0':
                        bits = 1
                    elif gt[0] == minor:
                        bits = 0
                    else:
                        bits = 3
                    byte |= bits << i
                    i = (i + 2 if i < 6 else 0)
#           File.write(struct.pack("B", byte))
            self._bytes[bi] = byte
            File.write(self._bytes)
        else:
            ln = self._selected_markers_.len()
            lnm = (ln & ~3) >> 2
            for i in range(0, lnm):
                self._bytes[i] = 0x55
            ln  = ln & 3
            if ln > 0:
                byte = 0
                for i in range(0, ln):
                    byte |= 1 << 2*i
                self._bytes[lnm] = byte
            File.write(self._bytes)

    def output_bed_snpOLD(self, File, options):
        MT = ('0',  '0')
        for snps in self._selected_markers_.chrm_list():
            for snp in snps:
                marker_id = self._selected_markers_.snp2id(snp)
                minor = self._select_.alleleorder(marker_id)[0]
                byte = []
                i    = 0
                for gt in (self._member_X_marker.get(memrec[0], {}).get(marker_id, MT)[0:2] for memrec in self._selected_members_):
                    if i == 0 and byte:
                        File.write(self.cnv(byte, minor))
                        byte = []
                    i = i + 1 if i < 3 else 0
                    byte.append(gt)
                File.write(self.cnv(byte, minor))
#        File.write(self.cnv(byte, minor))

    def output_bed_member_ppOLD(self, member_id, File, a1a2minor, options):
        byte = []
        MT   = ('0',  '0')
        i = 0
        Xm = self._member_X_marker.get(member_id, {})
        for snps in self._selected_markers_.chrm_list():
            for snp in snps:
                marker_id = self._selected_markers_.snp2id(snp)
                minor = self._select_.alleleorder(marker_id)[0]
                gt = Xm.get(marker_id, MT)[0:2]
                if True:
                    gt = ( gt[0], gt[1], minor )  # generating storage seems faster than the loopup
                elif gt:
                    gta = a1a2minor.get(gt, None) # below
                    if gta is not None:
                        gtb = gta.get(minor, None)
                        if gtb is not None:
                            gt = gtb
                        else:
                            gt = ( gt[0], gt[1], minor )
                            gta[minor] = gt
                    else:
                        gta = {}
                        a1a2minor[gt] = gta
                        gt = ( gt[0], gt[1], minor )
                        gta[minor] = gt
                else:
                    gt = MT
                if i == 0 and byte:
                    File.write(self.cnv(byte))
                    byte = []
                i = i + 1 if i < 3 else 0
                byte.append(gt)
        File.write(self.cnv(byte, minor))
#        File.write(self.cnv(byte, minor))

    def cnv(self, n4ple, minor=False):
#       printnl(n4ple)
        ans = 0
        for i, item in enumerate(n4ple):
            if item[0] != item[1]:
                bits2 = 2
            elif item[0] == '0':
                bits2 = 1
            elif minor:
                if item[0] == minor:
                    bits2 = 0
                else:
                    bits2 = 3
            else:
                if item[0] == item[2]:
                    bits2 = 0
                else:
                    bits2 = 3
            ans |=  bits2 << (2 *  i)
        return struct.pack("B", ans)

    def output_person_order_prolog(self, options):
        if options.type == 'tped':
            raise Exception("tped is not available with person_order")
        elif options.type in ('bed', 'lgen', 'ped'):
##1105      self.output_ped(options)  # may abort if 'bed' and > 2 alleles
##1105      self.output_phe(options)
##          self.output_map(options) # need chrs to be final; wait till epilog
            pass
        elif options.type == 'mega2':
            pass # need chrs to be final; wait till epilog
        elif options.type in ('long', 'wide', 'half'):
            pass
        else:
            raise Exception("{0} is not available with person_order".format(options.type))

        if options.type == 'bed':
            if self._bytes is None:
                self._bytes = bytearray( (self._selected_markers_.len() + 3 ) >> 2)
            FILE = options.dir + options.output + '.bed'
            File = gzopen(FILE, 'wb')
            File.write(self.BED_HDR)
            File.write("\x00")
        elif options.type == 'lgen':
            FILE = options.dir + options.output + '.lgen'
            File = gzopen(FILE, 'w')
        elif options.type == 'ped':   # this will regen output_ped() iff 'ped' to contain snps
            FILE = options.dir + options.output + '.ped'
            File = gzopen(FILE, 'w')
        elif options.type == 'mega2':
            FILE = options.dir + "pedin." + options.output
            File = gzopen(FILE, 'w')
            self.output_mega2_pedin_header(File, options)
        elif options.type == 'long':
            FILE = options.dir + options.output + '.txt'
            File = gzopen(FILE, 'w')
            self.output_long_header(File, options)
        elif options.type in ('wide', 'half'):
            FILE = options.dir + options.output + '.txt'
            File = gzopen(FILE, 'w')
            self.output_wide_header(File, options)

        return File

    def output_person_order_member(self, member_id, File, options):
        if options.type == 'bed':
            a1a2minor = {}
            self.output_bed_member_pp(member_id, File, a1a2minor, options)
        elif options.type == 'lgen':
            self.output_long_member(File, member_id, options)
        elif options.type == 'ped':
            self.output_ped_member(File, member_id, options)
        elif options.type == 'mega2':
            self.output_mega2_pedin_member(File, member_id, options)
        elif options.type == 'long':
            self.output_long_member(File, member_id, options)
        elif options.type in ('wide', 'half'):
            self.output_wide_member(File, member_id, options)

    def output_person_order_epilog(self, File, options):
        File.close()

        if options.type in ('bed', 'lgen', 'ped'):
            if options.type == 'bed':
                self.must_be_biallelic(options)
                self.output_ped(options)

            self.output_phe(options)
            self.output_map(options)
        elif options.type == 'mega2':
            self.output_mega2_map(options)
            self.output_mega2_names(options) # names need chr

    def must_be_biallelic(self, options):
        for snps in self._selected_markers_.chrm_list():
            for snp in snps:
                marker_id = self._selected_markers_.snp2id(snp)
                order = self._select_.alleleorder(marker_id)
                if len(order) > 2:
                    raise NoTrace("\nAborting file generation; data are not biallelic.")
