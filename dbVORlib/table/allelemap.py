#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from dbVORlib.marker           import Marker

from dbVORlib.table.dbtable    import DBTable


class AlleleMap(DBTable):

    def __init__(self, db):
        super(AlleleMap, self).__init__(db)
        self._marker_            = Marker(db)
        self._experiment_map     = {}
        self._experiment_name2id = {}
        self._inserted           = {}
        self._flip               = False

        self._name               = None
        self._marker             = None
        self._expr               = None
        self._im                 = None

    def add_options(self, config):
        super(AlleleMap, self).add_options(config)
        config.add_option("--marker", action="store", type="int", default=None,
                          help="choose column to get marker name from input")
        config.add_option("--experiments", action="store", type="int", default=None,
                          help="choose column to get experiment name from input")
        config.add_option("--allelemap", "--map", action="append", type="int", default=[],
                          help="choose columns to get allele mapping from input")
        config.add_option("--maptype", action="store", type="int", default=1,
                          help="maptype to apply mapping: 1->genotypes, 2->gwas, 3->fin")
        config.add_option("--active", action="store", type="int", default=1,
                          help="active value")

    def prolog(self, dbi, options):
        super(AlleleMap, self).prolog(dbi, options)
        cmde = "select id, filename, projectid, techid from experiments"
        self._experiment_map     = dict((i[0], i[1:]) for i in dbi.select_all(cmde))
        self._experiment_name2id = dict((v[0], k) for (k, v) in self._experiment_map.iteritems())

        if 4 <= options.maptype <= 6:
            self._flip = True
            options.maptype = options.maptype - 3

        cmdi = "insert into allele_map (marker, experiment, allelemap, maptype, active)"
        fmt  = "({0:d}, {1:d}, '{2:}', "
        fmt += " {0:d}, {1:d})".format(options.maptype, options.active)
        return self.insert_many(cmdi, fmt, options, cnt=1000)

    def epilog(self, dbi, options):
        if options.commit:
            self._im()
        return True

    def do_row(self, fields, dbi, options):
        self._expr = fields[options.experiments - self.ORIGIN]
        expr       = self._experiment_name2id.get(self._expr, None)
        if expr is None:
            self.pr_context("experiment: {0} is not valid".format(self._expr))
            options.error += 1
            return

        self._marker = fields[options.marker - self.ORIGIN]
        mid          = self._marker_.marker(self._marker, options)
        if mid is None: return

        name = "{0}/{1}".format(self._marker, self._expr)
        self._name = name

        if not 1 <= options.maptype <= 3:
            self.pr_context("bad --maptype value.  Line ignored. (1->genotypes, 2->gwas, 3->fin)")
            return
        allelemap = []
        l = len(fields)
        for idx in options.allelemap:
            if idx > l: continue
            entry = fields[idx  - self.ORIGIN].split(':')
            if self._flip:
                allelemap.append(entry[1].strip('\'",)}]') + ':' + entry[0].strip('\'",[{('))
            else:
                allelemap.append(entry[0].strip('\'",[{(') + ':' + entry[1].strip('\'",)}]'))
        allelemap = ' '.join(allelemap)

#       We may need a reverse lookup:
#           hash[mid] = self._marker -> self._marker_.rev(mid)
#           hash[expr] = self._expr  -> self._experiment_map[expr][0]
        self.row_util([mid, expr, allelemap, options.maptype, options.active], name, dbi, options)

    def pr_rec(self, fields, options, db=False):
        if options.verbose:
            cmd = "marker:{0} experiment:{1} allelemap:\"{2}\""
            return cmd.format(self._marker_.rev(fields[0]), self._experiment_map[fields[1]][0], fields[2])
        if options.debug:
            cmd = "marker:{0[0]!r} experiment:{0[1]!r} allelemap:\"{0[2]}\""
            return cmd.format(fields)

    cmds = "select marker, experiment, allelemap, maptype, active, id from allele_map where marker = ? and experiment = ? and maptype = ? and active = ?"
    _MT = []
    MT_HASH = {}
    def select(self, fields, options):
        rec = self._dbi_.select_all(self.cmds, fields[0], fields[1], fields[-2], fields[-1])
        return rec

    cmdd = "delete from allele_map where id = ?"
    def delete(self, rec, options):
        self._dbi_.execute(self.cmdd, rec[-1])

    def insert(self, fields, options):
        marker, experiment = fields[0:2]
        inserted = self._inserted.get(experiment, self.MT_HASH).get(marker, self.MT_HASH)
        if inserted:
            self.pr_context("Duplicated entry.  Original on line {0}".format(inserted))
            return

        if experiment not in self._inserted:
            self._inserted[experiment] = {}
        self._inserted[experiment][marker] = self._line_no

        self._im(fields)

    def cmp(self, rec, fields, options):
        return cmp(rec[0], fields[0]) or cmp(rec[1], fields[1]) or \
               cmp(rec[2], fields[2]) or cmp(rec[3], fields[3]) or \
               cmp(rec[4], fields[4])

    def validate(self, fields, name, options):
        ret = True
        return ret
