# use dbi.insert_many vs rolling our own
#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#
#
# External Methods
#	validate_project	-- look up project name -> project id [in Validate()]
#	validate_technology	-- look up technology name -> technology id [in Validate()]
#	validate_experiment	-- look up experiment name -> experiment id [in Validate()]
#				-- if --commit flag is set experiment will be added if not present
#	process_one		-- handle one pedigree/person/snip/allele entry

#import dbi
#import config

import time

from   validate import Validate
from   allele   import Allele
from   util     import printnl

ORIGIN  = 1

class Genotype(Validate):
    def __init__(self, db):
        super(Genotype, self).__init__(db)
        self._allele_ = Allele(db)

        self._batch = [None, time.time(), 0, 0, []] # [ID, start, full count, batch count, data list]
# tmp
        self._cache            = {}
        self._cache_id         = -1
        self._conflicts        = {}
        self._genotypes        = 0
        self._allele_map_geno  = None
        self._verify_not_found = 0
        self._verify_zero      = 0
        self._verify_different = 0
        self._verify_duplicate = 0

        self._proj2name        = {}
        self._tech2name        = {}
        self._expr2name        = {}

        self._errors           = 0

    def add_options(self, config):
        super(Genotype, self).add_options(config)
        config.add_option("--input", action="store", type="string",
                          help="input file needed for xlong fmt; default: None")

        config.add_option("--sep", "--file_separator", type="string",
                          help="prepDB experiment file internal separator; default white space")

        config.add_option("--pedigree", "--ped", type="int",
                          help="offset of pedigree value in data; origin is 1 [0 for not present]")
        config.add_option("--person", type="int",
                          help="offset of person value in data; origin is 1")
        config.add_option("--marker", type="int",
                          help="offset of marker info in data; origin is 1")

        config.add_option("--include", action="append",
                          help="persons only to be processed")
        config.add_option("--exclude", action="append",
                          help="persons not to process")

        config.add_option("--quality", type="float", default=-9.00,
                          help="genotypes table quality value; default: -9.00")
        config.add_option("--active", type="int", default=1,
                          help="genotypes table active value; default: 1")

        config.add_option("--translate", action="store_true", default=False,
                          help="replace file ped/person/snip with database ids")
        config.add_option("--xlate", action="store_true", default=False,
                          help="replace file ped/person/snip with database ids in sorted xlong fmt")
        config.add_option("--output", action="store", type="string",
                          help="output file optional for --translate and --xlate; default: None")


        config.add_option("--verify", action="store_true", default=False,
                          help="verify genotype data in database matches experiment file")
        config.add_option("--verify_in", action="store_true", default=False,
                          help="complain if experiment file not in database")
        config.add_option("--verify_show_all", action="store_true", default=False,
                          help="show all conflicts genotype data in database matches experiment file")
        config.add_option("--compare", action="store_true", default=False,
                          help="like verify except assume the data is already in the database")
        config.add_option("--allele_flip", action="store", type="int",
                          help="marker id to flip alleles on for verify/compare")

        config.add_option("--commit", action="store_true", default=False,
                          help="commit changes to database")
##      config.add_option("--comdrop", action="store_true", default=False,
##                         help="build like commit but do not insert genotype changes to database")
        config.add_option("--replace", action="store_true", default=False,
                          help="remove previous matches from database during commit")
        config.add_option("--insert_many", type="int", default=100)

        config.add_option("--rollback", action="store_true", default=False,
                          help="force all database operations to be aborted")

        config.add_option("--stat", "--status", "--statistics",
                          action="store_true", default=False,
                          help="show summaries/statistics at end of run")
        config.add_option("--nostat", "--nostatus", "--nostatistics",
                          action="store_true", default=False,
                          help="do not show summaries/statistics at end of run")

    def prolog(self, dbi, options):
# remove line below iff config.add_options(--comdrop ...) is enabled
        options.comdrop = False
        if options.comdrop:
            options.commit = True
        self._allele_map_geno = self._allele_.load_map(dbi, 1, options)

    def process_one(self, options, Id, marker_id, allele, pr_context):
        self._errors = 0
        def pr_file_data(*string):
            if self._errors == 0:
                if options.debug:
                    s = """raw data\t(ID, {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})""".format(options.project_id, Id, marker_id, options.experiment_id, options.tech_id, allele[0], allele[1], options.quality, options.active)
                    pr_context(s)
                else:
                    pr_context()
            self._errors += 1
            if len(string):
                for s in string: printnl(s)
            else:
                printnl('')

        self._genotypes += 1

# is either allele actually blank.  either both or none are allowed both ==> use 0,0
        if allele[0] is None or (allele[0] == ''):
            pr_file_data("Bad allele value")
            allele = ('0', allele[1])
            if allele[1] is None or (allele[1] == ''):
                allele = ('0', '0')
            else:
                pr_file_data("CONFLICTING Bad allele value")
                return
        elif allele[1] is None or (allele[1] == ''):
            pr_file_data("Bad allele value")
            pr_file_data("CONFLICTING Bad allele value")
            return
### New Improved
        allele = allele[0], allele[1], options.tech_id, options.experiment_id, options.active

        if options.verbose > 1:
            pr_file_data()

        if options.translate:
            print"{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}".format(0, options.project_id, Id, marker_id, options.experiment_id, options.tech_id, allele[0], allele[1], options.quality, options.active)
            return

        if options.verify or options.verify_in or options.compare or options.commit:
            pass   # the gate keeper; avoids the get*genotype() for nothing
        else:
            return

        if options.type == 'xlong':
            sel = self.get_cached_genotype(options, Id, marker_id)
        else:
## 03/12/12
##            sel = self.get_genotype(options, Id, marker_id)
            sel = self.get_cached_genotype(options, Id, marker_id)

        if options.verify or options.verify_in:
            self.verify(options, Id, marker_id, allele, sel, pr_file_data)
            return

# like verify but do not consider the experiment file data; just database
        if options.compare:
            self.verify(options, Id, marker_id, None, sel, pr_file_data)
            return

        if options.commit:
            if len(sel) > 0:    # out with the old ?
                if self.already_there(options, Id, marker_id, allele, sel, pr_file_data):
                    return

            cmd = """insert into genotypes
                        (projectid, member, marker, experiment, technology,
                          allele1, allele2, quality_value, active)  VALUES
                        (?, ?, ?, ?, ?, ?, ?, ?, ?)"""
            tupl =  (options.project_id, Id, marker_id,
                     options.experiment_id, options.tech_id,
                     allele[0], allele[1], options.quality, options.active)
            if options.insert_many == 1:
                if self._batch[0] != Id:
                    if self._batch[0] is not None:
                        self.batch_insert(options)
                    self._batch[:] = [Id, time.time(), 0, 0, []]

                self._dbi_.insert(cmd, [ tupl ] )
                self._batch[2] += 1
            else:
# if id changes commit batch
                if self._batch[0] != Id:
                    if self._batch[0] is not None:
                        self.batch_insert(options)
                    self._batch[:] = [Id, time.time(), 0, 0, []]
# batch up insert_many insertions
                if self._batch[3] < options.insert_many:
                    self._batch[4].append(tupl)
                    self._batch[3] += 1
                else:
# do many the inserts ... should be faster this way; not as many packets on the wire
#                    self._dbi_.insert(cmd, self._batch[4])
                    self.insert_run(cmd, self._batch[4], options)
                    self._batch[3:] = (1, [tupl])
                self._batch[2] += 1

    def process_last(self, options, member, marker):
        if options.commit and options.insert_many >= 1:
            self.batch_insert(options)

        if options.verify or options.verify_in or options.compare:
            self.ver_stat()
        else:
            printnl("\ngenotypes {0:d}".format(self._genotypes))

    def ver_stat(self):
        printnl("\ngenotypes {0:d}, present {1:d}, match {2:d}, zero {3:d}, conflict {4:d}, not present {5:d}\n".format(\
                self._genotypes, self._genotypes - self._verify_not_found,
                self._verify_duplicate, self._verify_zero,
                self._verify_different, self._verify_not_found))

    MT_HASH = {}
    def get_genotype(self, options, Id, marker_id):
        exact = False
        if exact:
            sel = self._dbi_.select_all("""select id,
                                             projectid, member, marker, experiment, technology,
                                             allele1, allele2, quality_value, active
                                            from genotypes where
                                             projectid = ? and  member = ? and marker = ? and
                                             experiment = ? and technology = ?""",
                                        options.project_id, Id, marker_id,
                                        options.experiment_id, options.tech_id)
        else:
            sel = self._dbi_.select_all("""select id,
                                             projectid, member, marker, experiment, technology,
                                             allele1, allele2, quality_value, active
                                            from genotypes where
                                             member = ? and marker = ?""",
                                        Id, marker_id)
        if self._allele_map_geno is None or not sel: return sel
        selmap = []
        for alleles in sel:
            remap = self._allele_map_geno.get(alleles[4], self.MT_HASH).get(alleles[3], self.MT_HASH)
            if remap:
                alleles = alleles[0], alleles[1], alleles[2], alleles[3], alleles[4], \
                          alleles[5], \
                          remap.get(alleles[6], '0'), remap.get(alleles[7], '0'), \
                          alleles[8], alleles[9]
            selmap.append(alleles)
        return selmap

    def get_cached_genotype(self, options, Id, marker_id):
        if Id != self._cache_id:
# get all the markers for this member
            exact = False
            if exact:
                sel = self._dbi_.select_all("""select id,
                                                 projectid, member, marker, experiment, technology,
                                                 allele1, allele2, quality_value, active
                                              from genotypes where
                                                projectid = ? and  member = ? and
                                                experiment = ? and technology = ?""",
                                            options.project_id, Id,
                                            options.experiment_id, options.tech_id)
            else:
                sel = self._dbi_.select_all("""select id,
                                                 projectid, member, marker, experiment, technology,
                                                 allele1, allele2, quality_value, active
                                                from genotypes where
                                                 member = ?""",
                                            Id)
            self._cache_id = Id
            self._cache    = {}
            if len(sel):
                for s in sel:
                    self._cache.setdefault(s[3], []).append(s)
        sel = self._cache.get(marker_id, ())
        if self._allele_map_geno is None or not sel: return sel
## 03/12/12 add remapping code
        selmap = []
        for alleles in sel:
            remap = self._allele_map_geno.get(alleles[4], self.MT_HASH).get(alleles[3], self.MT_HASH)
            if remap:
                alleles = alleles[0], alleles[1], alleles[2], alleles[3], alleles[4], \
                          alleles[5], \
                          remap.get(alleles[6], '0'), remap.get(alleles[7], '0'), \
                          alleles[8], alleles[9]
            selmap.append(alleles)
        return selmap

    def verify(self, options, Id, marker_id, allele, sel, pr_context):
        name = 'verify' if options.verify else 'compare'
        if len(sel) == 0:
            self._verify_not_found += 1
            if options.verify_in:
                pr_context("{0}: genotype not found".format(name))
            return

        if marker_id == options.allele_flip:
            if allele:
                allele = (allele[1], allele[0], allele[2], allele[3], allele[4])
            nel = []
            for s in sel:
                if options.experiment_id == s[4]:
                    s = list(s)
                    s[7], s[6] =  s[6], s[7]
                nel.append(s)
            sel = nel

        if allele is None:
            if len(sel) == 1:
                self._verify_not_found += 1
                return           # only one entry
            alleles = None
        else:
            alleles = [[1, allele]]
        for s in sel:
            if alleles:
                self._allele_.merge_alleles2(alleles, (str(s[6]), str(s[7]), s[5], s[4], s[9]))
            else:
                alleles = [[1, (str(s[6]), str(s[7]), s[5], s[4], s[9]) ]]

        if options.stat or not options.nostat:
            self._conflicts.setdefault(Id, {})[marker_id] = alleles

        if len(alleles) == 2 and alleles[0][1][0] == '0' and alleles[0][1][1] == '0':
            self._verify_zero += 1
            if options.verify_show_all:
                pr_context("{0}: zero genotypes found.".format(name))
                if options.debug:
                    for a in alleles: printnl("\t\t{0}".format(a))
                self.verify_show_all(options, Id, marker_id, allele, sel, pr_context)
        elif len(alleles) >= 2:
            self._verify_different += 1
            pr_context("{0}: different genotypes found.".format(name))
            if options.debug:
                for a in alleles: printnl("\t\t{0}".format(a))
#                   if options.verify_show_all:
            self.verify_show_all(options, Id, marker_id, allele, sel, pr_context)
        else:
            self._verify_duplicate += 1
            if options.verify_show_all:
                pr_context("{0}: duplicate genotypes found.".format(name))
                if options.debug:
                    for a in alleles: printnl("\t\t{0}".format(a))
                self.verify_show_all(options, Id, marker_id, allele, sel, pr_context)

    def verify_show_all(self, options, Id, marker_id, allele, sel, pr_context):
        if options.debug:
            self.verify_show_raw(options, Id, marker_id, allele, sel, pr_context)
        else:
            self.verify_show_cook(options, Id, marker_id, allele, sel, pr_context)

    def __cache_id2name(self, cache, di):
        if di in cache:
            return cache[di]

        if cache == self._proj2name:
            cmd = "select name from projects where id = ?"
        elif cache == self._tech2name:
            cmd = "select technology from technologies where id = ?"
        elif cache == self._expr2name:
            cmd = "select filename from experiments where id = ?"
        else:
            cmd = "select NULL"
        p = self._dbi_.select_one(cmd, di)
        p = '???' if p is None else p[0]
        cache[di] = p
        return p

    def verify_show_cook(self, options, Id, marker_id, allele, sel, pr_context):
        if allele:
            p = options.project
            t = options.technology
            x = options.experiment
#            pr = """verify: #{0:d}, db id {1:d}, mbr/mkr {2:d}/{3:d}, allele {4}/{5}, {6}/{7}/{8}""" .format \
#                 (0, 0, Id, marker_id, allele[0], allele[1], p, t, x)
            pr = """{0}: #{1:d}, db id {2:8d}, allele {3}/{4}, {5}/{6}/{7}""".format('new', 0, 0, allele[0], allele[1], p, t, x)
            pr_context(pr)

        for idx, s in enumerate(sel):
            p = self.__cache_id2name(self._proj2name, s[1])
            t = self.__cache_id2name(self._tech2name, s[5])
            x = self.__cache_id2name(self._expr2name, s[4])
#            pr = """verify: #{0:d}, db id {1:d}, mbr/mkr {2:d}/{3:d}, allele {4}/{5}, {6}/{7}/{8}""" .format \
#                 (idx+1, s[0], s[2], s[3], s[6], s[7], p, t, x)
            pr = """{0}: #{1:d}, db id {2:8d}, allele {3}/{4}, {5}/{6}/{7}""".format(' db', idx+1, s[0], s[6], s[7], p, t, x)
            pr_context(pr)

    def verify_show_raw(self, options, Id, marker_id, allele, sel, pr_context):
        pr1 = "{0}: all: #{1:d}, genotype.id {2:d}, member = {3}, marker = {4},"
        pr2 = "              projectid = {0}, experiment = {1}, technology = {2}"
        pr3 = "              allele1 = {0}, allele2 = {1}, quality_value = {2}, active = {3}"
        if allele:
            pr_context(pr1.format('new', 0, 0, Id, marker_id),
                       pr2.format(options.project_id, options.experiment_id, options.tech_id),
                       pr3.format(allele[0], allele[1], options.quality, options.active))

        for idx, s in enumerate(sel):
            pr_context(pr1.format(' db', idx+ORIGIN, s[0], s[2], s[3]),
                       pr2.format(s[1], s[4], s[5]),
                       pr3.format(s[6], s[7], s[8], s[9]))

    def verify_show_rawOLD(self, options, Id, marker_id, allele, sel, pr_context):
        if allele:
            pr =  """{0}: all: #{1:d}, genotype.id {2:d}, member = {3}, marker = {4},
                             projectid = {5}, experiment = {6}, technology = {7}
                            allele1 = {8}, allele2 = {9}, quality_value = {10}, active = {11}""".format \
                   ( 'new', 0, 0, Id, marker_id, options.project_id, options.experiment_id, \
                     options.tech_id, allele[0], allele[1],
                     options.quality, options.active)
            pr_context(pr)

        for idx, s in enumerate(sel):
            pr =  """{0}: all: #{1:d}, genotype.id {2:d}, member = {3}, marker = {4},
                         projectid = {5}, experiment = {6}, technology = {7}

                         allele1 = {8}, allele2 = {9}, quality_value = {10}, active = {11}""".format \
                ( ' db', idx+ORIGIN, s[0], s[2], s[3], s[1], s[4], s[5], s[6], s[7], s[8], s[9])
            pr_context(pr)


    def mm_match(self, options, Id, marker_id, allele, s, flag):
        if flag & 0x1:
            m = Id == s[2] and marker_id == s[3]
        if flag & 0x2:
            m = m and (allele[0] == str(s[6]) and allele[1] == str(s[7]))
        if flag & 0x4:
            m = m and (options.project_id == s[1] and options.experiment_id == s[4] and options.tech_id == s[5])
        if flag & 0x8:
            m = m and (options.quality == float(s[8]) and options.active == s[9])
        return m

# commit already present
    def already_there(self, options, Id, marker_id, allele, sel, pr_context):
        if not options.replace:
            ret = False
            s = "commit: member/marker is already in the database for some experiment(s):"
            pr_context(s)
## new ish
            self.verify_show_all(options, Id, marker_id, allele, sel, pr_context)
            for s in sel:
##              if options.debug: printnl("\t\t{0}".format(s))
                if self.mm_match(options, Id, marker_id, allele, s, 0x5):
                    printnl("ERROR (commit): exact match of new project/technology/experiment exists in db.  Ignoring NEW Value")
                    ret = True
                    options.error += 1
            return ret
        s = "commit: Match was previously in the database.  Flushing OLD Value:"
        pr_context(s)
## new ish
        self.verify_show_all(options, Id, marker_id, allele, sel, pr_context)
##          if options.debug:
##              for s in sel: printnl("\t\t{0}".format(s))

        self._dbi_.execute("""delete from genotypes where
                                     projectid = ? and  member = ? and marker = ? and
                                     experiment = ? and technology = ?""",
                           options.project_id, Id, marker_id,
                           options.experiment_id, options.tech_id)
        return False

# insert many tuples
    def batch_insert(self, options):
        cmd = """insert into genotypes
                    (projectid, member, marker, experiment, technology,
                      allele1, allele2, quality_value, active)  VALUES
                    (?, ?, ?, ?, ?, ?, ?, ?, ?)"""
#       if len(self._batch[4]): self._dbi_.insert(cmd, self._batch[4])
        if len(self._batch[4]): self.insert_run(cmd, self._batch[4], options)
## code below is confusing(possibly wrong).  If there is any error, we want everything to
## be rollback'ed.  do not commit "good" entries till the error.
## BUT we might as well rollback incrementally i don't think it is a linear process
####        if options.rollback or options.error:
####            self._dbi_.rollback()
##          pass
##      else:
##          self._dbi_.commit()
        if len(self._batch[4]):
            printnl("insert: member {0}, snps {1:d}, sec {2:.3f}".format \
                        (self._batch[0], self._batch[2], time.time() - self._batch[1]), tag='insert')

    def insert_run(self, cmd, tupls, options):
        cmd = """insert into genotypes
                    (projectid, member, marker, experiment, technology,
                      allele1, allele2, quality_value, active)  VALUES """
#  [(4L, 5069L, 2518L, 70L, 11L, 'A', 'A', -9.0, 1), (4L, 5069L, 2519L, 70L, 11L, 'A', 'A', -9.0, 1)        ... ]
        hfs1 = "({rec[0]:d}, {rec[1]:d}, {rec[2]:d}, {rec[3]:d}, {rec[4]:d}, '{rec[5]}', '{rec[6]}', {rec[7]:.2f}, {rec[8]:d})"
        if not options.comdrop:
            self._dbi_.execute(cmd + ', '.join([hfs1.format(rec=t) for t in tupls]))

    def stat(self, options, cmd, member, marker, snp_stat):
        if options.stat or not options.nostat:
            member.stat(options)
            marker.stat(snp_stat, options)
            if options.verify or options.compare:
                cmd.conflict_stats(options, member, marker)
                self.ver_stat()

    def conflict_stats(self, options, member, marker):
        self._allele_.conflict_stats(options, self._conflicts, member, marker)
