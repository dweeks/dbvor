
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import time

from   operator               import itemgetter

from   dbVORlib.validate import ValidateProject
from   dbVORlib.allele   import Allele
from   dbVORlib.util     import NoTrace, gzopen, printnl, printn, log

from   dbVORlib.select.trait    import TraitSelect
from   dbVORlib.select.marker   import MarkerSelect
from   dbVORlib.select.member   import MemberSelect
from   dbVORlib.select.output   import OutputPlink

class Select(object):
    def __init__(self, db, snp):
        self._dbi               = db
        self._validate_         = ValidateProject(db)
        self._allele_           = Allele(db)
        self._selected_traits_  = TraitSelect(db)
        self._selected_members_ = MemberSelect(db, self._selected_traits_)
        self._selected_markers_ = MarkerSelect(db, snp, self._selected_members_)

        self._member_X_marker           = {}
        self._member_X_marker_conflicts = {}
        self._unobserved                = {}
        self._allele_stat               = {}
        self._allele_order              = {}
        self._allele_map_geno           = None
        self._allele_map_gwas           = None
        self._allele_map_fini           = None
        self._output_                   = OutputPlink(db,
                                                      self,
                                                      self._selected_markers_,
                                                      self._selected_traits_,
                                                      self._selected_members_,
                                                      self._member_X_marker)

        self._batch_test                = False
        self._total                     = 0
        self._possible_conflicts        = 0
        self._conflicts                 = 0
        self._dups                      = 0
        self._zeros                     = 0
        self._actives                   = 0
        self._sanity                    = 0


    def add_options(self, config):
        self._validate_.add_options(config)
        self._selected_traits_.add_options(config)
        self._selected_markers_.add_options(config)
        self._selected_members_.add_options(config)
        self._output_.add_options(config)

        config.add_option("--use_inactive", action="store", type="boolean", default=False,
                          help="allow inactive genotype for each memberXmarker")
        config.add_option("--filter_by_active", action="store", type="boolean", default=True,
                          help="only consider the highest active value(s) of genotype for each memberXmarker")
#4/1    config.add_option("--active", action="store", type="int", default=1,
#4/1                      help="specify active level for genotypeblocks")
        config.add_option("--active", action="store", type="int", default=1,
                          help="specify active level for genotypeblocks")

        config.add_option("--person_order", action="store", type="boolean", default=False,
                          help="have to generate all snps for one person at a time each memberXmarker")

        config.add_option("--tech", action="store", type="boolean", default=True,
                          help="filter genotypes/gwas by technologies selected")

        config.add_option("--stat", "--status", "--statistics",
                          action="store_true", default=False,
                          help="show summaries/statistics at end of run")

        config.add_option("--heterozygote", action="store", default="lexical",
                          help="how to order heterozygote alleles")

        config.add_option("--unobserved", action="store_true", default=False,
                          help="show observed count per person")
        config.add_option("--no_markers", action="store_true", default=False,
                          help="don't display marker data")
        config.add_option("--no_traits", action="store_true", default=False,
                          help="don't display trait data")

    def alleleorder(self, marker_id):
        return self._allele_order[marker_id]

    def prolog(self, dbi, options):
        self._validate_.validate_project(options)

        if options.heterozygote not in ('unchanged', 'lexical', 'keep-allele-order', 'founders', 'allsubjects'):
            printnl("heterozygote option must be either 'unchanged', 'lexical', 'keep-allele-order', 'founders', or 'allsubjects'")
            return False
        options.allelestat = options.stat
        if options.heterozygote not in ('unchanged', 'lexical'):
            options.allelestat = True

        printnl("project {0}, id {1:d}"   .format(options.project, options.project_id))
        printnl('')
        return True

    def run(self, dbi, options):
        self._selected_traits_.run(dbi, options)

        self._selected_markers_.run(dbi, options)

        tech = self._selected_markers_.tech_ids() if options.tech else []
        self._selected_members_.run(dbi, tech, options)

        self._selected_markers_.run_exclusion(dbi, options)

        if options.person_order:
            self.set_default_alleleorder(dbi, options)

        self.select_genotypes(dbi, options)

        if options.person_order:
            return True
        if options.unobserved:
            self.unobserved(options)
            return True

        printnl('')
        log.push('exclusion statistics')
        self._selected_markers_.exclusion_stat(options)
        log.pop('exclusion statistics')

        printnl('')
        log.push('prune markers')
        self._selected_markers_.prune(dbi, options)
        log.pop('prune markers')

        log.push('prune members')
        self._selected_members_.prune(dbi, options, self._member_X_marker)
        log.pop('prune members')

        if options.heterozygote != 'unchanged':
            self.set_alleleorder(dbi, options)
            self.heterozygoteorder(options.heterozygote)

        if options.type in ('ped', 'tped', 'lgen', 'bed'):
            self._output_.output_plink(options)
        elif options.type in ('mega2', ):
            self._output_.output_mega2(options)
        elif options.type in ('long', 'wide', 'half'):
            self._output_.output_local(options)
        else:
            raise NoTrace("Output type should be ped, tped, lgen, or bed (for plink) or long, wide, half, or mega2.")

        return True

    def unobserved(self, options):
        FILE = options.dir + options.output + '.unobserved.txt'
        File = gzopen(FILE, 'w')

        pedl = log.pedigree_name_length
        perl = log.person_name_length
        def unobservedkey(rec):
            ped, per = self._selected_members_.name(rec[0])
            ped = ped.rjust(pedl)
            per = per.rjust(perl)
            return (rec[1][1], ped, per)
        lst = sorted(self._unobserved.iteritems(), key=unobservedkey)
        pedl = 7+1 if pedl < 7 else pedl + 2
        perl = 6+1 if perl < 6 else perl + 2
        File.write("{0[0]:>{1}} {0[1]:>{2}} {0[2]:>6} {0[3]:>7} {0[4]:>7}\n".format(("pedigree", "person", "count", "missing", "present"), pedl, perl))
        for el in lst:
            File.write("{1[0]:>{3}} {1[1]:>{4}} {0[1][0]:6d} {0[1][1]:7d} {2:7d}\n".format(el, self._selected_members_.name(el[0]), el[1][0]-el[1][1], pedl, perl))

        File.close()

    _cmdk = "select allele1, allele2, technology, experiment, active, member, marker from genotypes where marker = ?"
    _cmda = "select allele1, allele2, technology, experiment, active, member, marker from genotypes where 1 = 1"
    def fill_mxmhash(self, dbi, lmem, lmark, snps):
        mxm = {}
        if lmark * lmem < 10000 and False:
            sel = dbi.select_all(self._cmda)
            self.mxm_one_fill(mxm, sel)
            return mxm, False
        elif lmark < 250 :
            if lmark * lmem > 1000000:
                return mxm, False
            for marker_id in snps:
                sel = dbi.select_all(self._cmdk, marker_id)
                self.mxm_one_fill(mxm, sel)
            return mxm, False
        else:
            printnl("There are too much genotype data to fetch from the database at once ... this might run slow")
            return mxm, True

    def mxm_one_fill(self, mxm, sel):
        for dbr in sel:
            if dbr[-2] not in mxm:
                mxm[dbr[-2]] = {}
            if dbr[-1] not in mxm[dbr[-2]]:
                mxm[dbr[-2]][dbr[-1]] = []
            mxm[dbr[-2]][dbr[-1]].append(dbr)

    def allele_stat_common(self, allele_stat, alleleab, founder):
        allelea, alleleb = alleleab
        allele = allelea + alleleb if allelea <= alleleb else alleleb + allelea

        if founder:  #  match plink ... founders only
            allele_stat[2][allelea] = allele_stat[2].get(allelea, 0) + 1
            allele_stat[2][alleleb] = allele_stat[2].get(alleleb, 0) + 1
        allele_stat[3][allelea] = allele_stat[3].get(allelea, 0) + 1
        allele_stat[3][alleleb] = allele_stat[3].get(alleleb, 0) + 1
        allele_stat[4][allele]  = allele_stat[4].get(allele , 0) + 1
#       printnl("allele {0} -- {1:d}".format(allele, allele_stat[3][allele]))

    def resolve_conflict(self, member_id, marker_id, alleles, options):
        if options.funny:
            MT = ('F', 'F', 0, 0, 0)
        else:
            MT = ('0', '0', 0, 0, 0)
        cd = [[1, alleles[0]]]
        for allele in alleles[1:]:
            self._allele_.merge_alleles2(cd, allele)
        self._member_X_marker_conflicts[member_id][marker_id] = cd

        if len(cd) == 1:
            return cd[0][1], 1
        elif len(cd) == 2 and cd[0][1][0] == '0' and cd[0][1][1] == '0':
            return cd[1][1], 2
        else:
            if options.verbose or options.debug >= 2:
                log.newrecord()
                pedper = self._selected_members_.name(member_id)
                printnl("ERROR: pedigree:person {0}:{1}({2:d}) has conflicting genotypes for marker {3} (id {4}):".format(pedper[0], pedper[1], member_id, self._selected_markers_.name(marker_id), marker_id), tag="resolve")

                for allele in alleles:
                    printnl("\t{0} {1} {2}/{3}/{4}".format(allele[0], allele[1],
                                                           self._selected_markers_.technology_name(allele[2]),
                                                           self._selected_markers_.experiment_name(allele[3]), allele[4]), tag="resolve")

            return MT, 3

    _cmd = "select allele1, allele2, technology, experiment, active, member, marker from genotypes where member = ?"

    cmdB = "select data, chromosome, block, technology, experiment, active from genotypeblocks where member = ? and chromosome = ? and block = ? and technology = ?"
    cmdM = "select data, chromosome, block, technology, experiment, active from genotypeblocks where member = ? and chromosome = ? and technology = ?"
    cmdX = "select data, chromosome, block, technology, experiment, active from genotypeblocks where member = ? and technology = ? order by chromosome,block"
    _cmdC = "select count(*) from genotypeblocks where member = ? and technology = ?"


    MT_LIST = []
    MT_HASH = {}
    def select_genotypes_from_snps(self, dbi, member_id, memrec, mxm_hash, mxm_defer, markers, allehsh, options):
##        import pdb;pdb.set_trace()
        snps = self._selected_markers_.chrm_list(tech='genotypes')
        if member_id not in mxm_hash:
            mxm_hash[member_id] = {}
            if mxm_defer:
                self.mxm_one_fill(mxm_hash, dbi.select_all(self._cmd, member_id))
# snps
        if not mxm_hash[member_id]:
            if options.debug:
#                   printnl("No genotype samples for member {0}:{1} ({2:d})".format(memrec[1], memrec[2], member_id))
                pass
        for mkrlst in snps:
            for mkrrec in mkrlst:
                marker_id = mkrrec[-2]
                if marker_id not in self._allele_order:
                    self._allele_order[marker_id] = set()
                if options.allelestat:
                    if marker_id in self._allele_stat:
                        allele_stat = self._allele_stat[marker_id]
                    else:
                        allele_stat = [0, 0, {}, {}, {}]
                        self._allele_stat[marker_id] = allele_stat

                dbrlst = mxm_hash[member_id].get(marker_id, self.MT_LIST)

                for dbr in dbrlst:
                    a1, a2, tech, expr, active, mid, marker_id = dbr
# 0: db cnt, 1: cnt, 2: founder allele cnt, 3: all allele cnt, 4: pairs cnt, 5: order
                    if options.allelestat:
                        allele_stat[0] += 1
                    if self._selected_markers_.filter(memrec, mkrrec, tech, expr, active, options):
                        continue
                    markers[marker_id] = True

                    if options.unobserved:
                        if not member_id in self._unobserved:
                            self._unobserved[member_id] = [0, 0]
                        unobserved = self._unobserved[member_id]
                        unobserved[0] += 1
                        if a1 == '0' and a2 == '0':
                            unobserved[1] += 1
#                               unobserved[2].append(dbr)
                        continue

                    alleles = dbr[:-2]
                    remap = self._allele_map_geno and self._allele_map_geno.get(expr, self.MT_HASH).get(marker_id, self.MT_HASH)
                    if remap:
                        alleles = remap.get(a1, '0'), remap.get(a2, '0'), tech, expr, active
                    if alleles in allehsh:
                        alleles = allehsh[alleles]
                    else:
                        allehsh[alleles] = alleles
                    if marker_id not in self._member_X_marker[member_id]:
                        self._member_X_marker[member_id][marker_id] = []
                    self._member_X_marker[member_id][marker_id].append(alleles)

    def select_genotypes_from_gwas(self, dbi, member_id, memrec, gwas, markers, allehsh, membercnt, options):
# 414 sec
#       dbi.dbtable('snpblocks', options.db)

        cnttupl = dbi.select_one(self._cmdC, member_id, gwas)
        if cnttupl == None:
            pass
        elif cnttupl[0] == 0:
            if options.verbose:
                printnl("No gwas block samples for member {0}:{1} ({2:d})".format(memrec[1], memrec[2], member_id), tag="memberXmarker")
        else:
            db_block_hash = {}
            if cnttupl[0] <= 100:
                for rec in dbi.select_all(self.cmdX, member_id, gwas):
                    db_block_hash.setdefault(rec[1], {})[rec[2]] = rec

            for chrm, mkrlst in enumerate(self._selected_markers_.chrm_list(tech=gwas)):
                oblock = None
                db_block_hash = {}

                if mkrlst:   # don't bother iff MT
                    for rec in dbi.select_all(self.cmdM, member_id, chrm, gwas):
                        db_block_hash.setdefault(rec[1], {})[rec[2]] = rec

                for mkrrec in mkrlst:
                    block, offset = mkrrec[5:7]
# 0: db cnt, 1: cnt, 2: founder allele cnt, 3: all allele cnt, 4: pairs cnt, 5: order
                    marker_id = mkrrec[-2]
                    mkrchrm   = mkrrec[0]
                    if marker_id not in self._allele_order:
                        self._allele_order[marker_id] = set()
                    if options.allelestat:
                        if marker_id in self._allele_stat:
                            allele_stat = self._allele_stat[marker_id]
                        else:
                            allele_stat = [0, 0, {}, {}, {}]
                            self._allele_stat[marker_id] = allele_stat
                        allele_stat[0] += 1

                    if oblock != block or chrm != mkrchrm:
                        if chrm != mkrchrm:
                            if membercnt == 0 and options.debug:
                                printnl("Chrshift: now {0} was {1} marker {2}".format(chrm, mkrchrm, mkrrec[-1]))
                        dbr = db_block_hash.get(mkrchrm, {}).get(block, None)
                        if dbr == None:
                            dbr = dbi.select_one(self.cmdB, member_id, mkrchrm, block, gwas)
                            if membercnt == 0 and options.debug:
                                printnl("Chrshift: forced")
                            db_block_hash.setdefault(mkrchrm, {})[block] = dbr
                        elif self._batch_test: # check, for fun
                            dbx = dbi.select_one(self.cmdB, member_id, mkrchrm, block, gwas)
                            if dbr != dbx:
                                printnl("TILT: batch {}, single {}", dbr, dbx)
                        if dbr is None:
                            printnl("No samples for member {0}:{1} ({2:d}), chromosome {3:d}, data block {4:d}".format(memrec[1], memrec[2], member_id, chrm, block))
                            continue
                        if options.debug >= 3:    printnl(dbr)

                        data, tech, expr, active = dbr[0], dbr[3], dbr[4], dbr[5]
                    if self._selected_markers_.filter(memrec, mkrrec, tech, expr, active, options):
                        continue
                    markers[marker_id] = True

                    allekey = data[2*offset-2:2*offset] # offset starts at 1 vs 0
                    remap = self._allele_map_gwas and self._allele_map_gwas.get(expr, self.MT_HASH).get(marker_id, self.MT_HASH)
                    if remap:
                        allekey = remap.get(allekey[0], '0') + remap.get(allekey[1], '0')

                    if allekey in allehsh:
                        alleles = allehsh[allekey]
                    else:
#6/1                    k1, k2 = allekey
                        k1, k2 = allekey[0:2]
                        if options.onetwo:
                            if k1 != '0' and k2 != '0':
                                if k1 != 'W' and k2 != 'W':
                                    k1 = 'A' if k1 == mkrrec[7][0] else 'B'
                                    k2 = 'A' if k2 == mkrrec[7][0] else 'B'
                        alleles = (k1, k2, tech, expr, active) # need extra info
                        allehsh[allekey] = alleles
                    if marker_id in self._member_X_marker[member_id]:
                        pass
#                           printnl("both {0} {1} had {2} adding {3}".format(
#                                   marker_id, member_id, self._member_X_marker[member_id][marker_id], alleles),
#                                   tag=True)
                    else:
                        self._member_X_marker[member_id][marker_id] = []
                    self._member_X_marker[member_id][marker_id].append(alleles)

                    if options.debug >= 3:
                        printnl("member {0:d}, mkrrec {1}, alleles {2}".format(member_id, mkrrec, alleles))
                    oblock = block if chrm == mkrchrm else None

    def select_genotypes_merge(self, member_id, memrec, markers, founder, options):
# 203 sec
        genotypes = self._member_X_marker[member_id]
        for marker_id in sorted(markers.iterkeys()):
#           alleles = self._member_X_marker[member_id][marker_id]
            alleles = genotypes[marker_id]
            if options.allelestat:
                allele_stat = self._allele_stat[marker_id]
                allele_stat[1] += 1
            self._total += 1
            Lalleles = len(alleles)
            if (Lalleles) == 0:
                pedper = self._selected_members_.name(member_id)
                printnl("No alleles for pedigree:person {0}:{1} ({2:d}) for marker {3} (id {4}):".format(pedper[0], pedper[1], member_id, self._selected_markers_.name(marker_id), marker_id))
#               self._member_X_marker[member_id][marker_id] = ('0', '0')
                genotypes[marker_id] = ('0', '0')
            elif (Lalleles) == 1:
                self._sanity += 1
#               self._member_X_marker[member_id][marker_id] = alleles[0]
                genotypes[marker_id] = alleles[0]
            else:
                self._possible_conflicts += 1
                self._sanity += len(alleles)
                if options.filter_by_active:
                    most_active = max((tupl[-1] for tupl in alleles))
                    olleles = alleles
#6/1                alleles = filter(lambda(x): x[-1] == most_active, alleles)
                    alleles = [x for x in alleles if x[-1] == most_active]
                if len(alleles) == 1:
#                   self._member_X_marker[member_id][marker_id] = alleles[0]
                    genotypes[marker_id] = alleles[0]
                    self._actives += 1
                    log.newrecord(tag='resolve')
                    pedper = self._selected_members_.name(member_id)
                    printn("member {0}:{1} ({2:d}) marker {3} (id {4}): ".format(pedper[0], pedper[1], member_id, self._selected_markers_.name(marker_id), marker_id), tag='resolve')
                    printnl("Using Active (#{0}) {1} from {2}".format(most_active, alleles[0], olleles), tag='resolve')
                else:
                    ans = self.resolve_conflict(member_id, marker_id, alleles, options)
                    if   ans[1] == 1:    self._dups      += 1
                    elif ans[1] == 2:    self._zeros     += 1
                    elif ans[1] == 3:    self._conflicts += 1
#r#                     print ans[1], alleles
#                   self._member_X_marker[member_id][marker_id] = ans[0]
                    genotypes[marker_id] = ans[0]
#           allele = self._member_X_marker[member_id][marker_id]
            allele = genotypes[marker_id]
            alleleab = allele[0:2]
            remap = self._allele_map_fini and self._allele_map_fini.get(None, self.MT_HASH).get(marker_id, self.MT_HASH)
            if remap:
                alleleab = remap.get(alleleab[0], '0'), remap.get(alleleab[1], '0')
                allele = alleleab[0], alleleab[1], allele[2], allele[3], allele[4],
#               self._member_X_marker[member_id][marker_id] = allele
                genotypes[marker_id] = allele

            if not options.person_order:    # otherwise use default
                self._allele_order[marker_id].update(alleleab)
            else:                            # otherwise use default
                hmm = self._allele_order[marker_id]
                if not alleleab == ('0', '0'):
                    if   hmm[1] == '!': hmm[1] = alleleab[1]
                    elif hmm[0] == '?' and hmm[1] != alleleab[1]: hmm[0] = alleleab[1]
                    if   hmm[1] == '!': hmm[1] = alleleab[0]
                    elif hmm[0] == '?' and hmm[1] != alleleab[0]: hmm[0] = alleleab[0]

                    if alleleab[0] not in hmm:
                        printnl("marker {0:10} (# {1:4}) allele {2} not in SNP {3} ... adding it".format(self._selected_markers_.name(marker_id), marker_id, alleleab[0], hmm), tag='badSNP')
                        hmm.append(alleleab[0])
                    if alleleab[1] not in hmm:
                        printnl("marker {0:10} (# {1:4}) allele {2} not in SNP {3} ... adding it".format(self._selected_markers_.name(marker_id), marker_id, alleleab[1], hmm), tag='badSNP')
                        hmm.append(alleleab[1])

            if options.allelestat:
                self.allele_stat_common(allele_stat, alleleab, founder)

    def select_genotypes(self, dbi, options):
#       import pdb;pdb.set_trace()
        if options.person_order:
            BEDfile = self._output_.output_person_order_prolog(options)

        if not options.unobserved:
            log.push(('inactive', 'notrequested', 'techfilter', 'exprfilter', 'TECHFILTER', 'EXPRFILTER'))
            log.push('memberXmarker')

            log.push('exclude_marker')
            if len(self._selected_markers_.exclusion()):
                printnl(" ... at most one member will be shown for each excluded marker ...", tag='exclude_marker')
                log.endheader()

            log.push('exclude_person')
            if len(self._selected_markers_.exclusion()):
                printnl(" ... at most one marker will be shown for each exclude person ...", tag='exclude_person')
                log.endheader()

            log.push('resolve', newrecord=True)
            log.push('badSNP')
        membercnt = trumem = 0
        allehsh = {}
        allehsh['WW'] = ('0', '0', 0, 0, 1)  # W means that no reading for that SNP; has to be a list for cnv

        sels = ""
        if self._selected_markers_.tech_ids():
            sels += ' and technology in '
            sels += '(' + ", ".join((str(x) for x in self._selected_markers_.tech_ids())) + ')'
        if self._selected_markers_.expr_ids():
            sels += ' and experiment in '
            sels += '(' + ", ".join((str(x) for x in self._selected_markers_.expr_ids())) + ')'
        if sels != "" and False:
#            import pdb;pdb.set_trace()
            self._cmdk += sels
            self._cmda += sels
            self._cmd  += sels

        snps_cnt = gwas_cnt = 0
        snps = []
        for chrmlst in self._selected_markers_.chrm_list(tech='genotypes'):
            for snp in chrmlst:
                snps.append(snp[-2])
        snps_cnt = len(snps)

        gwas_list = {}
        for key in self._selected_markers_.chrm_keys():
            if key not in ('genotypes', 'fin'):
                cnt = sum(map(len, self._selected_markers_.chrm_list(tech=key)))
                gwas_list[key] = cnt
                gwas_cnt += cnt

        lmem, lmark = (self._selected_members_.len(), self._selected_markers_.len())
        printnl("members {0:d}, markers {1:d} (snps {2:d}, gwas {3:d})\n".format(lmem, lmark,
                                                                                 snps_cnt, gwas_cnt), tag=True)
        lmark = snps_cnt
        log.endheader()

        self._allele_map_geno = self._allele_.load_map(dbi, 1, options)
        self._allele_map_gwas = self._allele_.load_map(dbi, 2, options)
        self._allele_map_fini = self._allele_.load_map(dbi, 3, options)

        mxm_hash, mxm_defer = self.fill_mxmhash(dbi, lmem, lmark, snps)

        for memrec in self._selected_members_:
            member_id = memrec[0]
            if memrec[-1] == 0:
                if options.verbose:
                    printnl("member {0}:{1} ({2:d}) is NOT active {3}".format(memrec[1], memrec[2], member_id, memrec))
                continue
            Time = time.time()

            if options.person_order:    # reset
                self._member_X_marker.clear()
                self._member_X_marker_conflicts.clear()
            self._member_X_marker[member_id] = {}
            self._member_X_marker_conflicts[member_id] = {}
            founder = memrec[3] == '0' and memrec[4] == '0'
            markers = {}

            self.select_genotypes_from_snps(dbi, member_id, memrec, mxm_hash, mxm_defer, markers, allehsh, options)

            for gwas in gwas_list.iterkeys():
                allehsh = {}
                allehsh['WW'] = ('0', '0', 0, 0, 1)  # W means that no reading for that SNP; has to be a list for cnv
                self.select_genotypes_from_gwas(dbi, member_id, memrec, gwas, markers, allehsh, membercnt, options)

            if options.unobserved: continue

            self.select_genotypes_merge(member_id, memrec, markers, founder, options)

            if member_id in mxm_hash:
                mxm_hash[member_id].clear()

            if options.person_order:
#               POTime = time.time()
                self._output_.output_person_order_member(member_id, BEDfile, options)
#               printnl("Person Order Out time {0:6.3f}".format(time.time() - POTime))
#               import pdb;pdb.set_trace()

            membercnt += 1
            sumry = "{5:4d}/{6:d}: member {0:>6}:{1:<6} (#{2:4d}) {3:7d} snps {4:6.3f} sec"
            printnl(sumry.format(memrec[1], memrec[2], member_id, len(self._member_X_marker[member_id]),
                                 time.time()-Time, membercnt, lmem), tag='memberXmarker')
            if self._member_X_marker[member_id]: trumem += 1
##          if membercnt >= 5: break
##          if trumem >= 100: import pdb; pdb.set_trace()

        if options.person_order:  # safe to output map chr now
            self._output_.output_person_order_epilog(BEDfile, options)

        if not options.unobserved:
            log.beginfooter()
            printnl("total {0:d}, possible_conflicts {1:d} ({2:d}), chose active {3:d}, concordance {4:d}, ignore zero {5:d}, conflicts {6:d}".format(self._total, self._possible_conflicts, self._sanity, self._actives, self._dups, self._zeros, self._conflicts), tag=True)
            log.pop('badSNP')
            log.pop('resolve')
            log.pop('exclude_person')
            log.pop('exclude_marker')
            log.pop('memberXmarker')
            log.pop(('inactive', 'notrequested', 'techfilter', 'exprfilter', 'TECHFILTER', 'EXPRFILTER'))

    def set_default_alleleorder(self, dbi, options):
        for marker_id in self._selected_markers_:
#           self._allele_order[marker_id] = self._selected_markers_.alleles(marker_id)
            self._allele_order[marker_id] = ['?', '!']

    def set_alleleorder(self, dbi, options):
        log.push('noallele', newrecord=True)
        log.push('greater2', newrecord=True)
        for marker_id in self._selected_markers_:
            allelecopy = self._allele_order[marker_id]
            allelecopy.discard('0')

            allelecopy_len = len(allelecopy)
# allele info from snpblocks is NOT reliable!!
#           if allelecopy_len != 2 or options.heterozygote in ('unchanged', 'lexical'):
#               als = self._selected_markers_.alleles(marker_id)
#               if als:
#                   self._allele_order[marker_id] = als
#                   continue

            if allelecopy_len == 1:
                al = allelecopy.pop()
#                self._allele_order[marker_id] = sorted([al, self._allele_.guess_allele(al)])
                self._allele_order[marker_id] = ('?', al)
                continue
            elif allelecopy_len == 0:
                log.newrecord(tag='noallele')
                printn("no alleles for snp {0} ({1:d})\n".format(self._selected_markers_.name(marker_id), marker_id), tag='noallele')
                self._allele_order[marker_id] = ('?', '!')
                continue
            elif allelecopy_len != 2:
                log.newrecord(tag='greater2')
                sumry = "unexpected allele count {0:d} ({1}) for snp {2} ({3:d})"
                printn(sumry.format(len(allelecopy), allelecopy if len(allelecopy) else '',
                                    self._selected_markers_.name(marker_id),
                                    marker_id), tag='greater2')
                if options.allelestat:
                    printnl(" ... {0}".format(self._allele_stat[marker_id]), tag='greater2')
                else:
                    printnl("", tag='greater2')

            if options.heterozygote == 'lexical':
                self._allele_order[marker_id] = sorted(allelecopy)
                continue

            allelefreq = self._allele_stat[marker_id][3] # founders + nonfounders == allsubjects
            allelecopy = allelefreq.copy()
            if '0' in allelecopy:    del allelecopy['0']

            if options.heterozygote == 'keep-allele-order':
                memrec = self._selected_members_.__iter__().next()
                first = self._member_X_marker[memrec[0]][marker_id]
                order = allelecopy.keys()
                order.remove(first[0])
                if first[1] in order:   # 1 != 2
                    order.remove(first[1])
                    order = list(first) + order
                else:
                    order = [first[0]] + order
            elif options.heterozygote == 'founders':
                allelefreq = self._allele_stat[marker_id][2] # founders  == allsubjects - nonfounders
                allelecopy = allelefreq.copy()
                if '0' in allelecopy:
                    del allelecopy['0']
                order = tuple([x[0] for x in sorted(allelecopy.iteritems(), key=itemgetter(1))])
            elif options.heterozygote == 'allsubjects':
                order = tuple([x[0] for x in sorted(allelecopy.iteritems(), key=itemgetter(1))])
            self._allele_order[marker_id] = order
            if options.debug >= 3:
                printnl("allele frequency {0} ({1:d}): {2} {3}".format(self._selected_markers_.name(marker_id), marker_id, allelefreq, order))
        log.pop('greater2')
        log.pop('noallele')

    def heterozygoteorder(self, heterozygote):
#       log.push('heterozygoteeorder')
        for member_id, rec in self._member_X_marker.iteritems():
            for marker_id, alleles in rec.iteritems():
                order = self._allele_order[marker_id]
                if (alleles[0] != alleles[1]) and \
                        (order.index(alleles[0]) if alleles[0] in order else -1) > \
                        (order.index(alleles[1]) if alleles[1] in order else -1):
                    alleles = alleles[1], alleles[0], alleles[2], alleles[3], alleles[4]
                    self._member_X_marker[member_id][marker_id] = alleles
#                   printnl("{0:>15} {1:>10} {2}/{3} -> {4}".format(self._selected_members_.name(member_id), self._selected_markers_.name(marker_id), a1, a2, order))
#       log.pop('heterozygoteeorder')

    def allele_stat(self, options):
        cnt = 0
        log.push("allelestat", newrecord=True)
        printnl("ALLELE statistics")
        fmtl = self._selected_markers_.fmtlen('marker', 'snpname')
        # 0: db cnt, 1: cnt, 2: founder allele cnt, 3: all allele cnt, 4: pairs cnt, 5: order
        hfs1 = "{0:{fld[0]}} {1:{fld[1]}} {2:>6}  {3:>9}  {4:>2} {5:>6} {6:>2} {7:>6} {8:>6} {9:>6} {10:>6} {11:>6} {12:>6} {13:>9} {14:>7} {15:>9}"
        printnl(hfs1.format('marker', 'snpname', 'total', '#alleles', '#1', 'freq ', '#2', 'freq ', '00', '11', '12', '22', 'other', 'observed', 'repeat', 'singleton', fld=fmtl))
        log.endheader()
        hfs2 = "{0:{fld[0]}} {1:{fld[1]}} {2:>6} "
        for marker_id in self._selected_markers_:
            tpl = self._allele_stat.get(marker_id, None)
            if tpl:
                if tpl[1] > 0 or options.debug:
                    log.newrecord()
                    cnt += 1
                    printn(hfs2.format(marker_id, self._selected_markers_.name(marker_id), tpl[0], fld=fmtl))

                    alset = tpl[3]
                    if '0' in alset:
                        del alset['0']
                    if 'order' in alset:
                        del alset['order']
                    allab = sorted(alset.iterkeys())
                    if len(allab) == 0:
                        printnl('')
                        continue
                    elif len(allab) == 1:
                        allab.append('?')
                    alhash = tpl[4]

                    observed = sum(alhash.itervalues())
                    zero = alhash.get('00', 0)
                    observed -= zero
                    other = observed

                    tple = []
                    for k in (allab[0]+allab[0], allab[0]+allab[1], allab[1]+allab[1]):
                        v = alhash.get(k, 0)
                        other -= v
                        tple.append(v)

                    printn("{0:9d}  {1:>2} {2:6.4f} {3:>2} {4:6.4f}".format(len(alset), allab[0], float(2*tple[0]+tple[1])/observed/2,
                                                                            allab[1], float(2*tple[2]+tple[1])/observed/2))
#                   observed = alset[allab[0]] + alset[allab[1]]
#                   printn("[{0:d} {1:6.4f} {2:6.4f}]".format(observed, float(alset[allab[0]])/observed, float(alset[allab[1]])/observed))
                    printn("{0:6d}".format(zero))
                    printn("{0[0]:6d} {0[1]:6d} {0[2]:6d}".format(tple))

                    printn("{0:6d}".format(other))
                    printn('{0:9d}'.format(observed))

                    MTHASH = {}
                    untyp = people = zero = rep = 0
                    for memrec in self._selected_members_:
                        member_id = memrec[0]
                        if self._member_X_marker.get(member_id, True):
                            snp = self._member_X_marker[member_id].get(marker_id, None)
                            if snp and not (snp[0] == '0' and snp[1] == '0'):
                                people += 1
                                if self._member_X_marker_conflicts.get(member_id, MTHASH).get(marker_id, False):
                                    rep += 1
                            elif snp:
                                zero += 1
                            else:
                                untyp += 1
                        else:
                            untyp += 1
                    printnl("{0:7d} {1:9d}".format(rep, people - rep))
        log.beginfooter(tag="allelestat")
        printnl("{0:d} markers listed above".format(cnt), tag="always")
        log.pop("allelestat")

    GENDER = 5
    TYPE   = 7
    def pedigree_stat(self, options):
        pedigrees = people = males = females = missing = full = half = total = 0
        typed_pedigrees = typed_people = typed_males = typed_females = 0
        untyped_pedigrees = untyped_people = untyped_males = untyped_females = 0
        pedigree = {}
        typed_pedigree = {}
        untyped_pedigree = {}
#       memrec = "select id, pedigree, person, father, mother, gender, active, type from members"
        for memrec in self._selected_members_:
            member_id = memrec[0]
            memmissing = memtotal = 0
            for marker, genotypes in self._member_X_marker[member_id].iteritems():
                total += 1
                memtotal += 1
                if genotypes[0] == '0' and genotypes[1] == '0':
                    missing += 1
                    memmissing += 1
                elif genotypes[0] != '0' and genotypes[1] != '0':
                    full += 1
                else:
                    half += 1

            typed = True if memmissing != memtotal else False

            pedigree[memrec[1]] = True
            if typed:
                typed_pedigree[memrec[1]] = True
            else:
                untyped_pedigree[memrec[1]] = True

            people += 1
            if typed:
                typed_people += 1
            else:
                untyped_people += 1

            if memrec[self.GENDER] == 1:
                males += 1
                if typed:
                    typed_males += 1
                else:
                    untyped_males += 1
            elif memrec[self.GENDER] == 2:
                females += 1
                if typed:
                    typed_females += 1
                else:
                    untyped_females += 1
            else:
                printnl("Bad gender {0:d} in {1}".format(memrec[self.GENDER], memrec))

        pedigrees = len(pedigree)
        for ped in pedigree.iterkeys():
            if typed_pedigree.get(ped, False):
                typed_pedigrees += 1
            else:
                untyped_pedigrees += 1

        printnl("Pedigree statistics")
        hfs1 = "{0:>7} {1:>10} {2:>6} {3:>6} {4:>8} {5:>10} {6:>10} {7:>6}"
        printnl(hfs1.format('', '', '', '', '', 'Marker', 'Genotypes', ''))
        printnl(hfs1.format('', '', '', '', '', 'fully', 'Half', ''))
        printnl(hfs1.format('', 'Pedigrees', 'People', 'Males', 'Females', 'Typed', 'Typed', 'Total'))

        printnl(hfs1.format('Total', pedigrees, people, males, females, full, half, total-missing))
        hfs2 = "{0:>7} {1:10d} {2:6d} {3:6d} {4:8d}"
        printnl(hfs2.format('Typed', typed_pedigrees, typed_people, typed_males, typed_females))
        printnl(hfs2.format('Untyped', untyped_pedigrees, untyped_people, untyped_males, untyped_females))

    def epilog(self, dbi, options):
        if not options.stat or options.person_order:
            printnl('\nWork Summary:')
            if options.unobserved:
                FILE = options.dir + options.output + '.unobserved.txt'
                printnl(" Created data file: {0}".format(FILE))
                return
            flist = self._output_.files(options)
            if len(flist) == 1:
                printnl(" Created \"{0}\" format file: {1[0]}".format(options.type, flist))
            else:
                printnl(" Created \"{0}\" format files: {1}".format(options.type, flist))
            return

        self._selected_markers_.stat({}, options)
        self._selected_members_.stat(options)

        printnl('')
        self._selected_markers_.filter_stat(options)
        printnl('')
        self.pedigree_stat(options)
        printnl('')
        self.allele_stat(options)
        printnl('')
        self._selected_markers_.marker_stat(self._allele_stat, options)
        printnl('')

        self._allele_.conflict_stats(options, self._member_X_marker_conflicts, self._selected_members_, self._selected_markers_)

        printnl('\nWork Summary:')
        flist = self._output_.files(options)
        if len(flist) == 1:
            printnl(" Created \"{0}\" format file: {1[0]}".format(options.type, flist))
        else:
            printnl(" Created \"{0}\" format files: {1}".format(options.type, flist))

        ts   = {}
        tsdb = {}
        es   = {}
        esdb = {}
        mks  = {}
        mk0  = {}
        chhh = {}
        alsc = 0
        alscdb = 0
        memc = 0
        MTH  = {}
        MTT  = ()
        for mem, v in self._member_X_marker.iteritems():
            for mar, al5 in v.iteritems():
                a1, a2, t, e, a = al5
                ts[t] = ts.get(t, 0) + 1
                es[e] = es.get(e, 0) + 1
                alsc += 1
                cnfls = self._member_X_marker_conflicts.get(mem, MTH).get(mar, MTT)
                if len(cnfls):
#                    alscdb += reduce(lambda x, y:x+y[0], cnfls, 0)

                    for cnfl in cnfls:
                        for al5db in cnfl[1:]:
                            adb1, adb2, tdb, edb, adb = al5db
                            tsdb[tdb] = tsdb.get(tdb, 0) + 1
                            esdb[edb] = esdb.get(edb, 0) + 1
                            alscdb += 1
                else:
                    tsdb[t] = tsdb.get(t, 0) + 1
                    esdb[e] = esdb.get(e, 0) + 1
                    alscdb += 1

                mks[mar] = mks.get(mar, 0) + 1
                if a1 != '0' and a2 != '0':
                    mk0[mar] = mk0.get(mar, 0) + 1
            memc += 1
        disc  = es.pop(0, 0)
        disc1 = ts.pop(0, 0)

        for m, cnt in mk0.iteritems():
            chrm = self._selected_markers_.chrm(m)
            chhh[chrm] = chhh.get(chrm, 0) + cnt

        printnl(" {0} members, {1} markers, {2} untyped markers".format(memc, len(mks), (len(mks)-len(mk0)) ))
        printnl(" {0} observed genotypes, {1} database genotypes".format(sum(mk0.itervalues()), alscdb))
        printnl(" {0} discordant genotypes tuples".format(disc))
        printnl(" {0} output technologies, {1} database technologies".format(len(ts), len(tsdb)))
        printnl(" {0} output experiments, {1} database experiments".format(len(es), len(esdb)))

        printnl(" Chromosome coverage with counts:")
        j = 0
        for i in range(1, 26):
            cnt = chhh.get(i, None)
            if cnt:
                chrm = self._selected_markers_.int2chrm(i)
                if j > 4:
                    j = 0
                    printnl("")
                printn(" {0:>2}: {1:5d},".format(chrm, cnt))
                j += 1

        return True
