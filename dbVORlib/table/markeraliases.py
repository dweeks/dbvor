#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

#from dbVORlib.marker           import Marker
from dbVORlib.util             import printnl

from dbVORlib.table.dbtable    import DBTable



class MarkerAlias(DBTable):

    def __init__(self, db):
        super(MarkerAlias, self).__init__(db)
#       self._marker_    = Marker(db)
        self._aliases    = {}
        self._name       = None

    def add_options(self, config):
        super(MarkerAlias, self).add_options(config)
        config.add_option("--old", action="store", type="string", default="1",
                          help="choose columns to get old marker string from input")
        config.add_option("--new", action="store", type="string", default="2",
                          help="choose columns to get new marker string from input")

        config.add_option("--active", action="store", type="int", default=1,
                          help="active value")

    def hint(self):
        print "\neither: dbvor markeralias --old=name --new=name"
        print "or:     dbvor markeralias --old=column# --new=column# --input=filename\n"

    def prolog(self, dbi, options):
        super(MarkerAlias, self).prolog(dbi, options)
        for rec in self._dbi_.select_all("select lower(alias), marker, id from marker_aliases"):
            self._aliases[rec[0]] = rec
        return True

    def run(self, dbi, options):
        if options.input:
            return super(MarkerAlias, self).run(dbi, options)
        else:
            new = []
            new.append(options.old)
            new.append(options.new)
            self._name = new[0]
            new.append(options.active)
            self._line_no = 1
            self._line = "genodb markeralias --old={0} --new={1}".format(options.old, options.new)
            self.row_util(new, self._name, dbi, options)
            return True

    def do_row(self, fields, dbi, options):
        new = []
        new.append(fields[int(options.old) - self.ORIGIN])
        new.append(fields[int(options.new) - self.ORIGIN])
        self._name = new[0]
        new.append(options.active)
        self.row_util(new, self._name, dbi, options)

    cmds = "select marker from markers where id = ?"
    def select(self, fields, options):
        alias  = self._aliases.get(fields[0].lower(), None)
        if alias:
            marker = self._dbi_.select_one(self.cmds, alias[1])
            if marker:
                marker = marker[0]
            return ((fields[0], marker, alias[1], alias[2]),)
        else:
            return tuple()

    cmdm = "select marker, id from markers where marker = ?"
    cmdu = "update markers set marker = ? where id = ?"
    cmdU = "update marker_info set marker = ? where marker = ?"

    cmdd = "delete from marker_aliases where id = ?"
    def delete(self, rec, options):
        nw = self._dbi_.select_one(self.cmdm, rec[1])
        if nw:
            marker, Mid = nw
            rec2 = self._dbi_.select_one(self.cmdm, rec[0]+rec[1])
            if rec2 is None:
                self._dbi_.execute(self.cmdu, rec[0], Mid)
            else:
                self._dbi_.execute(self.cmdu, rec[0], rec2[1])
            self._dbi_.execute(self.cmdU, rec[0], rec[1])
            self._dbi_.execute(self.cmdd, rec[-1])

    cmdi = "insert into marker_aliases (alias, marker, projectid) VALUES (?, ?, ?)"
    def insert(self, fields, options):
        rec = self._dbi_.select_one(self.cmdm, fields[0])
        if rec:
            marker, Mid = rec
            rec2 = self._dbi_.select_one(self.cmdm, fields[1])
            if rec2 is None:
                self._dbi_.insert(self.cmdi, [(fields[0], Mid, options.project_id)])
                self._dbi_.execute(self.cmdu, fields[1], Mid)
            else:
                self._dbi_.insert(self.cmdi, [(fields[0], rec2[1], options.project_id)])
                self._dbi_.execute(self.cmdu, fields[0]+fields[1], Mid)
                printnl("MarkerAlias: both {0}".format(fields), tag=True)
            self._dbi_.execute(self.cmdU, fields[1], fields[0])
        else:
            printnl("MarkerAlias: disappeared {0}".format(fields), tag=True)


    def cmp(self, rec, fields, options):
        return cmp(rec[0], fields[0]) or cmp(rec[1], fields[1])

    def validate(self, fields, name, options):
        return True

class GMIMarkerAlias(MarkerAlias):
    sentinel = "Names of the following SNPs have merged:"
    brake    = "---------------------------------------------------"
    intro    = "Old	New"
    def __init__(self, db):
        super(GMIMarkerAlias, self).__init__(db)
        self._gmi_process = None
        self._count       = 0

    def do_row(self, fields, dbi, options):
        if self._gmi_process:
            if self._line == self.brake:
                if self._count > 0:
                    self._gmi_process = False
                return
            elif self._line == self.intro:
                return
            else:
                self._count += 1
                fields.append(options.active)
                self._name = fields[0]
                printnl("{0:5d}: GMI merging markers: {1} (old) into {2} (merged)".format(self._line_no, fields[0], fields[1]), tag='verbose')
                super(GMIMarkerAlias, self).row_util(fields, self._name, dbi, options)
        elif self._line == self.sentinel:
            self._gmi_process = True

    def pr_rec(self, fields, options, db=False):
        if options.verbose:
            return "snp:{0[0]} merged to snp:{0[1]}".format(fields)
        else:
            return "snp:{0[0]} merged to snp:{0[1]} active:{0[2]}".format(fields)

    def epilog(self, dbi, options):
        ret = super(GMIMarkerAlias, self).epilog(dbi, options)
        if self._gmi_process is None:
            printnl("\nNote: GMI found no Marker Aliases")
        elif self._count > 0:
            printnl("\nGMI processed {0:d} Marker Aliases".format(self._count))
        return ret
