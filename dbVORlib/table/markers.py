#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from dbVORlib.marker           import Marker
from dbVORlib.util             import NoTrace, printnl, printn, log
from dbVORlib.chrm             import Chrm

from dbVORlib.table.dbtable    import DBTable


class BaseMMarker(DBTable):

    def __init__(self, db):
        super(BaseMMarker, self).__init__(db)
        self._marker_    = Marker(db)
        self._builds     = {}
        self._aliases    = {}
        self._use_cache  = True
        self._cache      = {}
        self._name       = None
        self._im         = None

    def add_options(self, config):
        super(BaseMMarker, self).add_options(config)
        config.add_option("--build", action="store", type="string",
                          help="build name")
        config.add_option("--active", action="store", type="int", default=1,
                          help="active value")

    def prolog(self, dbi, options):
        super(BaseMMarker, self).prolog(dbi, options)
        for rec in self._dbi_.select_all("select lower(alias), marker, id from marker_aliases"):
            self._aliases[rec[0]] = rec

        self._builds = dict(self._dbi_.select_all("select lower(build), id from builds"))
        if options.build is None:
            printn("\n Existing Database names: ", tag=True)
            for name in sorted(self._builds.iterkeys()):
                printn("{0}".format(name), tag=True)
            printnl("", tag=True)
            raise NoTrace("ERROR: You must specify a build name: either an existing name or a new name.")

        if not options.build.lower() in self._builds:
            self.commit_build(options)
            if options.commit: printnl("Adding build name \"{0}\" to database".format(options.build))

        cmdi = "insert into marker_info (marker, build, chromosome, avg, male, female, pos, extrapolation, active)"
        fmt  = "('{0}', {1:d}, '{2:}', {3:}, {4:}, {5:}, {6:}, {7:}, {8:d})"
#       fmt += " {0:d})".format(options.active)
        return self.insert_many(cmdi, fmt, options, cnt=1000)

    def epilog(self, dbi, options):
        if options.commit:
            if self._im:
                self._im()
        return True

    def commit_build(self, options):
        self._dbi_.insert("""insert into builds (build) VALUES (?)""", [(options.build,)])
        mid = self._dbi_.select_one("select id from builds where build = ?", options.build)
        if mid is None:
            raise Exception("oops: Build({0}) insert into database failed".format(options.build))
        self._builds[options.build.lower()] = mid[0]

    def row_util(self, fields, name, dbi, options):
        if fields[0] in self._aliases:
            alias = self._aliases[fields[0]][1]
            merge = dbi.select_one("select marker from markers where id = ?", alias)
            fields[0] = merge[0]
        super(BaseMMarker, self).row_util(fields, name, dbi, options)

    def pr_rec(self, fields, options, db=False):
        if options.verbose:
            cmd = "marker:{0[0]!r} bldstr:{0[1]!r} chromosome:{0[2]} avg:{0[3]} male:{0[4]} female:{0[5]} pos:{0[6]}"
        else:
            cmd = "marker:{0[0]!r} bldstr:{0[1]!r} chromosome:{0[2]} avg:{0[3]} male:{0[4]} female:{0[5]} pos:{0[6]} extrapolation:{0[7]} active:{0[8]}"

        return cmd.format(fields)

    cmds = "select marker, 'bldstr', chromosome, avg, male, female, pos, extrapolation, active, id from marker_info where marker = ? and build = ?"
    cmd1 = "select marker, 'bldstr', chromosome, avg, male, female, pos, extrapolation, active, id from marker_info where chromosome = ? and build = ?"
    _MT = []
    def select(self, fields, options):
        bld = self._builds[fields[1].lower()]
        if self._use_cache:
            if not self._cache:
                cmds = self.cmd1.replace("bldstr", fields[1])
                for i, v in enumerate(Chrm(self._dbi_).chrm_0()):
                    for rec in self._dbi_.select_all(cmds, i, bld):
                        self._cache[rec[0]] = (rec,)
            ret = self._cache.get(fields[0], self._MT)
            return ret
        else:
            cmds = self.cmds.replace("bldstr", fields[1])
            return self._dbi_.select_all(cmds, fields[0], bld)

    cmdd = "delete from marker_info where id = ?"
    def delete(self, rec, options):
        self._dbi_.execute(self.cmdd, rec[-1])

    def insert(self, fields, options):
        fields[1] = self._builds[fields[1].lower()]
#6/1    self._im(map(lambda x:(x if x is not None else 'NULL'), fields))
        self._im([x if x is not None else 'NULL' for x in fields])

# defunct
    cmdi = "insert into marker_info (marker, build, chromosome, avg, male, female, pos, extrapolation, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
    def Zinsert(self, fields, options):
        fields[1] = self._builds[fields[1].lower()]
        self._dbi_.insert(self.cmdi, [fields])

    def cmp(self, rec, fields, options):
        return cmp(rec[0], fields[0]) or \
               cmp(rec[1], fields[1]) or cmp(rec[2], fields[2]) or \
               cmp(rec[3], fields[3]) or cmp(rec[4], fields[4]) or \
               cmp(rec[5], fields[5]) or cmp(rec[6], fields[6]) or \
               cmp(rec[7], fields[7]) or cmp(rec[8], fields[8])

    def validate(self, fields, name, options):
        ret = True
        NA = None
        if self._marker_.chrm_valid(fields[2]):
            fields[2] = self._marker_.chrm2int(fields[2])
        else:
            self.pr_context("marker {0} (build {1}) bad chromosome: {2}".format(fields[0], fields[1], fields[2]))
            options.error += 1
            ret = False

        try: fields[3] = NA if fields[3] == 'NA' else float(fields[3])
        except TypeError:
            self.pr_context("average genetic distance is not a floating point number, viz. {0}".format(fields[3]))
            options.error += 1
            ret = False

        try: fields[4] = NA if fields[4] == 'NA' else float(fields[4])
        except TypeError:
            self.pr_context("male genetic distance is not a floating point number, viz. {0}".format(fields[4]))
            options.error += 1
            ret = False

        try: fields[5] = NA if fields[5] == 'NA' else float(fields[5])
        except TypeError:
            self.pr_context("female genetic distance is not a floating point number, viz. {0}".format(fields[5]))
            options.error += 1
            ret = False

        try: fields[6] = NA if fields[6] == 'NA' else int(fields[6])
        except TypeError:
            self.pr_context("gene position is not a integer number, viz. {0}".format(fields[6]))
            options.error += 1
            ret = False

        try: fields[7] = NA if fields[7] == 'NA' else int(fields[7])
        except TypeError:
            self.pr_context("extrapolation value is not a integer number, viz. {0}".format(fields[7]))
            options.error += 1
            ret = False

        return ret

class MMarker(BaseMMarker):

    def add_options(self, config):
        super(MMarker, self).add_options(config)
        config.add_option("--marker", action="store", type="int", default=None,
                          help="choose columns to get marker string from input")
        config.add_option("--chromosome", action="store", type="int", default=None,
                          help="choose columns to get chromosome string from input")
        config.add_option("--average", "--avg", action="store", type="int", default=None,
                          help="choose columns to get average genetic distance float number from input")
        config.add_option("--male", action="store", type="int", default=None,
                          help="choose columns to get male genetic distance float number from input")
        config.add_option("--female", action="store", type="int", default=None,
                          help="choose columns to get female genetic distance float number from input")
        config.add_option("--position", "--pos", action="store", type="int", default=None,
                          help="choose columns to get absolute distance integer value from input")
        config.add_option("--extrapolation", action="store", type="int", default=None,
                          help="choose columns to get extrapolation integer value from input")

    def header(self, File, dbi, options):
        line = File.readline().strip()
        fields = line.split()
        new = []
        new.append(fields[options.marker - self.ORIGIN])
        new.append(options.build)

        new.append('U' if options.chromosome     is None else fields[options.chromosome - self.ORIGIN])
        new.append('NA' if options.average       is None else fields[options.average - self.ORIGIN])
        new.append('NA' if options.male          is None else fields[options.male - self.ORIGIN])
        new.append('NA' if options.female        is None else fields[options.female - self.ORIGIN])
        new.append('NA' if options.position      is None else fields[options.position - self.ORIGIN])
        new.append('NA' if options.extrapolation is None else fields[options.extrapolation - self.ORIGIN])
        new.append(options.active)
        printnl("{9:4} {0:10} {1:15} {2:6} {3:6} {4:6} {5:6} {6:8} {7:6} {8:6}".format("Marker", "Build", "Chrm", "AvgGen", "MalGen", "FemGen", "BpPos", "Extrap", "Active", ""), tag="verbose")
        printnl("hdr: {0[0]:10} {0[1]:15} {0[2]:6} {0[3]:6} {0[4]:6} {0[5]:6} {0[6]:8} {0[7]:6} {0[8]:6}".format(new), tag="verbose")

    def prolog(self, dbi, options):
        super(MMarker, self).prolog(dbi, options)
        if options.marker is None:
            raise NoTrace("ERROR: You must specify a marker position.")
        return True

    def do_row(self, fields, dbi, options):
        new = []
        new.append(fields[options.marker - self.ORIGIN])
        self._name = new[0]
        new.append(options.build)

        new.append('U' if options.chromosome     is None else fields[options.chromosome - self.ORIGIN])
        new.append('NA' if options.average       is None else fields[options.average - self.ORIGIN])
        new.append('NA' if options.male          is None else fields[options.male - self.ORIGIN])
        new.append('NA' if options.female        is None else fields[options.female - self.ORIGIN])
        new.append('NA' if options.position      is None else fields[options.position - self.ORIGIN])
        new.append('NA' if options.extrapolation is None else fields[options.extrapolation - self.ORIGIN])
        new.append(options.active)
        log.newrecord()
        printnl("{1:4} {0[0]:10} {0[1]:15} {0[2]:6} {0[3]:6} {0[4]:6} {0[5]:6} {0[6]:8} {0[7]:6} {0[8]:6}".format(new, ""), tag="verbose")
        self.row_util(new, self._name, dbi, options)

class GMIMMarker(BaseMMarker):
#    def __init__(self, db):
#        super(GMIMMarker, self).__init__(db)

#    def add_options(self, config):
#        super(GMIMMarker, self).add_options(config)


    _gmi_head    = ['Chromosome', 'Map.k.a', 'Name', 'Map.k.m', 'Map.k.f', 'Ensembl', 'X.Extrapolation']
    _gmi_headX   = ['Chromosome', 'Map.k.a', 'Name', 'Map.k.m', 'Map.k.f', 'User.p', 'X.Extrapolation', 'X.Chromosome.e', 'X.Map.k.a.e', 'X.Map.k.m.e', 'X.Map.k.f.e', 'X.Ensembl', 'X.Extrapolation.e']
    def header(self, File, dbi, options):
        line = File.readline().strip()
        header = line.split()
        if len(header) == 7:
            if header[0:5] != self._gmi_head[0:5] or header[6] != self._gmi_head[6] or not header[5].startswith(self._gmi_head[5]):
                raise Exception("File \"{0}\": bad header/format\nread: {1}\nneed: {2}\n".format(File.name, header, self._gmi_head))
        elif len(header) == 14-1:    # name is not repeated
            if header[0:5] != self._gmi_headX[0:5] or header[6:11] != self._gmi_headX[6:11] or header[12] != self._gmi_headX[12]\
               or not (header[5].startswith(self._gmi_headX[5])) \
               or not header[11].startswith(self._gmi_headX[11]):
                raise Exception("File \"{0}\": bad header/format\nread: {1}\nneed: {2}\n".format(File.name, header, self._gmi_headX))
        else:
            raise Exception("File \"{0}\": bad header/format\nread: {1}\nneed: {2}\nor need: {3}\n".format(File.name, header, self._gmi_head, self._gmi_head))

        printnl("valid gmi header: {0}\n".format(line))

    def do_row(self, fields, dbi, options):
        if len(fields) > 7:
            fields = fields[0:7]
        fields[0], fields[1], fields[2] = fields[2], fields[0], fields[1]
        self._name = fields[0]
        fields.insert(1, options.build)
        fields.append(options.active)
        if options.debug > 1:
            printnl("line {0:4d}: {1}".format(self._line_no, fields))

        self.row_util(fields, fields[0], dbi, options)
