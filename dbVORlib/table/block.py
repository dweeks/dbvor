#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import os.path

from dbVORlib.member   import Member
from dbVORlib.validate import Validate
from dbVORlib.util     import gzopen, printnl, printn

#  'insert into samples (projectid, techid, subject, activefwd, pedigree, person, well, misc, sample, active)'
#  = "({0:d}, {1:d}, .format(options.project_id, options.tech_id)"
#  = "{0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', "
#  = "{0:d}).format(options.active)"

#  'insert into snpblocks (projectid, technology, experiment, snp, rs, chromosome, bp, cm, block, offset, allele, active)'
#  = "({0:d}, {1:d}, {2:d},.format(options.project_id, options.tech_id, options.experiment_id)"
#  = " '{0}', '{1}', {2:d}, {3:d}, '{4:f}', {5:d}, {6:d}, '{7}',"
#  = "((r[Snp.pid], r[Snp.rs], r[Snp.chr], r[Snp.bp], r[Snp.cm], r[Snp.blk], r[Snp.off], r[Snp.ab]))"
#  = " {0:d}).format(options.active)"

#  'insert into genotypeblocks (projectid, experiment, technology, member, sampleid, chromosome, block, active, seqlock, data) VALUES '
#  = "(options.project_id, options.experiment_id, options.tech_id, member, sampleid, chrm, blk, active, self._seqlock, content)"

class BlockCommon(Validate):
    NO_DIR = True
    NO_LOG = True

    def __init__(self, db):
        self._samples     = {}
        self._experiments = {}
        super(BlockCommon, self).__init__(db)

    def add_options(self, config):
        super(BlockCommon, self).add_options(config)
        config.change_default("config", None)    # can operate w/o config file
        config.add_option("--commit", action="store_true", default=False,
                          help="commit changes to database")
        config.add_option("--rollback", action="store_true", default=False,
                          help="force all database operations to be aborted")
        config.add_option("--active", action="store", type="int", default=1,
                          help="active value for member; members with 0 are ignored")

    def prolog(self, dbi, options):
        dbi.db_might_commit(True)
        self.validate_required(options, experiment=True)
        return False if options.error else True

    def get_samples(self, options):
        cmd = "select id, activefwd, subject, pedigree, person, filename, plate, well, misc, experiment, sample, time_stamp from samples where active = ?"
        for record in self._dbi_.select_all(cmd, options.active):
            self._samples[(record[-2], record[-3])] = record

    def get_experiments(self, options):
        cmd = "select id, filename from experiments"
        self._experiments = dict(self._dbi_.select_all(cmd))

    def reset_filename(self, filename, sample, sid, options):
        if not self._samples: self.get_samples(options)

        cmdfn = "update samples set filename = {0} where id = {1} and experiment = {2}"
        printnl(cmdfn.format('NULL', repr(sample), repr(options.experiment)))

        cmdfnQ = cmdfn.format('?', '?', '?')
        if options.commit:
            self._dbi_.execute(cmdfnQ, None, sid, options.experiment_id)


class DeleteSnpBlockMap(BlockCommon):

    def prolog(self, dbi, options):
        dbi.db_might_commit(True)
        self.validate_required(options, technology=True)
        return False if options.error else True

    def run(self, dbi, options):

#        cmd = "delete from snpblocks where projectid = {0} and experiment = {1} and technology = {2}"
#        cmdQ = cmd.format('?', '?', '?')
#        printnl(cmd.format(repr(options.project), repr(options.experiment), repr(options.technology)))
#        if options.commit:
#            dbi.execute(cmdQ, options.project_id, options.experiment_id, options.tech_id)

        cmd = "delete from snpblocks where projectid = {0} and technology = {1}"
        cmdQ = cmd.format('?', '?')

        printnl(cmd.format(repr(options.project), repr(options.technology)))

        if options.commit:
            dbi.execute(cmdQ, options.project_id, options.tech_id)

        return True


class DeleteBlockAllSamples(BlockCommon):

    def run(self, dbi, options):

        cmd = "delete from genotypeblocks where projectid = {0} and experiment = {1} and technology = {2}"
        cmdQ = cmd.format('?', '?', '?')
#       cmdu = "update samples, genotypeblocks set filename = 3 where sample.id = genotypeblocks.sampleid and genotypeblocks.projectid = {0} and genotypeblocks.experiment = {1} and genotypeblocks.technology = {2}"

        cmds = "select distinct(sampleid) from genotypeblocks where genotypeblocks.projectid = {0} and genotypeblocks.experiment = {1} and genotypeblocks.technology = {2}"
        cmdsQ = cmds.format('?', '?', '?')

        cmdu = "update samples set filename = {3} where samples.id in (select distinct(sampleid) from genotypeblocks where genotypeblocks.projectid = {0} and genotypeblocks.experiment = {1} and genotypeblocks.technology = {2})"
        cmduQ = cmdu.format('?', '?', '?', '?')


        printnl(cmdu.format(repr(options.project), repr(options.experiment), repr(options.technology), None))
        printnl(cmd.format(repr(options.project), repr(options.experiment), repr(options.technology)))

        if options.commit:
            printn("resetting filenames for samples:")
            for smple in dbi.select_all(cmdsQ, options.project_id, options.experiment_id, options.tech_id):
                printn(" {0[0]}, ".format(smple))
            printnl("")
            dbi.execute(cmduQ, None, options.project_id, options.experiment_id, options.tech_id)
            dbi.execute(cmdQ, options.project_id, options.experiment_id, options.tech_id)

        return True


class DeleteBlockMember(BlockCommon):
    def __init__(self, db):
        self._member_ = None
        super(DeleteBlockMember, self).__init__(db)

    def add_options(self, config):
        config.add_option("--member", action="append", default=[],
                          help="members to delete")
        super(DeleteBlockMember, self).add_options(config)

    def prolog(self, dbi, options):
        self.get_samples(options)
        self._member_ = Member(dbi)
        return super(DeleteBlockMember, self).prolog(dbi, options)

    def run(self, dbi, options):

        def pr_context(arg, **tag):
            printnl(arg, **tag)

        cmdu = "update samples set filename = {0} where samples.id in (select distinct(sampleid) from genotypeblocks where genotypeblocks.projectid = {1} and genotypeblocks.experiment = {2} and genotypeblocks.technology = {3} and member = {4})"
        cmduQ = cmdu.format('?', '?', '?', '?', '?')

        cmd = "delete from genotypeblocks where projectid = {0} and experiment = {1} and technology = {2} and member = {3}"
        cmdQ = cmd.format('?', '?', '?', '?')

        for memstr in options.member:
            pedper = memstr.strip().split(':')
            if len(pedper) == 1:
                member_id = self._member_.member('', memstr, options, pr_context)
                if member_id is None:
                    printnl("can not lookup id for member {0}".format(memstr), tag="exclmember")
                    options.error += 1
                    continue
            elif len(pedper) == 2:
                member_id = self._member_.member(pedper[0], pedper[1], options, pr_context)
                if member_id is None:
                    printnl("can not lookup id for member {0}:{1}".format(pedper[0], pedper[1]), tag="exclmember")
                    options.error += 1
                    continue
            else:
                printnl("bad format member designation: {0}".format(memstr), tag="exclmember")
                options.error += 1
                continue

            printnl("")
            printnl(cmdu.format(None, repr(options.project), repr(options.experiment), repr(options.technology), repr(memstr)))
            printnl(cmd.format(repr(options.project), repr(options.experiment), repr(options.technology), repr(memstr)))

            if options.commit:
                dbi.execute(cmduQ, None, options.project_id, options.experiment_id, options.tech_id, member_id)
                dbi.execute(cmdQ, options.project_id, options.experiment_id, options.tech_id, member_id)

        return True


class DeleteBlockSample(BlockCommon):

    def add_options(self, config):
        config.add_option("--sample", action="append", default=[],
                          help="samples to delete")
        super(DeleteBlockSample, self).add_options(config)

    def prolog(self, dbi, options):
        self.get_samples(options)
        return super(DeleteBlockSample, self).prolog(dbi, options)

    def run(self, dbi, options):

        cmd = "delete from genotypeblocks where projectid = {0} and experiment = {1} and technology = {2} and sampleid = {3}"
        cmdQ = cmd.format('?', '?', '?', '?')

        for sample in options.sample:
            if (sample, options.experiment_id)  not in self._samples:
                printnl("Can not lookup sample {0}".format((sample, options.experiment_id)))
                options.error += 1
                continue
            sample_id = self._samples[(sample, options.experiment_id)][0]

            printnl("")
            self.reset_filename(None, sample, sample_id, options)

            printnl(cmd.format(repr(options.project), repr(options.experiment), repr(options.technology), repr(sample)))

            if options.commit:
                dbi.execute(cmdQ, options.project_id, options.experiment_id, options.tech_id, sample_id)

        return True


class ShowSamples(BlockCommon):

    def add_options(self, config):
        config.add_option("--sample", action="append", default=[],
                          help="samples to show")
        config.add_option("--output", action="store", default=None,
                          help="output samples to specified file")
        super(ShowSamples, self).add_options(config)

    def prolog(self, dbi, options):
        self.get_samples(options)
        self.get_experiments(options)
#       ret = super(ShowSamples, self).prolog(dbi, options)
        dbi.db_might_commit(False)
        self.validate_required(options, experiment=True if options.sample else False)
        return False if options.error else True

    def run(self, dbi, options):

        File = {}

        cmd = "{0:>10}, {1:>4}, {2:>15}, {3:2}, {4:<15}, {5:<10}"
        printnl(cmd.format("sample", "#id", "experiment", "id", "pedigree:person", "filename"))

        cmd = "{2:>10}, {3:4}, {4:>15}, {5:2}, {1:>15}, {0[5]:10}"

        cmdo = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}"

        if options.sample:
            if not options.experiment_id:
                printnl("Experiment \"{0}\" is not in the database for technology \"{1}\" and project \"{2}\".".format(options.experiment, options.technology, options.project))
                printnl("Perhaps it is misspelt.")
                return
            samples = sorted( ((x, options.experiment_id) for x in options.sample) )
        else:
            samples = sorted(self._samples.iterkeys())

        for (sample, eid) in samples:
            if (sample, eid) not in self._samples:
                printnl("Can not lookup sample {0}, experiment {1}".format(sample, self._experiments[eid]))
                continue
            sample_rec = self._samples[(sample, eid)]
            sample_id = sample_rec[0]
            pedper    = sample_rec[3] + ':' + sample_rec[4]
            experiment = self._experiments[eid]

            printnl(cmd.format(sample_rec, pedper, sample, sample_id, experiment, eid))
            if options.output:
                if experiment not in File:
                    FILE = os.path.join(options.dir, options.output + '.' + experiment + '.txt')
                    F = gzopen(FILE, "w")
                    File[experiment] = F
                    print >>F, cmdo.format('subject_id', 'active_value', 'pedigree', 'person', 'plate', 'well', 'misc', 'sample_id', 'filename', 'id', 'experiment')
                else:
                    F = File[experiment]

                print >>F, cmdo.format(sample_rec[2],  sample_rec[1], sample_rec[3], sample_rec[4],
                                       sample_rec[6],  sample_rec[7], sample_rec[8],
                                       sample_rec[10], sample_rec[5], sample_rec[0],
                                       experiment)


        if File:
            for F in File.itervalues():
                F.close()
        return True

