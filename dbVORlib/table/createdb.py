#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import os.path
import getpass

from dbVORlib.validate   import ValidateProject
from dbVORlib.util       import printnl


class CreateDB(ValidateProject):
    NO_DIR = True
    NO_LOG = True
    NO_DB  = True

    def __init__(self, db):
        super(CreateDB, self).__init__(db)

    def add_options(self, config):
        super(CreateDB, self).add_options(config)
        config.change_default("config", None)    # can operate w/o config file

        config.add_option("--root", type="string", default="root",
                          help="root name for database access")

        config.add_option("--sqlpath", type="string", default="~/dbvor/sql",
                          help="path to sql to initialize database")

        config.add_option("--enablenetworklogin", action="store_true", default=False,
                          help="allow user to access the database from the network")

        config.add_option("--revoke", action="store_true", default=False,
                          help="revoke database access and drop database")


    def hint(self):
        print "\nINPUT cfg FORMAT: --root=<> --sqlpath=<> --user=<> --db=<> --host=localhost --password=..."

    def prolog(self, dbi, options):
        printnl("This program will ask you for the database root's password.")
        printnl("It needs this privilege level to create/destroy a database and user and to load tables.")
        return True

    def epilog(self, dbi, options):
        return True

    def run(self, dbi, options):

        password = getpass.getpass("Password for {0}: ".format(options.root))
        printnl("")

        rootdb = dbi.connect(options.host, 'information_schema', options.root, password, options)

        if options.revoke:
            self.revoke(dbi, options)
            return

        printnl("On server \"{0}\", we will create database \"{1}\" for user \"{2}\"".format( \
                options.host, options.db, options.user))
        if not os.path.isfile(os.path.expanduser(options.sqlpath + "/sql.completeblock.txt")):
            printnl("{0} is a bad sql directory; it does not contain {1}\n".format(options.sqlpath, "sql.completeblock.txt"))
            return False
        printnl("The database schema will be created from {0}\n".format(options.sqlpath + "/sql.completeblock.txt"))

        while True:
            options.password = getpass.getpass("Password for {0}: ".format(options.user))
            check = getpass.getpass("Password for {0} (retype): ".format(options.user))
            if options.password == check: break
            printnl("Passwords differ.  Please try again")
        printnl("")

        db = dbi.select_one("select * from schemata where schema_name = ?", options.db)
        if db is not None:
            printnl("database \"{0}\" already exists.".format(options.db))
            dbcreated = False
        else:
            cmd = "create database {0};".format(options.db)
            printnl(cmd)
            dbi.execute(cmd)
            dbcreated = True

        host = "%" if options.enablenetworklogin else options.host
        db = dbi.select_one("select distinct(table_schema) from schema_privileges where grantee = ?", "'{0}'@'{1}'".format(options.user, host))
        if db is not None and options.db in db:
            printnl("user '{0}'@'{1}' can already access the \"{2}\" database".format(options.user, host, options.db))
        else:
            cmd = "grant all privileges on {0}.* to '{1}'@'{2}' identified by '...';".format(options.db, options.user, host)
            printnl(cmd)
            cmd = cmd.replace('...', options.password)
            dbi.execute(cmd)

        path = os.path.join(os.path.expanduser("~"), ".dbvorrc")
        if not os.path.isfile(path):
            gfile = open(path, "w")
            print >>gfile, "#The 3 lines below are included to make it easy to transfer other .cfg files and data to"
            print >>gfile, "#other users"
            print >>gfile, "host:     {0}".format(options.host)
            print >>gfile, "user:     {0}".format(options.user)
            print >>gfile, "password: {0}".format(options.password)
            print >>gfile
            print >>gfile, "#The lines below are added for convenience;  They should be really specified in"
            print >>gfile, "#individual .cfg files, not here"
            print >>gfile, "db:       {0}".format(options.db)
            print >>gfile, "dir:      ."
            print >>gfile, "project:  project"
            gfile.close()
            import stat
            os.chmod(path, stat.S_IRUSR | stat.S_IWUSR)

        if dbcreated:
            cmd = "cat {0}/sql.completeblock.txt | mysql -u {1} -h {2} -D {3} -p".format(options.sqlpath, options.user, options.host, options.db)
            printnl(cmd)
            os.system(cmd + options.password)

        return True

    def revoke(self, dbi, options):
        host = "%" if options.enablenetworklogin else options.host
        db = dbi.select_one("select distinct(table_schema) from schema_privileges where grantee = ?", "'{0}'@'{1}'".format(options.user, host))
        if db is None or options.db not in db:
            printnl("user '{0}'@'{1}' can not access the \"{2}\" database".format(options.user, host, options.db))
        else:
            cmd = "revoke all privileges on {0}.* from '{1}'@'{2}';".format(options.db, options.user, host)
            printnl(cmd)
            dbi.execute(cmd)

        if db is not None and len(db) <= 1:
            db = dbi.select_one("select count(*) from user_privileges where grantee = ?", "'{0}'@'{1}'".format(options.user, host))
            if db[0] == 0:
                printnl("user '{0}'@'{1}' has no rights".format(options.user, host))
            elif db[0] == 1:
                cmd = "drop user '{0}'@'{1}';".format(options.user, host)
                printnl(cmd)
                dbi.execute(cmd)

        db = dbi.select_all("select distinct(grantee) from schema_privileges where table_schema = ?", options.db)
        if not db:
            db = dbi.select_one("select * from schemata where schema_name = ?", options.db)
            if db is None:
                printnl("database \"{0}\" does not exists.".format(options.db))
            else:
                cmd = "drop database {0};".format(options.db)
                printnl(cmd)
                dbi.execute(cmd)
        else:
            printnl("Database \"{0}\" not deleted because it is still accessible by {1}.".format(options.db, db))
