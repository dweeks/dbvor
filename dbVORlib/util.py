#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#
# note: some printnl's worth pushing in glob and maybe read_lines but then again ...

import types
import sys, os.path
import re

class NoTrace(Exception):
    NoTrace = True

class log(object):
    __logs__ = []
    __fils__ = []
    __keys__ = []
    __hash__ = {}
    __rcrd__ = {}
    __nrec__ = {}
    __clr__  = {}
    __strt__ = {}
    __alwy__ = []

    def __init__(self):
        pass

    @classmethod
    def append(cls, value):
        if value is None:
            cls.__logs__.append(sys.stderr)   # print to stderr
        else:
            cls.__logs__.append(value)
            cls.__fils__.append(value)

    @classmethod
    def empty(cls):
        cls.__logs__ = [ ]
        cls.__fils__ = [ ]
        cls.__keys__ = []
        cls.__hash__ = {}
        cls.__rcrd__ = {}
        cls.__nrec__ = {}
        cls.__clr__  = {}
        cls.__strt__ = {}
        cls.__alwy__ = []

#   to keep pylint happy ... I guess it does not like setattr's
    marker_length = None
    member_length = None
    snp_name_length = None
    person_name_length = None
    pedigree_name_length = None
    @classmethod
    def sizes(cls, **ks):
        for k, v in ks.iteritems():
#           printnl("XXX log.sizes(): {0}: {1}".format(k, v), tag=True)
            setattr(cls, k, v)

    @classmethod
    def key(cls, **k):
        return (k['tag'] if k['tag'] in cls.__rcrd__ else None) if k.get('tag', False) else (cls.__keys__[0] if len(cls.__keys__) else None)

    @classmethod
    def check(cls, l, **k):
        key =  cls.key(**k)
        if key and key not in log.__alwy__ and 'all' not in log.__alwy__:
            if cls.__rcrd__[key] > cls.__hash__[key]:
                return l if l else cls.__fils__
        return l if l else cls.__logs__

    @classmethod
    def grpinc(cls, force=False, **k):
        key =  cls.key(**k)
        if key:
            if (not cls.__nrec__[key]) or force:
                cls.__rcrd__[key] = cls.__rcrd__[key] + 1
            if cls.__rcrd__[key] <= cls.__hash__[key]:
                if cls.__strt__[key]:
                    printnl("\n===== Starting the \"{0}\" subsection".format(key), tag=True) #hack not to count
                    cls.__strt__[key] = False
                if k.get('show', False):
                    printnl("===== \"{0}\" record {1:d}".format(key, cls.__rcrd__[key]), tag=True) #hack not to count
                ckey = cls.__clr__.get(key, False)
                if ckey and cls.__hash__.get(ckey, 0) < cls.__rcrd__.get(ckey, 0):
                    cls.popepilog(ckey)
                    cls.__rcrd__[ckey] = 0
##           print " ##{0:d}[{1}]{{{2}}}".format(cls.__rcrd__[key], key, ckey)

    fmt_hash = {}
    @classmethod
    def fmt(cls, s, p):
        ans = cls.fmt_hash.get(s, {}).get(p, None)
        if ans: return ans

        exp = re.split(r"(%/\d+)", s)
        filtr = []
        for token in exp:
            if token.startswith('%/'):
                filtr.append('%{0:d}'.format(p[int(token[2:])]))
            else:
                filtr.append(token)
        ans = ''.join(filtr)
        if s not in cls.fmt_hash:    cls.fmt_hash[s] = {}
        cls.fmt_hash[s][p] = ans
        return ans

    @classmethod
    def printnl(cls, s, *l, **k):
        cls.grpinc(**k)
        for f in cls.check(l, **k):
            if "line" in k: print >>f, ("{0:d}) ".format(k["line"])) ,
            print >>f, s

    @classmethod
    def printn(cls, s, *l, **k):
        for f in cls.check(l, **k):
            print >>f, s,

    @classmethod
    def push(cls, keys, cnt=10, newrecord=False, clear=None):
        if type(keys) in (types.ListType, types.TupleType):
#           printnl("\n===== Starting the \"{0}\" subsections".format("\", \"".join(keys)), tag=True) #hack not to count
            keys = list(keys)
            keys.reverse()
            for key in keys:
                cls.__keys__.insert(0, key)
                cls.__hash__[key] = cnt
                cls.__rcrd__[key] = 0
                cls.__nrec__[key] = newrecord
                cls.__strt__[key] = True
                cls.__clr__[key]  = clear
        else:
            key = keys
#           printnl("\n===== Starting the \"{0}\" subsection".format(key), tag=True) #hack not to count
            cls.__keys__.insert(0, key)
            cls.__hash__[key] = cnt
            cls.__rcrd__[key] = 0
            cls.__nrec__[key] = newrecord
            cls.__strt__[key] = True
            cls.__clr__[key]  = clear

    @classmethod
    def pop(cls, keys):
        for key in keys if type(keys) in (types.ListType, types.TupleType) else (keys, ):
            if key in cls.__keys__:
                cls.__nrec__[key] = 0
                cls.__keys__.remove(key)
                cls.popepilog(key)
            else:
                print "request to pop \"{0}\" where \"{1}\" was not previously pushed".format(key, key)

    @classmethod
    def popepilog(cls, key):
        if cls.__rcrd__.get(key, 0):
            if key not in cls.__alwy__ and 'all' not in cls.__alwy__:
                if cls.__hash__.get(key, 0) < cls.__rcrd__.get(key, 0) or cls.__nrec__.get(key, 0):
                    print >>sys.stderr, "\n===== The text above shows the first {0:d} records from a total of {1:d} records in the log file for the \"{2}\" subsection\n".format(cls.__hash__.get(key, 0) if cls.__hash__.get(key, 0) < cls.__rcrd__.get(key, 0) else  cls.__rcrd__.get(key, 0), cls.__rcrd__.get(key, 0), key)
            else:
                print >>sys.stderr, "\n===== The text above shows the {0:d} records in the log file for the \"{1}\" subsection\n".format(cls.__rcrd__.get(key, 0), key)

    @classmethod
    def newrecord(cls, **k):
        cls.grpinc(force=True, **k)

    @classmethod
    def begingroup(cls, **k):
        key = cls.key(**k)
        if key:
#           cls.newrecord(**k)
            cls.__nrec__[key] = True

    @classmethod
    def endgroup(cls, **k):
        key = cls.key(**k)
        if key:
            cls.__nrec__[key] = False

    @classmethod
    def endheader(cls, **k):
        key = cls.key(**k)
        if key:
            cls.__rcrd__[key] = 0

    @classmethod
    def beginfooter(cls, **k):
        key = cls.key(**k)
        if key:
            cls.__nrec__[key] = True

    @classmethod
    def pdb(cls):
        import pdb
        pdb.set_trace()

def printfmt(s, v, p, *l, **k):
    log.printnl(log.fmt(s, p).format(v), *l, **k)

def printnl(s, *l, **k):
    log.printnl(s, *l, **k)

def printn(s, *l, **k):
    log.printn(s, *l, **k)

def logs_set(*logs):
    if len(logs)== 0:
        log.empty()
    else:
        for l in logs:
            log.append(l)

def printalways(l):
    if l:
        log.__alwy__.extend(l)
    else:
        log.__alwy__ = []

def input_dir(options, name=None):
    if name is None:
        name = options.input
    idir = getattr(options, "inputdir", None)
    if idir is None:
        FILE = options.dir + name
    else:
        if not idir.endswith("/"): idir += "/"
        FILE = idir + name
    return FILE

def gzopen(FILE, mode='Ur', compr=9):
    if FILE.endswith('.gz'):
        import gzip
        try:
            if mode == 'Ur': mode = 'r'
            File = gzip.open(FILE, mode, compr)
        except Exception, error:
            printnl("Failed to open gz experiment file {0}\n{1}".format(FILE, error))
            raise
    else:
        File    = file(FILE, mode)
    return File


def read_fields(FILE, File=None, cnt=0, sep=None, field_cnt=None):
    if File is None:
        File = gzopen(FILE)
    line   = ''
    fields = []
    for line in File:
        cnt += 1
        sline = line.strip()
        if sline == '' or sline.startswith('#'):
            continue
        fields = sline.split(sep)
        if field_cnt is not None and len(fields) != field_cnt:
            printnl("ignoring line -- too few field. expected {0:d} got {1:d}\nFile: {2}\n{3:d}|{4}".format(field_cnt, len(fields), FILE, cnt, line))
            continue
        yield cnt, sline, fields
    if File != sys.stdin:
        File.close()

import zipfile
def zipopen(FILE, member=None, fmode="r", compr=zipfile.ZIP_DEFLATED, z64=True, mode="rU", pwd=None):
    try:
        if not zipfile.is_zipfile(FILE):
            raise Exception("{0} is not a zip file".format(FILE))
        Archive = zipfile.ZipFile(FILE, fmode, compr, z64)
        if member == None:
            if FILE.endswith('.zip'):
                member = os.path.basename(FILE[:-4])
            else:
                printnl("Following member names are available in \"{0}\"\n\t{1}".format(os.path.basename(FILE), Archive.namelist()))
                raise Exception("No member specified for \"{0}\"".format(FILE))
        File = Archive.open(member, mode, pwd)
    except Exception, error:
        printnl("{0}\nFailed to open zip member \"{1}\" in experiment file \n\"{2}\"".format(error, member, FILE))
        raise

    return File

import glob

class Glob(object):
    def glob(self, dbi, options):
        input_cnt = errors = 0
        FILE = input_dir(options)
        for File in glob.iglob(FILE):
            File = os.path.expanduser(File)
#           File = os.path.expandvars(File)
            if not os.path.isfile(File):
                raise Exception("cannot find experiment file \"{0}\"".format(File))
            input_cnt += 1
            options.input = os.path.basename(File)
            printnl("# {0:4d} file \"{1}\"".format(input_cnt, File))
            if options.pdb:
                self.run(dbi, options)
            else:
                try:
                    self.run(dbi, options)
                except Exception, error:
                    errors += 1
                    printnl("# processing \'{0}' failed\n{1}".format(options.input, error))

            dbi.endbatch(options)

            if options.debug and options.error > 0:
                printnl("# sample file: CUMULATIVE ERROR COUNT {0:d}".format(options.error))

        if input_cnt == 0:
            raise Exception("NO files match \"{0}\" in directory\n\"{1}\"".format(options.input, options.dir))
        printnl("# {0:4d} files read, {1:d} files crashed".format(input_cnt, errors))

        return True

    def run(self, dbi, options):
        raise Exception("Glob.run is a virtual function and SHOULD BE DEFINED in the Class using Glob")
