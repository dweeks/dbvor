#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#
# External Methods
#	validate_project	-- look up project name -> project id
#	validate_technology	-- look up technology name -> technology id
#	validate_experiment	-- look up experiment name -> experiment id
#				-- if --commit flag is set experiment will be added if not present

from   util     import NoTrace, printnl

class ValidateProject(object):
    def __init__(self, db):
        self._dbi_ = db

    def add_options(self, config):
        config.add_option("-p", "--project", type="string",
                          help="prepDB project; default: GARM")
        config.add_option("--dir", "--directory", type="string",
                          help="prepDB path to experiment file name; no default")

    def validate_project(self, options, required=False, create=False):
        if options.project == None:
            raise NoTrace("No project was specified")
#           options.error += 1
#           return
        mid = self._dbi_.select_one("select id from projects where name = ?", options.project)
        if mid:
            options.project_id = mid[0]
            return
        if create:
            if options.commit:
                printnl("Adding project name \"{0}\" to database".format(options.project))
                self.commit_project(options)
                return
        if required:
            printnl("Project name \"{0}\" not found in database".format(options.project))
            options.error += 1
        options.project_id = 0

    def commit_project(self, options):
        self._dbi_.insert("""insert into projects (name, active) VALUES (?, ?)""",
                          [(options.project, 1)])
        mid = self._dbi_.select_one("select id from projects where name = ?", options.project)
        if mid is None:
            raise Exception("oops: Project({0}) insert into database failed".format(options.project))
        options.project_id = mid[0]

class Validate(ValidateProject):
    def __init__(self, db):
        super(Validate, self).__init__(db)

    def add_options(self, config):
        super(Validate, self).add_options(config)
        config.add_option("-t", "--technology", type="string",
                          help="prepDB technology: no default")

        config.add_option("-e", "--experiment", type="string",
                          help="experiment name; no default")
        config.add_option("--experiment_date", type="string", default="1000-01-01",
                          help="experiment date MM/DD/YYYY; default=1000-01-01")

        config.add_option("--type", "--file_type", type="string",
                          help="prepDB experiment file type: xlong or long or wide")

    def validate_technology(self, options, required=False, create=False):
        if options.technology == None:
            if required:
                printnl("No technology was specified")
                options.error += 1
            else:
                options.technology = 'Missing'
                options.tech_id    = 0
            return
        mid = self._dbi_.select_one("select id from technologies where technology = ?", options.technology)
        if mid:
            options.tech_id = mid[0]
            return
        if create:
            if options.commit:
                printnl("Adding technology name \"{0}\" to database".format(options.technology))
                self.commit_technology(options)
                return
        if required:
            printnl("Technology name \"{0}\" not found in database".format(options.technology))
            options.error += 1
        options.tech_id = 0

    def commit_technology(self, options):
        self._dbi_.insert("""insert into technologies (technology) VALUES (?)""",
                          [(options.technology,)])
        mid = self._dbi_.select_one("select id from technologies where technology = ?", options.technology)
        if mid is None:
            raise Exception("oops: Technology({0}) insert into database failed".format(options.technology))
        options.tech_id = mid[0]

    def validate_experiment(self, options, required=False, create=False):
        if options.experiment == None:
            if required:
                printnl("No experiment was specified")
                options.error += 1
            else:
                options.experiment    = 'Missing'
                options.experiment_id = 0
            return
        mid = self._dbi_.select_one("""select id from experiments where filename = ? and
                                         projectid = ? and techid = ?""",
                                    options.experiment, options.project_id, options.tech_id)
        if mid is not None:
            options.experiment_id = mid[0]
            return

        if create:
            if options.commit:
                printnl("Adding experiment name \"{0}\" to database".format(options.experiment))
                self.commit_experiment(options)
                return
        if required:
            printnl("Experiment name \"{0}\" not found in database with project = \"{1}\" (id#{2:d}) and technology = \"{3}\" (id#{4:d})".format(options.experiment, options.project, options.project_id, options.technology, options.tech_id))
            raise NoTrace("Check experiment name \"{0}\" and technology name \"{1}\" for compatibility.".format(options.experiment, options.technology))
        options.experiment_id = 0

    def commit_experiment(self, options):
        date = self._dbi_.dbdate(options.experiment_date.strip())

        self._dbi_.insert("""insert into experiments
                                (projectid, techid, filename, `generated`, technology) VALUES
                                (?, ?, ?, ?, ?)""",
                          [(options.project_id, options.tech_id, options.experiment,
                            date, options.technology)])
        mid = self._dbi_.select_one("""select id from experiments where filename = ? and
                                         projectid = ? and techid = ?""",
                                    options.experiment, options.project_id, options.tech_id)
        if mid is None:
            raise Exception("oops: Experiment({0}) insert into database failed".format(options.experiment))
        else:
            options.experiment_id = mid[0]

    def validate_all(self, options):
        self.validate_project(options, required=True)
        self.validate_technology(options, required=True)
        self.validate_experiment(options, required=True)

    def validate_required(self, options, project=False, technology=False, experiment=False):
        self.validate_project(options, required=project)
        if options.error:    raise NoTrace("Try again!")

        self.validate_technology(options, required=technology)
        if options.error:    raise NoTrace("Try again!")

        self.validate_experiment(options, required=experiment)
        if options.error:    raise NoTrace("Try again!")

        self.show_projtechexp(options)


    def show_projtechexp(self, options):
        printnl('')
        printnl("project:    \"{0}\", id {1:d}"   .format(options.project, options.project_id))
        printnl("technology: \"{0}\", id {1:d}".format(options.technology, options.tech_id))
        printnl("experiment: \"{0}\", id {1:d}".format(options.experiment, options.experiment_id))
        printnl('')
