#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import time

from   operator import itemgetter

from   dbVORlib.member   import Member
from   dbVORlib.util     import printn, printnl, log

class MemberSelect(Member):
    def __init__(self, db, trait):
        super(MemberSelect, self).__init__(db)
        self._trait_         = trait

        self._member_map     = {}
        self._member_iterrec = []      # lint fwd ref

    def add_options(self, config):
        config.add_option("--member", action="append", default=[],
                          help="persons to process")
#       config.add_option("--linkage", action="store", type="boolean", default=False,
#                         help="add father, mother, etc")
        config.add_option("--include", action="append",
                          help="persons only to be processed")
        config.add_option("--exclude", action="append",
                          help="persons not to process")
        config.add_option("--prune_members", action="store", type="boolean", default=False,
                          help="flush members for which there are no genotypes")
        config.add_option("--fill_pedigree", action="store", type="boolean", default=False,
                          help="include all members of any pedigree that is used")

    def __iter__(self):
        return self._member_iterrec.__iter__()

    def sortedlist(self):
        return (x[0] for x in self)

    def name(self, member_id=None, rec=None):
        return (rec if rec else self._member_map[member_id])[1:3]

    def linkage(self, member_id=None, rec=None):
        return (rec if rec else self._member_map[member_id])[1:6]

#   def type(self, member_id=None, rec=None):
#       return (rec if rec else self._member_map[member_id])[-1]

    def len(self):
        return len(self._member_iterrec)

    def prune(self, dbi, options, mXm):
        self.pp_name_length()

        if options.prune_members:
            if options.fill_pedigree:
                pedhash = {}
                for memrec in self:
                    member_id = memrec[0]
                    if member_id in mXm and len(mXm[member_id]) != 0:
                        pedhash[memrec[1]] = True

            for memrec in self:
                member_id = memrec[0]
                ped       = memrec[1]
                if options.fill_pedigree:
                    if pedhash.get(ped, False) != True:
                        printnl("pruning members: dropping {0[0]}:{0[1]} -- no markers".format(self.name(member_id)) )
                        del self._member_map[member_id]
                elif member_id not in mXm or len(mXm[member_id]) == 0:
                    printnl("pruning members: dropping {0[0]}:{0[1]} -- no markers".format(self.name(member_id)) )
                    del self._member_map[member_id]

        log.beginfooter(tag='prune members')
        if len(self._member_iterrec) == len(self._member_map):
            printnl("pruning members: none pruned - {0:d} entries available".format(len(self._member_map)) )
        else:
            printnl("pruning members: had {0:d} entries pruned to {1:d} entries".format(len(self._member_iterrec), len(self._member_map)), tag=True )
        self._member_iterrec = sorted(self._member_map.itervalues(), key=itemgetter(1, 2))

    def pp_name_length(self):
        pp = zip(*[(self.name(rec=x)) for x in self._member_map.itervalues() ])
        ped_name_length = reduce(max, (len(x) for x in pp[0]))
        per_name_length = reduce(max, (len(x) for x in pp[1]))
        log.sizes(pedigree_name_length=ped_name_length, person_name_length=per_name_length)

    def member_filter(self, member_id, memrec, options):
        trt = self._trait_.get_traits(self._dbi_, member_id, options)
        flush = self._trait_.filter(member_id, memrec, trt, self.rev, options)
        if flush:
# should delete all existence
#        del self._members_traits[member_id]
#        self._lookup_.del(member_id)
            return
        self._member_map[member_id] = memrec

    def run(self, dbi, tech, options):
        self.Lookup_members(dbi, tech, options)
        self._member_iterrec = sorted(self._member_map.itervalues(), key=itemgetter(1, 2))

    selmem  = "select id, pedigree, person, father, mother, gender, active, type from members"
    _selsnps = "select distinct(member), 1 from genotypes"
    _selgwas = "select distinct(member), 1 from genotypeblocks"
    def Lookup_members(self, dbi, tech, options):
        if tech:
            tail = ' where technology in (' + ', '.join(map(str, tech)) + ')'
            self._selsnps += tail
            self._selgwas += tail
        ped = {}
        def pr_context(unused):
            pass

        if tech:
            Time = time.time()
            printn("collecting distinct members from genotypes", tag=True)
            selsnps = dict(dbi.select_all(self._selsnps))
            printnl("#{0}, {1:6.3f} sec".format(selsnps, time.time()-Time), tag=True)

            Time = time.time()
            printn("collecting distinct members from gwas", tag=True)
            selgwas = dict(dbi.select_all(self._selgwas))
            printnl("#{0}, {1:6.3f} sec".format(selgwas, time.time()-Time), tag=True)

        if not options.member:
            log.push(('exclmember', 'traitvalue', 'traitgender', 'traittype'))
            for memrec in dbi.select_all(self.selmem):
                member_id = memrec[0]
                if tech:
                    if member_id not in selsnps and member_id not in selgwas:
                        printnl("excluding member {0}:{1} ({2}) completely ungenotyped in technologies selected".format(memrec[1], memrec[2], member_id), tag="exclmember")
## old                  printnl("excluding member {0}:{1} ({2}) not in technologies".format(memrec[1], memrec[2], member_id), tag="exclmember")
                        continue

                allowed_member_id = self.cacheload(memrec[1], memrec[2], member_id, options, pr_context)
##f              allowed_member_id = self.member(memrec[1], memrec[2], options, pr_context)
                if allowed_member_id is None:
                    printnl("excluding member {0}:{1}".format(memrec[1], memrec[2]), tag="exclmember")
                    continue
                elif allowed_member_id is False:
                    printnl("odd pedigree/person ({t2[0]}/{t2[1]}) in member table multiple times".format(t2=memrec[1:3]), tag="exclmember")
                    continue
                self.member_filter(member_id, memrec, options)
            self.fill_ped(dbi, options)
            log.pop(('exclmember', 'traitvalue', 'traitgender', 'traittype'))
            return

        log.push(('exclmember', 'traitvalue', 'traitgender', 'traittype'))
        for memstr in options.member:
            pedper = memstr.strip().split(':')
            if len(pedper) == 1:
                member_id = self.member('', memstr, options, pr_context)
                if member_id is None:
                    printnl("Cannot lookup id for member {0}".format(memstr), tag="exclmember")
                    continue
            elif len(pedper) == 2:
                member_id = self.member(pedper[0], pedper[1], options, pr_context)
                if member_id is None:
                    printnl("Cannot lookup id for member {0}:{1}".format(pedper[0], pedper[1]), tag="exclmember")
                    continue
            else:
                printnl("Bad format member designation: {0}".format(memstr), tag="exclmember")
                continue

            memrec = dbi.select_one(self.selmem + ' where id = ?', member_id)
            if tech:
                if member_id not in selsnps and member_id not in selgwas:
                    printnl("excluding member {0}:{1} not in technologies".format(memrec[1], memrec[2]), tag="exclmember")
                    continue

            self.member_filter(member_id, memrec, options)
        self.fill_ped(dbi, options)
        log.pop(('exclmember', 'traitvalue', 'traitgender', 'traittype'))


    selped = "select id, pedigree, person, father, mother, gender, active, type from members where pedigree = ?"
    def fill_ped(self, dbi, options):
        if not options.fill_pedigree:
            return

        def pr_context(unused):
            pass

        pedhash = {}
        for memrec in self._member_map.itervalues():
            pedhash.setdefault(memrec[1], 1)
        pedigree = pedhash.iterkeys()

        log.push('fillped')
        for ped in pedigree:
            for memrec in dbi.select_all(self.selped, ped):
                member_id = memrec[0]
                if member_id not in self._member_map:
                    if not self.rev(member_id):
                        allowed_member_id = self.cacheload(memrec[1], memrec[2], member_id, options, pr_context)
                        if allowed_member_id is None:
                            printnl("excluding member {0}:{1}".format(memrec[1], memrec[2]), tag="exclmember")
                            continue
                    self._member_map[member_id] = memrec

                    printnl("adding pedigree {0} person {1} to fill out pedigree".format(memrec[1], memrec[2]))
        log.pop('fillped')
