#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import os.path
import re

from operator import itemgetter

from   genotype import Genotype
from   allele   import Allele
from   marker   import Marker
from   member   import Member
from   util     import printnl, printn, log, gzopen, read_fields

ORIGIN  = 1

class Plx(object):
    activities = ('prolog', 'allelekey', 'glob', 'genotype', 'stat')

    def __init__(self, db):
#       super(Plx, self).__init__(db)
        self._genotype_      = Genotype(db)
        self._allele_        = Allele(db)
        self._marker_        = Marker(db)
        self._member_        = Member(db)

        self._snp_map        = {}
        self._map            = {}
        self._map_rev        = {}
        self._other_sample   = {}
        self._samples        = {}
        self._process_count  = 0

        self._stat_total     = 0
        self._stat_possible_conflict = 0
        self._stat_multiple  = 0
        self._stat_zero      = 0
        self._stat_conflict  = 0

        self._options        = None     # forward ref for pylint used by pr_genotype
        self._member         = None     # forward ref for pylint used by pr_genotype
        self._marker         = None     # forward ref for pylint used by pr_genotype
        self._allele         = None     # forward ref for pylint used by pr_genotype

        self._sample         = None     # forward ref for pylint used by pr_context
        self._line_no        = 0        # forward ref for pylint used by pr_context
        self._old_line_no    = 0        # forward ref for pylint used by pr_context
        self._line           = None     # forward ref for pylint used by pr_context
        self._this_line      = 0

    def add_options(self, config):
#       super(Plx, self).add_options(config)

        config.add_option("--quality_file", action="store", type="string", default="",
                          help="output raw well data to this file")

        config.add_option("--allelekey", action="store", type="string",
                          help="allele to number mapping")
        config.add_option("--onetwo", action="store", type="boolean", default=False,
                          help="convert allele to number mapping")

        config.add_option("--header", action="store", type="string", default=r"sample.*id",
                          help="header of first column in sample file")

        config.add_option("--include_samples", action="append", type="string", default=[],
                          help="samples to include")
        config.add_option("--include_regexps", action="append", type="string", default=[],
                          help="regexp patterns for samples to include")
        config.add_option("--exclude_samples", action="append", type="string", default=[],
                          help="samples to exclude")
        config.add_option("--exclude_regexps", action="append", type="string", default=[],
                          help="regexp patterns for samples to exclude")

        self._genotype_.add_options(config)

    def prolog(self, dbi, options):
        self._genotype_.validate_project(options)
        self._genotype_.validate_technology(options)
        self._genotype_.validate_experiment(options, create=options.commit)
        if options.error:
            raise Exception("try again!")
## bit of a hack. I don't want the rollbacks per file in glob() to flush the experiment
        if options.commit:
            dbi.commit()

        printnl("project {0}, id {1:d}"   .format(options.project, options.project_id))
        printnl("technology {0}, id {1:d}".format(options.technology, options.tech_id))
        printnl("experiment \"{0}\", id {1:d}".format(options.experiment, options.experiment_id))
        printnl('')

#6/1    options.include_samples = map(lambda s: s.lower(), options.include_samples)
#6/1    options.exclude_samples = map(lambda s: s.lower(), options.exclude_samples)
        options.include_samples = [s.lower() for s in options.include_samples]
        options.exclude_samples = [s.lower() for s in options.exclude_samples]

        if options.quality_file:
            QFILE = options.dir + options.quality_file
            QFile = gzopen(QFILE, "w")
            print >>QFile, "{0:>12} {1:>12} {2:>7} {3:>7} {4:>4} {5:>5} {6:>4} {7:>2} {8:>2}".format("sample", "snp", "allele1", "allele2", "plex", "plate", "well", "we", "ll")
            QFile.close()

        return True

    def allelekey(self, dbi, options):
        if options.allelekey is None:
            if options.onetwo:
                raise Exception("For \"onetwo\" option, the allelekey Map file must be specified")
            return True  # sigh ...
        FILE = options.dir+options.allelekey
        if not os.path.isfile(FILE):
            raise Exception("For \"{0}\", cannot find allelekey Map:\n\"{1}\"".format(options.config, FILE))
        File = gzopen(FILE)
        if options.debug:
            log.push('allelekey')
            printnl("Allelekeys from file \"{0}\"".format(FILE))
            log.endheader()
        Line = 1
        head = File.readline()        # corn flakes
        head = head.split("\t")
        for line in File:
            Line += 1
            sline = line.strip()
            if sline == '' or sline.startswith('#'): continue
            fields = sline.split("\t")
            rs = fields[0]
            dna_hash = {}
            dna_hash[fields[1]] = '1'
            dna_hash[fields[2]] = '2'
            dna_hash[fields[3]] = '1'
            dna_hash[fields[4]] = '2'
#           dna_hash[fields[5]] = '1'
#           dna_hash[fields[6]] = '2'
            fields.append(dna_hash)
            error = options.error
            marker_id = self._marker_.marker(rs, options)
            if error != options.error:
                printnl("error converting marker to marker ids; skipping marker")
            fields.append(marker_id)
            self._snp_map[rs] = (rs, marker_id, dna_hash)
            if options.debug:
                printnl("{0}: {1}".format(rs, self._snp_map[rs]))
        File.close()
        if options.debug:
            log.pop('allelekey')
        return True

    def pr_context(self, *err, **kw):
        tagv = kw.get('tag', "misc")
        log.begingroup(tag=tagv)
        if self._old_line_no != self._line_no:
            if self._options.debug >= 3:
                printn("\nLine {0:8d}|".format(self._line_no), tag=tagv)
                printnl(self._line, tag=tagv)
                printn("sample id \"{0}\" ".format(self._sample), tag=tagv)
            else:
                printn("Line {0:8d}, sample id \"{1}\" ".format(self._line_no, self._sample), tag=tagv)
            self._this_line = 0
        self._old_line_no = self._line_no
        self._this_line += 1
        for e in err: printnl(e, line=self._this_line, tag=tagv)
        log.endgroup(tag=tagv)

    patterns = ( (re.compile(r".*\.Plx(?P<plex>\d+)\..late(?P<plate>\d+).csv"),  0,   0),
                 (re.compile(r".*\.Plx(?P<plex>\d+)\..edo(?P<plate>\d+).csv"),   0, 100),
                 (re.compile(r".*\.AllPlx\..late(?P<plate>\d+).csv"),          100,   0,)
               )
    def plexplate(self, FILE):
        plex, plate = 0, 0
        for pattern in self.patterns:
            match = pattern[0].match(FILE)
            if match:
                dg = match.groupdict()
                plex, plate = int(dg.get('plex', 0))+pattern[1], int(dg.get('plate', 0))+pattern[2]
                break
        return plex, plate

    def run(self, dbi, options):
#CHS206,C,rs4743305,A01,A.Conservative,CHARLES,On,-1.3,5,0.371444,,597.347,0,7.63863,0
        self._options = options
        sampleid = re.compile(options.header, re.IGNORECASE)

        seen_sample = {}
        FILE = options.dir + options.input
        File = gzopen(FILE)
        if options.quality_file:
            plex, plate = self.plexplate(options.input)
            QFILE = options.dir + options.quality_file
            QFile = gzopen(QFILE, "a")

        self._process_count += 1
        self._old_line_no = 0
        self._line_no = 0

        for self._line in File:
            self._line_no += 1
            sline = self._line.strip()
            if sline == '' or sline.startswith('#'): continue
            if sampleid.match(sline.lower()):
                break
            self._line = sline
            if options.debug >= 3:
                self.pr_context(" -- line does not match {0}".format(sampleid.pattern))
        header_line = self._line_no

        for self._line_no, self._line, fields in read_fields(FILE, File=File, sep=',',
                                                             cnt=self._line_no):
            sample = fields[0]
            dna    = fields[1]
            rs     = fields[2]

            if len(dna) == 0:
                dna_a = dna_b = '0'
            elif len(dna) == 1:
                dna_a = dna_b = dna
            elif len(dna) == 2:
                dna_a = dna[0]
                dna_b = dna[1]
            else:
                printnl("bad dna value {0}".format(dna))
                continue

            if options.quality_file:
#               sample  snp  allele1 allele2  plex  plate  well  we ll
#               CHS137 rs101   1       1       1     1     A10    1 10
#               CHS137 rs210   0       0       2     2     B11    2 11
                well = fields[3]
                print >>QFile, "{0:>12} {1:>12} {2:>7} {3:>7} {4:>4} {5:>5} {6:>4} {7:>2} {8:>2}".format(sample, rs, dna_a, dna_b, plex, plate, well, ord(well[0].lower()) - ord('a') + 1, well[1:])
                continue

            self._sample = sample

            lsample = sample.lower()

            if self.include(lsample, options):
                if sample in self._samples:
                    self._samples[sample] += 1
                else:
                    self._samples[sample]  = 1
            else:
                if sample in self._other_sample:
                    self._other_sample[sample] += 1
                else:
                    self._other_sample[sample]  = 1
                continue

            member_id = self._member_.member('', sample, options, self.pr_context)
            if member_id is None :
#               what to say
                continue

            rs_data = self._snp_map.get(rs)
            if rs_data is None:
                if options.allelekey:
                    printnl("rs {0} not in allelekey".format(rs))
                    continue
                else:
                    error = options.error
                    marker_id = self._marker_.marker(rs, options)
                    if error != options.error:
                        printnl("error converting marker to marker ids; skipping marker")
                        continue
                    rs_data = (rs, marker_id, {}, None)
                    self._snp_map[rs] = rs_data

            if options.onetwo:
                dna_a = '0' if dna_a == '0' else rs_data[2].get(dna_a, None)
                if dna_a == None:
                    printnl("dna value {0} not in map".format(rs_data[2]))
                    continue
                dna_b = '0' if dna_b == '0' else rs_data[2].get(dna_b, None)
                if dna_b == None:
                    printnl("dna value {0} not in map".format(rs_data[2]))
                    continue

            if options.debug >= 3:
                printnl("sample {0} (id {1:d}): {2} (id {3:d}) {4} {5}".format(sample, member_id, rs, rs_data[1], dna_a, dna_b))

            self._map.setdefault(member_id, {}).setdefault(rs_data[1], []).append((dna_a, dna_b, options.technology, options.experiment, os.path.basename(FILE)))
            self._map_rev.setdefault(member_id, {})[rs_data[1]] = (fields[0], rs)

        if header_line == self._line_no:
            printnl("header line identifier (regular expression \"{0}\") not found in the above file.\n".format(sampleid.pattern))
        return True

    def include(self, lsample, options):
        include = inc = False
        if options.include_samples:
            include = True
            if lsample in options.include_samples:
                inc = True
                if options.debug >= 3:
                    self.pr_context("include_samples: in {0}".format(options.include_samples))

        if options.include_regexps:
            include = True
            mtch = False
            regexp = False #... pylint
            for regexp in options.include_regexps:
                mtch = re.search(regexp, lsample)
                if mtch: break
            if mtch:
                inc = True
                if options.debug >= 3:
                    self.pr_context("include_regexps: pattern {0}".format(regexp))

        if include and not inc:
            if options.debug >= 2:
                self.pr_context("excluded because it is NOT in an include list")
            return False

        inc = True
        if lsample in options.exclude_samples:
            inc = False
            if options.debug >= 3:
                self.pr_context("exclude_samples: in {0}".format(options.exclude_samples))

        mtch = False
        for regexp in options.exclude_regexps:
            mtch = re.search(regexp, lsample)
            if mtch: break
        if mtch:
            inc = False
            if options.debug >= 3:
                self.pr_context("exclude_regexps: pattern {0}".format(regexp))

        return inc

    def fix_conflicts(self, dbi, options):
        if options.debug >= 2:
            printnl("EXCLUDED SAMPLE SUMMARY")
            for sample in sorted(self._other_sample.keys()):
                printnl("excluded sample {0}, {1:d} snps".format(sample, self._other_sample[sample]))

        if options.debug >= 2:
            printnl("SAMPLE SUMMARY")
            for sample in sorted(self._samples.keys()):
                printnl("sample {0}, {1:d} snps".format(sample, self._samples[sample]))

        for member in sorted(self._map.keys()):
            for marker in sorted(self._map[member].keys()):
                self._stat_total += 1
                ls = self._map[member][marker]
                if len(ls) > 1:
                    rev = self._map_rev[member][marker]
                    self._stat_possible_conflict += 1
                    if options.debug >= 2:
                        self.pr_alleles("Bfr", rev[0], member, rev[1], marker, ls)
                    cd = [[1, ls[0]]]
                    for allele in ls[1:]:
                        self._allele_.merge_alleles2(cd, allele)

                    if len(cd) > 1 and not (len(cd) == 2 and cd[0][1][0] == '0' and cd[0][1][1] == '0'):
                        self._stat_conflict += 1
                        self.pr_alleles("ERR", rev[0], member, rev[1], marker, cd)
                    elif len(cd) == 1:
#hmm
                        self._map[member][marker] = (cd[0][1], )
                        self._stat_multiple += 1
                        if options.debug >= 2:
                            self.pr_alleles("OKm", rev[0], member, rev[1], marker, cd)
                    else:  # 2 with (0,0) first
#hmm
                        self._map[member][marker] = (cd[1][1], )
                        self._stat_zero += 1
                        if options.debug >= 2:
                            self.pr_alleles("OKz", rev[0], member, rev[1], marker, cd)
        self.plx_stat()

    def plx_stat(self):
        printnl("total memXmarker {0:d}, possible marker conflicts {1:d}, duplicate {2:d}, missing once {3:d}, actual conflicts {4:d}\n".format(self._stat_total, self._stat_possible_conflict, self._stat_multiple, self._stat_zero, self._stat_conflict))

    def pr_alleles(self, msg, memstr, member, mkrstr, marker, cd):
        printnl("{0}: sample {1} (id {2:d}), marker {3} (id {4:d}):".format(msg, memstr, member, mkrstr, marker))
        for c in cd:
            printnl("\t{0}".format(c))

    def pr_genotype(self, *err):
        person, snp = self._map_rev[self._member][self._marker]
        rs = snp
        log.newrecord()
        if self._options.debug:
            printnl("person {0} -> {1}; snp[{2}] -> {3} ({4})".format(person, self._member, rs, self._marker, self._allele[-1]))
#            if self._options.onetwo:
#                printnl("\t{0}".format(snp))
        else:
            printn("person {0} [snp {1}] ({2})".format(person, rs, self._allele[-1]))
        for e in err: printnl(e)

    def genotype(self, dbi, options):
        if options.quality_file:
            return
        self._options = options
        self.fix_conflicts(dbi, options)

        if options.commit: log.push('insert', clear='genotype.plx')
        log.push('genotype.plx', 5, newrecord=True)
        for self._member, rest in sorted(self._map.iteritems(), key=itemgetter(0)):
            for self._marker, alleles in sorted(rest.iteritems(), key=itemgetter(0)):
                for self._allele in alleles:
                    self._genotype_.process_one(options, self._member, self._marker, self._allele, self.pr_genotype)
        self._genotype_.process_last(options, self._member_, self._marker_)
        log.pop('genotype.plx')
        if options.commit: log.pop('insert')
        return True

    def stat(self, dbi, options):
        if options.quality_file:
            return
        self._genotype_.stat(options, self._genotype_, self._member_, self._marker_, {})
        self.plx_stat()
        return True
