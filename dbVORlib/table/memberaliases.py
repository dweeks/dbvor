#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from dbVORlib.util             import NoTrace, printnl

from dbVORlib.table.dbtable    import DBTable



class MemberAlias(DBTable):

    def __init__(self, db):
        super(MemberAlias, self).__init__(db)
#       self._member_    = Member(db)
        self._aliases    = {}
        self._name       = None

    def add_options(self, config):
        super(MemberAlias, self).add_options(config)
        config.add_option("--oldpedigree", action="store", type="string",
                          help="choose columns to get old pedigree string from input")
        config.add_option("--oldperson", action="store", type="string",
                          help="choose columns to get old person string from input")
        config.add_option("--newpedigree", action="store", type="string",
                          help="choose columns to get new pedigree string from input")
        config.add_option("--newperson", action="store", type="string",
                          help="choose columns to get new person string from input")

        config.add_option("--active", action="store", type="int", default=1,
                          help="active value")

    def prolog(self, dbi, options):
        super(MemberAlias, self).prolog(dbi, options)
        if not options.oldpedigree or not options.oldperson or not options.newperson:
            raise NoTrace("You must always specify: --oldpedigree, --oldperson, and --newperson")
        for rec in self._dbi_.select_all("select lower(ped_alias), lower(alias), member, id from member_aliases"):
            ped, alias, member, Aid = rec
            if alias not in self._aliases:
                self._aliases[alias] = {}
            if ped not in self._aliases[alias]:
                self._aliases[alias][ped] = {}
            self._aliases[alias][ped] = rec
        return True

    def do_row(self, fields, dbi, options):
        new = []
        flen = len(fields)

        idx = int(options.oldpedigree)
        if idx > flen:
            printnl("old pedigree offset {0} extends beyond list of items {1}".format(idx, fields))
            return
        else:
            new.append(fields[idx - self.ORIGIN])

        idx = int(options.oldperson)
        if idx > flen:
            printnl("old person offset {0} extends beyond list of items {1}".format(idx, fields))
            return
        else:
            new.append(fields[idx - self.ORIGIN])

        if options.newpedigree:
            idx = int(options.newpedigree)
            if idx > flen:
                printnl("new pedigree offset {0} extends beyond list of items {1}".format(idx, fields))
                return
            else:
                new.append(fields[idx - self.ORIGIN])
        else:
            new.append('UNUSED')

        idx = int(options.newperson)
        if idx > flen:
            printnl("new person offset {0} extends beyond list of items {1}".format(idx, fields))
            return
        else:
            new.append(fields[idx - self.ORIGIN])

        self._name = new[2]+ ':' + new[3]
        new.append(options.active)
        self.row_util(new, self._name, dbi, options)

    def pr_rec(self, fields, options, db=False):
        if options.verbose:
            cmd = "old pedigree/person:{0[0]}/{0[1]} new pedigree/person:{0[2]}/{0[3]}"
        else:
            cmd = "old pedigree/person:{0[0]}/{0[1]} new pedigree/person:{0[2]}/{0[3]} active:{0[4]}"
        return cmd.format(fields)

    cmds = "select pedigree, person from members where id = ?"
    def select(self, fields, options):
        alias  = self._aliases.get(fields[3].lower(), None)      # person
        if alias is not None: alias = alias.get(fields[2].lower(), None) # pedigree
        if alias:
            member = self._dbi_.select_one(self.cmds, alias[2])
            if member:
                pedigree, person = member
            return ((pedigree, person, alias[0], alias[1], alias[2], alias[3]), )
        else:
            return tuple()

    cmdm = "select id from members where pedigree = ? and person = ?"

    cmdd = "delete from member_aliases where id = ?"
    def delete(self, rec, options):
        self._dbi_.execute(self.cmdd, rec[-1])

    cmdi = "insert into member_aliases (ped_alias, alias, member, projectid) VALUES (?, ?, ?, ?)"
    def insert(self, fields, options):
        rec = self._dbi_.select_one(self.cmdm, fields[0], fields[1])
        if rec:
            Mid, = rec
            self._dbi_.insert(self.cmdi, [(fields[2], fields[3], Mid, options.project_id)])


    def cmp(self, rec, fields, options):
        return cmp(rec[0], fields[0]) or cmp(rec[1], fields[1]) or cmp(rec[2], fields[2]) or cmp(rec[3], fields[3])

    def validate(self, fields, name, options):
        ret = True
        return ret

