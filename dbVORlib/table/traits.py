#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import re

from dbVORlib.util             import NoTrace, printnl, log

from dbVORlib.table.memtable   import MemTable
from dbVORlib.table.members    import FAMParse


class PreTrait(MemTable):

    def __init__(self, db):
        super(PreTrait, self).__init__(db)
        self._trait     = None
        self._ttype     = None
        self._traitmap  = {}
        self._traitstat = {}

    def prolog(self, dbi, options):
        super(PreTrait, self).prolog(dbi, options)

        if options.titles and len(options.columns) != len(options.titles):
            raise NoTrace("\n!! YOU MUST have the same number of titles as columns !!")
        for ttype in options.types:
            if ttype not in ('A', 'T', 'C', 'NA'):
                raise NoTrace("\n!! The type values MUST be either 'A', 'T', 'C' or 'NA'. !!")

        return self.insert_many(self.cmdi, self.fmti.format(options.project_id), options)

    def epilog(self, dbi, options):
        super(PreTrait, self).epilog(dbi, options)
        if options.stat or not options.nostat:
            cnttrait = 0
#           for trait, value_cnt in self._traitstat.iteritems():
            for traitrec in self._data:
                trait = traitrec[2]
                value_cnt = self._traitstat[trait]
                cnttrait += 1
#               traitrec = self._traitmap[trait]
                log.push(traitrec[1])
                printnl("trait \"{0}\" type {1} col {2:d} (db id {3:d})".format(traitrec[1], traitrec[3], traitrec[0]+self.ORIGIN, trait))
                printnl("\t{0:>12} {1:>6}".format("value", "count"))
                log.endheader()
                if traitrec[3] in ('T', 'A'):
                    self.trait_sort_numeric(value_cnt, traitrec)
                else:
                    self.trait_sort_literal(value_cnt, traitrec)
            printnl('')
            printnl("{0} trait{1} per line read from {2} lines in file {3}".format(cnttrait, ("" if cnttrait == 1 else "s"), self._line_no, options.input))

            self._member_.stat(options)

    def trait_sort_numeric(self, value_cnt, traitrec):
        meankey = cntkey = 0
        na = value_cnt.pop('NA', 0)
        mt = value_cnt.pop('', 0)
        if not value_cnt.keys(): return
        for value in sorted(value_cnt.keys(), key=float, reverse=True):
            cnt  = value_cnt[value]
            printnl("\t{0:>12} {1:6d}".format(repr(value), cnt, tag=traitrec[1]))
            keyv = float(value)
            if cntkey == 0: minkey = maxkey = keyv
            if keyv < minkey: minkey = keyv
            if keyv > maxkey: maxkey = keyv
            meankey += keyv * cnt
            cntkey  += cnt
        log.pop(traitrec[1])
        printnl("{0}: min {1}, max {2}, avg {3}, count {4}, NA's {5}, \"\"'s {6}".format(traitrec[1], minkey, maxkey, meankey/cntkey, cntkey, na, mt))

    def trait_sort_literal(self, value_cnt, traitrec):
        for value in sorted(value_cnt.keys()):
            cnt  = value_cnt[value]
            printnl("\t{0:>12} {1:6d}".format(repr(value), cnt, tag=traitrec[1]))
        log.pop(traitrec[1])

    def header(self, File, dbi, options):
        if File:
            header = super(PreTrait, self).header(File, dbi, options)
        else:
            header = [ ]

        maxid = dbi.select_one("select max(id) from traitmeta")
        maxid = (maxid[0] if not ((maxid is None) or (maxid[0] is None)) else 1000000) + 1
        for i, column in enumerate(options.columns):
            title = options.titles[i] if i < len(options.titles) else None
            if title is None:
                title = header[column-self.ORIGIN]
            select = dbi.select_one("select id, type from traitmeta where name = ?", title)
            if select is None:
                if i > len(options.types):
                    printnl("not enough type values for trait {0}".format(title))
                    continue
                ttype = options.types[i]
                if options.commit:
                    dbi.insert("insert into traitmeta (name, projectid, type) VALUES (?, ?, ?)" ,
                               [(title, options.project_id, ttype)])
                    select = dbi.select_one("select id, type from traitmeta where name = ?", title)
                    if select is None:
                        printnl("db insert of {0} failed. trait will not be loaded !!".format(title))
                        continue
                else:
                    select = (maxid + i, ttype)
            trait_id, ttype = select
            rec = [column-self.ORIGIN, title, trait_id, ttype]
            self._data.append(rec)
            self._traitmap[trait_id] = rec
            if options.stat or not options.nostat:
                self._traitstat[trait_id] = {}

    def do_row(self, fields, dbi, options):
        super(PreTrait, self).do_row(fields, dbi, options)

        self._member = self._member_.member(self._pedigree, self._person, options, self.pr_context)
        if self._member is None:
            return

        for column, name, trait, ttype in self._data:
            self._name  = name
            self._trait = trait
            self._ttype = ttype
            if column >= len(fields):
                self.pr_context("too few columns({0:d}) on this row to extract column {1:d}".format(len(fields), column+self.ORIGIN), tag='Columns')
                continue
            field       = fields[column]
            if field == options.missing_phenotype:
                field = '0'
            self.row_util([self._member, trait] + [field], name, dbi, options)
            if options.stat or not options.nostat:
                if field not in self._traitstat[trait]: self._traitstat[trait][field] = 0
                self._traitstat[trait][field] = self._traitstat[trait][field] + 1

    cmds = "select member, trait, value, id from traits where member = ? and trait = ? and projectid = ?"
    def select(self, fields, options):
        return self._dbi_.select_all(self.cmds, self._member, self._trait, self._project_id)

    cmdd = "delete from traits where id = ?"
    def delete(self, fields, options):
        self._dbi_.execute(self.cmdd, fields[-1])

    cmdi = "insert into traits (projectid, member, trait, value)"
    fmti = "({0:d}, {{0}}, {{1}}, {{2}})"
    def insert(self, fields, options):
        if fields[-1] is None:    fields[-1] = "NULL"
        self._im(fields)

    def update(self, fields, db, options):
        cmd = "update traits set value = ? where id = ?"
        self._dbi_.execute(cmd, fields[2], db[0][-1])
        

    def pr_rec(self, x, options, db=False):
        if options.verbose:
            if db:
                return "value:{0!r}".format(x[-2])
            else:
                return "value:{0!r}".format(x[-1])
        ans = list(self._member_.rev(x[0]))
        ans += [x[0]]
        ans += self._traitmap[x[1]]
        ans += [x[2] if x[2] is not None else 'NA']
        if db:    ans += [x[3]]
        if not options.debug:
            return "pedigree:{0[0]!r} person:{0[1]!r} trait:{0[4]!r} value:{1[2]!r}".format(ans, x)
        else:
            return ans

    def cmp(self, rec, fields, options):
        return cmp(rec[0], fields[0]) or cmp(rec[1], fields[1]) or cmp(rec[2], fields[2])

    NUMBER = re.compile(r"^-?((\d+)|(\d+.\d+)|(.\d+))$")
# should inherit
#   tags = ('verbose', 'update', 'compare', 'misc')
    tags = ('verbose', 'update', 'compare', 'misc', "Columns", "NoMember", "NoValue", "NA", "BadTypeA", "BadTypeT")
    def validate(self, fields, name, options):
        if fields[2] == '':
            self.pr_context('no value \"{0}\" for \"{1}\" type {2}'.format(fields[2], name, self._ttype), tag="NoValue")
            return False
        elif fields[2] == 'NA':
            fields[2] = None
            self.pr_context('switching NA to None for \"{0}\" type {1}'.format(name, self._ttype), tag="NA")
            return True
        elif self._ttype == 'A':
            if fields[2] not in ('0', '1', '2'):
                self.pr_context('bad value \"{0}\" for \"{1}\" type {2}'.format(fields[2], name, self._ttype), tag="BadTypeA")
                options.error += 1
                return False
        elif self._ttype == 'T':
            try:
                ans = float(fields[2])
            except Exception, error:
#               if not self.NUMBER.match(fields[2]):
                self.pr_context('\"{0}\" not a float for \"{1}\" type {2}'.format(fields[2], name, self._ttype), tag="BadTypeT")
                options.error += 1
                return False
        return True

class TTrait(PreTrait):

    def add_options(self, config):
        super(TTrait, self).add_options(config)

        config.add_option("--columns", action="append", type="int", default=[],
                          help="choose columns to get from input")
        config.add_option("--titles", action="append", default=[],
                          help="database title for trait columns")
        config.add_option("--types", action="append", default=[],
                          help="type for trait columns")
        config.add_option("--missing_phenotype", action="store", default='-9',
                          help="default value to indicate missing phenotype")
        config.add_option("--record", action="replay",
                          default=['columns', 'titles', 'types'],
                          help="spread data out to column, titles, type")
        config.add_option("--stat", action="store_true", default=False,
                          help="statistics for trait columns")
        config.add_option("--nostat", action="store_true", default=False,
                          help="no statistics for trait columns")

class FAMTrait(FAMParse, PreTrait):

    def add_options(self, config):
        super(FAMTrait, self).add_options(config)

        config.add_option("--missing_phenotype", action="store", default='-9',
                          help="default value to indicate missing phenotype")
        config.add_option("--default_trait_type", action="store", default='A',
                          help="type of default trait (col 6): A or T")
        config.add_option("--default_trait_name", action="store", default='PLINKTrait',
                          help="name for default trait (col 6) default: PLINKTrait")

        config.add_option("--stat", action="store_true", default=False,
                          help="statistics for trait columns")
        config.add_option("--nostat", action="store_true", default=False,
                          help="no statistics for trait columns")

    def prolog(self, dbi, options):
        if options.trait:
            options.columns = [ options.trait ]
            options.titles =  [ options.default_trait_name ]
            options.types  =  [ options.default_trait_type ]
            return super(FAMTrait, self).prolog(dbi, options)
        else:
            printnl("\nNO trait column was specified.  Not storing a trait.")
            return False

    def header(self, File, dbi, options):
        super(FAMTrait, self).header(None, dbi, options)
