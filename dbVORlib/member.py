#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#
# Member aliasing
#
# External Class
#	 Member
# External Methods
#	 member	-- look up pedigree/person

import types

from   util     import printnl, log

class Member_Alias(object):
    def __init__(self, dbi):
        self._dbi_     = dbi
        self._mt       = None
        self._db_cache = {}

    def prime_cache(self):
#       for sel in self._dbi_.select_all("select member, alias, ped_alias, id from member_aliases"):
        for sel in self._dbi_.select_all("select ped_alias, alias, member, id from member_aliases"):
            person = sel[1].lower()
            if person not in self._db_cache:
                self._db_cache[person] = []
            self._db_cache[person].append(sel)

# needs cache
    def lookup(self, pedigree, person, pr_context, fmtlen, options):
        if self._mt is False:
            return None
        elif self._mt is None:
            count = self._dbi_.select_one("select count(*) from member_aliases")
            if count[0] > 0:
                self.prime_cache()
                self._mt = True
            else:
                self._mt = False
                return None

        Midc = self._db_cache.get(person.lower(), [])
#        if options.debug:
#            Mid = self._dbi_.select_all("""select member, alias, ped_alias, id from member_aliases
#                                                  where alias = ?""", person)
#            if list(Mid) != Midc:
#                print 'TILTA', Mid, Midc
        Mid = Midc

        if len(Mid) == 0:
            return None
        if len(Mid) == 1:
            if (not pedigree) or Mid[0][0].lower() == pedigree.lower():
                if person != Mid[0][1]:
                    printnl("Warning:  person input as \"{0}\", but spelled \"{1}\" in the database member_aliase table.".format(person, Mid[0][1]))
                return Mid[0][2]
            return None
# begin for now ...
        if not pedigree:
            pedigree = 'UNUSED'
# end for now ...
        if not pedigree:
            s = "ERROR: multiple MEMBER_ALIASES entries for alias {0} but no pedigree given: {1}.".format(person, Mid)
            pr_context(s)
            options.error += 1
            return Mid[0][2]
        for entry in Mid:
            if entry[0] == pedigree:    return entry[2]

        cols = ("alias_pedigree", "alias_person", "member_id", "alias_id")
        fmtl  = fmtlen(cols, ped=0, per=1, member=2)
        hfs1 = "\t\t{t4[0]:{fld[0]}} {t4[1]:{fld[1]}} {t4[2]:{fld[2]}} {t4[3]:{fld[3]}}"
        hfs2 = "\t\t{t4[0]:{fld[0]}} {t4[1]:{fld[1]}} {t4[2]:{fld[2]}d} {t4[3]:{fld[3]}d}"

        s = "ERROR: No entry in the MEMBER_ALIASES table matches person {0} with pedigree {1}:".format(person, pedigree)
        pr_context(s, tag='NoMember')
        printnl("    There is more than one entry in the MEMBER_ALIAS table for person {0}:".format(person), tag='NoMember')
        printnl(hfs1.format(t4=cols, fld=fmtl), tag='NoMember')

        for m in Mid:
            printnl(hfs2.format(t4=m, fld=fmtl), tag='NoMember')
        options.error += 1
        return None

class Member(object):
    def __init__(self, dbi):
        self._dbi_     = dbi
        self._alias_   = Member_Alias(dbi)
        self._db_cache = None    # don't initialize iff cacheload only
        self._P2id     = {}
        self._PP2id    = {}
        self._revr     = {}
        self._missing  = []
        self._aliased  = []
        self._multi    = []

        self._includeped = []
        self._includeper = []
        self._includeppp = {}
        self._excludeped = []
        self._excludeper = []
        self._excludeppp = {}

        self._included   = None
        self._includedpp = []
        self._excluded   = None
        self._excludedpp = []


    def member(self, pedigree, person, options, pr_context):
        if self._included is None:
            self.expand_pedigree_person_lists(options)
            self._included = 0
            self._excluded = 0
        if self._db_cache is None:
            self._db_cache = {}
            self.prime_cache()

        if pedigree == '':                 # no pedigree
            if person in self._P2id:    # cached
                Id = self._P2id[person]
#               pr_context("person cached {0} -> {1}".format(person, Id))
                return Id
        else:
            if pedigree not in self._PP2id: self._PP2id[pedigree] = {}
            if person in self._PP2id[pedigree]: # cached
                Id = self._PP2id[pedigree][person]
#               pr_context("pedigree:person cached {0}:{1} -> {2}".format(pedigree, person, Id))
                return Id
        error = options.error
        Id = self.member_cache(pedigree, person, options, pr_context)
        if error != options.error:
            if options.debug:
                pr_context("ERROR: converting pedigree:person to member id; skipping ID")
        return Id

    def prime_cache(self):
        for sel in self._dbi_.select_all("select id, projectid, pedigree, person, father, mother from members"):
            pedigree, person = sel[2:4]
            pedigree, person = pedigree.lower(), person.lower()
            if person not in self._db_cache:
                self._db_cache[person] = {}
            if pedigree not in self._db_cache[person]:
                self._db_cache[person][pedigree] = []
            self._db_cache[person][pedigree].append(sel)

    def member_cache(self, pedigree, person, options, pr_context):
        if pedigree == '':                 # NO PEDIGREE
            if not self.includepp(pedigree, person, options):
                self.cache2listP(person, None, pr_context)
                return None
            Id = self._alias_.lookup(pedigree, person, pr_context, self.fmtlen, options)
            if Id:                         # maybe as alias
                self._aliased.append(('', person, Id))
                self.cache2listP(person, Id, pr_context)
                return Id
                                           # member lookup
            Idc = []
            for val in self._db_cache.get(person.lower(), {}).itervalues():  # merge for different pedigree
                Idc.extend(val)
#            if options.debug:
#                Ids = self._dbi_.select_all("select id, projectid,pedigree,person,father,mother from members where person = ?", person)
#                if list(Ids) != Idc:
#                    print 'TILTP', Ids, Idc
            Ids = Idc
            if len(Ids) == 0:              # no
                pr_context("ERROR: pedigree:person :{0} is not in the MEMBER table.".format(person), tag='NoMember')
                self._missing.append(('', person))
                options.error += 1
                Id = None
            else:                          # one or more
                if len(Ids) > 1:         # ... more
                    self._multi.append(('', person, Ids))
                    pr_context("ERROR: :person :{0} appears multiple times in the MEMBERS table.".format(person))
                    for i in Ids: pr_context("\t\t{0}".format(i))
                    options.error += 1
                if person != Ids[0][3]:
                    printnl("Warning: person input as \"{0}\", but spelled \"{1}\" in the database member table.".format(person, Ids[0][3]))
                Id = Ids[0][0]
            self.cache2listP(person, Id, pr_context)
            return Id
        else:                              # PEDIGREE
            if not self.includepp(pedigree, person, options):
                self.cache2listPP(pedigree, person, None, pr_context)
                return None
            Id = self._alias_.lookup(pedigree, person, pr_context, self.fmtlen, options)
            if Id:                         # maybe as alias
                self._aliased.append((pedigree, person, Id))
                self.cache2listPP(pedigree, person, Id, pr_context)
                return Id
                                           # member lookup
            Idc = self._db_cache.get(person.lower(), {}).get(pedigree.lower(), [])
#            if options.debug:
#                Ids = self._dbi_.select_all("select id, projectid,pedigree,person,father,mother from members where person = ? and pedigree = ?", person, pedigree)
#                if list(Ids) != Idc:
#                    print 'TILTPP', Ids, Idc
            Ids = Idc
            if len(Ids) == 0:              # no
                self._missing.append((pedigree, person))
                pr_context("ERROR: pedigree:person {0}:{1} is not in the MEMBER table.".format(pedigree, person))
                options.error += 1
                Id = None
            else:                          # one or more
                if len(Ids) > 1:         # ... more
                    self._multi.append((pedigree, person, Ids))
                    pr_context("ERROR: pedigree:person {0}:{1} appears multiple times in the MEMBER table.".format(pedigree, person))
                    for i in Ids: pr_context("\t\t{0}".format(i))
                    options.error += 1
                if pedigree != Ids[0][2] or person != Ids[0][3]:
                    printnl("Warning: pedigree:person input as \"{0}:{1}\", but spelled \"{2}:{3}\" in the database member table.".format(pedigree, person, Ids[0][2], Ids[0][3]))
                Id = Ids[0][0]
            self.cache2listPP(pedigree, person, Id, pr_context)
        return Id

    def rev(self, Id):
        return self._revr.get(Id, None)

    def cacheload(self, ped, per, Id, options, pr_context):
        if self._included is None:
            self.expand_pedigree_person_lists(options)
            self._included = 0
            self._excluded = 0
        ret = self.includepp(ped, per, options)
        if ret is False:
            return None
        if Id in self._revr:
            return False
        if ped is None:
            self.cache2listP(per, Id, pr_context)
        else:
            self.cache2listPP(ped, per, Id, pr_context)
        return ret

    def cache2multi(self, Id):
        old = self._revr[Id]
        if type(old) != types.ListType:
            old = [old]
            self._revr[Id] = old
        return old

    def cache2listP(self, person, Id, pr_context):
        self._P2id[person] = Id
        if Id is None: return
        if Id not in self._revr:
            self._revr[Id] = (None, person)
        else:
            old = self.cache2multi(Id)
            pr_context("Multiple MEMBERS maps to {0}: others are:".format(Id))
            for v in old:
                pr_context("person {0}".format(v[1]))
            old.append((None, person))


    def cache2listPP(self, pedigree, person, Id, pr_context):
        self._PP2id.setdefault(pedigree, {})[person] = Id
        if Id is None: return
        if Id not in self._revr:
            self._revr[Id] = (pedigree, person)
        else:
            old = self.cache2multi(Id)
            pr_context("Multiple PEDIGREE:PERSON maps to member_id {0}; the others are:".format(Id),
                       tag='MultiPedPer')
            for v in old:
                printnl("    pedigree:person {t2[0]}:{t2[1]}".format(t2=v), tag='MultiPedPer')
            old.append((pedigree, person))

    def list(self):
        return sorted(self._revr.iterkeys(), key=lambda x: self._revr[x])

    def sortedlist(self):
        return self.list().__iter__()

    def includepp(self, pedigree, person, options):
        ped = pedigree.lower()
        per = person.lower()
#tmp
        if len(ped) > log.pedigree_name_length:
            log.sizes(pedigree_name_length=len(ped))
        if len(per) > log.person_name_length:
            log.sizes(person_name_length=len(per))
#tmp
        include = inc = False
        if self._includeped:
            include = True
            if ped in self._includeped:
                inc = True
#               s = "pedigree:person {0}:{1} included because pedigree {2} is in the \"include\" list".format(pedigree, person, pedigree)
#               printnl(s)
        if self._includeper:
            include = True
            if per in self._includeper:
                inc = True
#               s = "pedigree:person {0}:{1} included because person {2} is in the \"include\" list".format(pedigree, person, person)
#               printnl(s)
        if self._includeppp:
            include = True
            if ped in self._includeppp:
                persons = self._includeppp[ped]
                if per in persons:
                    inc = True
#                   s = "pedigree:person {0}:{1} included because {2}:{3} is in the \"include\" list".format(pedigree, person, pedigree, person)
#                   printnl(s)

        if include and not inc:
            if options.debug:
                s = "pedigree:person {0}:{1} excluded because {2}:{3} is NOT in the explicit \"include\" list".format(pedigree, person, pedigree, person)
                printnl(s)
            self._excludedpp.append((pedigree, person))
            self._excluded += 1
            return False         # fail not explicitly included

        inc = True
        if self._excludeped:
            if ped in self._excludeped:
                inc = False
#               s = "pedigree:person {0}:{1} excluded because pedigree {2} is in the \"exclude\" list".format(pedigree, person, pedigree)
#               printnl(s)
        if self._excludeper and inc:
            if per in self._excludeper:
                inc = False
#               s = "pedigree:person {0}:{1} excluded because person {2} is in the \"exclude\" list".format(pedigree, person, person)
#               printnl(s)
        if self._excludeppp and inc:
            if ped in self._excludeppp:
                persons = self._excludeppp[ped]
                if per in persons:
                    inc = False
#                   s = "pedigree:person {0}:{1} excluded because {2}:{3} is in the \"exclude\" list".format(pedigree, person, pedigree, person)
#                   printnl(s)
        if inc:
            self._includedpp.append((pedigree, person))
            self._included += 1
        else:
            self._excludedpp.append((pedigree, person))
            self._excluded += 1
        return inc

    def expand_pedigree_person_lists(self, options):
        if getattr(options, 'include', False):
            self.parse_xxclude(options.include,
                               self._includeped, self._includeper, self._includeppp)

        if getattr(options, 'exclude', False):
            self.parse_xxclude(options.exclude,
                               self._excludeped, self._excludeper, self._excludeppp)

    def parse_xxclude(self, lst, ped, per, ppp):
        if not lst: return
        for ppcp in lst:              # pedigree: or person or pedigree:person
            ppcp = ppcp.lower()
            eles = ppcp.split(':')
            if len(eles) == 1:        #  person
                if eles[0] in per: continue
                per.append(eles[0])
            elif eles[0] == '':       # :person
                if eles[1] in per: continue
                per.append(eles[1])
            elif eles[1] == '':       # pedigree:
                if eles[0] in ped: continue
                ped.append(eles[0])
            else:
                if (eles[0] in ppp) and (eles[1] in ppp[eles[0]]): continue
                ppp.setdefault(eles[0], []).append(eles[1])

    def stat(self, options):
        def pedper_key(ta, tb):
            return cmp(ta[0], tb[0]) or cmp(ta[1], tb[1])

        if getattr(options, 'include', False) or getattr(options, 'exclude', False) or True:
            printnl("\n{0:d} members included; {1:d} members excluded".format(\
                    self._included, self._excluded))
            self._includedpp.sort(pedper_key)
            self._excludedpp.sort(pedper_key)
            self.pr_pp(options)

        if self._missing:
            self._missing.sort(pedper_key)
            self.pr_missing(options)

        if self._aliased:
            self._aliased.sort(pedper_key)
            self.pr_alias(options)

        if self._multi:
            self._multi.sort(pedper_key)
            self.pr_multi(options)

    def pr_missing(self, options):
        FILE = options.dir + options.cfgroot + '.MissingMembers.txt'
        File = open(FILE, 'w')
        File.write("#{0:d} pedigree:persons are not in the member table or alias table\n".format(len(self._missing)))
        File.write("{0}\n".format('#exclude: pedigree:person'))

        log.push('member_missing')
        printnl("\n{0:d} pedigree:persons are not in the member table or alias table:".format(len(self._missing)))
        printnl("Exclusion file was written to {0}".format(FILE))
        ppl = self.fmtlen('pedigree', 'person', member=None, ped=0, per=1)
        hfs1 = "{0:>{fld[0]}} {1:>{fld[1]}}"
        hfs2 = "{tpl2[0]:>{fld[0]}} {tpl2[1]:>{fld[1]}}"
        printnl(hfs1.format('pedigree', 'person', fld=ppl))
        log.endheader()
        for tupl in self._missing:
            printnl(hfs2.format(tpl2=tupl, fld=ppl))
            File.write("exclude: {0[0]}:{0[1]}\n".format(tupl))
        File.close()
        log.pop('member_missing')
        printnl("\n")

    def pr_pp(self, options):
        log.push("includedpp")
        printnl("\n{0:d} pedigree:persons included:".format(len(self._includedpp)))

        ppl = self.fmtlen('pedigree', 'person', member=None, ped=0, per=1)
        hfs1 = "{0:>{fld[0]}} {1:>{fld[1]}}"
        hfs2 = "{tpl2[0]:>{fld[0]}} {tpl2[1]:>{fld[1]}}"
        printnl(hfs1.format('pedigree', 'person', fld=ppl))
        log.endheader()
        for pp in self._includedpp:
            printnl(hfs2.format(tpl2=pp, fld=ppl))
        log.pop("includedpp")

        if self._excludedpp:
            log.push("excludedpp")
            printnl("\n{0:d} pedigree:persons excluded:".format(len(self._excludedpp)))
            printnl(hfs1.format('pedigree', 'person', fld=ppl))
            log.endheader()
            for pp in self._excludedpp:
                printnl(hfs2.format(tpl2=pp, fld=ppl))
            log.pop("excludedpp")

    def pr_alias(self, options):
#       FILE = options.dir + options.cfgroot + '.AliasedMembers.txt'
#       File = open(FILE, 'w')
#       File.write("#{0:d} pedigree:persons were found in the member_alias table\n".format(len(self._aliased)))
#       File.write("{0} {1} {2}\n".format('#pedigree:person', 'member_ID', 'new ped:new person'))

        log.push('member_alias')
        printnl("\n{0:d} pedigree:persons were found in the member_alias table:".format(len(self._aliased)))
#       printnl("A listing file was written to {0}".format(FILE))
        ppl = self.fmtlen('pedigree', 'person', 'member_id', member=2, ped=0, per=1)
        hfs1 = "{0:>{fld[0]}} {1:>{fld[1]}} {2:{fld[2]}} {3:>{fld[0]}} {4:>{fld[1]}}"
        hfs2 = "{0:>{fld[0]}} {1:>{fld[1]}} {2:9} {3:>{fld[0]}} {4:>{fld[1]}}"
        printnl(hfs1.format('alias', 'alias', '', 'original', 'original', fld=ppl))
        printnl(hfs1.format('pedigree', 'person', 'member_id', 'pedigree', 'person', fld=ppl))
        log.endheader()
        for tupl in self._aliased:
            pedigree, person, ID = tupl
            pedper = self._dbi_.select_one("select pedigree, person from members where id = ?", ID)
            printnl(hfs2.format(pedigree, person, ID, pedper[0], pedper[1], fld=ppl))
#           File.write("{0}:{1} {2:d} {3}:{4}\n".format(pedigree, person, ID, pedper[0], pedper[1]))
#       File.close()
        log.pop('member_alias')

    def pr_multi(self, options):
#       FILE = options.dir + options.cfgroot + '.MultipleMembers.txt'
#       File = open(FILE, 'w')
#       File.write("#{0:d} pedigree:persons were found multiple times in  member table\n".format(len(self._multi)))
#       File.write("{0} {1}\n".format('#pedigree:person', 'Id') )

        log.push('member_error', newrecord=True)
        printnl("\n{0:d} pedigree:persons were found multiple times in  member table".format(len(self._multi)))
#       printnl("A listing file was written to {0}".format(FILE))
        ppl = self.fmtlen('pedigree', 'person', 'Id', member=2, ped=0, per=1)
        hfs1 = "{0:>{fld[0]}} {1:>{fld[1]}} {2:}"
        hfs2 = "{tupl2[0]:>{fld[0]}} {tupl2[1]:>{fld[1]}} {0:}"
        printnl(hfs1.format('pedigree', 'person', 'Id', fld=ppl))
        log.endheader()
        for tupl in self._multi:
            log.newrecord()
            for i in tupl[2]:
                printnl(hfs2.format(i, tupl2=tupl, fld=ppl))
#               File.write("{0}:{1} {2:d}".format(tupl[0], tupl[1], i))
#       File.close()
        log.pop('member_error')

    def fmtlen(self, *ls, **ks):
        if len(ls) and type(ls[0]) in (types.ListType, types.TupleType):
            ls = ls[0]
        ans = map(len, ls)
        member = ks.get('member', 0)
        if member is not None:
            ans[member] = max(ans[member], log.member_length)
        ped = ks.get('ped', 1)
        if ped is not None:
            ans[ped] = max(ans[ped], log.pedigree_name_length)
        per = ks.get('per', 2)
        if per is not None:
            ans[per] = max(ans[per], log.person_name_length)
        return tuple(ans)
