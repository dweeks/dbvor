#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from   operator import itemgetter
from   snp      import Snp
from   member   import Member
from   util     import printnl, printn, log, gzopen

class BlockSnp(Snp):
    def __init__(self, db):
        super(BlockSnp, self).__init__(db)
        self._member_           = Member(db)
        self._process_count     = 0

        self._subjects          = {} # Subject class defunct
        self._subject_count     = 0  # Subject class defunct

        self._experiments       = {}
        self._samplemap         = {}
        self._samplemapnew      = []
        self._samplemap_count   = 0
        self._untracked_samples = {}
        self._undefined_snp     = {}

        self._seqlock = 0


    def add_options(self, config):
        super(BlockSnp, self).add_options(config)

        config.add_option("--file_output", action="store", type="string",
                          help="write out dna data to file")
        config.add_option("--db_output", action="store_true", default=False,
                          help="insert dna data into database")
        config.add_option("--seqlocklim", action="store", type="int",
                          help="upper bound for seqlock field")

        config.add_option("--sep", "--file_separator", type="string",
                          help="prepDB experiment file internal separator; default white space")

#       config.add_option("--verify", action="store_true", default=False,
#                         help="verify genotype data in database matches experiment file")
#       config.add_option("--verify_in", action="store_true", default=False,
#                         help="complain if experiment file not in database")
#       config.add_option("--verify_show_all", action="store_true", default=False,
#                         help="show all conflicts genotype data in database matches experiment file")
#       config.add_option("--compare", action="store_true", default=False,
#                         help="like verify except assume the data is already in the database")

#was but see Snp class for "named" constants
#           0        1    2   3        4        5   6  7    8    9   10
#   r = [CALL, quality, snp, rs, int(chrm), int(bp), a, b, blk, off, cnt

    cmdu = "update samples set filename = ? where id = ?"
    def insert_blocks(self, sample, options):
        samplemapped = self.sample_map(sample, options)
        if samplemapped[2][0] is None:
            expr = options.experiment_id
            printnl("Sample \"{0}\", experiment \"{1}\" ({2}) is not in the samples table.  It will be now ignored.".format(sample, self._experiments[expr], expr))
            return 0
        if samplemapped[2][5] is not None:
            expr = options.experiment_id
            printnl("Sample \"{0}\" was previously loaded by file \"{1}\".  ".format(sample, samplemapped[2][5]))
            printnl("It will now be ignored; it may not be loaded again.  Ignoring sample \"{0}\", experiment \"{1}\" ({2}).".format(sample, self._experiments[expr], expr))
            return 0
        if options.commit:
            self._dbi_.execute(self.cmdu, options.input, samplemapped[2][0])
        ooff = chrm = b = None
#       pairs = []
        pairs = bytearray(self._block_size * 2)
        ix = 0
        for lst in self._chr_list:
            for r in lst:
                if chrm != r[Snp.chr] or b != r[Snp.blk]:
                    if b != None:
                        self.db_block(samplemapped, sample, chrm, b, ooff, pairs, options)
#                   pairs = []
                    pairs = bytearray(self._block_size * 2)
                    ix = 0
                chrm = r[Snp.chr]
                b = r[Snp.blk]
                ooff = r[Snp.off]
#               print r[Snp.chr], b, ooff
                if r[Snp.cll] is None:
                    if options.NotSampled:
                        printnl('{0} {1} Not Sampled'.format(r[Snp.pid], sample))
#                   pairs.extend(('W', 'W'))
                    pairs[ix] = 'W'
                    pairs[ix+1] = 'W'
                    ix += 2
                    r[Snp.cnt] += 0             # No Sample present
                elif r[Snp.cll] == '--' or r[Snp.cll] == 'NoCall':
                    if options.NoCall:
                        printnl("{0} {1} {2} {3}".format(r[Snp.pid], sample, r[Snp.cll], r[Snp.qul]))
#                   pairs.extend(('0', '0'))
                    pairs[ix] = '0'
                    pairs[ix+1] = '0'
                    ix += 2
                    r[Snp.cnt] += 1
                elif r[Snp.cll] == '':
                    printnl("{0} {1} {2} {3}".format(r[Snp.pid], sample, r[Snp.cll], r[Snp.qul]))
#                   pairs.extend(('0', '0'))
                    pairs[ix] = '0'
                    pairs[ix+1] = '0'
                    ix += 2
                    r[Snp.cnt] += 1
                else:
#                   pairs.append(r[6:8][ord(r[0][0])-A])        # slower
#                   a,b = r[Snp.ab]                            # 4-5x slower
#                   pairs.append(a if r[0][0] == 'A' else b)
#                   pairs.append(a if r[0][1] == 'A' else b)
##                  pairs.append(r[Snp.ab][0] if r[0][0] == 'A' else r[Snp.ab][1])
## ab!= AB          pairs.append(r[Snp.ab][0] if r[0][1] == 'A' else r[Snp.ab][1])
#                   pairs.extend(list(r[Snp.cll]))
                    cll = r[Snp.cll]
                    pairs[ix] = cll[0]
                    pairs[ix+1] = cll[1]
                    ix += 2
                    r[Snp.cnt] += 1
        if ooff is not None and ooff != self._block_size:
            self.db_block(samplemapped, sample, chrm, b, ooff, pairs, options)

        return 1

    def db_block(self, samplemapped, sample, chrm, blk, ooff, data, options):
#       dbi = self._dbi_
        member   = samplemapped[0]
        active   = samplemapped[1]
        sampleid = samplemapped[2][0]
#       content = ''.join(data).ljust(2*self._block_size, 'Z')
        data = data.replace('\x00', 'Z')
        content = str(data)
        if options.file_output:
            print >>options.File, "{0} {1} {2} {3} {4} {5} {6}".format(member, sampleid, sample, chrm, blk, self._seqlock, content)
        if options.db_output:
            cmd = 'insert into genotypeblocks (projectid, experiment, technology, member, sampleid, chromosome, block, active, seqlock, data) VALUES '
#           fmt = "({0:d}, {1:d}, {2:d}, {3}, '{4}', {5:d}, {6:d}, {7:d}, {8:d}, '{9}')"
            fmt = "({0[0]:d}, {0[1]:d}, {0[2]:d}, {0[3]}, '{0[4]}', {0[5]:d}, {0[6]:d}, {0[7]:d}, {0[8]:d}, '{0[9]}')"
### dan
# what changes when pedigree structure changes ??
            val = (options.project_id, options.experiment_id, options.tech_id, member, sampleid, chrm, blk, active, self._seqlock, content)
            if not options.comdrop:
                self._dbi_.execute(cmd + (fmt.format(val)))

        if options.debug > 1:
            printnl("{0} {1} {2:d} {3:d} {4:d} {5:d} {6}".format(samplemapped, sample, chrm, blk, active, self._seqlock, content))
        else:
            if options.verbose >= 2:
                printn("{0}/{1}".format(chrm, ooff))

        if options.seqlocklim:
            self._seqlock += 1
            if self._seqlock > options.seqlocklim:
                self._seqlock = 0

    def get_line(self, line, Line, Lmarkers, sep):
        line = line.rstrip()
        if line == '' or line.startswith('#'):
            return None
        data = line.split(sep)
        if len(data) != Lmarkers:
            printnl("Skipping runt line(#{0:d}) {1}".format(Line, data))
            return None
        return data

    def stat(self, dbi, options):
        printnl("\n{0:d} snps, {1:d} samples\n".format(self._snp_count, self._process_count))

        log.push("NotMember")
        for sample, rec in sorted(self._untracked_samples.iteritems()):
            printnl('pedigree:person {0}:{1}" not found in members table; full sample:\n{2}'.format(rec[3], rec[4], rec))
        log.pop("NotMember")

        log.push("Undefined SNP")
        for snp in sorted(self._undefined_snp.keys()):
            printnl('snp {0} appeared {1:d} times in sample files but is not in mapping'.format(snp, self._undefined_snp[snp]))
        log.pop("Undefined SNP")

        log.push("Unused Probes")
        printnl("Unused Probes")
        printnl("{0:>19} {1:>12} {2:>3} {3:>10} {4:>8} {5:>4} {6:>3} {7:>2}".format('Affy Probe Id', 'SNP Id', 'chr', 'Position', 'Morgan', 'blk', 'off', 'allele'))
        log.endheader()
        for lst in self._chr_list:
            for r in lst:
                if r[10] == 0:
                    hfs1 = "{t8[0]:>19} {t8[1]:>12} {t8[2]:3d} {t8[3]:10d} {t8[4]:8.3f} {t8[5]:4d} {t8[6]:3d} {t8[7]:>2}"
                    printnl(hfs1.format(t8=r[2:-1]))
        log.pop("Unused Probes")

        FILE = options.dir + options.cfgroot + '.probeRs'
        File = gzopen(FILE, "w")
        print >>File, "{0:>19} {1:>12} {2:>3} {3:>10} {4:>8} {5:>4} {6:>3} {7:>2}".format('Affy Probe Id', 'SNP Id', 'chr', 'Position', 'Morgan', 'blk', 'off', 'allele')
        hfs2 = "{t9[0]:>19} {t9[1]:>12} {t9[2]:3d} {t9[3]:10d} {t9[4]:8.3f} {t9[5]:4d} {t9[6]:3d} {t9[7]:>2} {t9[8]:4d}"
        for lst in sorted(self._snp_hash.itervalues(), key=itemgetter(10, 4, 5)):
            print >>File, (hfs2.format(t9=lst[2:]))
        File.close()
        return False

################################################################
# Class sample
    def sample_select(self, dbi, options):
        cmd = "select id, activefwd, subject, pedigree, person, filename, plate, well, misc, experiment, sample, time_stamp from samples where active = ?"
        cnt = 0
        self._samplemap_count = 0
        for record in dbi.select_all(cmd, options.active):
            cnt += 1
            sample = record[-2]
            self._samplemap[(sample, record[-3])] = record
            self._samplemap_count += 1
            if options.debug > 2:
                printnl("{0}, {1}: {2}".format(sample, record[-3], record))
        cmd = "select id, filename from experiments"
        self._experiments = dict(dbi.select_all(cmd))
# 'samples'
        if cnt == 0:
            printnl('\nThe subjects table in the database is empty.  Please load it by specifying the --samplemap parameter.')
            return
        if cnt == 0:
            raise Exception('\nThe subjects table in the database is empty.  Please load it by specifying the --samplemap parameter.')
        printnl('Selected sample info ({0:d} records) from \"{1}.samples\"'.format(self._samplemap_count, options.db))
        printnl('')

    def sample_map(self, sample, options):
        def pr_context(x):
            pass
        samplerec = self._samplemap.get((sample, options.experiment_id), (None, None, sample, None, None, 'MissingExperiment'))
        if samplerec[0] is None:
#2/2        printnl("sample \"{0}\" not in samples table".format(sample))
            return 'Null', 0, samplerec
# NULL NULL means ??? don't look up
        if (samplerec[3] and (samplerec[3] != 'UNUSED')) or (samplerec[4] and (samplerec[4] != 'UNUSED')):
            member_id = self._member_.member(samplerec[3], samplerec[4], options, pr_context)
        else:
            member_id = None

        if member_id is None:
#rvb 5.24
            options.error = 0
            printnl("no corresponding member for pedigree:person {0}:{1} from sample\n{2}".format(samplerec[3], samplerec[4], samplerec))
            self._untracked_samples[sample.lower()] = samplerec
            return 'Null', 0, samplerec
        return member_id, samplerec[1], samplerec

# this is old code also.  There is now a "plate" column before the well.  In addition
# dbvor sample loads samples and
# dbvor member loads members
    def sample_read(self, dbi, options):
        if options.samplemap is None:
            raise Exception("samplemap not set")
        self._samplemap_count = 0
        FILE = options.dir + options.samplemap
        File = gzopen(FILE)
#subject	active	pedigree	person	well	misc	sample
#NOTE subject IS NOT UNIQUE sample IS
        old = new = 0
        for line in File:
            line = line.strip()
            if line == '' or line.startswith('#'):
                continue
            subject = line.split(options.sep)
            subject.append(None) # time_stamp
            sample = subject[-2].lower()
            if sample in self._samplemap:
                old += 1
            else:
#               NB: sample_select inserts and db ID as the first element of subject
                self._samplemap[sample] = subject
                new += 1
                self._samplemapnew.append(sample)
            self._samplemap_count += 1
            if options.debug:
                printnl("{0}: {1}".format(subject[-2], subject))
        File.close()
        printnl('File: {0}, {1:d} samples (new {2:d}, old {3:d})'.format(FILE, self._samplemap_count, new, old))

##r defunct !!
    def sample_insert(self, dbi, options):
        cmd  = 'insert into samples (projectid, techid, subject, activefwd, pedigree, person, well, misc, sample, active)'
        fmt  = "({0:d}, {1:d}, ".format(options.project_id, options.tech_id)
        fmt += "{0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', "
        fmt += "{0:d})".format(options.active)

#       def skf(item):
#           return item[0]
#       for unused, mapping in sorted(self._samplemap.iteritems(), key=skf):
        total = 0
        im = self._dbi_.insert_many(cmd, fmt, self._insert_size)
        for key in self._samplemapnew:
            mapping = self._samplemap[key]
            total += 1
            im(mapping[:-1])
        im()
        printnl('\n{}.samples table: {:d} new samples inserted.'.format(options.db, total))

        selm   = 'select id from members where pedigree = ? and person = ?'
        cmdm   = 'insert into members (projectid, pedigree, person, father, mother, gender, active)'
        fmtm   = "({0:d},".format(options.project_id)
        fmtm  += " '{0}', '{1}', '{2}', '{3}', '{4}',"
        fmtm  += " {0:d})".format(options.active)
        totalm = 0
        seen   = {}
        imm    = self._dbi_.insert_many(cmdm, fmtm, self._insert_size)
        for key in self._samplemapnew:
            mapping = self._samplemap[key]
            if mapping[2] != "UNUSED" and mapping[3] != "UNUSED":    # no person
# imm delays insert so using db is not good enough
                beseen = seen.setdefault(mapping[2], {}).setdefault(mapping[3], [])
                if beseen:
#                   pdb.set_trace()
                    printnl("sample member: first   insert {0}".format(beseen[0]))
                    printnl("sample member: ignored insert {0}".format(mapping))
                    continue
                beseen.append(mapping)
                chk = dbi.select_one(selm, mapping[2], mapping[3])
                if chk is None:    # new ... unseen ... not preloaded
                    totalm += 1
                    imm([ mapping[2], mapping[3], 0, 0, 0, ])
        imm()
        printnl("{0}.members table: {1:d} new person:pedigree's inserted.".format(options.db, totalm))

        cmdt   = 'insert into traits (projectid, member, trait, value)'
        fmtt   = "({0:d},".format(options.project_id)
        fmtt  += " '{0}', '{1}', '{2}')"
        totalt = 0
        seen   = {}
        imt    = self._dbi_.insert_many(cmdt, fmtt, self._insert_size)
        for key in self._samplemapnew:
            mapping = self._samplemap[key]
            if mapping[2] != "UNUSED" and mapping[3] != "UNUSED":    # no person
                chk = dbi.select_one(selm, mapping[2], mapping[3])
                member = chk[0]
                if seen.get(member, True):
                    totalt += 1
                    imt( [ member, 1, 0 ] )
                    seen[member] = False
        imt()
        printnl("{0}.traits table: {1:d} new trait[1] inserted.".format(options.db, totalt))

#       self._dbi_.endbatch(options, commit=True)
        self._dbi_.endbatch(options)

# code below (esp subject map) is toooo affy ome specific; above just read the mapping
    def subject_select(self, dbi, options):
        cmd = "select id, projectid, pedigree, person, father, mother, gender, type, active, subject, time_stamp from members"
        cnt = 0
        for record in dbi.select_all(cmd):
            cnt += 1
            subject = record[-2]
            if subject is not None:
                self._subjects[subject] = record
                self._subject_count += 1
            if options.debug > 2:
                printnl("{0}: {1}".format(subject, record))
        printnl('')

    def subject_map(self, sample):
        sample = sample.lower()
        index = sample.find('_')
        if 0 <= index <= 2:                         # some sort of prefix
            sample = sample[index+1:]
            index = sample.find('_')
        subj = sample[:index] if index >= 0 else sample
        if subj in self._subjects:
            member = self._subjects[subj]
        elif subj.endswith('ex') and subj[:-2] in self._subjects:
            member = self._subjects[subj[:-2]]
        else: # generally member is a line of pedigree info ending with the subject
            printnl("sample \"{0}\" not in members table".format(sample))
            self._untracked_samples[sample] = subj
            member = ['Null', 'Null']
        return member

#   not currently used
#4/1def pedmap_read(self, dbi, options):
#4/1    if options.pedmap is None:
#4/1        raise Exception("pedmap not set")
#4/1    FILE = options.dir + options.pedmap
#4/1    File = gzopen(FILE)
#4/1    head = File.readline().strip().split(options.sep)
#4/1    labID  = len(head) - 1
#4/1    for line in File:
#4/1        subject = line.strip().split(options.sep)
#4/1        if len(subject) == labID + 1 and int(subject[labID]) > 99:
#4/1            self._subjects[subject[labID]] = subject
#4/1            self._subject_count += 1
#4/1            if options.debug:
#4/1                printnl("{0}: {1}".format(subject[labID], subject))
#4/1    File.close()
#4/1    printnl('File: {0}, {1:d} subjects'.format(FILE, self._subject_count))
