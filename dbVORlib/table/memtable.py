#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from dbVORlib.util             import NoTrace, printnl, printn, log
from dbVORlib.member           import Member

from dbVORlib.table.dbtable    import DBTable


class MemTable(DBTable):

    def __init__(self, db):
        super(MemTable, self).__init__(db)
        self._member_    = Member(db)
        self._project_id = None
        self._pedigree   = None
        self._person     = None
        self._member     = None
        self._im         = None

    def add_options(self, config):
        super(MemTable, self).add_options(config)

        config.add_option("--pedigree", "--ped", type="int",
                          help="offset of pedigree value in data; origin is 1 [0 for not present]")
        config.add_option("--person", type="int",
                          help="offset of person value in data; origin is 1")
        config.add_option("--member", type="int",
                          help="offset of member id in data; origin is 1")

        config.add_option("--include", action="append",
                          help="persons only to be processed")
        config.add_option("--exclude", action="append",
                          help="persons not to process")

#       config.add_option("--pedigree_size", type="int",
#                         help="digits in pedigree value in data")
#       config.add_option("--person_size", type="int",
#                         help="digits in person value in data")

    def prolog(self, dbi, options):
        super(MemTable, self).prolog(dbi, options)

        if options.person is None:
            raise NoTrace("\n!! YOU MUST SPECIFY THE PERSON COLUMN.  OPERATION ABORTED !!")

        self._project_id = options.project_id

# moved to dbtable.py
#    def insert_many(self, cmd, fmt, options):
#        if options.error == 0:
#            if options.commit:
#                self._im = self._dbi_.insert_many(cmd, fmt, 100)
#            return True
#        else:
#            return False

    def epilog(self, dbi, options):
        if options.commit:
            self._im()
        return True

    def header(self, File, dbi, options):
        header = File.readline().strip().split(options.sep)
        self._line_no = 1
        if options.verbose:
            printnl(header, tag='verbose')
        return header

    def do_row(self, fields, dbi, options):
        self._pedigree = fields[options.pedigree - self.ORIGIN] if options.pedigree else 'UNUSED'
        self._person   = fields[options.person   - self.ORIGIN]
        self._member   = None

#       if self._pedigree is not None and len(self._pedigree) and options.pedigree_size:
#           self._pedigree = self._pedigree.rjust(options.pedigree_size, '0')
#       if self._person is not None and len(self._person) and options.person_size:
#           self._person = self._person.rjust(options.person_size, '0')

    def pr_context(self, *err, **kw):
        tagv = kw.get('tag', None)
        if tagv:
            log.begingroup(tag=tagv)
        if self._old_line_no != self._line_no:
            if tagv:
                log.newrecord(tag=tagv)
            else:
                log.newrecord()
            if self._options.debug:
                printn('\nLine {0:8d}|'.format(self._line_no), tag=tagv)
                printnl(self._line, tag=tagv)
            if self._member:
                if self._options.debug:
                    s = "pedigree[@{0}]:person[@{1}] {2}:{3} [member {4:d}]  ".format(self._options.pedigree, self._options.person, self._pedigree, self._person, self._member)
                    printnl(s, tag=tagv)
                else:
                    s = "\nLine {0:8d}: pedigree:person {1}:{2} [member {3:d}] ".format(self._line_no, self._pedigree, self._person, self._member)
                    printnl(s, tag=tagv)
            else:
                if self._options.debug:
                    s = "pedigree[@{0}]:person[@{1}] {2}:{3}".format(self._options.pedigree, self._options.person, self._pedigree, self._person)
                    printnl(s, tag=tagv)
                else:
                    s = "\nLine {0:8d}: pedigree:person {1}:{2} ".format(self._line_no, self._pedigree, self._person)
                    printnl(s, tag=tagv)
            self._old_line_no = self._line_no
            self._this_line   = 0
        self._this_line += 1
        for e in err: printnl(e, tag=tagv, line=self._this_line)
        if tagv:
            log.endgroup(tag=tagv)
