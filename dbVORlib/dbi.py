#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#
# Hide all database access.  Currently connects to MySQL.
# External Methods:
#	 add_options	-- optparse.add_options for db specific parameters
#	 connect	-- connect to database using options parameters
#        commit		-- execute database commit
#        rollback	-- execute database rollback
#        select_one	-- execute select statement expecting a one line answer
#        select_all	-- execute select statement expecting a multiline answer
#        select_many	-- execute select statement expecting a several lines each time
#        execute	-- execute any statment with no return value
#        insert		-- use an executemany to feed multiple tuples to an insert

# HISTORY
#  05/14/2010
#	Stable!
#
#  05/19/2010
#       Insert takes one explicit argument, not *tupl
#       Print tuple as part of the error message

import re
import MySQLdb
import string

from   util     import NoTrace, printnl

class DBI(object):
    class BatchInsert(object):
        def __init__(self, dbi, sql, fmt, size):
            self._dbi_  = dbi
            self._sql   = sql
            self._fmt   = fmt
            self._size  = size

            self._cnt   = 0
            self._tupls = []
            self._mt    = {}
            self._fmtr  = string.Formatter()

        def __call__(self, *tupls):
            if tupls:
                for tupl in tupls:
                    if self._cnt < self._size:
#                       if tupl in self._tupls:
#                           printnl("DUP {0} is already scheduled to be inserted".format(tupl))
#                           options.error += 1
                        self._tupls.append(tupl)
                        self._cnt += 1
                    else:
#                       self._dbi_.insert_many(self._sql, self._fmt, self._tupls)
#6/1                    self._dbi_.execute(self._sql + ' VALUES ' + ', '.join([self._fmt.format(*t) for t in self._tupls]))
                        vals = (self._fmtr.vformat(self._fmt, t, self._mt) for t in self._tupls)
                        self._dbi_.execute(self._sql + 'VALUES' + ', '.join(vals))
                        self._tupls = [tupl]
                        self._cnt = 1
            elif self._cnt > 0:
#6/1            self._dbi_.execute(self._sql + 'VALUES' + ', '.join([self._fmt.format(*t) for t in self._tupls]))

                vals = (self._fmtr.vformat(self._fmt, t, self._mt) for t in self._tupls)
                self._dbi_.execute(self._sql + 'VALUES' + ', '.join(vals))

        def pylint(self):
            pass

    def __init__(self, strict=True):
        self._strict   = strict
        self._dbd_     = None
        self._db_might = False
        self._MySQL    = True

    def add_options(self, config):
        config.add_option("--host", action="store", type="string", help="database server")
## 3/15/16 added
        config.add_option("--hostport", action="store", type="int", default=3306,
                          help="database server port")
        config.add_option("--db", "--database", type="string", help="database schema")
        config.add_option("--user", type="string", help="database user")
        config.add_option("--password", type="string", help="database users password")
        config.add_option("--oursql", action="store_true", default=False,
                          help="oursql database interface")

    def check_and_connect(self, options, **k):
        if options.host is None or options.db is None or \
           options.user is None or options.password is None:
            raise NoTrace("ERROR: Bad database connection parameters: host \"{0}\", database \"{1}\", user \"{2}\"".format(options.host, options.db, options.user))

        return self.connect(options.host, options.db, options.user, options.password, options, **k)

    def connect(self, host, db, user, passwd, options, **k):
        if options.oursql:
            raise NoTrace("ERROR: python oursql module is not available")
#6/1        try:
#6/1            import oursql ... check this for 2.7
#6/1            SQLdb = oursql
#6/1            self._MySQL  = False
#6/1        except:
#6/1            raise NoTrace("ERROR: python oursql module is not available")
        else:
            SQLdb = MySQLdb

        try:
            self._dbd_ = SQLdb.connect(host=host, db=db, user=user, passwd=passwd, port=options.hostport, **k) # NOTE: ALL args are keyword
            return True                                                                 #       NOT positional
        except Exception, error:
            s = "ERROR: CANNOT CONNECT TO DATABASE: HOST \"{0}\", DATABASE \"{1}\", USER \"{2}\"\nError: {3}".format(host, db, user, error)
            raise NoTrace(error.__class__(s))

#   do I want or need this; do I have the privileges to run it;

#   def create(self, options, **k):
#       if options.host is None or options.db is None or \
#          options.user is None or options.password is None:
#           raise Exception, "Bad database connection parameters: host \"{0}\", database \"{1}\", user \"{2}\"".format(options.host, options.db, options.user)
#       try:
#           self._dbd_ = SQLdb.connect(host=options.host, user=options.user,
#                                          passwd=options.password, **k);
#           self.execute("grant all privileges on {0}.* to '{1}'@'{2}';".format(options.db, options.user, options.host))
#           self.execute("create database {0};".format(options.db))
#           self.execute("use {};".format(options.db))
#           return True
#       except Exception, error:
#           s = "CANNOT CONNECT TO DATABASE: HOST \"{0}\", DATABASE \"{1}\", USER \"{2}\"\nError: {3}".format(options.host, options.db, options.user, error)
#           raise error.__class__(s)

    def db_expect_commit(self):
        return self._db_might

    def db_might_commit(self, action=True):
        self._db_might = action

    def close(self):
        if self._dbd_:
            self._dbd_.close()

    def commit(self):
        if self._dbd_:
            self._dbd_.commit()

    def rollback(self):
        if self._dbd_:
            self._dbd_.rollback()

    def endbatch(self, options, commit=False):
        if options.commit or commit:
            if options.error:
                printnl("\nAborting Database Operations for Errors.")
                printnl("The Database will be restored to its state before the program was run.")
                self.rollback()
            elif not commit and options.rollback:
                printnl("\nAborting Database Operations Rollback Requested.")
                printnl("The Database will be restored to its state before the program was run.")
                self.rollback()
            else:
                printnl("\nCommitting Changes to the Database.")
                self.commit()

    def select_one(self, stmnt, *tupl):
        row = c = None
        if self._MySQL and tupl:
            stmnt = stmnt.replace('%', '%%').replace('?', '%s')
        try:
            c = self._dbd_.cursor()
            c.execute(stmnt, tupl)
        except Exception, error:
            s = "DATABASE EXECUTE() STATEMENT FAILED: \"{0}\"\nArgs: {1}\nError: {2}".format(stmnt, tupl, error)
            printnl(s)
            if c: c.close()
            if self._strict: raise
            return None

        try:
            row = c.fetchone()
        except Exception, error:
            s = "DATABASE FETCHONE() FAILED: \"{0}\"\nArgs: {1}\nError: {2}".format(stmnt, tupl, error)
            printnl(s)
            if self._strict: raise
        finally:
            c.close()
        return row

    def select_all(self, stmnt, *tupl):
        rows = c = None
        if self._MySQL and tupl:
            stmnt = stmnt.replace('%', '%%').replace('?', '%s')
        try:
            c = self._dbd_.cursor()
            c.execute(stmnt, tupl)
        except Exception, error:
            s = "DATABASE EXECUTE() STATEMENT FAILED: \"{0}\"\nArgs: {1}\nError: {2}".format(stmnt, tupl, error)
            printnl(s)
            if c: c.close()
            if self._strict: raise
            return None

        try:
            rows = c.fetchall()
        except Exception, error:
            s = "DATABASE FETCHALL() FAILED: \"{0}\"\nArgs: {1}\nError: {2}".format(stmnt, tupl, error)
            printnl(s)
            if self._strict: raise
        finally:
            c.close()
        return rows

    def select_col(self, stmnt, *tupl):
        rows = self.select_all(stmnt, *tupl)
        if rows:  # not None or []
            return [r[0] for r in rows]
        else:
            return rows

# test me
    def select_many(self, stmnt, count=10, *tupl):
        rows = c = None
        if self._MySQL and tupl:
            stmnt = stmnt.replace('%', '%%').replace('?', '%s')
        try:
            c = self._dbd_.cursor()
            c.execute(stmnt, tupl)
        except Exception, error:
            s = "DATABASE EXECUTE() STATEMENT FAILED: \"{0}\"\nArgs: {1}\nError: {2}".format(stmnt, tupl, error)
            printnl(s)
            if c: c.close()
            if self._strict: raise
            return None

        try:
            rows = c.fetchmany(count)
        except Exception, error:
            s = "DATABASE FETCHMANY({0:d}) FAILED: \"{1}\"\nArgs: {2}\nError: {3}".format(count, stmnt, tupl, error)
            printnl(s)
            if self._strict: raise
        finally:
            c.close()
        return rows

    def execute(self, stmnt, *tupl):
        self.execute_tuple(stmnt, tupl)

    def execute_tuple(self, stmnt, tupl):
        c = None
        if self._MySQL and tupl:
            stmnt = stmnt.replace('%', '%%').replace('?', '%s')
        try:
            c = self._dbd_.cursor()
            c.execute(stmnt, tupl)

#           if self._MySQL:
#               printnl("rows {0:d}, errors {1:d}".format(self._dbd_.affected_rows(), self._dbd_.warning_count()))
        except Exception, error:
            s = "DATABASE EXECUTE() STATEMENT FAILED: \"{0}\"\nArgs: {1}\nError: {2}".format(stmnt, tupl, error)
            printnl(s)
            if self._strict: raise
            return None
        finally:
            if c: c.close()

    def insert(self, stmnt, tupls):
        c = None
        if self._MySQL and tupls:
            stmnt = stmnt.replace('%', '%%').replace('?', '%s')
        try:
            c = self._dbd_.cursor()
            c.executemany(stmnt, tupls)
        except Exception, error:
            s = "DATABASE EXECUTEMANY() STATEMENT FAILED: \"{0}\"\nArgs: {1}\nError: {2}".format(stmnt, tupls, error)
            printnl(s)
            if self._strict: raise
            return None
        finally:
            if c: c.close()

#   def insert_many(self, cmd, fmt, tupls):
#       self.execute(cmd + ', '.join([fmt.format(tuple(t) for t in tupls])))

    def insert_many(self, sql, fmt, size):
        return self.BatchInsert(self, sql, fmt, size)

    def dbdate(self, edate):
        DATE1 = re.compile(r"^(\d{1,2})/(\d{1,2})/(\d{2,4})$")
        DATE2 = re.compile(r"^(\d{2,4})-(\d{1,2})-(\d{1,2})$")
        new = DATE1.match(edate)
        if new:
            return '{0}-{1}-{2}'.format(new.group(3), new.group(1), new.group(2))
        else:
            new = DATE2.match(edate)
            if new:
                return '{0}-{1}-{2}'.format(new.group(1), new.group(2), new.group(3))
            else:
                return "0000-00-00"

    def dbcolumns(self, table, db, columns=None):
        cmdT = "select column_name, data_type, column_type, column_default from information_schema.columns where table_name = '{0}' and table_schema = '{1}'"
        cmd = cmdT.format(table, db)
        if columns:
            cmd += " and column_name in ('" + "', '".join(columns) + "')"

        ans = {}
        for col in self.select_all(cmd):
            ans[col[0]] = {'data_type':col[1],
                           'column_type':col[2],
                           'column_default':col[3]}

        return ans

    def dbtable(self, table, db):
        cmdT = "select count(*) from information_schema.tables where table_name = '{0}' and table_schema = '{1}'"
        cmd = cmdT.format(table, db)
        ans = self.select_one(cmd)
        return (True if ans[0] else False) if ans else False
