#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

from   operator import itemgetter

from   dbVORlib.util     import printnl

class TraitSelect(object):
    def __init__(self, db):
        self._dbi               = db
        self._member_traits     = {}
        self._trait_constraint  = {}

        self._traitmeta_map      = None    # lint fwd ref
        self._traitmeta_name2id  = None    # lint fwd ref
        self._trait_ids          = None    # lint fwd ref

    def add_options(self, config):
        config.add_option("--trait", action="append", default=[],
                          help="traits to process")
        config.add_option("--missing_phenotype", type="string", default="-9",
                          help="value to report as missing value iff in any plink mode")

    def __iter__(self):
        return self._trait_ids.__iter__()

    def traittype(self, trait_id):
        return self._traitmeta_map[trait_id][0:2]

    def names(self):
        return (self._traitmeta_map[t][0] for t in self)

    def values(self, member_id, options, fill=None):
        vals = []
        T  = fill if fill is not None else 'Q' if options.funny else '0'
        trait_hash = self._member_traits.get(member_id, {})
#       return (trait_hash.get(i, T) for i in self)
        for val, (tname, ttype, na_value, no_value) in ((trait_hash.get(i, None), self._traitmeta_map[i]) for i in self):
            if options.type in ('bed', 'ped', 'tped', 'lgen'):
                if val is None:
                    val = options.missing_phenotype
                elif val == na_value:
                    val = options.missing_phenotype

            else:
                if val is None:
                    val = no_value
                elif val == na_value:
                    val = 'NA'
            vals.append(str(val))
        return vals

    def mega2_values(self, member_id, options, fill=None):
        vals = []
        trait_hash = self._member_traits.get(member_id, {})
        for val, (tname, ttype, na_value, no_value) in ((trait_hash.get(i, None), self._traitmeta_map[i]) for i in self):
#old        if val is None or val == MT:
#old            val = 0 if ttype == 'A' else 'NA'
            if val is None:
                val = no_value
            elif val == na_value:
                val = 'NA'
            vals.append(str(val))
        return vals

    def first(self):
        return self.names().next()

    def firstvalue(self, member_id, options):
#       return self.values(member_id, options).next()  no longer a generator
        return self.values(member_id, options)[0]

    def get_traits(self, dbi, member_id, options):
        trt = self._member_traits.get(member_id, None)
        if trt: return trt

        if options.no_traits:
            return self._member_traits.setdefault(member_id, {})

        cmdt = "select trait, value from traits where member = ? "
        cmde = ",".join((str(tid) for tid in self))
        if cmde != '':
            cmde = "trait in (" + cmde + ")"

        if not options.member:
            cmda = "select member, trait, value from traits "
            if cmde != '': cmda += 'where '
            group = {}
            member_id = None # stupid pylint !!
            for mem_id, trait, value in dbi.select_all(cmda+cmde):
                group.setdefault(mem_id, {})[trait] = value
            for mem_id, trait_hash in group.iteritems():
                self._member_traits[mem_id] = trait_hash
        else:
            if cmde != '': cmdt += 'and '
            trait_hash = dict(dbi.select_all(cmdt+cmde, member_id))
            self._member_traits[member_id] = trait_hash

        return self._member_traits.get(member_id, {})

    GENDER = 5
    TYPE   = 7
    def filter(self, member_id, memrec, trt, pedper_get, options):
        mtrait = self._trait_constraint.get('trait', None)
        if mtrait:
            trait_hash = self._member_traits.get(member_id, {})
            for trait_id, values in mtrait.iteritems():
                tv = trait_hash.get(trait_id, None)
                if tv and tv not in values:
                    pedper = pedper_get(member_id)
                    printnl("skipping: pedigree:person {0}:{1} (id {2:d}) trait \"{3[0]}\" (id {4:d}) value {5} not in allowed list {6}".format(pedper[0], pedper[1], member_id, self._traitmeta_map[trait_id], trait_id, tv, values), tag="traitvalue")
                    return True

        mlink = self._trait_constraint.get('linkage', None)
        if mlink:
            mgender = mlink.get('gender', None)
# "select id, pedigree, person, father, mother, gender, active, type from members"
            if mgender and str(memrec[self.GENDER]) not in mgender:
                pedper = pedper_get(member_id)
                printnl("skipping: pedigree:person {0}:{1} (id {2:d}) gender value {3} not in allowed list {4}".format(pedper[0], pedper[1], member_id, memrec[self.GENDER], mgender), tag="traitgender")
                return True
#            mtype = mlink.get('type', None)
            mtype = None
            if mtype and memrec[self.TYPE] not in mtype:
                pedper = pedper_get(member_id)
                printnl("skipping: pedigree:person {0}:{1} ({2:d}) type value {3} not in {4}".format(pedper[0], pedper[1], member_id, memrec[self.TYPE], mtype), tag="traittype")
                return True
        return False

    def run(self, dbi, options):
        self.Traitmeta(dbi, options)
        self.Lookup_traitmeta(dbi, options)

    def Traitmeta(self, dbi, options):
        sel = "select column_name from information_schema.columns where table_schema = ? and table_name = 'traitmeta' and column_name = 'na_value'"
        new = dbi.select_one(sel, options.db)
        if new is None:
            self._traitmeta_map = {}
            for selected in dbi.select_all("select id, name, type from traitmeta"):
                ttype = selected[2]
                self._traitmeta_map[selected[0]] = (selected[1], ttype,
                                                    ('0' if  ttype == 'A' else '-9' if ttype == 'T' else ''),
                                                    ('NA' if ttype == 'A' else 'NA' if ttype == 'T' else 'NA') )
        else:
            self._traitmeta_map     = dict((item[0], item[1:]) for item in dbi.select_all("select id, name, type, ifnull(na_value,'NA'), ifnull(no_value,'NA') from traitmeta"))
        self._traitmeta_name2id = dict((v[0], k) for (k, v) in self._traitmeta_map.iteritems())

    def Lookup_traitmeta(self, dbi, options):
        if options.no_traits:
            self._trait_ids = []
            return True
        elif not options.trait:
            self._trait_ids = [v for (k, v) in sorted(self._traitmeta_name2id.iteritems(), key=itemgetter(0))]
            return True

        self._trait_ids = []
        for trait in options.trait:
            constraint = trait.split('|')
#           if constraint[0] in ('gender', 'type'):
            if constraint[0] in ('gender', ):
                self.Linkage_constraint(constraint)
                continue
            trait_id = self._traitmeta_name2id.get(constraint[0], None)
            if trait_id is None:
                raise Exception("invalid traitmeta name \"{0}\"; choose from {1}".format(constraint[0], sorted(self._traitmeta_name2id.iterkeys() )))
            if trait_id not in self._trait_ids:
                self._trait_ids.append(trait_id)

            if len(constraint) > 2:
                if constraint[1] == 'value':
# the "append" allows different values for the same trait to be on different lines!! -- important
                    for value in constraint[2:]:
                        self._trait_constraint.setdefault('trait', {}).setdefault(trait_id, []).append(value)
## alternatively
##                      if 'trait' not in self._trait_constraint:
##                          self._trait_constraint['trait'] = {}
##                      if trait_id not in self._trait_constraint['trait']:
##                          self._trait_constraint['trait'][trait_id] = []
##                      self._trait_constraint{'trait'}{trait_id}.append(value)

                else:
                    printnl("bad trait constraint \"{0}\" should be \"value\" or \"...\"".format(constraint[1]))
                    continue

        traits = self._trait_constraint.get('trait', {})
        if len(traits):
            for k, v in traits.iteritems():
                traits[k] = tuple(v)
##      else:  # just gender and/or linkage but no trait contraints  ... use all
##          self._trait_ids = [v for (k, v) in sorted(self._traitmeta_name2id.iteritems(), key=itemgetter(0))]

    def Linkage_constraint(self, constraint):
        key = constraint[0]
        start = 2 if constraint[1] == 'value' else 1
        for value in constraint[start:]:
            self._trait_constraint.setdefault('linkage', {}).setdefault(key, []).append(value)
