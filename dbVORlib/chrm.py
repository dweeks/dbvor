#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

class Chrm(object):
    CHRM = 27
    chrmint    = { 'X': 23, 'Y': 24, 'XY': 25, 'MT': 26, 'M': 26, 'U': 0}
    intchrm    = dict( (v, k) for k, v in chrmint.iteritems() )
    mega2map   = {23:'X', 24:'Y', 25: 'MT', 26: 'MT', 0: 'U'}
    mega2names = {23:'X', 24:'Y'}

    def __init__(self, db):
#       super(Chrm, self).__init__(db)
        self._dbi_  = db

    def chrm_mt(self, cnt=CHRM):
        return [ [] for i in range(0, cnt) ]

    def chrm_0(self, cnt=CHRM):
        return [ 0 for i in range(0, cnt) ]

    def chrm2int(self, chrm):
        ans = self.chrmint.get(chrm.upper(), None)
        return ans if ans is not None else int(chrm)

    def int2chrm(self, chrmi):
        return str(chrmi) if chrmi < 23 else self.intchrm.get(chrmi, '0')

    def int2Mega2Map(self, chrmi):
        return str(chrmi) if chrmi < 23 else self.mega2map.get(chrmi, 'U')

    def int2Mega2MapNames(self, chrmi):
        return self.mega2names.get(chrmi, 'M')

    def chrm_valid(self, chrm):
        if chrm in ( '1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9', '10',
                     '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
                     '21', '22', '23', '24', '25', '26',
                     '0', 'X', 'XY', 'Y', 'MT', 'U'):
            return True
        else:
            return False

