#
# ===========================================================================
#
#   dbVOR: The Vor Genetic Database System
#   Copyright (C) 2011-2015 Robert Baron, Daniel E. Weeks, and the
#   University of Pittsburgh
#
#   This file is part of the dbVOR package, which is free software; you
#   can redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software Foundation;
#   either version 3 of the License, or (at your option) any later
#   version.
#
#   dbVOR is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For further information contact:
#       Robert Baron
#       e-mail: rvb5@pitt.edu
#       or
#       Daniel E. Weeks
#       e-mail: weeks@pitt.edu
#
# ===========================================================================
#

import os, os.path
import time

from   validate import Validate
from   blocksnp import BlockSnp
from   snp      import Snp
from   util     import printnl, input_dir, gzopen, Glob

class GWAS(BlockSnp, Glob):
    activities = ('prolog', 'glob', 'stat')

    def __init__(self, db):
        self._validate_   = Validate(db)
        super(GWAS, self).__init__(db)
        self._sample_file_cache         = []
        self._sample_file_header        = []
        self._sample_file_header_offset = []

    def add_options(self, config):
        self._validate_.add_options(config)
        super(GWAS, self).add_options(config)
        config.add_option("--snp_exclude", action="append", type="string", default=[],
                          help="snp's in affymap to ignore")
        config.add_option("--filter_samples", action="store", type="boolean", default=0,
                          help="ignore unknown samples and samples that do not map or are inactive")

        config.add_option("--NoCall", action="store", type="boolean", default=False,
                          help="report NoCall events")
        config.add_option("--NotSampled", action="store", type="boolean", default=False,
                          help="SNP_A not sampled")

        config.add_option("--inputdir", action="store", type="string",
                          help="directory file for sample data")
        config.add_option("--input", action="store", type="string",
                          help="file for sample data")
        config.add_option("--commit", action="store_true", default=False,
                          help="commit changes to database")
##      config.add_option("--comdrop", action="store_true", default=False,
##                        help="build like commit but do not insert genotype changes to database")
        config.add_option("--active", action="store", type="int", default=1,
                          help="active flag for insertion or selection")
        config.add_option("--rollback", action="store_true", default=False,
                          help="force all database operations to be aborted")

## begin virtual fns
    def make_snps(self, dbi, options):
        pass
    def marker_file(self, FILE, dbi, snp_exclude, options):
        pass
    def sample_file(self, FILE, options):
        pass
## end virtual fns

#   prolog() and hence init() is executed once to either create or load markers
    def prolog(self, dbi, options):    # prolog and hence init are done once per glob
# remove line below iff config.add_options(--comdrop ...) is enabled
        options.comdrop = False
        if options.comdrop:
            options.commit = True
        self._validate_.validate_project(options,    create=options.commit)
        self._validate_.validate_technology(options, create=options.commit)
        self._validate_.validate_experiment(options, create=options.commit)
        if options.error:
            raise Exception("try again!")

        printnl("project {0}, id {1:d}"   .format(options.project, options.project_id))
        printnl("technology {0}, id {1:d}".format(options.technology, options.tech_id))
        printnl('')

        return self.init(dbi, options)

    def init(self, dbi, options):    # prolog and hence init are done once per glob
#       import pdb;pdb.set_trace()
#4/1    if options.make_snps or options.make_rs_file:
        if options.make_snps:
            self.make_snps(dbi, options)
            return False    # terminate

        if options.dedup:
            self.dedup(dbi, options)
            return False    # terminate

        if options.file_output:      # file_output is appended to by each run() job !!
            FILENAME = options.dir + options.file_output
            if os.path.isfile(FILENAME):
                os.remove(FILENAME)

        self.sample_select(dbi, options)    # load once

        if options.snp_db_input:
            self._snp_count = self.snp_db_input(options)
        elif options.snp_file_input:
            self._snp_count = self.snp_file_input(options)
        else:
            raise Exception("No SNP source specified.")
        return True

    def read_snps(self, dbi, FILE, options):

        if options.probeid_remap:
            self.probeid_remap_read(options)
        if options.rs_remap:
            self.rs_remap_read(options)

        snp_exclude = {}
        for snp in options.snp_exclude:
            snp_exclude[snp] = 1

        self.marker_file(FILE, dbi, snp_exclude, options)

        self.do_stat_remap()

#4/1    if options.make_rs_file:
#4/1        self.rs_file_output(options)
#4/1        printnl("\nrun:\n\tgmi.pl {0} --outfile {1}".format(options.dir+options.rs_file_output, options.dir+options.rs_file_output.replace('.txt', '.out')))
#4/1        return False

#4/1    if options.genmap == 'gmi':
#4/1        if options.reposition:
#4/1            self.reposition_read(options)
#4/1            self.do_reposition(options)

        if options.snp_file_output:
            self.snp_file_output(options)
        if options.snp_gclean_file_output:
            self.snp_gclean_file_output(options)
        if options.snp_db_output:
            self.snp_db_output(options)

#   run() is executed once for each sample file
    def run(self, dbi, options):

        if options.filter_samples:
            sample = options.input
            sample_map = self.sample_map(sample, options)
            member, active = sample_map[0:2]
            printnl("Sample \"{0}\": member {1}, active {2}.".format(sample, member, active))

            if sample_map[2][0] is None:
                printnl("Sample \"{0}\" is not in the samples table. Ignored!\n".format(sample))
                return True
            elif member == 'Null':
                printnl("Sample \"{0}\" is not mapped to a person. Ignored!\n".format(sample))
                return True
            elif active == 0:
                printnl("Sample \"{0}\" is not active. Ignored!\n".format(sample))
                return True

        FILE = input_dir(options)

        otime = time.time()
        opc   = self._process_count
        if options.file_output:
            FILENAME = options.dir + options.file_output
            options.File = gzopen(FILENAME, 'a')

        self.sample_file(FILE, options)

        if options.file_output:
            options.File.close()

        sec, cnt = (time.time() - otime, self._process_count - opc)
        printnl("     {0:d} samples in {1:.3f} sec == {2:.3f} sec per sample\n".format(cnt, sec, sec/cnt))

        return True

    _BP = 1
    _Uchr  = 0
    def define_snp(self, snp, rs, chrm, bp, gm, ab, Line, options):
        if chrm == '---' or bp == '---': # hm no snp
            if options.verbose:
                printnl("{0:6d}: {1:>19} {2:>12} {3:>3} {4:>3} {5}".format(Line, snp, rs, chrm, bp, ab))
            chrm = 0
            orec = self._rs_hash.get(rs, None)
            if orec:
                r = [None, None, snp, rs, self._Uchr, orec[0][Snp.bp], 0.0, 0, 0, ab, 0]  # chrm 0
            else:
                r = [None, None, snp, rs, self._Uchr, self._BP, 0.0, 0, 0, ab, 0]  # chrm 0
                self._BP += 1
        else:
            chrm = self.chrm2int(chrm)
            bp = int(bp)
            r = [None, None, snp, rs, chrm, bp, gm, 0, 0, ab, 0]
        l = len(self.record(r, options))
        if l > 1:
            r[Snp.rs] += "_" + str(l)    # rename rs123.2, etc


    def undefined_snp(self, snp, options):
        if snp in self._undefined_snp:
            self._undefined_snp[snp] += 1
        else:
            self._undefined_snp[snp]  = 1
        if options.debug > 1:
            printnl('{0} not defined'.format(snp))

    def store_sample(self, subject, options):
        if options.debug and options.error > 0:
            printnl("preinsert:  CUMULATIVE ERROR COUNT {0:d}".format(options.error))
        if options.debug > 1:
            for lst in self._chr_list:
                for r in lst:
                    printnl(r)

        ret = self.insert_blocks(subject, options)

#1/23   self._dbi_.endbatch(options)
        self.clean()
        if options.debug and options.error > 0:
            printnl("postinsert: CUMULATIVE ERROR COUNT {0:d}".format(options.error))

        return ret
